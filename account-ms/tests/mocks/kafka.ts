import * as kafka from '../../src/kafka';

export const produceResult = {
  accounts: {
    value:
      '{"error": false, "message": { "Data": [{"AvailableBalance": 12, "Currency": "EGP", "CategoryCode": "1001"}, {"AvailableBalance": 23, "Currency": "EGP", "CategoryCode": "1001"}] }}'
  },
  accountsForOwn: {
    value:
      '{"error": false, "message": { "Data": [{"AccID": "10100216100","AvailableBalance": 12, "Currency": "EGP", "CategoryCode": "1001","Category": "1001","Status": "ACTIVE"}, {"AccID": "1010021611","AvailableBalance": 23, "Currency": "EGP", "CategoryCode": "1001","Category": "1001","Status": "ACTIVE"}] }}'
  },
  account: {
    value:
      '{"error": false, "message": { "Data": [{"AvailableBalance": 12, "Currency": "EGP", "CategoryCode": "1001","Status": "ACTIVE"}] }}'
  },
  debitCards: {
    value: '{"error": false, "message": { "Data": [] }}'
  },
  transferAccounts: {
    value:
      '{"error": false, "message": { "Data": [{"AccID": "10100216100","AvailableBalance": 12, "Currency": "EGP", "Category": "test", "Status": "ACTIVE"}, {"AccID": "1010021611", "AvailableBalance": 23, "Currency": "EGP", "Category": "test", "Status": "ACTIVE"}] }}'
  },
  transactions: {
    value:
      '{"error": false, "message": { "Data": [{"AccID":"1010021511070201","BookingDate":"20210101","TransRef":"FT210018CFXP","Description":"Quarterly Stamp Tax Deductions","ValueDate":"20201231","Withdrawals":-0.42,"Deposits":0,"Balance":-63.42,"Currency":"EGP", "correlationId": "FT210018CFXP-undefined"}] }}'
  },
  pendingTransactions: {
    value:
      '{"error": false, "message": { "Data": [{"Account":"1010021511070201","FromDate":"20210101","TerminalLoc": "Testing", "ToDate": "20210505", "IDRef":"FT210018CFXP","Description":"Quarterly Stamp Tax Deductions","ValueDate":"20201231","SettlementAmount":-0.42,"Deposits":0,"DateTime":"20201231.0000","SettlementCurrency":"EGP", "correlationId": "FT210018CFXP-undefined"}] }}'
  },
  conversion: {
    value: '{"error": false, "message": { "Data": [{"CurrencyCode": "EGP", "TransferBuyRate": 1}]}}'
  },
  defaultValue: {
    value: '{"error": false, "message": { "TransactionId": 12345 }}'
  },
  loansValue: {
    value:
      '{"error": false, "message": { "Data": [{"ID": "LD2030039097", "Company": "EG0010001", "Customer": "10100998","Currency": "EGP", "LoanAmount": 100000, "TotalPaid": 27220.100000000006, "LDAmount": 0, "Outstanding": 0 }]}}'
  },
  loansDuesValue: {
    value:
      '{"error": false, "message": { "Data": [{"ID": "LD2030039097", "Company": "EG0010001", "Customer": "10100998", "TotalAmountOverDue": 20317, "PastDues": [] }]}}'
  },
  beneficiary: {
    value: '{"error": false, "message": { "accountNumber": "12345", "swift": "ARAIEGCXXXX" }}'
  },
  schedule: {
    value:
      '{"error": false, "message": { "Data": [ { "Date": 20201103, "InstallmentAmount": 2502.9, "PrincipalAmount": 1337.85, "InterestAmount": 1031.29, "ChargeAmount": 133.76, "OutstandingAmount": 81165.57}] }}'
  }
};

export const transactionResult = [
  {
    AccID: '1010021511070201',
    BookingDate: '20210101',
    TransRef: 'FT210018CFXP',
    Description: 'Quarterly Stamp Tax Deductions',
    ValueDate: '20201231',
    Withdrawals: -0.42,
    Deposits: 0,
    correlationId: 'FT210018CFXP-undefined',
    Balance: -63.42,
    Currency: 'EGP'
  },
  {
    AccID: '1010021511070201',
    BookingDate: '20210101',
    TransRef: 'FT210018CFXP',
    Description: 'Testing',
    Withdrawals: 0.42,
    Currency: 'EGP',
    Deposits: 0,
    Timestamp: '20201231.0000',
    Status: 'pre-authorized',
    ValueDate: '20210505',
    correlationId: 'FT210018CFXP-20201231.0000'
  }
];

export const accountsResult = {
  accounts: [
    {
      AvailableBalance: 12,
      Currency: 'EGP',
      CategoryCode: '1001',
      country: 'EG',
      debitCards: []
    },
    {
      AvailableBalance: 23,
      Currency: 'EGP',
      CategoryCode: '1001',
      country: 'EG',
      debitCards: []
    }
  ],
  balance: {
    EGAccountsCount: 2,
    EGAmount: 35,
    amount: 35,
    currency: 'EGP'
  },
  accountsCurrencies: ['EGP'],
  notFetched: null
};

export const loansResult = {
  loans: [
    {
      ID: 'LD2030039097',
      Company: 'EG0010001',
      Customer: '10100998',
      Currency: 'EGP',
      LoanAmount: 100000,
      TotalPaid: 27220.100000000006,
      Outstanding: 0,
      LDAmount: 0,
      country: 'EG'
    }
  ],
  loansData: {
    currency: 'EGP',
    EGAmount: 0,
    EGLoansCount: 1,
    amount: 0
  },
  notFetched: null
};

export const loansDuesValue = {
  ID: 'LD2030039097',
  Company: 'EG0010001',
  Customer: '10100998',
  TotalAmountOverDue: 20317,
  PastDues: []
};
export const loansScheduleValue = [
  {
    Date: 20201103,
    InstallmentAmount: 2502.9,
    PrincipalAmount: 1337.85,
    InterestAmount: 1031.29,
    ChargeAmount: 133.76,
    OutstandingAmount: 81165.57
  }
];

export const transferResult = {
  creditBalance: 25,
  debitBalance: 10,
  currency: 'EGP',
  transactionId: 12345
};

export const internalTransferResult = {
  debitBalance: 10,
  currency: 'EGP',
  transactionId: 12345
};

export const mockKafkaSuccess = (fromDate: string, toDate: string) => {
  jest.spyOn(kafka, 'createConsumer');
  const mock = jest.spyOn(kafka, 'produce');
  mock.mockImplementation(async (data) => {
    switch (data.function) {
      case 'getUserBeneficiariesById':
        return Promise.resolve(produceResult.beneficiary);
      case 'updateUserBeneficiaries':
        return Promise.resolve(produceResult.defaultValue);
    }
    switch (data.route) {
      case 'account/customer/10100215':
        return Promise.resolve(produceResult.accounts);
      case 'account/list':
        return Promise.resolve(produceResult.accountsForOwn);
      case 'account/customer/111111':
        return Promise.resolve(produceResult.transferAccounts);
      case 'txn/acc2acc':
        return Promise.resolve(produceResult.defaultValue);
      case 'txn/ach/local':
        return Promise.resolve(produceResult.defaultValue);
      case 'rates/exchange/EGP':
        return Promise.resolve(produceResult.conversion);
      case `account/txns/12345/${fromDate}/${toDate}`:
        return Promise.resolve(produceResult.transactions);
      case `debitcards/periodpendingtxns/12345/${fromDate}/${toDate}`:
        return Promise.resolve(produceResult.pendingTransactions);
      case `ld/loan/customer/10100215`:
        return Promise.resolve(produceResult.loansValue);
      case `pd/loan/LD123456789`:
        return Promise.resolve(produceResult.loansDuesValue);
      case `ld/loan/schedule/LD123456789`:
        return Promise.resolve(produceResult.schedule);
      case `account/10100216100`:
        return Promise.resolve(produceResult.account);
      case `debitcards/customer/10100215`:
        return Promise.resolve(produceResult.debitCards);
    }
  });
};

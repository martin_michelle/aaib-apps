import * as service from '../../../src/services/accounts';
import {
  mockKafkaSuccess,
  transactionResult,
  accountsResult,
  transferResult,
  internalTransferResult
} from '../../mocks/kafka';
import moment from 'moment';
import { transOptions } from '../../../src/config';
const fromDate = moment().subtract(transOptions.duration, 'months').format(transOptions.dateFormat);
const toDate = moment().format(transOptions.dateFormat);

describe('Get User Accounts', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    mockKafkaSuccess(fromDate, toDate);
    const response = await service.getUserAccounts(
      {
        allcifs: [
          {
            cif: '10100215',
            country: 'EG'
          }
        ],
        country: 'EG'
      },
      1001,
      ''
    );
    expect(response).toStrictEqual(accountsResult);
  });
});

describe('Get account transactions', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    mockKafkaSuccess(fromDate, toDate);
    const response = await service.getAccountTransactions(
      12345,
      {
        fromDate,
        toDate,
        search: '',
        lastTransaction: '',
        limit: 2
      },
      ''
    );
    expect(response.transactions).toStrictEqual(transactionResult);
  });
});

describe('Transfer between accounts', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    mockKafkaSuccess(fromDate, toDate);
    const response = await service.ownAccountsTransfer(
      {
        debitAcc: '10100216100',
        creditAcc: '1010021611',
        amount: 2,
        description: 'Unit test'
      },
      '111111',
      '',
      '',
      ''
    );
    expect(response).toStrictEqual(transferResult);
  });
});

describe('Transfer between internal accounts', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    mockKafkaSuccess(fromDate, toDate);
    const response = await service.allTransfers(
      {
        debitAcc: '10100216100',
        creditAcc: '1010021611',
        amount: 2,
        currency: 'EGP',
        swift: 'ARAIEGCXXXX',
        beneficiaryName: '',
        chargeType: '',
        description: 'Unit Testt',
        country: 'EG'
      },
      '',
      '111111',
      '',
      '',
      ''
    );
    expect(response).toStrictEqual(internalTransferResult);
  });
});

describe('Transfer between beneficiary accounts', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success(Internal)', async () => {
    mockKafkaSuccess(fromDate, toDate);
    const response = await service.transferByBeneficiaryId(
      '123456789',
      {
        debitAcc: '10100216100',
        amount: 2,
        currency: 'EGP',
        otp: '',
        chargeType: 'OUR',
        description: 'Unit Test'
      },
      true,
      false,
      '',
      '111111',
      '',
      '',
      ''
    );

    expect(response).toStrictEqual(internalTransferResult);
  });
  it('Should return success(External)', async () => {
    mockKafkaSuccess(fromDate, toDate);
    const response = await service.transferByBeneficiaryId(
      '12345678910',
      {
        debitAcc: '10100216100',
        amount: 2,
        currency: 'EGP',
        otp: '',
        chargeType: 'OUR',
        description: 'Unit Test'
      },
      true,
      false,
      '',
      '111111',
      '',
      '',
      ''
    );
    expect(response).toStrictEqual(internalTransferResult);
  });
});

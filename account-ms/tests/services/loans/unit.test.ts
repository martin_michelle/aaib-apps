import * as service from '../../../src/services/loans';
import {
  mockKafkaSuccess,
  loansResult,
  loansDuesValue,
  loansScheduleValue
} from '../../mocks/kafka';

describe('Get User Loans', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    mockKafkaSuccess('', '');
    const response = await service.getLoans(
      {
        country: 'EG',
        allcifs: [
          {
            country: 'EG',
            cif: '10100215'
          }
        ]
      },
      ''
    );
    expect(response).toStrictEqual(loansResult);
  });
});

describe('Get Loans Dues', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    mockKafkaSuccess('', '');
    const response = await service.getLoanPastDues('LD123456789', 'EG', '');
    expect(response).toStrictEqual(loansDuesValue);
  });
});

describe('Get Loans schedule', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    mockKafkaSuccess('', '');
    const response = await service.getScheduleLoans('LD123456789', 'EG', '');
    expect(response).toStrictEqual(loansScheduleValue);
  });
});

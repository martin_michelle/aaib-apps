import { Response, NextFunction } from 'express';
import { ProtectedRequest } from 'app-request';
import { kafkaTopics } from '../config';
import { AuthFailureError, BlockedTokenError, InvalidOtpError } from '../core/ApiError';
import messages from '../messages';
import { produce, prepareKafkaResponse } from '../kafka';
import tracingHeaders from './tracingHeaders';

export default () => async (req: ProtectedRequest, res: Response, next: NextFunction) => {
  try {
    const data = {
      topic: kafkaTopics.validateTopic,
      authorization: req.headers.authorization,
      account: req.params.account || '',
      apiRoute: req.originalUrl || '',
      apiMethod: req.method || '',
      otp: req.body.otp || ''
    };
    const producerData = await produce(data, tracingHeaders(req));
    const result = await prepareKafkaResponse(producerData);
    if (result && result.user) {
      req.user = result.user;
      req.accessToken = result.token;
      next();
    } else next(new AuthFailureError(messages.authorization.invalid.en));
  } catch (error) {
    if (error.message === messages.authorization.otpBlocked.en) {
      next(new BlockedTokenError(messages.authorization.otpBlocked.en));
    } else if (error.message === messages.authorization.invalidOtp.en) {
      next(new InvalidOtpError());
    } else {
      next(new AuthFailureError(messages.authorization.invalid.en));
    }
  }
};

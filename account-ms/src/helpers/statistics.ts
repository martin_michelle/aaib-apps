import { produce } from '../kafka';
import { kafkaTopics, statistics } from '../config';
import correlation from 'express-correlation-id';
import messages from '../messages';

export default async (updateValue: string, message = '', kibanaInfo = {}, correlationId = '') => {
  try {
    if (updateValue) {
      const data = {
        topic: kafkaTopics.statistics,
        correlationId: correlation.getId() || correlationId,
        date: new Date(),
        updateValue:
          message === messages.accounts.timeout.en ? statistics.timeoutTransfers : updateValue
      };
      await produce(data, null, false);
    }
    if (Object.keys(kibanaInfo).length) {
      const kibanaData = {
        topic: kafkaTopics.kibana,
        correlationId: correlation.getId() || correlationId,
        date: new Date(),
        kibanaInfo
      };
      await produce(kibanaData, null, false, correlationId);
    }
  } catch (e) {
    return null;
  }
};

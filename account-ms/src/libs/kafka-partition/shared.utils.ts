export const isUndefined = (obj: any): obj is undefined => typeof obj === 'undefined';

export const isNil = (obj: any): obj is null | undefined => isUndefined(obj) || obj === null;

export const isString = (fn: any): fn is string => typeof fn === 'string';

// eslint-disable-next-line @typescript-eslint/ban-types
export const isObject = (fn: any): fn is object => !isNil(fn) && typeof fn === 'object';

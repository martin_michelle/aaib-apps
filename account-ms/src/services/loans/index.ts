import { prepareKafkaResponse, produce } from '../../kafka';
import { logger } from '../../core/Logger';
import messages from '../../messages';
import { InternalError } from '../../core/ApiError';
import updateStatistics from '../../helpers/statistics';
import {
  kafkaTopics,
  status,
  defaultCurrency,
  currencies,
  ismocking,
  AEError,
  EGError
} from '../../config';
import correlation from 'express-correlation-id';
import promiseAllsettled from 'promise.allsettled';
import { mockedLoans } from '../../data/mockingLoans';
import { getConversionRates, getBalance } from '../accounts';

export const getLoans = async (user: any, authorization: string, tracingHeaders?: any) => {
  try {
    const mainCifCurrency = currencies[user.country] || defaultCurrency;
    const conversions = await getConversionRates(defaultCurrency, false, tracingHeaders);
    const requests = user.allcifs.map(async (cifObj: { country: string; cif: string }) => {
      try {
        const cif = parseInt(cifObj.cif);
        const currentCifcurrency = currencies[cifObj.country] || mainCifCurrency;

        let result = [];
        if (ismocking && cifObj.country === 'AE') {
          if (AEError) throw new Error('AE is down');
          result = mockedLoans[cifObj.country];
          logger.info(`loans are mocked from ${cifObj.country}`);
        } else {
          if (AEError && cifObj.country === 'AE') throw new Error('AE is down');
          if (EGError && cifObj.country === 'EG') throw new Error('EG is down');
          const data = {
            route: `ld/loan/customer/${cif}`,
            authorization,
            country: cifObj.country
          };
          const producerData = await produce(
            data,
            tracingHeaders,
            true,
            `${correlation.getId()}-${cifObj.cif}`
          );
          result = await prepareKafkaResponse(producerData);
        }

        const { defaultCurrencyTotal, total } = await getBalance(
          result,
          conversions,
          currentCifcurrency,
          'Outstanding'
        );

        result.forEach((loan: any) => {
          loan['country'] = cifObj.country;
        });

        return {
          country: cifObj.country,
          loans: result,
          loansData: {
            defaultCurrencyAmount: Number(defaultCurrencyTotal),
            amount: Number(total),
            currentCifcurrency
          }
        };
      } catch (error) {
        logger.error(`in ${cifObj.country}: ${error.message}`);
        throw new Error(`${cifObj.country}`);
      }
    });

    const results: any = [];
    let notFetched: string | null = null;
    const res = await promiseAllsettled(requests);

    res.forEach((result) => {
      if (result.status === 'fulfilled') results.push(result.value);
      else {
        notFetched = (result.reason as Error).message;
      }
    });

    const allError = res.every((result) => {
      return result.status !== 'fulfilled';
    });

    if (allError) throw new InternalError(messages.loans.allCountries.en);

    const loans: any = [];
    const loansData: any = {};
    loansData.amount = 0;
    results.forEach((result: any) => {
      loans.push(...result.loans);
      loansData[`${result.country}LoansCount`] = result.loans.length;
      loansData[`${result.country}Amount`] = Number(result.loansData.amount.toFixed(2));
      loansData.amount += Number(result.loansData.defaultCurrencyAmount);
    });
    if (mainCifCurrency !== defaultCurrency) {
      loansData.amount /= conversions[mainCifCurrency];
    }
    loansData.amount = Number(loansData.amount.toFixed(2));
    loansData.currency = mainCifCurrency;

    return {
      loans,
      loansData,
      notFetched
    };
  } catch (e) {
    logger.error(e);
    throw new InternalError(messages.loans.internalError.en);
  }
};

export const getLoanPastDues = async (
  loan: string,
  country = 'EG',
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `pd/loan/${loan}`,
      authorization,
      country
    };
    const producerData = await produce(data, tracingHeaders);
    const pastDues = await prepareKafkaResponse(producerData);
    return pastDues[0] || {};
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

export const getScheduleLoans = async (
  loan: string,
  country = 'EG',
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `ld/loan/schedule/${loan}`,
      authorization,
      country
    };
    const producerData = await produce(data, tracingHeaders);
    return await prepareKafkaResponse(producerData);
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

export const requestLoan = async (
  body: any,
  cif: string,
  user: string,
  phoneNumber: string,
  platform: string
) => {
  try {
    const data = {
      topic: kafkaTopics.smtp,
      function: 'sendEmailService',
      body,
      cif,
      template: 'loan'
    };
    await produce(data, null, false);
    updateStatistics('', '', {
      cif,
      user,
      type: 'request',
      subType: 'requestLoan',
      date: new Date(),
      mobile: phoneNumber,
      microservice: 'account',
      status: status.success,
      platform: platform
    }).catch((e) => e.message);
    return { message: 'Success' };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

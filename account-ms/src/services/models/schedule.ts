import mongoose from 'mongoose';

const model = new mongoose.Schema(
  {
    body: {
      debitAcc: String,
      amount: Number,
      description: String,
      currency: String,
      chargeType: String,
      beneficiaryID: mongoose.Types.ObjectId
    },
    transferFrequency: String,
    scheduleDate: Date,
    nextTransferDate: Date,
    currentTransferCount: { type: Number, default: 0 },
    //If total transfer count is 0, the number of transfers is considered infinite
    totalTransferCount: { type: Number, default: 0 },
    user: String,
    userId: String,
    platform: String
  },
  { timestamps: true }
);
model.index({ user: 1 });
model.index({ nextTransferDate: 1 });
export default mongoose.model('ScheduleTransfer', model);

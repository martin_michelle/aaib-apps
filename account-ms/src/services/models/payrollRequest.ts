import mongoose from 'mongoose';

const model = new mongoose.Schema(
  {
    cif: String,
    lastRequestDate: {
      type: Date,
      required: true
    }
  },
  { timestamps: true }
);
model.index({ cif: 1 });
export default mongoose.model('PayrollRequest', model);

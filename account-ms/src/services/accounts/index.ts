import { getHeartbeatTime, produce, prepareKafkaResponse } from '../../kafka';
import generateAdminToken from '../../helpers/adminToken';
import { endOfDay, startOfDay } from 'date-fns';
import {
  transOptions,
  defaultCurrency,
  currencies,
  transferOpts,
  kafkaTopics,
  statistics,
  status,
  corporateRange,
  columns,
  whitelistRange,
  transactionCodes,
  enableCron
} from '../../config';
import {
  DebitError,
  ForbiddenError,
  InternalError,
  NotFoundError,
  TimeError,
  TransferDayError
} from '../../core/ApiError';
import { logger } from '../../core/Logger';
import messages from '../../messages';
import updateStatistics from '../../helpers/statistics';
import { handlePagination } from '../../helpers/pagination';
import * as lodash from 'lodash';
import moment from 'moment';
import { subAccounts, transferFrequencies, ismocking, AEError, EGError } from '../../config';
import { CronJob } from 'cron';
import ScheduleTransfers from '../models/schedule';
import PayrollRequests from '../models/payrollRequest';
import { randomUUID } from 'crypto';
import { Consumer } from 'kafkajs';
import adminToken from '../../helpers/adminToken';
import { maskString } from '../../helpers/maskString';
import mongoose from 'mongoose';
import promiseAllsettled from 'promise.allsettled';
import correlation from 'express-correlation-id';
import { mockedAccounts } from '../../data/mockingAccounts';

let partitionId: any;

export const getUserAccounts = async (
  user: any,
  sector: number,
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const mainCifCurrency = currencies[user.country] || defaultCurrency;
    const accountsCurrencies = new Set();
    const conversions = await getConversionRates(defaultCurrency, false, tracingHeaders);

    const requests = user.allcifs.map(async (cifObj: { country: string; cif: string }) => {
      try {
        const cif = parseInt(cifObj.cif);
        const currentCifcurrency = currencies[cifObj.country] || mainCifCurrency;
        accountsCurrencies.add(currentCifcurrency);

        let result = [];

        if (ismocking && cifObj.country === 'AE') {
          if (AEError) throw new Error('AE is down');
          result = mockedAccounts[cifObj.country];
          logger.info(`accounts are mocked from ${cifObj.country}`);
        } else {
          if (AEError && cifObj.country === 'AE') throw new Error('AE is down');
          if (EGError && cifObj.country === 'EG') throw new Error('EG is down');
          const data = {
            route: `account/customer/${cif}`,
            authorization,
            country: cifObj.country
          };
          const producerData = await produce(
            data,
            tracingHeaders,
            true,
            `${correlation.getId()}-${cifObj.cif}`
          );
          result = await prepareKafkaResponse(producerData); //result would be declared here after remove mocking
          result = prepareWhitelistAccounts(result);
          const debitCards = await getListOfDebitCard(
            cif,
            authorization,
            sector,
            tracingHeaders,
            `${correlation.getId()}-${cifObj.cif}`
          );
          result = await linkDebitToAccount(result, debitCards);
        }

        const { defaultCurrencyTotal, total } = await getBalance(
          result,
          conversions,
          currentCifcurrency,
          'AvailableBalance'
        );

        result.forEach((account: any) => {
          account['country'] = cifObj.country;
          accountsCurrencies.add(account.Currency);
        });
        return {
          country: cifObj.country,
          accounts: result,
          balance: {
            defaultCurrencyAmount: Number(defaultCurrencyTotal),
            amount: Number(total),
            currentCifcurrency
          }
        };
      } catch (error) {
        logger.error(`in ${cifObj.country}: ${error.message}`);
        throw new Error(`${cifObj.country}`);
      }
    });

    const results: any = [];
    let notFetched: string | null = null;
    const res = await promiseAllsettled(requests);

    res.forEach((result) => {
      if (result.status === 'fulfilled') results.push(result.value);
      else {
        notFetched = (result.reason as Error).message;
      }
    });

    const allError = res.every((result) => {
      return result.status !== 'fulfilled';
    });

    if (allError) throw new InternalError(messages.accounts.allCountries.en);

    const accounts: any = [];
    const balance: any = {};
    balance.amount = 0;
    results.forEach((result: any) => {
      accounts.push(...result.accounts);
      balance[`${result.country}AccountsCount`] = result.accounts.length;
      balance[`${result.country}Amount`] = Number(result.balance.amount.toFixed(2));
      balance.amount += Number(result.balance.defaultCurrencyAmount);
    });
    if (mainCifCurrency !== defaultCurrency) {
      balance.amount /= conversions[mainCifCurrency];
    }
    balance.amount = Number(balance.amount.toFixed(2));
    balance.currency = mainCifCurrency;
    return {
      accounts,
      balance,
      accountsCurrencies: Array.from(accountsCurrencies),
      notFetched
    };
  } catch (e) {
    logger.error(e);
    throw new InternalError(messages.loans.internalError.en);
  }
};

export const getAccountTransactions = async (
  account: number,
  query: {
    fromDate: string;
    toDate: string;
    search: string;
    lastTransaction: string;
    limit: number;
  },
  authorization: string,
  pagination = true,
  tracingHeaders?: any,
  country = 'EG'
) => {
  try {
    if (!query.toDate) query.toDate = moment().format(transOptions.dateFormat);
    if (!query.fromDate)
      query.fromDate = moment()
        .subtract(transOptions.duration, 'months')
        .format(transOptions.dateFormat);
    const data = {
      route: `account/txns/${account}/${query.fromDate}/${query.toDate}`,
      authorization,
      country
    };
    const producerData = await produce(data, tracingHeaders);
    let result = await prepareKafkaResponse(producerData);
    if (result && result.length)
      result.map((item: any) => (item.correlationId = `${item.TransRef}-${item.Timestamp}`));

    const comparisonDate = moment().subtract(1, 'months').format(transOptions.dateFormat);
    if (query.toDate >= comparisonDate)
      result = await mergeTransactions(
        result,
        query.fromDate,
        query.toDate,
        account,
        authorization
      );

    if (query.search) result = handleSearch(result, query.search);
    result = lodash.orderBy(result, ['Timestamp'], ['desc']);
    if (!pagination) return result;
    return {
      transactions: handlePagination(
        result,
        query.limit || 20,
        query.lastTransaction || '',
        'correlationId'
      )
    };
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError(messages.accounts.internalError.en);
  }
};

export const getAccountDetails = async (
  account: string,
  country = 'EG',
  authorization: string,
  cif: string,
  tracingHeaders?: any
) => {
  if (!account.includes(cif)) {
    logger.debug('Invalid account number for user');
    throw new InternalError();
  }
  const data = {
    route: `account/${account}`,
    authorization,
    country
  };
  const producerData = await produce(data, tracingHeaders);
  const result = await prepareKafkaResponse(producerData);
  if (!result || (result && !result.length)) {
    logger.debug(messages.accounts.notFound.en);
    throw new NotFoundError(messages.accounts.notFound.en);
  }
  const details = result[0];
  return {
    currentAccount: {
      number: account,
      name: details.Category
    },
    generalInformation: {
      accountName: details.Category,
      openingDate: details.OpenDate,
      accountNumber: account,
      status: details.Status,
      iban: details.IBAN,
      cardAssigned: details.CreditCard
    },
    accountPlan: {
      annualInterest: details.InterestRate
    },
    balanceDetails: {
      availableBalance: {
        amount: details.AvailableBalance,
        currency: details.Currency
      },
      currentBalance: {
        amount: details.WorkingBalance,
        currency: details.Currency
      },
      holdAmount: {
        amount: details.LockedAmount,
        currency: details.Currency
      }
    },
    statementDetails: {
      statementType: details.StatementType,
      statementFrequency: details.StatementFreq
    }
  };
};

export const getBalance = async (
  items: any,
  conversions: any,
  currentCurrency: string,
  summedField: string
) => {
  let total = 0;
  for (const item of items) {
    if (item.Currency !== defaultCurrency) {
      total += item[summedField] * conversions[item.Currency];
    } else {
      total += item[summedField];
    }
  }

  const result = { defaultCurrencyTotal: total, total };

  if (currentCurrency !== defaultCurrency) {
    result.total /= conversions[currentCurrency];
  }
  return result;
};

export const getConversionRates = async (
  currency: string,
  allRecord = false,
  tracingHeaders?: any
) => {
  const data = {
    route: `rates/exchange/${currency}`
  };
  const producerData = await produce(data, tracingHeaders);

  const result = await prepareKafkaResponse(producerData);
  if (allRecord) return result;
  const conversion: { [key: string]: number } = {};
  for (const item of result) {
    conversion[item.CurrencyCode] = item.TransferBuyRate;
  }
  return conversion;
};

export const getCurrencyConversionRate = async (
  user: any,
  currency: string,
  tracingHeaders?: any
) => {
  try {
    const mainCifCurrency = currencies[user.country] || defaultCurrency;
    if (currency === mainCifCurrency) {
      return { theRate: 1 };
    }
    const conversions = await getConversionRates(defaultCurrency, false, tracingHeaders);
    const currencyToDefaultRate = currency === defaultCurrency ? 1 : conversions[currency];

    if (!currencyToDefaultRate) {
      throw Error("this currency's conversion rate is not available");
    }
    const defaultCurrencyEquivalent =
      mainCifCurrency === defaultCurrency ? 1 : conversions[mainCifCurrency];
    const theRate = defaultCurrencyEquivalent / currencyToDefaultRate;
    return { theRate };
  } catch (e) {
    throw new InternalError(e.message);
  }
};

export const handleSearch = (result: any, search: string) => {
  return lodash.filter(result, (item) => {
    return new RegExp('\\b' + search, 'gi').test(item.Description);
  });
};

export const mergeTransactions = async (
  completedResult: any[],
  fromDate: string,
  toDate: string,
  account: number,
  authorization: string,
  tracingHeaders?: any
) => {
  const data = {
    route: `debitcards/periodpendingtxns/${account}/${fromDate}/${toDate}`,
    authorization
  };
  const producerData = await produce(data, tracingHeaders);
  const pendingResult = await prepareKafkaResponse(producerData);
  let mappingResult = [];
  let description: string;
  if (pendingResult && pendingResult.length) {
    mappingResult = pendingResult.map((item: any) => {
      description = item.TerminalLoc;
      if (item.TransactionType === 'Purchase') description = `POS Purchase - ${item.TerminalLoc}`;
      if (item.TransactionType === 'Withdrawal')
        description = `ATM Withdrawal - ${item.TerminalLoc}`;
      return {
        AccID: item.Account,
        BookingDate: item.FromDate,
        TransRef: item.IDRef,
        Description: description,
        Withdrawals: -item.SettlementAmount,
        Currency: item.SettlementCurrency,
        Deposits: 0,
        Timestamp: item.DateTime,
        ValueDate: item.ToDate,
        Status: 'pre-authorized',
        correlationId: `${item.IDRef}-${item.DateTime}`
      };
    });
  }
  return completedResult.concat(mappingResult);
};

export const ownAccountsTransfer = async (
  body: {
    debitAcc: string;
    creditAcc: string;
    amount: number;
    description: string;
  },
  cif: string,
  user: string,
  authorization: string,
  platform: string,
  tracingHeaders?: any
) => {
  try {
    await validateDebitAccount(body.debitAcc);
    if (body.debitAcc === body.creditAcc)
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    const listAccount: any = [];
    let debitCurrency = '';
    let creditCurrency = '';
    let debitBalance = 0;
    let creditBalance = 0;
    const accountsData = {
      route: `account/list`,
      method: 'post',
      body: { AccId: [body.debitAcc, body.creditAcc] },
      authorization
    };
    const producerData = await produce(accountsData, tracingHeaders);
    const accounts = await prepareKafkaResponse(producerData);
    if (accounts && accounts.length) {
      accounts.map((item: any) => {
        if (
          item.AccID === body.debitAcc &&
          item.AvailableBalance >= body.amount &&
          item.Status === 'ACTIVE'
        ) {
          debitBalance = item.AvailableBalance;
          debitCurrency = item.Currency;
          listAccount.push(item);
        }
        if (
          body.creditAcc.includes(item.AccID) &&
          !item.Category.toLowerCase().includes('staff account') &&
          item.Status === 'ACTIVE'
        ) {
          creditBalance = item.AvailableBalance;
          creditCurrency = item.Currency;
          listAccount.push(item);
        }
      });
      if (listAccount.length !== transferOpts.minAccountsNum)
        throw new ForbiddenError(messages.accounts.transferAccount.en);
      const transferData: any = {
        route: `txn/acc2acc`,
        method: 'post',
        authorization,
        body: {
          CountryCode: transferOpts.countryCode,
          TransCode:
            platform === 'web' ? transactionCodes.ownAccountTransfer.web : transferOpts.transCode,
          DebitAcc: body.debitAcc,
          CreditAcc: body.creditAcc,
          Cif: cif,
          Currency: debitCurrency,
          Amount: body.amount,
          DebitDesc: body.description,
          CreditDesc: body.description,
          DebitReference: platform === 'web' ? transferOpts.webReference : transferOpts.reference,
          CreditReference: platform === 'web' ? transferOpts.webReference : transferOpts.reference,
          DebitValueDate: transferOpts.valueDate,
          CreditValueDate: transferOpts.valueDate
        }
      };
      if (debitCurrency !== creditCurrency) {
        transferData.route = `txn/fxacc2acc`;
        transferData.body.TargetCurrency = creditCurrency;
        transferData.body.TransCode =
          platform === 'web' ? transactionCodes.fxTransfer.web : transferOpts.fxTransCode;
        transferData.body.AmountType = 'NORMAL';
      }
      const producerTransferData = await produce(transferData, tracingHeaders);
      const finalData = await prepareKafkaResponse(producerTransferData);
      updateCustomerTransfers(cif, authorization, tracingHeaders);
      updateStatistics(statistics.ownTransferSuccess, '', {
        cif,
        user,
        type: 'transfer',
        subType: 'ownAccountTransfer',
        date: new Date(),
        index: 'transfer',
        microservice: 'account',
        source: 'account',
        beneficiaryBank: 'AAIB',
        debit: body.debitAcc,
        beneficiary: body.creditAcc,
        transactionId: finalData.TransactionId,
        amount: `EGP ${body.amount}`,
        status: status.success,
        platform: platform
      });
      return {
        creditBalance: creditBalance + body.amount,
        debitBalance: debitBalance - body.amount,
        currency: creditCurrency,
        transactionId: finalData.TransactionId
      };
    } else {
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    }
  } catch (e) {
    logger.error(e);
    updateStatistics(statistics.ownTransferFail, e.message, {
      cif,
      user,
      type: 'transfer',
      subType: 'ownAccountTransfer',
      date: new Date(),
      index: 'transfer',
      microservice: 'account',
      source: 'account',
      beneficiaryBank: 'AAIB',
      debit: body.debitAcc,
      beneficiary: body.creditAcc,
      amount: `EGP ${body.amount}`,
      status: e.message === messages.accounts.timeout.en ? status.timeout : status.fail,
      platform: platform
    });
    if (e.message === messages.accounts.transferAccount.en)
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    if (e.message === messages.accounts.overDraftAccount.en) throw new ForbiddenError(e.message);
    else throw new InternalError(messages.accounts.internalError.en);
  }
};

export const allTransfers = async (
  body: {
    debitAcc: string;
    creditAcc: string;
    amount: number;
    swift: string;
    currency: string;
    beneficiaryName: string;
    chargeType: string;
    description: string;
    country?: string;
  },
  correlationId = '',
  cif: string,
  user: string,
  authorization: string,
  platform: string,
  tracingHeaders?: any,
  isMobileNumber = false,
  scheduledTransfer = false
) => {
  try {
    await validateDebitAccount(body.debitAcc);

    if (scheduledTransfer) logger.debug('schedule transfer');

    let internalSwift = true;
    const transferType = 'Account/IBAN Number';

    if (body.swift && body.swift !== transferOpts.defaultSwift) {
      const swiftCountry = body.swift.substring(4, 6);
      if (swiftCountry != 'EG') internalSwift = false;

      const beneficiary = {
        countryCode: internalSwift ? transferOpts.countryCode : body.country,
        swift: body.swift,
        name: body.beneficiaryName,
        accountNumber: body.creditAcc
      };
      if (body.currency != transferOpts.currency || !internalSwift) {
        return internationalTransfer(
          body,
          cif,
          user,
          beneficiary,
          authorization,
          platform,
          correlationId,
          transferType,
          scheduledTransfer,
          tracingHeaders
        );
      } else {
        return externalTransfer(
          body,
          cif,
          user,
          beneficiary,
          authorization,
          platform,
          correlationId,
          transferType,
          scheduledTransfer,
          tracingHeaders
        );
      }
    }

    return internalTransfer(
      body,
      cif,
      user,
      authorization,
      platform,
      correlationId,
      transferType,
      scheduledTransfer,
      isMobileNumber,
      tracingHeaders
    );
  } catch (e) {
    logger.error(e);
    updateStatistics(
      statistics.internalTransferFail,
      e.message,
      {
        cif,
        user,
        type: 'transfer',
        subType: 'internalAccountTransfer',
        date: new Date(),
        microservice: 'account',
        index: 'transfer',
        source: 'account',
        beneficiaryBank: 'AAIB',
        debit: body.debitAcc,
        beneficiary: body.creditAcc,
        amount: `EGP ${body.amount}`,
        status: e.message === messages.accounts.timeout.en ? status.timeout : status.fail,
        platform: platform
      },
      correlationId
    );
    if (e.message === messages.accounts.transferAccount.en)
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    if (e.message === messages.accounts.overDraftAccount.en) throw new ForbiddenError(e.message);
    else throw new InternalError(messages.accounts.internalError.en);
  }
};

export const transferByBeneficiaryId = async (
  id: string,
  body: {
    debitAcc: string;
    amount: number;
    currency: string;
    description: string;
    chargeType: string;
    otp?: string;
  },
  scheduledTransfer: boolean,
  multipletransfers: boolean,
  correlationId = '',
  cif: string,
  user: string,
  authorization: string,
  platform: string,
  tracingHeaders?: any
) => {
  try {
    await validateDebitAccount(body.debitAcc);
    const beneficiary = await getBeneficiaryById(
      cif,
      id,
      authorization,
      multipletransfers ? `${correlationId}-mu` : correlationId,
      tracingHeaders
    );
    if (beneficiary.requiresOtp && !body.otp && !scheduledTransfer && !multipletransfers)
      throw new ForbiddenError();

    let transfer: any = {};
    if (beneficiary.isCreditCardNumber) {
      const adminToken = await generateAdminToken();
      const adminAuthorizationHeader = `Bearer ${adminToken}`;

      const contractId = await getContractIdForCreditCard(
        adminAuthorizationHeader,
        beneficiary.accountNumber,
        tracingHeaders
      );
      const availableBalance = await getAccountAvailableBalance(
        body.debitAcc,
        authorization,
        correlationId,
        tracingHeaders
      );

      try {
        const creditTransfer = await creditCardTransfer(
          {
            Cif: cif,
            CountryCode: 'EG',
            DebitAcc: body.debitAcc,
            ContractId: contractId,
            Currency: body.currency,
            Amount: body.amount,
            DebitDesc: body.description,
            CreditDesc: body.description,
            DebitReference: platform === 'web' ? transferOpts.webReference : transferOpts.reference,
            CreditReference: platform === 'web' ? transferOpts.webReference : transferOpts.reference
          },
          adminAuthorizationHeader,
          correlationId,
          tracingHeaders
        );
        if (!scheduledTransfer) updateCustomerTransfers(cif, authorization, tracingHeaders);
        updateStatistics(
          statistics.creditCardTransferSuccess,
          '',
          {
            cif,
            user,
            type: 'transfer',
            subType: 'creditCardTransfer',
            transferType: 'Credit Card Number',
            date: new Date(),
            index: 'transfer',
            microservice: 'account',
            source: 'account',
            beneficiaryBank: beneficiary.swift,
            debit: body.debitAcc,
            transactionId: creditTransfer.TransactionId,
            beneficiary: maskString(beneficiary.accountNumber),
            amount: `${body.currency} ${body.amount}`,
            status: status.success,
            platform: platform
          },
          correlationId
        );
        transfer = {
          debitBalance: availableBalance - body.amount,
          currency: body.currency,
          transactionId: creditTransfer.TransactionId
        };
      } catch (e) {
        updateStatistics(
          statistics.creditCardTransferFail,
          e.message,
          {
            cif,
            user,
            type: 'transfer',
            subType: 'creditCardTransfer',
            transferType: 'Credit Card Number',
            date: new Date(),
            index: 'transfer',
            microservice: 'account',
            source: 'account',
            beneficiaryBank: beneficiary.swift,
            debit: body.debitAcc,
            beneficiary: maskString(beneficiary.accountNumber),
            amount: `${body.currency} ${body.amount}`,
            status: e.message === messages.accounts.timeout.en ? status.timeout : status.fail,
            platform: platform
          },
          correlationId
        );
        throw new InternalError();
      }
    } else {
      transfer = await allTransfers(
        {
          debitAcc: body.debitAcc,
          creditAcc: beneficiary.accountNumber || beneficiary.iban,
          amount: body.amount,
          swift: beneficiary.swift,
          currency: body.currency,
          chargeType: body.chargeType,
          description: body.description,
          beneficiaryName: beneficiary.name,
          country: beneficiary.country
        },
        multipletransfers ? `${correlationId}-mu` : correlationId,
        cif,
        user,
        authorization,
        platform,
        tracingHeaders,
        beneficiary.isMobileNumber,
        scheduledTransfer
      );
    }
    if (beneficiary.trusted) {
      await updateBeneficiary(cif, user, platform, id, authorization, correlationId);
    }
    return transfer;
  } catch (e) {
    logger.error(e);
    if (e.message === messages.accounts.transferAccount.en)
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    if (e.message === messages.accounts.forbidden.en) throw new ForbiddenError();
    if (e.message === messages.accounts.overDraftAccount.en) throw new ForbiddenError(e.message);
    else throw new InternalError(messages.accounts.internalError.en);
  }
};

export const multipletransfers = async (
  body: {
    transfers: [
      {
        id: string;
        accountData: {
          debitAcc: string;
          amount: number;
          currency: string;
          description: string;
          chargeType: string;
        };
      }
    ];
    otp: string;
  },
  cif: string,
  user: string,
  authorization: string,
  platform: string,
  tracingHeaders?: any
) => {
  try {
    if (body.transfers.length > 10) {
      throw new ForbiddenError();
    }
    const successResults: any = [];
    const failedResults: any = [];
    const promises = body.transfers.map((transfer) => {
      return new Promise(async (resolve, reject) => {
        try {
          const result = await transferByBeneficiaryId(
            transfer.id,
            transfer.accountData,
            false,
            true,
            `${correlation.getId()} - ${lodash.random(999)}`,
            cif,
            user,
            authorization,
            platform,
            tracingHeaders
          );
          resolve({ ...result, benefID: transfer.id, amount: transfer.accountData.amount });
        } catch (error) {
          logger.error(error.message);
          reject({ benefID: transfer.id });
        }
      });
    });
    const res = await promiseAllsettled(promises);
    res.forEach((result) => {
      if (result.status === 'fulfilled') successResults.push(result.value);
      else failedResults.push(result.reason);
    });
    return { success: successResults, failed: failedResults };
  } catch (e) {
    logger.error(e);
    if (e.message === messages.accounts.transferAccount.en)
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    if (e.message === messages.accounts.forbidden.en) throw new ForbiddenError();
    else throw new InternalError(messages.accounts.internalError.en);
  }
};

export const getContractIdForCreditCard = async (
  authorization: string,
  CardNumber: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `creditcards/card`,
      method: 'post',
      authorization,
      body: {
        CardNumber
      }
    };
    const producerData = await produce(data, tracingHeaders);
    const cardDetails = await prepareKafkaResponse(producerData);
    if (cardDetails && cardDetails.length > 0) {
      return cardDetails[0].ContractId;
    } else {
      throw new NotFoundError();
    }
  } catch (e) {
    logger.debug(e.message);
    throw new NotFoundError();
  }
};

const getAccountAvailableBalance = async (
  accountNumber: string,
  authorization: string,
  correlationId: string,
  tracingHeaders?: any
) => {
  try {
    const accountsData = {
      route: `account/${accountNumber}`,
      authorization
    };
    const producerData = await produce(accountsData, tracingHeaders, true, correlationId);
    const accounts = await prepareKafkaResponse(producerData);
    if (!accounts || (accounts && !accounts.length)) throw new ForbiddenError();
    const debitAccount = accounts[0];
    return debitAccount.AvailableBalance;
  } catch (e) {
    throw new InternalError();
  }
};

export const creditCardTransfer = async (
  body: {
    CountryCode: string;
    Cif: string;
    DebitAcc: string;
    ContractId: string;
    Currency: string;
    Amount: number;
    DebitDesc?: string;
    CreditDesc?: string;
    DebitReference?: string;
    CreditReference?: string;
  },
  authorization: string,
  correlationId = '',
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `txn/pay/creditcard`,
      method: 'post',
      authorization,
      body: {
        Cif: body.Cif,
        CountryCode: body.CountryCode || 'EG',
        DebitAcc: body.DebitAcc,
        ContractId: body.ContractId,
        Currency: body.Currency || 'EGP',
        Amount: body.Amount,
        DebitDesc: body.DebitDesc,
        CreditDesc: body.CreditDesc,
        DebitReference: body.DebitReference,
        CreditReference: body.CreditReference
      }
    };
    const producerData = await produce(data, tracingHeaders, true, correlationId);
    return await prepareKafkaResponse(producerData);
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const internationalTransfer = async (
  body: {
    debitAcc: string;
    amount: number;
    description: string;
    chargeType: string;
    currency: string;
  },
  cif: string,
  user: string,
  beneficiary: any,
  authorization: string,
  platform: string,
  correlationId: string,
  transferType: string,
  scheduledTransfer = false,
  tracingHeaders?: any
) => {
  try {
    const accountsData = {
      route: `account/${body.debitAcc}`,
      authorization
    };
    const producerData = await produce(accountsData, tracingHeaders, true, correlationId);
    const accounts = await prepareKafkaResponse(producerData);
    if (!accounts || (accounts && !accounts.length)) throw new ForbiddenError();
    const debitAccount = accounts[0];
    if (debitAccount.Currency !== body.currency) throw new ForbiddenError();
    if (debitAccount.AvailableBalance < body.amount) throw new ForbiddenError();
    const debitBalance = debitAccount.AvailableBalance;
    const transferInternationalData = {
      route: `txn/swift/international`,
      authorization,
      method: 'post',
      body: {
        Cif: cif,
        DebitAcc: body.debitAcc,
        Amount: body.amount,
        Currency: body.currency,
        ChargesType: body.chargeType || transferOpts.chargeType,
        ReasonForPayment: body.description,
        SwiftCode: beneficiary.swift,
        BenificiaryName: beneficiary.name,
        BenificiaryAccount: beneficiary.accountNumber || beneficiary.iban,
        BenificiaryCountry: beneficiary.countryCode,
        DebitReference: platform === 'web' ? transferOpts.webReference : transferOpts.reference,
        CreditReference: platform === 'web' ? transferOpts.webReference : transferOpts.reference
      }
    };
    const producerTransferData = await produce(
      transferInternationalData,
      tracingHeaders,
      true,
      correlationId
    );
    const finalData = await prepareKafkaResponse(producerTransferData);
    if (!scheduledTransfer) updateCustomerTransfers(cif, authorization, tracingHeaders);
    updateStatistics(
      statistics.internationalTransferSuccess,
      '',
      {
        cif,
        user,
        type: 'transfer',
        subType: 'internationalAccountTransfer',
        transferType,
        date: new Date(),
        microservice: 'account',
        index: 'transfer',
        source: 'account',
        beneficiaryBank: beneficiary.swift,
        debit: body.debitAcc,
        transactionId: finalData.TransactionId,
        beneficiary: beneficiary.accountNumber || beneficiary.iban,
        beneficiaryCountry: beneficiary.countryCode,
        amount: `${body.currency} ${body.amount}`,
        status: status.success,
        platform: platform
      },
      correlationId
    );
    return {
      debitBalance: debitBalance,
      currency: debitAccount.Currency,
      transactionId: finalData.TransactionId
    };
  } catch (e) {
    logger.error(e);
    updateStatistics(
      statistics.internationalTransferFail,
      e.message,
      {
        cif,
        user,
        type: 'transfer',
        subType: 'internationalAccountTransfer',
        transferType,
        date: new Date(),
        index: 'transfer',
        microservice: 'account',
        source: 'account',
        beneficiaryBank: beneficiary.swift,
        debit: body.debitAcc,
        beneficiary: beneficiary.accountNumber || beneficiary.iban,
        amount: `${body.currency} ${body.amount}`,
        beneficiaryCountry: beneficiary.countryCode,
        status: e.message === messages.accounts.timeout.en ? status.timeout : status.fail,
        platform: platform
      },
      correlationId
    );
    if (e.message === messages.accounts.transferAccount.en)
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    if (e.message === messages.accounts.forbidden.en) throw new ForbiddenError();
    else throw new InternalError(messages.accounts.internalError.en);
  }
};

export const externalTransfer = async (
  body: {
    debitAcc: string;
    amount: number;
    description: string;
    chargeType: string;
  },
  cif: string,
  user: string,
  beneficiary: any,
  authorization: string,
  platform: string,
  correlationId: string,
  transferType: string,
  scheduledTransfer = false,
  tracingHeaders?: any
) => {
  try {
    const accountsData = {
      route: `account/${body.debitAcc}`,
      authorization
    };
    const producerData = await produce(accountsData, tracingHeaders, true, correlationId);
    const accounts = await prepareKafkaResponse(producerData);
    if (!accounts || (accounts && !accounts.length)) throw new ForbiddenError();
    const debitAccount = accounts[0];
    if (debitAccount.Currency !== defaultCurrency) throw new ForbiddenError();
    if (debitAccount.AvailableBalance < body.amount) throw new ForbiddenError();
    const debitBalance = debitAccount.AvailableBalance;
    const transCode = platform === 'web' ? transactionCodes.externalTransfer.web : null;
    const transferInternalData = {
      route: `txn/ach/local`,
      authorization,
      method: 'post',
      body: {
        CountryCode: beneficiary.countryCode || transferOpts.countryCode,
        TransCode: transCode,
        Cif: cif,
        DebitAcc: body.debitAcc,
        Amount: body.amount,
        ChargesType: body.chargeType || transferOpts.chargeType,
        ReasonForPayment: body.description,
        SwiftCode: beneficiary.swift,
        BenificiaryName: beneficiary.name,
        BenificiaryAccount: beneficiary.accountNumber || beneficiary.iban,
        DebitReference: platform === 'web' ? transferOpts.webReference : null,
        CreditReference: platform === 'web' ? transferOpts.webReference : null
      }
    };
    const producerTransferData = await produce(
      transferInternalData,
      tracingHeaders,
      true,
      correlationId
    );
    const finalData = await prepareKafkaResponse(producerTransferData);
    const correlationLog = correlation.getId();
    console.log(`inside externalTransfer ${correlationLog} , Response: ${finalData}`);
    if (!scheduledTransfer) updateCustomerTransfers(cif, authorization, tracingHeaders);
    updateStatistics(
      statistics.externalTransferSuccess,
      '',
      {
        cif,
        user,
        type: 'transfer',
        subType: 'externalAccountTransfer',
        transferType,
        date: new Date(),
        microservice: 'account',
        index: 'transfer',
        source: 'account',
        beneficiaryBank: beneficiary.swift,
        debit: body.debitAcc,
        transactionId: finalData.TransactionId,
        beneficiary: beneficiary.accountNumber || beneficiary.iban,
        amount: `EGP ${body.amount}`,
        status: status.success,
        platform: platform
      },
      correlationId
    );
    return {
      debitBalance: debitBalance - body.amount,
      currency: debitAccount.Currency,
      transactionId: finalData.TransactionId
    };
  } catch (e) {
    logger.error(e);
    updateStatistics(
      statistics.externalTransferFail,
      e.message,
      {
        cif,
        user,
        type: 'transfer',
        subType: 'externalAccountTransfer',
        transferType,
        date: new Date(),
        index: 'transfer',
        microservice: 'account',
        source: 'account',
        beneficiaryBank: beneficiary.swift,
        debit: body.debitAcc,
        beneficiary: beneficiary.accountNumber || beneficiary.iban,
        amount: `EGP ${body.amount}`,
        status: e.message === messages.accounts.timeout.en ? status.timeout : status.fail,
        platform: platform
      },
      correlationId
    );
    if (e.message === messages.accounts.transferAccount.en)
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    if (e.message === messages.accounts.forbidden.en) throw new ForbiddenError();
    else throw new InternalError(messages.accounts.internalError.en);
  }
};
export const internalTransfer = async (
  body: {
    debitAcc: string;
    creditAcc: string;
    amount: number;
    swift: string;
    currency: string;
    beneficiaryName: string;
    chargeType: string;
    description: string;
    country?: string;
  },
  cif: string,
  user: string,
  authorization: string,
  platform: string,
  correlationId: string,
  transferType: string,
  scheduledTransfer = false,
  isMobileNumber = false,
  tracingHeaders?: any
) => {
  try {
    let benificiaryMobile = null;
    if (isMobileNumber) {
      benificiaryMobile = body.creditAcc;
      const creditAccount = await getAccountByMobileNumber(
        authorization,
        body.creditAcc,
        tracingHeaders
      );
      if (creditAccount) {
        body.creditAcc = creditAccount;
        transferType = 'Mobile Number';
      } else {
        logger.error('Transfer by mobile number - Cannot find account number for this mobile');
        throw new InternalError();
      }
    }
    if (body.debitAcc === body.creditAcc)
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    if (body.creditAcc.includes(cif))
      return ownAccountsTransfer(body, cif, user, authorization, platform, tracingHeaders || null);
    const accountData = {
      route: `account/${body.debitAcc}`,
      authorization
    };
    const producerData = await produce(accountData, tracingHeaders, true, correlationId);
    const account = await prepareKafkaResponse(producerData);
    if (
      account.length === 0 ||
      account[0]?.Currency !== defaultCurrency ||
      account[0]?.Status !== 'ACTIVE' ||
      account[0]?.AvailableBalance < body.amount
    ) {
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    }

    const transCode =
      platform === 'web' ? transactionCodes.internalTransfer.web : transferOpts.internalTransCode;
    const internalTransferData = {
      route: `txn/acc2acc`,
      authorization,
      method: 'post',
      body: {
        CountryCode: transferOpts.countryCode,
        TransCode: transCode,
        Cif: cif,
        DebitAcc: body.debitAcc,
        CreditAcc: body.creditAcc,
        Currency: body.currency,
        Amount: body.amount,
        DebitDesc: body.description,
        CreditDesc: body.description,
        SwiftCode: body.swift,
        BenificiaryName: body.beneficiaryName,
        BenificiaryAccount: benificiaryMobile ? benificiaryMobile : body.creditAcc,
        DebitReference: platform === 'web' ? transferOpts.webReference : transferOpts.reference,
        CreditReference: platform === 'web' ? transferOpts.webReference : transferOpts.reference,
        DebitValueDate: transferOpts.valueDate,
        CreditValueDate: transferOpts.valueDate
      }
    };
    const producerTransferData = await produce(
      internalTransferData,
      tracingHeaders,
      true,
      correlationId
    );
    const transferResult = await prepareKafkaResponse(producerTransferData);
    if (!scheduledTransfer) updateCustomerTransfers(cif, authorization, tracingHeaders);
    updateStatistics(
      statistics.internalTransferSuccess,
      '',
      {
        cif,
        user,
        type: 'transfer',
        subType: 'internalAccountTransfer',
        transferType,
        date: new Date(),
        index: 'transfer',
        microservice: 'account',
        source: 'account',
        beneficiaryBank: 'AAIB',
        debit: body.debitAcc,
        transactionId: transferResult.TransactionId,
        beneficiary: body.creditAcc,
        amount: `EGP ${body.amount}`,
        status: status.success,
        platform: platform
      },
      correlationId
    );

    return {
      debitBalance: account[0]?.AvailableBalance - body.amount,
      currency: body.currency,
      transactionId: transferResult.TransactionId
    };
  } catch (e) {
    logger.error(e);
    updateStatistics(
      statistics.internalTransferFail,
      e.message,
      {
        cif,
        user,
        type: 'transfer',
        subType: 'internallAccountTransfer',
        transferType,
        date: new Date(),
        index: 'transfer',
        microservice: 'account',
        source: 'account',
        beneficiaryBank: body.swift,
        debit: body.debitAcc,
        beneficiary: body.creditAcc,
        amount: `EGP ${body.amount}`,
        status: e.message === messages.accounts.timeout.en ? status.timeout : status.fail,
        platform: platform
      },
      correlationId
    );
    if (e.message === messages.accounts.transferAccount.en)
      throw new ForbiddenError(messages.accounts.transferAccount.en);
    if (e.message === messages.accounts.forbidden.en) throw new ForbiddenError();
    else throw new InternalError(messages.accounts.internalError.en);
  }
};

export const getBeneficiaryById = async (
  cif: string,
  id: string,
  authorization: string,
  correlationId: string,
  tracingHeaders?: any
) => {
  try {
    const beneficiaryData = {
      function: 'getUserBeneficiariesById',
      params: {
        id,
        cif
      },
      authorization,
      topic: kafkaTopics.beneficiaryRequest
    };
    const producerBeneficiaryData = await produce(
      beneficiaryData,
      tracingHeaders,
      true,
      correlationId
    );
    return prepareKafkaResponse(producerBeneficiaryData);
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const updateBeneficiary = async (
  cif: string,
  user: string,
  platform: string,
  id: string,
  authorization: string,
  correlationId: string,
  tracingHeaders?: any
) => {
  try {
    const trustedData = {
      function: 'updateUserBeneficiaries',
      params: {
        id,
        body: {
          requiresOtp: false
        },
        cif,
        user,
        platform
      },
      authorization,
      topic: kafkaTopics.beneficiaryRequest
    };
    const producerTrustedData = await produce(trustedData, tracingHeaders, true, correlationId);
    await prepareKafkaResponse(producerTrustedData);
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const getListOfDebitCard = async (
  cif: number,
  authorization: string,
  sector: number,
  tracingHeaders: any,
  requestCorrelation: string
) => {
  try {
    if (lodash.inRange(Number(sector), corporateRange.min, corporateRange.max)) return [];
    const data = {
      topic: kafkaTopics.euronetRequest,
      route: `debitcards/customer/${cif}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders, true, requestCorrelation);
    return await prepareKafkaResponse(producerData);
  } catch (e) {
    logger.error(`Error in getting debit cards for user ${cif}`);
    logger.error(e.message);
    return [];
  }
};

export const linkDebitToAccount = async (accounts: any, debitCards: any) => {
  try {
    debitCards.sort((card: any) => {
      return card.CardStatus.toLowerCase() === 'active' ? -1 : 1;
    });
    debitCards.map((debitCard: any) => {
      if (!debitCard.accounts) debitCard.accounts = [];
      debitCard.LinkedAccounts.map((account: any) => debitCard.accounts.push(account.Number));
    });
    accounts.map((account: any) => {
      if (!account.debitCards) account.debitCards = [];
      debitCards.map((debitCard: any) => {
        if (debitCard.accounts.indexOf(account.AccID) !== -1) {
          account.debitCards.push(debitCard);
        }
      });
    });
    return accounts;
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const getAccountUsage = async (id: string, authorization: string, tracingHeaders: any) => {
  try {
    const data = {
      route: `account/usage/${id}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    return result && result[0] ? result[0] : {};
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const sendStatement = async (
  cif: string,
  type: string,
  account: number,
  authorization: string,
  email: any,
  query: {
    fromDate: string;
    toDate: string;
  },
  platform: string,
  tracingHeaders: any
) => {
  try {
    const isWeb = platform === 'web' ? true : false;
    if (!isWeb && (!email || !email.length))
      throw new InternalError(messages.authorization.emailNotFound.en);
    if (!query.toDate) query.toDate = moment().format(transOptions.dateFormat);
    if (!query.fromDate)
      query.fromDate = moment()
        .subtract(transOptions.duration, 'months')
        .format(transOptions.dateFormat);
    let transactionsData = await getAccountTransactions(
      account,
      { fromDate: query.fromDate, toDate: query.toDate, search: '', lastTransaction: '', limit: 0 },
      authorization,
      false,
      tracingHeaders
    );
    transactionsData = filterTransactionsData(transactionsData);
    if (transactionsData.length) {
      const data = {
        topic: kafkaTopics.smtp,
        function: type === 'excel' ? 'generateExcelFile' : 'generatePDFFile',
        body: {
          name:
            type === 'excel'
              ? `Transaction-History-${new Date().getTime()}.xlsx`
              : `Transaction-History-${new Date().getTime()}.pdf`,
          data: transactionsData,
          email,
          isWeb,
          columns: type === 'excel' ? [] : columns,
          columnName: 'Transaction history',
          title: `Transaction history  for ${cif}`
        }
      };
      if (!isWeb) {
        await produce(data, tracingHeaders, false);
        return { message: 'Success' };
      } else {
        const producerData = await produce(data, tracingHeaders, true);
        const result = await prepareKafkaResponse(producerData);
        return { base64Data: result.base64Data, fileName: 'E-statement' };
      }
    } else throw new NotFoundError(messages.authorization.transactions.en);
  } catch (e) {
    logger.error(e.message);
    if (e.type === 'NotFoundError') {
      throw new NotFoundError(messages.authorization.transactions.en);
    }
    throw new InternalError();
  }
};

export const filterTransactionsData = (data: any) => {
  return data.map((item: any) => {
    return {
      Description: item.Description,
      'Booking Date': item.BookingDate,
      'Value Date': item.ValueDate,
      Withdrawals: item.Withdrawals,
      Deposits: item.Deposits,
      Balance: item.Balance,
      Currency: item.Currency,
      Time: item.Timestamp
    };
  });
};

export const generateTransferReceipt = async (
  body: any,
  cif: string,
  language: string,
  tracingHeaders: any
) => {
  try {
    body = { ...body, name: `Transfer-Receipt-${new Date().getTime()}.pdf` };
    const data = {
      topic: kafkaTopics.smtp,
      function: 'generateTransferReceipt',
      body,
      name: `Transaction-History-${new Date().getTime()}.pdf`,
      cif,
      template: language == 'ar' ? 'transferReceiptAR' : 'transferReceipt'
    };
    const producerData = await produce(data, tracingHeaders, true);
    const result = await prepareKafkaResponse(producerData);
    return {
      base64Data: result.base64Data,
      fileName: `Receipt of ${body.transactionDate}_${body.transactionId}`
    };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const mailbox = async (
  body: { message: string; category: string; phoneNumber: string },
  cif: string,
  id: string,
  platform: string
) => {
  try {
    const data = {
      topic: kafkaTopics.smtp,
      function: 'sendEmailService',
      body,
      cif,
      template: 'mailbox'
    };
    await produce(data, null, false);
    updateStatistics('', '', {
      cif,
      id,
      type: 'request',
      subType: 'requestMessage',
      date: new Date(),
      mobile: body.phoneNumber,
      microservice: 'account',
      category: body.category,
      message: body.message,
      status: status.success,
      platform: platform
    }).catch((e) => e.message);
    return { message: 'Success' };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

export const mailboxCategory = async () => {
  try {
    const data = {
      category: [
        'Accounts',
        'Credit Cards',
        'Investments',
        'Loans',
        'Disputes',
        'Transfers',
        'Payments'
      ]
    };
    return data;
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

export const handleAccountCategory = (accountSubType: string, accountFrequency: string) => {
  if (accountSubType == subAccounts.subTypes.call) {
    return subAccounts.categories.currentCall;
  }
  if (accountSubType == subAccounts.subTypes.demand) {
    return subAccounts.categories.currentDemand;
  }
  if (accountSubType == subAccounts.subTypes.golden) {
    return `SAVING GOLDEN ${accountFrequency}`;
  }

  if (accountFrequency == subAccounts.frequencies.monthly) {
    return subAccounts.categories.savingMonthly;
  }
  if (accountFrequency == subAccounts.frequencies.quarterly) {
    return subAccounts.categories.savingQuarterly;
  }
  if (accountFrequency == subAccounts.frequencies.semiAnually) {
    return subAccounts.categories.savingSemiAnnually;
  }
  if (accountFrequency == subAccounts.frequencies.annually) {
    return subAccounts.categories.savingAnnually;
  }
};

export const openSubAccount = async (
  body: any,
  cif: string,
  id: string,
  platform: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    body.Customer = cif;
    const data = {
      route: `account/open/sub`,
      authorization,
      method: 'post',
      body
    };

    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);

    updateStatistics('', '', {
      cif,
      id,
      type: 'subAccount',
      subType: 'opneSubAccount',
      date: new Date(),
      microservice: 'account',
      accountType: body.Type,
      currency: body.Currency,
      status: status.success,
      platform: platform
    }).catch((e) => e.message);
    return {
      AccID: result.TransactionId,
      Category: handleAccountCategory(body.SubType, body.Frequency),
      IBAN: result.IBAN,
      AvailableBalance: '0',
      Currency: body.Currency,
      Status: 'ACTIVE'
    };
  } catch (e) {
    logger.error(`inside open/sub account, ${e.message}`);
    throw new InternalError(e.message);
  }
};

export const prepareWhitelistAccounts = (result: any) => {
  try {
    const accounts: any[] = [];
    result.forEach((account: any) => {
      whitelistRange.forEach((item: any) => {
        if (Number(account.CategoryCode) >= item.min && Number(account.CategoryCode) <= item.max) {
          accounts.push(account);
        }
      });
    });
    return accounts;
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

export const getCurrencyList = async (tracingHeaders?: any) => {
  try {
    const data = {
      route: `list/currency`,
      method: 'get'
    };
    const producerData = await produce(data, tracingHeaders);
    return prepareKafkaResponse(producerData);
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};
export const fxCalculator = async (
  SrcCur: string,
  DstCur: string,
  Amount: number,
  tracingHeaders?: any,
  requestCorrelation?: string
) => {
  try {
    if (Amount === 0)
      return {
        SourceCurrency: SrcCur,
        DestinationCurrency: DstCur,
        SourceAmount: Amount,
        DestinationAmount: Amount
      };
    const data = {
      route: `rates/fxcalculator/${SrcCur}/${DstCur}/${Amount}`
    };
    const producerData = await produce(data, tracingHeaders, true, requestCorrelation);
    const result = await prepareKafkaResponse(producerData);

    if (result && result.length > 0) {
      return result[0];
    }
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

export const getSubAccountsCurrencies = () => {
  return subAccounts.currencies;
};

const checkDebitAccount = (debitAcc: string, cif: string) => {
  if (debitAcc.slice(0, 8) !== cif) {
    throw new DebitError();
  }
};

export const scheduleTransfer = async (transferBody: any, scheduleBody: any) => {
  try {
    if (scheduleBody.transferFrequency == transferFrequencies.once) {
      scheduleBody.totalTransferCount = 1;
    }
    checkDebitAccount(transferBody.debitAcc, scheduleBody.user);
    transferBody.beneficiaryID = new mongoose.Types.ObjectId(transferBody.beneficiaryID);
    await new ScheduleTransfers({
      body: transferBody,
      ...scheduleBody
    }).save();
    return { message: 'Success' };
  } catch (e) {
    if (e.message === messages.authorization.scheduleTransfer.en) throw new DebitError();
    else throw new InternalError();
  }
};

export const getScheduleTransfers = async (cif: string) => {
  try {
    return ScheduleTransfers.aggregate([
      {
        $match: { user: cif }
      },
      {
        $lookup: {
          from: 'beneficiaries',
          localField: 'body.beneficiaryID',
          foreignField: '_id',
          as: 'beneficiary'
        }
      },
      {
        $unwind: '$beneficiary'
      },
      {
        $sort: { updatedAt: -1 }
      }
    ]);
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

const conditionalDate = (type: string, date?: any, month?: any, week?: any, day?: any): any => {
  switch (type) {
    case 'timestamp': {
      if (date) {
        return new Date(date).getTime();
      }
      return moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).toDate().getTime();
    }
    case 'moment': {
      if (week && date) {
        const oldWeekday = moment(date).day();
        const today = moment().day();
        if (oldWeekday > today) {
          week -= 1;
        }
        return moment()
          .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
          .add(week, 'weeks')
          .day(oldWeekday);
      }
      if (month && day) {
        return moment()
          .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
          .add(month, 'months')
          .set({ date: day });
      }
      if (date) {
        return moment(date);
      }
      if (day) {
        return moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).set({ date: day });
      }
      return moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    }
    case 'date': {
      if (date) {
        return moment(date).date();
      }
      return moment().date();
    }
    default:
      break;
  }
};

const isTransferDate = (oldScheduleBody: any): boolean => {
  const { nextTransferDate: nextTransferFormat } = oldScheduleBody;
  const dateNow = conditionalDate('timestamp');
  const nextTransferDate = conditionalDate('timestamp', nextTransferFormat);
  if (nextTransferDate === dateNow) {
    return true;
  }
  return false;
};

const validateTime = (oldScheduleBody: any, newScheduleBody: any): boolean => {
  const { scheduleDate: oldScheduleDateFormat, transferFrequency: oldScheduleType } =
    oldScheduleBody;
  const { scheduleDate: newScheduleDateFormat, transferFrequency: newScheduleType } =
    newScheduleBody;
  const oldScheduleDate = conditionalDate('timestamp', oldScheduleDateFormat);
  const newScheduleDate = conditionalDate('timestamp', newScheduleDateFormat);
  const dateNow = conditionalDate('timestamp');
  if (
    newScheduleDate !== oldScheduleDate &&
    (newScheduleDate < dateNow || newScheduleDate === dateNow)
  ) {
    throw new TimeError();
  }
  const isChanged = isTransferDate(oldScheduleBody);
  if (isChanged) throw new TransferDayError();
  if (
    oldScheduleDate === newScheduleDate &&
    oldScheduleType !== newScheduleType &&
    oldScheduleDate < dateNow
  ) {
    return true;
  }
  return false;
};

const checkingValidateValue = (oldScheduleBody: any, newScheduleBody: any): any => {
  const {
    scheduleDate: oldScheduleDateFormat,
    nextTransferDate: olNextTransferDate,
    transferFrequency: oldScheduleType
  } = oldScheduleBody;
  const { transferFrequency: newScheduleType } = newScheduleBody;
  let nextTransferDate = null;
  const oldDay = conditionalDate('date', oldScheduleDateFormat);
  const myDay = conditionalDate('date');
  let changeDate = false;
  if (oldDay < myDay) {
    changeDate = true;
  }

  //Set the next transfer date
  if (newScheduleType === transferFrequencies.once) {
    nextTransferDate = olNextTransferDate;
  }
  if (newScheduleType === transferFrequencies.weekly) {
    if (oldScheduleType === transferFrequencies.once || changeDate) {
      nextTransferDate = conditionalDate('moment', oldScheduleDateFormat, null, 1, null);
    } else {
      nextTransferDate = conditionalDate('moment', oldScheduleDateFormat, null, 0, null);
    }
  }
  if (newScheduleType === transferFrequencies.monthly) {
    if (
      oldScheduleType === transferFrequencies.weekly ||
      oldScheduleType === transferFrequencies.once ||
      changeDate
    ) {
      nextTransferDate = conditionalDate('moment', null, 1, null, oldDay);
    } else {
      nextTransferDate = conditionalDate('moment', null, null, null, oldDay);
    }
  }
  if (newScheduleType === transferFrequencies.quarterly) {
    if (
      oldScheduleType === transferFrequencies.monthly ||
      oldScheduleType === transferFrequencies.weekly ||
      oldScheduleType === transferFrequencies.once ||
      changeDate
    ) {
      nextTransferDate = conditionalDate('moment', null, 3, null, oldDay);
    } else {
      nextTransferDate = conditionalDate('moment', null, null, null, oldDay);
    }
  }
  if (newScheduleType === transferFrequencies.semiAnnually) {
    if (oldScheduleType != transferFrequencies.annually || changeDate) {
      nextTransferDate = conditionalDate('moment', null, 6, null, oldDay);
    } else {
      nextTransferDate = conditionalDate('moment', null, null, null, oldDay);
    }
  }
  if (newScheduleType === transferFrequencies.annually) {
    if (changeDate) {
      nextTransferDate = conditionalDate('moment', null, 12, null, oldDay);
    } else {
      nextTransferDate = conditionalDate('moment', null, null, null, oldDay);
    }
  }
  newScheduleBody.nextTransferDate = nextTransferDate;
  return newScheduleBody;
};

const createNewScheduleBody = (status: boolean, options: any): any => {
  const { oldScheduleBody, newScheduleBody } = options;
  let myBody = {
    ...newScheduleBody
  };

  const oldScheduleDate = conditionalDate('timestamp', oldScheduleBody.scheduleDate);
  const newScheduleDate = conditionalDate('timestamp', newScheduleBody.scheduleDate);
  const oldNextScheduleDate = conditionalDate('timestamp', oldScheduleBody.nextTransferDate);

  if (status) {
    myBody = checkingValidateValue(oldScheduleBody, newScheduleBody);
  } else if (oldScheduleDate == newScheduleDate && oldNextScheduleDate != newScheduleDate) {
    myBody.nextTransferDate = oldScheduleBody.nextTransferDate;
  }
  return myBody;
};

export const updateScheduleTransfer = async (id: string, transferBody: any, scheduleBody: any) => {
  try {
    checkDebitAccount(transferBody.debitAcc, scheduleBody.user);
    const oldScheduleBody = await ScheduleTransfers.findOne({
      _id: id
    });
    if (!oldScheduleBody) throw new InternalError();
    const status = validateTime(oldScheduleBody, scheduleBody);
    const newBody = createNewScheduleBody(status, {
      oldScheduleBody,
      newScheduleBody: scheduleBody
    });
    transferBody.beneficiaryID = new mongoose.Types.ObjectId(transferBody.beneficiaryID);
    await ScheduleTransfers.updateOne(
      { _id: id },
      {
        $set: {
          body: transferBody,
          ...newBody
        }
      }
    );
    return { message: 'Success' };
  } catch (e) {
    if (e.message === messages.authorization.timeScheduleTransfer.en) {
      throw new TimeError();
    } else if (e.message === messages.authorization.timeTransferDay.en) {
      throw new TransferDayError();
    } else {
      throw new InternalError();
    }
  }
};

export const deleteScheduleTransfers = async (user: string, id: any) => {
  try {
    await ScheduleTransfers.deleteOne({ _id: id, user });
    return { message: 'Success' };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const runScheduleTransfer = async () => {
  try {
    await getPartitionId();
    const job = new CronJob(
      '0 */10 * * * *',
      async () => {
        logger.info(`try cron job on partition ${partitionId} and enable cron ${enableCron}`);
        if (partitionId === 0 && enableCron) {
          logger.info('Job Started');
          // const startDate = new Date(moment().format('YYYY-MM-DD'));
          // const endDate = new Date(moment().add(1, 'days').format('YYYY-MM-DD'));
          const startDate = startOfDay(new Date());
          const endDate = endOfDay(new Date());
          const query = {
            nextTransferDate: {
              $gte: startDate,
              $lt: endDate
            }
          };
          const count = await ScheduleTransfers.countDocuments(query);
          logger.info(`processing ${count} scheduled transfers`);
          let recordCount = 0;
          const scheduleData = await ScheduleTransfers.find(query).cursor();
          scheduleData.on('data', async (data: any) => {
            scheduleData.pause();
            const transferData = {
              topic: kafkaTopics.scheduleTransfer,
              data
            };
            const correlationId = randomUUID();
            await produce(transferData, {}, false, correlationId);
            recordCount++;
            if (recordCount === count) scheduleData.close();
            scheduleData.resume();
          });
          scheduleData.on('close', () => logger.info('Streaming data is closed'));
        }
      },
      null,
      true,
      'Africa/Cairo'
    );
    job.start();
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const getPartitionId = async () => {
  try {
    const options: { consumer: Consumer; interval: number } = await getHeartbeatTime();
    const consumerAssignments: { [key: string]: number } = {};
    options.consumer.on(options.consumer.events.GROUP_JOIN, (data: any) => {
      if (Object.keys(data.payload.memberAssignment)?.length) {
        Object.keys(data.payload.memberAssignment).forEach((memberId) => {
          consumerAssignments[memberId] = Math.min(...data.payload.memberAssignment[memberId]);
          partitionId = consumerAssignments[kafkaTopics.accountResponse];
        });
      } else partitionId = null;
    });
  } catch (e) {
    partitionId = null;
  }
};

export const executeScheduleTransfer = async (message: any, correlationId: any) => {
  if (!message.value) throw new InternalError();
  const kafkaData = JSON.parse(message.value.toString());
  let nextTransferDate = kafkaData.data.nextTransferDate;
  try {
    const authorization = await adminToken();
    const {
      data: { currentTransferCount, totalTransferCount }
    } = kafkaData;

    if (totalTransferCount && currentTransferCount >= totalTransferCount) {
      throw new ForbiddenError(messages.accounts.scheduleTransferCount.en);
    }
    await transferByBeneficiaryId(
      kafkaData.data.body.beneficiaryID,
      {
        debitAcc: kafkaData.data.body.debitAcc,
        amount: kafkaData.data.body.amount,
        currency: kafkaData.data.body.currency,
        description: kafkaData.data.body.description,
        chargeType: kafkaData.data.body.chargeType,
        otp: ''
      },
      true,
      false,
      correlationId,
      kafkaData.data.user,
      kafkaData.data.userId,
      `Bearer ${authorization}`,
      kafkaData.data.platform,
      {}
    );
    logger.info(
      `Scheduled transfer success for user ${kafkaData.data.user} / ${kafkaData.data.userId} schedule transfer ${kafkaData.data._id}`,
      { correlationId }
    );
  } catch (e) {
    updateStatistics(
      '',
      '',
      {
        type: 'scheduledTransfer',
        error: e.message,
        cif: kafkaData.data.user,
        microservice: 'account',
        user: kafkaData.data.userId,
        date: new Date(),
        platform: kafkaData.data.platform
      },
      correlationId
    );
    logger.error(
      `Error in scheduled transfer for user ${kafkaData.data.user} / ${kafkaData.data.userId} schedule transfer ${kafkaData.data._id}`,
      { correlationId }
    );
    logger.error(e.message, { correlationId });
  }
  switch (kafkaData.data.transferFrequency) {
    case transferFrequencies.weekly:
      nextTransferDate = moment().add(7, 'days').format();
      break;
    case transferFrequencies.monthly:
      nextTransferDate = moment().add(1, 'months').format();
      break;
    case transferFrequencies.quarterly:
      nextTransferDate = moment().add(3, 'months').format();
      break;
    case transferFrequencies.semiAnnually:
      nextTransferDate = moment().add(6, 'months').format();
      break;
    case transferFrequencies.annually:
      nextTransferDate = moment().add(12, 'months').format();
      break;
    case transferFrequencies.once:
      nextTransferDate = null;
      break;
  }

  return ScheduleTransfers.updateOne(
    { _id: kafkaData.data._id },
    { $set: { nextTransferDate: nextTransferDate }, $inc: { currentTransferCount: 1 } }
  );
};

export const getUserTransfersHistory = async (
  cif: string,
  authorization: string,
  count = 10,
  tracingHeaders: any
) => {
  try {
    const Type = 'ACMF';
    const data = {
      route: `customer/payments/excludetype/${Type}/${cif}/${count}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    return result[0];
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const getUserTransfersHistoryByDuration = async (
  cif: string,
  authorization: string,
  from: string,
  to: string,
  tracingHeaders: any
) => {
  try {
    const data = {
      route: `customer/payments/${cif}/${from}/${to}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    if (result && result.length > 0) {
      return result[0].filter((item: any) => {
        return item.Type !== 'ACMF';
      });
    }
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const getUserTransfersHistoryByType = async (
  cif: string,
  type: string,
  authorization: string,
  count: number,
  tracingHeaders: any
) => {
  try {
    const data = {
      route: `customer/payments/type/${type}/${cif}/${count}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    return result[0];
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const getUserTransfersHistoryByBeneficiary = async (
  cif: string,
  benificiaryAccount: string,
  count: number,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const data = {
      route: `customer/payments/credit/${cif}/${benificiaryAccount}/${count}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    return result[0];
  } catch (e) {
    logger.error(e.message);
    throw new NotFoundError();
  }
};

export const getAccountByMobileNumber = async (
  authorization: string,
  mobileNumber: string,
  tracingHeaders: any
) => {
  try {
    const data = {
      topic: kafkaTopics.customerResponse,
      function: 'getCustomerMobileTransfer',
      consumerType: 'mobileTransfer',
      body: {
        mobileNumber
      },
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    return result.accountNumber;
  } catch (e) {
    logger.error(e.message);
    throw new NotFoundError();
  }
};

export const requestCd = async (
  body: any,
  cif: string,
  user: string,
  phoneNumber: string,
  platform: string
) => {
  try {
    const data = {
      topic: kafkaTopics.smtp,
      function: 'sendEmailService',
      body,
      cif,
      template: 'cd'
    };
    await produce(data, null, false);

    updateStatistics('', '', {
      cif,
      user,
      type: 'request',
      subType: 'requestCD',
      date: new Date(),
      mobile: phoneNumber,
      microservice: 'account',
      status: status.success,
      platform: platform
    }).catch((e) => e.message);
    return { message: 'Success' };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

export const requestPayrollAdvance = async (
  cif: string,
  user: string,
  phoneNumber: string,
  platform: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const isAllowedToRequest = await canRequestPayroll(cif, authorization, tracingHeaders);
    if (isAllowedToRequest) {
      const data = {
        topic: kafkaTopics.smtp,
        function: 'sendEmailService',
        cif,
        template: 'requestPayrollAdvance'
      };
      await produce(data, tracingHeaders, false);
      updateStatistics('', '', {
        cif,
        user,
        mobile: phoneNumber,
        type: 'request',
        subType: 'requestPayrollAdvance',
        date: new Date(),
        microservice: 'account',
        platform
      }).catch((e) => e.message);

      await updatePayrollRequestDate(cif).catch((e) => e.message);
      return { message: 'Success' };
    } else {
      throw new InternalError();
    }
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

const updatePayrollRequestDate = async (cif: string) => {
  const payrollRequest = await PayrollRequests.findOne({ cif });

  if (payrollRequest) await PayrollRequests.updateOne({ cif }, { lastRequestDate: new Date() });
  else {
    const newPayrollReq = new PayrollRequests({
      cif,
      lastRequestDate: new Date()
    });
    await newPayrollReq.save();
  }
};

const notPayrollAlreadyRequested = async (cif: string) => {
  try {
    const payrollRequest = await PayrollRequests.findOne({ cif });
    if (!payrollRequest) return true;
    else {
      if (payrollRequest.lastRequestDate.getMonth() === new Date().getMonth()) {
        return false;
      }
      return true;
    }
  } catch (error) {
    logger.error(error.message);
    throw new InternalError();
  }
};

export const canRequestPayroll = async (
  cif: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const isAllowedToRequest = await notPayrollAlreadyRequested(cif);

    if (isAllowedToRequest) {
      const data = {
        topic: kafkaTopics.t24Request,
        route: `customer/${cif}`,
        authorization
      };
      const producerData = await produce(data, tracingHeaders);
      const kafkaData = await prepareKafkaResponse(producerData);
      if (kafkaData && kafkaData[0].AccPurpose.includes('Payroll')) {
        return true;
      } else return false;
    }
    return false;
  } catch (error) {
    logger.error(error.message);
    throw new InternalError();
  }
};

export const canRequestUAEAccount = async (
  userInfo: { cif: string; name: string; phoneNumber: string },
  tracingHeaders: any
) => {
  try {
    const data = {
      topic: kafkaTopics.smtp,
      function: 'sendEmailService',
      cif: userInfo.cif,
      template: 'requestUAEAccount'
    };

    await produce(data, tracingHeaders, false);
    updateStatistics('', '', {
      cif: userInfo.cif,
      user: userInfo.name,
      mobile: userInfo.phoneNumber,
      type: 'request',
      subType: 'requestUAEAccount',
      date: new Date(),
      microservice: 'account'
    }).catch((e) => e.message);
    return { message: 'Success' };
  } catch (error) {
    logger.error(error.message);
    throw new InternalError();
  }
};

export const validateDebitAccount = async (debitAcc: string) => {
  const draftAccRegex = /^[0-9]{12}15[0-9][1-9]$/;
  if (draftAccRegex.test(debitAcc)) throw new ForbiddenError(messages.accounts.overDraftAccount.en);
};

export const updateCustomerTransfers = async (
  cif: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const data = {
      topic: kafkaTopics.customerResponse,
      function: 'updateCustomerTransfers',
      consumerType: 'customerSettings',
      body: {
        cif
      },
      authorization: `Bearer ${authorization}`
    };
    await produce(data, tracingHeaders, false);
  } catch (error) {
    logger.error(error.message);
  }
};

export const getSchedule = async () => {
  try {
    const scheduleTransfers = await ScheduleTransfers.find();
    return scheduleTransfers;
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

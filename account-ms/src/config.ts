// Mapper for environment variables

export const environment = process.env.NODE_ENV;
export const port = process.env.PORT;

export const corsUrl = process.env.CORS_URL;
export const enableCron = process.env.ENABLE_CRON !== 'false';

export const kafkaOptions = {
  clientId: process.env.KAFKA_CLIENT_ID || '',
  hosts: process.env.KAFKA_HOSTS?.split(',') || [],
  connectionTimeout: parseInt(process.env.KAFKA_CONNECTION_TIMEOUT || '3000'),
  requestTimeout: parseInt(process.env.KAFKA_REQUEST_TIMEOUT || '25000'),
  initialRetryTime: parseInt(process.env.KAFKA_INITIAL_RETRY_TIME || '1'),
  retries: parseInt(process.env.KAFKA_RETRIES || '1'),
  producerPolicy: {
    allowAutoTopicCreation: true
  },
  consumerPolicy: {
    groupId: process.env.CONSUMER_GROUP_ID || 'account-ms',
    maxWaitTimeInMs: Number(process.env.CONSUMER_MAX_WAIT_TIME || '100'),
    scheduleGroupId: process.env.SCHEDULE_CONSUMER_GROUP_ID || 'schedule-account-ms',
    allowAutoTopicCreation: true,
    sessionTimeout: Number(process.env.CONSUMER_SESSION_TIMEOUT || '30000'),
    heartbeatInterval: Number(process.env.CONSUMER_HEART_BEAT || '3000'),
    retry: {
      retries: parseInt(process.env.KAFKA_RETRIES || '1')
    }
  },
  numberOfPartitions: Number(process.env.KAFKA_PARTITION_NUMBER || '3')
};

export const kafkaTopics = {
  t24Request: 't24Request',
  accountResponse: 'accountResponse',
  beneficiaryRequest: 'beneficiaryRequest',
  validateTopic: 'validateTopic',
  euronetRequest: 'euronetRequest',
  statistics: 'statistics',
  kibana: 'kibanastatistics',
  smtp: 'smtp',
  scheduleTransfer: 'scheduleTransfer',
  customerResponse: 'customerResponse'
};

export const status = {
  success: 'success',
  fail: 'fail',
  timeout: 'timeout'
};

export const corporateRange = {
  min: 2000,
  max: 2999
};

export const promiseTimeout = parseInt(process.env.PROMISE_TIMEOUT || '20000');

export const transOptions = {
  dateFormat: 'YYYYMMDD',
  duration: '3'
};

export const defaultCurrency = 'EGP';

export const currencies: { [key: string]: string } = {
  EG: 'EGP',
  AE: 'AED'
};

export const ismocking = process.env.IS_MOCKING === '1' ? true : false;
export const AEError = process.env.AE_ERROR === '1' ? true : false;
export const EGError = process.env.EG_ERROR === '1' ? true : false;

export const whitelistAccounts = [
  '1001',
  '1002',
  '1003',
  '1004',
  '1005',
  '6001',
  '6002',
  '6003',
  '6004',
  '6005'
];

export const whitelistRange = [
  {
    min: 1001,
    max: 1005
  },
  {
    min: 1501,
    max: 1526
  },
  {
    min: 1550,
    max: 1680
  },
  {
    min: 6001,
    max: 6005
  }
];

export const transferOpts = {
  minAccountsNum: 2,
  transCode: process.env.OWN_ACCOUNT_TRANSFER_CODE || 'ACIB',
  fxTransCode: process.env.FX_OWN_ACCOUNT_TRANSFER_CODE || 'AC',
  internalTransCode: process.env.INTERNAL_ACCOUNT_TRANSFER_CODE || 'ACIB',
  countryCode: 'EG',
  currency: 'EGP',
  chargeType: 'SHA',
  description: 'Own account transfer',
  reference: 'Mobile own TRX',
  webReference: 'Online Bnk TRX',
  valueDate: 0,
  defaultSwift: 'ARAIEGCXXXX'
};

export const statistics = {
  ownTransferSuccess: 'ownTransferSuccess',
  ownTransferFail: 'ownTransferFail',
  timeoutTransfers: 'timeoutTransfers',
  internationalTransferSuccess: 'internationalTransferSuccess',
  internationalTransferFail: 'internationalTransferFail',
  internalTransferSuccess: 'internalTransferSuccess',
  internalTransferFail: 'internalTransferFail',
  externalTransferSuccess: 'externalTransferSuccess',
  externalTransferFail: 'externalTransferFail',
  creditCardTransferSuccess: 'creditCardTransferSuccess',
  creditCardTransferFail: 'creditCardTransferFail'
};

export const transactionCodes = {
  ownAccountTransfer: {
    web: 'ACOI'
  },
  fxTransfer: {
    web: 'SPT'
  },
  internalTransfer: {
    web: 'ACBI'
  },
  externalTransfer: {
    web: 'ACHO'
  },
  fawryPayment: {
    web: 'ACIF'
  },
  cardPayment: {
    web: 'ACCI'
  },
  anyAaibCreditCard: {
    web: 'ACCA'
  }
};

export const columns = [
  {
    id: 'Description',
    header: 'Description',
    headerPadding: [10],
    padding: [4],
    width: 100
  },
  {
    id: 'Value Date',
    header: 'Value Date',
    headerPadding: [10],
    padding: [4],
    width: 100
  },
  {
    id: 'Deposits',
    header: 'Deposits',
    headerPadding: [10],
    padding: [4],
    width: 100
  },
  {
    id: 'Balance',
    header: 'Balance',
    headerPadding: [10],
    padding: [4],
    width: 100
  },
  {
    id: 'Currency',
    header: 'Currency',
    headerPadding: [10],
    padding: [4],
    width: 100
  },
  {
    id: 'Timestamp',
    header: 'Timestamp',
    headerPadding: [10],
    padding: [4],
    width: 100
  }
];

export const subAccounts = {
  types: { saving: 'SAVING', current: 'CURRENT' },
  subTypes: { call: 'CALL', demand: 'DEMAND', golden: 'GOLDEN' },
  frequencies: { monthly: 'M', quarterly: 'Q', semiAnually: 'X', annually: 'Y' },
  categories: {
    currentCall: 'CurrAcc Call',
    currentDemand: 'CurrAcc Demand',
    savingMonthly: 'SAVING AC MONTH',
    savingQuarterly: 'SAVING AC QUART',
    savingSemiAnnually: 'SAVING AC SEM A',
    savingAnnually: 'SAVING AC ANUAL'
  },
  currencies: [
    {
      ID: 'EGP',
      Code: '',
      EnglishName: 'Egyptian Pound',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'USD',
      Code: '',
      EnglishName: 'United States Dollar',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'EUR',
      Code: '',
      EnglishName: 'Euro',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'AED',
      Code: '',
      EnglishName: 'United Arab Emirates Dirham',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'CAD',
      Code: '',
      EnglishName: 'Canadian Dollar',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'SAR',
      Code: '',
      EnglishName: 'Saudi Riyal',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'KWD',
      Code: '',
      EnglishName: 'Kuwaiti Dinar',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'CHF',
      Code: '',
      EnglishName: 'Swiss Franc',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'DKK',
      Code: '',
      EnglishName: 'Danish Krone',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'JPY',
      Code: '',
      EnglishName: 'Japanese Yen',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'SEK',
      Code: '',
      EnglishName: 'Swedish Krona',
      ArabicName: '',
      Value: ''
    },
    {
      ID: 'GBP',
      Code: '',
      EnglishName: 'Pound sterling',
      ArabicName: '',
      Value: ''
    }
  ]
};

export const transferFrequencies = {
  once: 'ONCE',
  weekly: 'WEEKLY',
  monthly: 'MONTHLY',
  quarterly: 'QUARTERLY',
  semiAnnually: 'SEMI-ANNUALLY',
  annually: 'ANNUALLY'
};

export const mongoConfig = {
  url: process.env.DB_URL || ''
};

export const adminCredentials = {
  username: process.env.ADMIN_USER,
  password: process.env.ADMIN_PASSWORD
};

export const t24Url = process.env.T24_BASE_URL;

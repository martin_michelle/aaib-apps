import { CompressionTypes, Consumer, Kafka, KafkaMessage, Producer } from 'kafkajs';
import { kafkaOptions, kafkaTopics, promiseTimeout } from '../config';
import { EventEmitter } from 'events';
import { logger } from '../core/Logger';
import correlation from 'express-correlation-id';
import { promiseWithTimeout } from '../helpers/asyncHandler';
import { ClientKafka } from '../libs/kafka-partition/client-kafka';
import messages from '../messages';
import { executeScheduleTransfer, runScheduleTransfer } from '../services/accounts';
import {
  BlockedTokenError,
  ForbiddenError,
  InternalError,
  InvalidOtpError
} from '../core/ApiError';

let producer: Producer;
let consumer: Consumer;
let partitionId: number;
let client: ClientKafka;
let eventEmitter: EventEmitter;
let interval: number;

export const connector = () => {
  return new Kafka({
    clientId: kafkaOptions.clientId,
    brokers: kafkaOptions.hosts,
    connectionTimeout: kafkaOptions.connectionTimeout,
    requestTimeout: kafkaOptions.requestTimeout,
    retry: {
      initialRetryTime: kafkaOptions.initialRetryTime,
      retries: kafkaOptions.retries
    }
  });
};

export const createKafkaClient = () => {
  if (client) return client;
  return new ClientKafka({
    consumer: {
      groupId: kafkaOptions.consumerPolicy.groupId,
      maxWaitTimeInMs: kafkaOptions.consumerPolicy.maxWaitTimeInMs
    },
    client: {
      clientId: kafkaOptions.clientId,
      brokers: kafkaOptions.hosts,
      connectionTimeout: kafkaOptions.connectionTimeout,
      requestTimeout: kafkaOptions.requestTimeout,
      retry: {
        initialRetryTime: kafkaOptions.initialRetryTime,
        retries: kafkaOptions.retries
      }
    }
  });
};

export const createProducer = async () => {
  if (producer) return producer;
  const kafka = connector();
  producer = kafka.producer(kafkaOptions.producerPolicy);
  await producer.connect();
  return producer;
};

export const createConsumer = async () => {
  try {
    client = createKafkaClient();
    consumer = await client.connect();
    await client.createKafkaTopic(kafkaTopics.accountResponse, kafkaOptions.numberOfPartitions);
    await consumer.connect();
    await consumer.subscribe({ topic: kafkaTopics.accountResponse });
    const consumerAssignments: { [key: string]: number } = {};
    consumer.on(consumer.events.GROUP_JOIN, (data: any) => {
      Object.keys(data.payload.memberAssignment).forEach((memberId) => {
        consumerAssignments[memberId] = Math.min(...data.payload.memberAssignment[memberId]);
        partitionId = consumerAssignments[kafkaTopics.accountResponse];
      });
    });
    consumer.on(consumer.events.HEARTBEAT, ({ timestamp }) => {
      interval = timestamp;
    });
    await consumer.run({
      eachMessage: async ({ message }) => {
        if (!message.headers?.correlationId) {
          logger.error('Missing correlationId');
        } else {
          const eventEmitter = createEventEmitter();
          eventEmitter.emit(message.headers.correlationId.toString(), message);
        }
      }
    });
    return consumer;
  } catch (e) {
    logger.debug(e.message);
  }
};

export const createEventEmitter = () => {
  if (eventEmitter) return eventEmitter;
  eventEmitter = new EventEmitter();
  return eventEmitter;
};

export const produce = async (
  data: any,
  tracingHeaders: any,
  waitForResponse = true,
  requestCorrelation = ''
) => {
  if (!producer) producer = await createProducer();
  const correlationId = requestCorrelation || correlation.getId();
  const producerOptions = {
    topic: data.topic || kafkaTopics.t24Request,
    compression: CompressionTypes.GZIP,
    acks: 1,
    messages: [
      {
        value: JSON.stringify(data),
        headers: {
          correlationId,
          otp: data.otp || '',
          partitionReply: partitionId.toString(),
          replyTopic: kafkaTopics.accountResponse,
          authorization: data.authorization || '',
          params: JSON.stringify(data.params || {}),
          route: data.apiRoute || '',
          method: data.apiMethod || '',
          function: data.function || '',
          consumerType: data.consumerType || '',
          ...tracingHeaders
        }
      }
    ]
  };
  await producer.send(producerOptions);
  if (waitForResponse) {
    const eventEmitter = createEventEmitter();
    const response = () => {
      return new Promise((resolve, reject) => {
        eventEmitter.once(correlationId || '', async (value) => {
          try {
            resolve(value);
          } catch (e) {
            logger.error(e.message);
            reject(e);
          }
        });
      });
    };
    return await promiseWithTimeout(promiseTimeout, response, messages.accounts.timeout.en);
  }
};

export const prepareKafkaResponse = async (message: KafkaMessage) => {
  if (!message.value) throw new InternalError(messages.accounts.missingValue.en);
  const data = JSON.parse(message.value.toString());
  if (!data.error) {
    return data.message.Data || data.message.data || data.message;
  } else {
    logger.debug(data.message);
    if (data.message === messages.authorization.otpBlocked.en) {
      throw new BlockedTokenError();
    } else if (data.message === messages.accounts.forbidden.en) {
      throw new ForbiddenError();
    } else if (data.message === messages.authorization.invalidOtp.en) {
      throw new InvalidOtpError();
    } else {
      throw new InternalError(messages.accounts.internalError.en);
    }
  }
};

export const disconnect = async () => {
  client = createKafkaClient();
  return client.close();
};

export const getHeartbeatTime = async () => {
  return {
    consumer,
    interval,
    partitionId: partitionId === Infinity ? null : partitionId
  };
};

export const createScheduleConsumer = async () => {
  try {
    const scheduleClient = new ClientKafka({
      consumer: {
        groupId: kafkaOptions.consumerPolicy.scheduleGroupId
      },
      client: {
        clientId: kafkaOptions.clientId,
        brokers: kafkaOptions.hosts,
        connectionTimeout: kafkaOptions.connectionTimeout,
        requestTimeout: kafkaOptions.requestTimeout,
        retry: {
          initialRetryTime: kafkaOptions.initialRetryTime,
          retries: kafkaOptions.retries
        }
      }
    });
    const scheduleConsumer = await scheduleClient.connect();
    await scheduleClient.createKafkaTopic(
      kafkaTopics.scheduleTransfer,
      kafkaOptions.numberOfPartitions
    );
    await scheduleConsumer.connect();
    await scheduleConsumer.subscribe({ topic: kafkaTopics.scheduleTransfer });
    runScheduleTransfer().catch();
    await scheduleConsumer.run({
      eachMessage: async ({ message }) => {
        if (!message.value) throw new InternalError();
        if (!message.headers?.correlationId) {
          logger.error('Missing correlationId');
        } else {
          await executeScheduleTransfer(message, message.headers.correlationId.toString());
        }
      }
    });
    return consumer;
  } catch (e) {
    logger.debug(e.message);
  }
};

import express from 'express';
import { SuccessResponse } from '../../core/ApiResponse';
import asyncHandler from '../../helpers/asyncHandler';
import * as service from '../../services/accounts';
import * as loansService from '../../services/loans';
import authenticate from '../../helpers/authenticate';
import { validate } from 'express-validation';
import { ProtectedRequest } from 'app-request';
import schema from './schema';
import tracingHeaders from '../../helpers/tracingHeaders';

const router = express.Router();

router.get(
  '/',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getUserAccounts(
      req.user,
      Number(req.user.sector || '0'),
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/currency',
  asyncHandler(async (req: ProtectedRequest, res) => {
    const currency = String(req.query.currency || 'EGP');
    const result = await service.getConversionRates(
      currency.toUpperCase(),
      true,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/currencyConversionRate',
  authenticate(),
  validate(schema.currencyConversionRate),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getCurrencyConversionRate(
      req.user,
      String(req.query.currency),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/transactions/:account',
  authenticate(),
  validate(schema.accountTransactions),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getAccountTransactions(
      parseInt(req.params.account),
      {
        fromDate: String(req.query.fromDate || ''),
        toDate: String(req.query.toDate || ''),
        search: String(req.query.search || '').toLowerCase(),
        lastTransaction: String(req.query.lastTransaction || ''),
        limit: Number(req.query.limit || '0')
      },
      req.headers.authorization || '',
      tracingHeaders(req),
      req.query.country || 'EG'
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/details/:account',
  authenticate(),
  validate(schema.accountDetails),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getAccountDetails(
      req.params.account,
      req.query.country as string,
      req.headers.authorization || '',
      req.user.cif,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/transactions/acc2acc',
  authenticate(),
  validate(schema.transfer),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.ownAccountsTransfer(
      req.body,
      req.user.cif,
      req.user.id,
      req.headers.authorization || '',
      String(req.headers.platform),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/internal/acc2acc',
  authenticate(),
  validate(schema.internalTransfer),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.allTransfers(
      req.body,
      '',
      req.user.cif,
      req.user.id,
      req.headers.authorization || '',
      String(req.headers.platform),
      tracingHeaders(req),
      req.body.isMobileNumber
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/internal/acc2acc/:id',
  authenticate(),
  validate(schema.transferByBeneficiaryId),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.transferByBeneficiaryId(
      req.params.id,
      req.body,
      false,
      false,
      '',
      req.user.cif,
      req.user.id,
      req.headers.authorization || '',
      String(req.headers.platform),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/multiple/acc2acc',
  authenticate(),
  validate(schema.multipleTransfers),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.multipletransfers(
      req.body,
      req.user.cif,
      req.user.id,
      req.headers.authorization || '',
      String(req.headers.platform),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/loans',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await loansService.getLoans(
      req.user,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/loan/:id',
  authenticate(),
  validate(schema.getLoanById),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await loansService.getLoanPastDues(
      req.params.id,
      req.query.country as string,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/repayment/:id',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await loansService.getScheduleLoans(
      req.params.id,
      req.query.country as string,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/request/loan',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await loansService.requestLoan(
      req.body,
      req.user.cif,
      req.user.id,
      req.user.phoneNumber,
      String(req.headers.platform)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/request/cd',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.requestCd(
      req.body,
      req.user.cif,
      req.user.id,
      req.user.phoneNumber,
      String(req.headers.platform)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/usage/:id',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getAccountUsage(
      req.params.id,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/statement',
  validate(schema.sendStatement),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.sendStatement(
      req.user.cif,
      String(req.query.type),
      Number(req.query.account),
      String(req.headers.authorization || ''),
      req.user.email,
      {
        fromDate: String(req.query.fromDate || ''),
        toDate: String(req.query.toDate || '')
      },
      String(req.headers.platform),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/mailbox',
  validate(schema.sendMessage),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    req.body.phoneNumber = req.user.phoneNumber;
    const result = await service.mailbox(
      req.body,
      req.user.cif,
      req.user.id,
      String(req.headers.platform)
    );
    new SuccessResponse('Success', result).send(res);
  })
);
router.get(
  '/mailboxCategory',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.mailboxCategory();
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/generateTransferReceipt',
  validate(schema.transferReceipt),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const language = req.headers['accept-language'] || 'en';
    const result = await service.generateTransferReceipt(
      req.body,
      req.user.cif,
      language,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/open/sub',
  validate(schema.openSubAccount),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.openSubAccount(
      req.body,
      req.user.cif,
      req.user.id,
      String(req.headers.platform),
      String(req.headers.authorization || ''),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/schedule/transfer/list',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getScheduleTransfers(req.user.cif);
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/schedule/transfer',
  validate(schema.scheduleTransfer),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.scheduleTransfer(req.body, {
      user: req.user.cif,
      userId: req.user.id,
      scheduleDate: req.body.scheduleDate,
      nextTransferDate: req.body.scheduleDate,
      transferFrequency: req.body.transferFrequency,
      totalTransferCount: req.body.totalTransferCount,
      platform: req.headers.platform
    });
    new SuccessResponse('Success', result).send(res);
  })
);

router.put(
  '/schedule/transfer/:id',
  validate(schema.editScheduleTransfer),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.updateScheduleTransfer(req.params.id, req.body, {
      user: req.user.cif,
      userId: req.user.id,
      transferFrequency: req.body.transferFrequency,
      scheduleDate: req.body.scheduleDate,
      nextTransferDate: req.body.scheduleDate,
      totalTransferCount: req.body.totalTransferCount || 0
    });
    new SuccessResponse('Success', result).send(res);
  })
);

router.delete(
  '/schedule/transfer/:id',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.deleteScheduleTransfers(req.user.cif, req.params.id);
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/list/currency',
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getCurrencyList(tracingHeaders(req));
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/subAcc/currency',
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getSubAccountsCurrencies();
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/currency/rates/fxcalculator',
  validate(schema.fxCalculator),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.fxCalculator(
      String(req.query.srcCur),
      String(req.query.dstCur),
      Number(req.query.amount),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/customer/payments',
  validate(schema.userTransfersHistory),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getUserTransfersHistory(
      req.user.cif,
      req.headers.authorization || '',
      Number(req.query.count),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/customer/payments/:from/:to',
  validate(schema.userTransfersHistoryByDuration),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getUserTransfersHistoryByDuration(
      req.user.cif,
      req.headers.authorization || '',
      req.params.from,
      req.params.to,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/customer/payments/type',
  validate(schema.userTransfersHistoryByType),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getUserTransfersHistoryByType(
      req.user.cif,
      String(req.query.type),
      req.headers.authorization || '',
      Number(req.query.count),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/customer/payments/beneficiary',
  validate(schema.userTransfersHistoryByBeneficiary),
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getUserTransfersHistoryByBeneficiary(
      req.user.cif,
      String(req.query.benificiaryAccount),
      Number(req.query.count),
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/requestPayrollAdvance',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.requestPayrollAdvance(
      req.user.cif,
      req.user.id,
      req.user.phoneNumber,
      String(req.headers.platform),
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);
router.get(
  '/canRequestPayroll',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.canRequestPayroll(
      req.user.cif,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/canRequestUAEAccount',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.canRequestUAEAccount(
      {
        cif: req.user.cif,
        name: req.user.id,
        phoneNumber: req.user.phoneNumber
      },
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/schedule',
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getSchedule();
    new SuccessResponse('Success', result).send(res);
  })
);

export default router;

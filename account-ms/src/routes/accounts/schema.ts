import Joi from 'joi';
import { subAccounts, transferFrequencies } from '../../config';

export default {
  accountTransactions: {
    params: Joi.object({
      account: Joi.string().required()
    }),
    query: Joi.object({
      fromDate: Joi.string().optional(),
      toDate: Joi.string().optional(),
      search: Joi.string().optional(),
      lastTransaction: Joi.string().optional(),
      limit: Joi.number().optional().min(1).max(100),
      country: Joi.string().optional()
    })
  },
  accountDetails: {
    params: Joi.object({
      account: Joi.string().required()
    }),
    query: Joi.object({
      country: Joi.string().optional()
    })
  },
  transfer: {
    body: Joi.object({
      debitAcc: Joi.string().required(),
      creditAcc: Joi.string().required(),
      amount: Joi.number().required().min(0),
      description: Joi.string().optional().allow('')
    })
  },
  internalTransfer: {
    body: Joi.object({
      debitAcc: Joi.string().required(),
      creditAcc: Joi.string().required(),
      amount: Joi.number().required().min(0),
      otp: Joi.string().required().length(6),
      description: Joi.string().optional().allow(''),
      swift: Joi.string().optional().allow(''),
      currency: Joi.string().optional().allow(''),
      beneficiaryName: Joi.string().optional().allow(''),
      chargeType: Joi.string().optional().allow(''),
      isMobileNumber: Joi.boolean().optional().allow(''),
      country: Joi.string().optional().when('currency', {
        not: 'EGP',
        then: Joi.string().required()
      })
    })
  },
  transferByBeneficiaryId: {
    params: Joi.object({
      id: Joi.string().required()
    }),
    body: Joi.object({
      amount: Joi.number().required().min(0),
      debitAcc: Joi.string().required(),
      otp: Joi.string().optional().length(6),
      chargeType: Joi.string().optional().allow('').valid('SHA', 'BEN', 'OUR'),
      description: Joi.string().optional().allow(''),
      currency: Joi.string().optional().allow('')
    })
  },

  multipleTransfers: {
    body: Joi.object({
      transfers: Joi.array()
        .items(
          Joi.object({
            id: Joi.string().required(),
            accountData: Joi.object({
              amount: Joi.number().required().min(0),
              debitAcc: Joi.string().required(),
              chargeType: Joi.string().optional().allow('').valid('SHA', 'BEN', 'OUR'),
              description: Joi.string().optional().allow(''),
              currency: Joi.string().optional().allow('')
            })
          })
        )
        .min(1),
      otp: Joi.string().required()
    })
  },

  scheduleTransfer: {
    body: Joi.object({
      debitAcc: Joi.string().required(),
      amount: Joi.number().required().min(0),
      description: Joi.string().required(),
      currency: Joi.string().optional().allow(''),
      chargeType: Joi.string().optional().allow('').valid('SHA', 'BEN', 'OUR'),
      beneficiaryID: Joi.string().required(),
      scheduleDate: Joi.date()
        .required()
        .greater(new Date(new Date().setHours(0, 0, 0, 0))),
      transferFrequency: Joi.string()
        .required()
        .valid(
          transferFrequencies.once,
          transferFrequencies.weekly,
          transferFrequencies.monthly,
          transferFrequencies.quarterly,
          transferFrequencies.semiAnnually,
          transferFrequencies.annually
        ),
      totalTransferCount: Joi.number().optional().min(0),
      otp: Joi.string().required().length(6)
    })
  },
  editScheduleTransfer: {
    body: Joi.object({
      debitAcc: Joi.string().required(),
      amount: Joi.number().required().min(0),
      description: Joi.string().required(),
      currency: Joi.string().optional().allow(''),
      chargeType: Joi.string().optional().allow('').valid('SHA', 'BEN', 'OUR'),
      beneficiaryID: Joi.string().required(),
      scheduleDate: Joi.date().required(),
      transferFrequency: Joi.string()
        .required()
        .valid(
          transferFrequencies.once,
          transferFrequencies.weekly,
          transferFrequencies.monthly,
          transferFrequencies.quarterly,
          transferFrequencies.semiAnnually,
          transferFrequencies.annually
        ),
      totalTransferCount: Joi.number().optional().min(0),
      otp: Joi.string().required().length(6)
    })
  },
  getLoanById: {
    params: Joi.object({
      id: Joi.string().required()
    })
  },
  sendStatement: {
    query: Joi.object({
      type: Joi.string().required().valid('excel', 'pdf'),
      account: Joi.string().required(),
      fromDate: Joi.string().optional(),
      toDate: Joi.string().optional()
    })
  },

  sendMessage: {
    body: Joi.object({
      message: Joi.string()
        .regex(/^[^<>#*=+^}[\]]+$/)
        .required(),
      category: Joi.string().required()
    })
  },
  transferReceipt: {
    body: Joi.object({
      transactionDate: Joi.string().required(),
      transactionId: Joi.string().required(),
      accountName: Joi.string().required(),
      accountNumber: Joi.string().required(),
      bank: Joi.string().required(),
      fromAccount: Joi.string().required(),
      transferCurrency: Joi.string().required(),
      transferAmount: Joi.string().required(),
      transferReason: Joi.string().required()
    })
  },
  openSubAccount: {
    body: Joi.object({
      Type: Joi.string().valid(subAccounts.types.current, subAccounts.types.saving).required(),

      SubType: Joi.when('Type', {
        is: 'CURRENT',
        then: Joi.string().valid(subAccounts.subTypes.call, subAccounts.subTypes.demand).required(),
        otherwise: Joi.string().valid('').optional()
      }).when('Type', {
        is: 'SAVING',
        then: Joi.string().valid(subAccounts.subTypes.golden).required(),
        otherwise: Joi.string().valid('').optional()
      }),

      Currency: Joi.string().when('Type', {
        is: 'SAVING',
        then: Joi.when('SubType', {
          is: 'GOLDEN',
          then: Joi.string().valid('EGP'),
          otherwise: Joi.string().valid('EUR', 'EGP', 'USD', 'GBP').required()
        }),
        otherwise: Joi.string().required()
      }),

      Frequency: Joi.when('Type', {
        is: 'SAVING',
        then: Joi.string()
          .valid(
            subAccounts.frequencies.monthly,
            subAccounts.frequencies.quarterly,
            subAccounts.frequencies.semiAnually,
            subAccounts.frequencies.annually
          )
          .required(),
        otherwise: Joi.string().valid('').optional()
      })
    })
  },
  fxCalculator: {
    query: Joi.object({
      srcCur: Joi.string().required(),
      dstCur: Joi.string().required(),
      amount: Joi.number().required()
    })
  },
  currencyConversionRate: {
    query: Joi.object({
      currency: Joi.string().required()
    })
  },
  userTransfersHistory: {
    query: Joi.object({
      count: Joi.number().positive().min(0).optional()
    })
  },
  userTransfersHistoryByDuration: {
    params: Joi.object({
      from: Joi.string().required(),
      to: Joi.string().required()
    })
  },
  userTransfersHistoryByType: {
    query: Joi.object({
      count: Joi.number().positive().min(0).optional(),
      type: Joi.string().required()
    })
  },
  userTransfersHistoryByBeneficiary: {
    query: Joi.object({
      benificiaryAccount: Joi.string().required(),
      count: Joi.number().positive().min(0).required()
    })
  }
};

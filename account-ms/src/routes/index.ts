import express from 'express';
import accounts from './accounts/accounts';

const router = express.Router();

router.use('/accounts', accounts);

export default router;

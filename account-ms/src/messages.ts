export default {
  accounts: {
    notRegistered: {
      en: 'Account not registered'
    },
    timeout: {
      en: 'Request Timeout'
    },
    missingValue: {
      en: 'Missing Value'
    },
    internalError: {
      en: 'Internal Server Error'
    },
    notFound: {
      en: 'Account Not Found'
    },
    transferAccount: {
      en: 'Invalid credit or debit account'
    },
    forbidden: {
      en: 'Permission denied'
    },
    scheduleTransferCount: {
      en: 'Transfer limit exceeded'
    },
    overDraftAccount: {
      en: 'Draft account not allowed to transfer'
    },
    allCountries: {
      en: "All countries' requests have thrown an Error"
    }
  },
  loans: {
    internalError: {
      en: 'Internal Server Error'
    },
    allCountries: {
      en: "All countries' requests have thrown an Error"
    }
  },
  authorization: {
    notFound: {
      en: 'Authorization must be provided'
    },
    invalid: {
      en: 'Invalid token'
    },
    otpBlocked: {
      en: 'Your token is locked'
    },
    invalidOtp: {
      en: 'Invalid OTP'
    },
    emailNotFound: {
      en: 'Email not found'
    },
    transactions: {
      en: 'No transaction was found'
    },
    scheduleTransfer: {
      en: 'Invalid debit account'
    },
    timeScheduleTransfer: {
      en: 'Schedule date should be greater than today'
    },
    timeTransferDay: {
      en: 'You cannot edit the transfer in transfer day'
    }
  },
  kafka: {
    notFound: {
      en: 'This server does not host this topic-partition'
    }
  }
};

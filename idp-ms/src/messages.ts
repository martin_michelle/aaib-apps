export default {
  authentication: {
    authFail: {
      en: 'Authentication failure'
    },
    timeout: {
      en: 'Request Timeout'
    },
    missingValue: {
      en: 'Missing Value'
    },
    emailNotFound: {
      en: 'Email not found'
    },
    notRegistered: {
      en: 'Not registered'
    },
    duplicateSession: {
      en: 'Duplicate session'
    },
    passwordExpired: {
      en: 'Password Expired'
    },
    conflict: {
      en: 'Username already taken'
    },
    registerConflict: {
      en: 'Username already exist'
    },
    registered: {
      en: 'Customer already registered'
    },
    corporateError: {
      en: 'Only allowed for Individual Customers'
    },
    invalid: {
      en: 'Invalid OTP'
    },
    forbidden: {
      en: 'Permission denied'
    },
    invalidPassword: {
      en: 'Invalid Password'
    },
    invalidOtp: {
      en: 'Invalid OTP'
    },
    invalidGrant: {
      en: 'invalid_grant'
    },
    blockedOtp: {
      en: 'Your token is locked'
    },
    maxAttempt: {
      en: 'maximum attempts exceeded'
    },
    inactiveUser: {
      en: 'User is inactive'
    },
    minorUserLogin: {
      en: 'Minor customers are not allowed to manage their accounts. Please contact us for any inquiries or requests.'
    },
    minorUserRegistration: {
      en: 'Not allowed for Minor Customers'
    },
    jointUserRegistration: {
      en: 'Not allowed for Joint Customers'
    },
    internalError: {
      en: 'Internal Server Error'
    },
    fraudDetected: {
      en: 'Potential fraud has been detected from this device'
    }
  },
  profile: {
    allCountries: {
      en: "All countries' requests have thrown an Error"
    },
    internalError: {
      en: 'Internal Server Error'
    }
  }
};

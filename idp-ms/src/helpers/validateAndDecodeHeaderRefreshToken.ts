import { Response, NextFunction } from 'express';
import { ProtectedRequest } from 'app-request';
import { AuthFailureError } from '../core/ApiError';
import messages from '../messages';
import { verify } from 'jsonwebtoken';
import { tokenInfo } from '../config';

export default () => async (req: ProtectedRequest, res: Response, next: NextFunction) => {
  try {
    const { user, headers } = req;
    const { refreshtoken } = headers;
    const data: any = verify(refreshtoken, tokenInfo.secret) || '';
    if (data?.sub && data?.sub?.refreshToken && data?.sub?.user) {
      if (user.id !== data.sub.user)
        next(new AuthFailureError(messages.authentication.authFail.en));
      else {
        req.headers.refreshtoken = data.sub.refreshToken;
        next();
      }
    } else next(new AuthFailureError(messages.authentication.authFail.en));
  } catch (error) {
    console.log(error);
    next(new AuthFailureError(messages.authentication.authFail.en));
  }
};

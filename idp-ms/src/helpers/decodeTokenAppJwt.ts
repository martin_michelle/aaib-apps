import { Response, NextFunction } from 'express';
import { ProtectedRequest } from 'app-request';
import { AuthFailureError } from '../core/ApiError';
import messages from '../messages';
import * as JWT from 'jsonwebtoken';
import { tokenApp } from '../config';

export default () => async (req: ProtectedRequest, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const data = await JWT.verify(token, tokenApp.secret as string);
    if (data) {
      next();
    } else next(new AuthFailureError(messages.authentication.authFail.en));
  } catch (error) {
    next(new AuthFailureError(messages.authentication.authFail.en));
  }
};

import { privateDecrypt } from 'crypto';
import { readFileSync } from 'fs';
import { ProtectedRequest } from 'app-request';
import { NextFunction, Response } from 'express';
import { AuthFailureError } from '../core/ApiError';
import messages from '../messages';
import { passphrase, keyPath } from '../config';
import { logger } from '../core/Logger';

export default () => (req: ProtectedRequest, res: Response, next: NextFunction) => {
  try {
    if (req.headers.method === 'encryption') {
      const filePath = `${keyPath}private.pem`;
      const privateKey = readFileSync(filePath).toString();
      const fields = Object.keys(req.body);
      fields.forEach((field) => {
        try {
          const buffer = Buffer.from(req.body[field], 'base64');
          const decodedData = privateDecrypt(
            {
              key: privateKey,
              passphrase
            },
            buffer
          );
          req.body[field] = decodedData.toString();
        } catch (error) {
          logger.error(error.message);
        }
      });
      next();
    } else {
      next();
    }
  } catch (error) {
    logger.error(error.message);
    next(new AuthFailureError(messages.authentication.authFail.en));
  }
};

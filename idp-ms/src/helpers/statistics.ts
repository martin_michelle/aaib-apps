import { produce } from '../kafka';
import { kafkaTopics } from '../config';
import correlation from 'express-correlation-id';

export default async (updateValue: string, kibanaInfo = {}) => {
  try {
    if (updateValue) {
      const data = {
        topic: kafkaTopics.statistics,
        correlationId: correlation.getId() || '',
        date: new Date(),
        updateValue
      };
      await produce(data, null, false);
    }
    if (Object.keys(kibanaInfo).length) {
      const kibanaData = {
        topic: kafkaTopics.kibana,
        correlationId: correlation.getId() || '',
        date: new Date(),
        kibanaInfo
      };
      await produce(kibanaData, null, false);
    }
  } catch (e) {
    return null;
  }
};

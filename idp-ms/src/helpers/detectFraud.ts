import fs from 'fs';
import https from 'https';
import axios from 'axios';
import moment from 'moment';
import { NextFunction, Response } from 'express';
import { ProtectedRequest } from '../types/app-request';
import { ApiError, SuspendedError } from '../core/ApiError';
import messages from '../messages';
import {
  securityMode,
  maxGap,
  takInterfaceUrl,
  deviceRTIProperties,
  takCrtPath,
  takKeyPath
} from '../config';
import { TakDFPVerificationResponse, DeviceRti, TakStatus } from '../types/tak-deviceInfo';
import { logger } from '../core/Logger';

const takCert = fs.readFileSync(takCrtPath);
const takCertKey = fs.readFileSync(takKeyPath);
const options = {
  cert: takCert,
  key: takCertKey,
  keepAlive: true,
  rejectUnauthorized: false
};
const axiosInstance = axios.create({
  httpsAgent: new https.Agent(options)
});

const getDeviceInfoFromTak = async (
  url: string,
  id: string
): Promise<TakDFPVerificationResponse> => {
  const takRes = await axiosInstance.get(`${url}${id}`, {
    httpsAgent: new https.Agent(options),
    headers: { 'Content-Type': 'application/json' },
    params: {
      include_matches: true,
      include_device_info: true
    }
  });
  return takRes.data;
};

const isFraudDetected = (
  deviceInfo: TakDFPVerificationResponse
): { reason?: string; fraud: boolean } => {
  const { deviceRti, timestamp } = deviceInfo;
  const appOpenDate = moment(timestamp);
  const requestDate = moment(Date.now());
  const gap = moment.duration(requestDate.diff(appOpenDate)).asHours();

  for (const deviceInfoProperty of deviceRTIProperties) {
    if (deviceRti[deviceInfoProperty as keyof DeviceRti])
      return { reason: deviceInfoProperty, fraud: true };
  }

  if (gap >= maxGap) {
    return { reason: 'suspicious gap', fraud: true };
  }

  return { fraud: false };
};

const changeDeviceStatus = async (url: string, id: string, status: TakStatus) => {
  await axiosInstance.put(`${url}${id}/status/${status}`);
};

export default async (req: ProtectedRequest, res: Response, next: NextFunction) => {
  try {
    console.log('Started TAK check');
    const enableTAK = req.headers['enabletak'] === 'true' ? true : false;

    if (!securityMode || !enableTAK) return next();

    if (req.headers['platform'] === 'web') return next();

    const takId = req.headers['tak-id'];
    console.log(takId);

    if (!takId) throw new SuspendedError(messages.authentication.fraudDetected.en);
    console.log('Passed takID present check', takId);

    const deviceInfo: TakDFPVerificationResponse = await getDeviceInfoFromTak(
      takInterfaceUrl,
      takId
    );
    console.log('passed get device info');
    const checkFraud = isFraudDetected(deviceInfo);
    console.log('passed fraud detection');
    if (checkFraud.fraud) {
      logger.error(`Reason for fraud: ${checkFraud.reason}`);
      await changeDeviceStatus(takInterfaceUrl, takId, 'LOCK_REQUEST');
      throw new SuspendedError(messages.authentication.fraudDetected.en);
    }
    console.log('ALL DONE');
    return next();
  } catch (err) {
    console.error(err.message);

    const errInstance =
      err instanceof ApiError || err instanceof SuspendedError
        ? err
        : new SuspendedError(messages.authentication.fraudDetected.en);
    next(errInstance);
  }
};

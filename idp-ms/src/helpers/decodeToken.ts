import { Response, NextFunction } from 'express';
import { ProtectedRequest } from 'app-request';
import { AuthFailureError } from '../core/ApiError';
import messages from '../messages';
import { decode } from 'jsonwebtoken';

export default () => async (req: ProtectedRequest, res: Response, next: NextFunction) => {
  try {
    const authorization = req.headers.authorization.split(' ')[1];
    const data = decode(authorization) || '';
    // @ts-ignore
    if (data.sub && data.sub.userInfo) {
      // @ts-ignore
      req.user = data.sub.userInfo;
      next();
    } else next(new AuthFailureError(messages.authentication.authFail.en));
  } catch (error) {
    next(new AuthFailureError(messages.authentication.authFail.en));
  }
};

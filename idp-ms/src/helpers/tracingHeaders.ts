import { Request } from 'express';

const propagated = [
  // zipking trace context headers
  'x-b3-traceid',
  'x-b3-parentspanid',
  'x-b3-spanid',
  'x-b3-sampled',
  // w3c trace context headers
  'traceparent',
  'tracestate'
];

export default (req: Request) =>
  Object.keys(req.headers)
    .filter((key) => propagated.includes(key))
    .reduce((obj: any, key: string) => {
      obj[key] = req.headers[key];
      return obj;
    }, {});

import { AuthFailureError } from '../core/ApiError';
import { adminCredentials } from '../config';
import { logger } from '../core/Logger';
import * as config from '../config';
import axios from 'axios';

export default async (tracingHeaders: any) => {
  try {
    const body = {
      user: adminCredentials.username,
      password: adminCredentials.password
    };
    const url = `${config.t24Url}hid/generatetoken`;
    const result = await axios.post(url, body, {
      timeout: config.promiseTimeout,
      headers: tracingHeaders
    });
    return String(result.data?.tokens?.accessToken || '');
  } catch (e) {
    logger.debug(e.message);
    throw new AuthFailureError();
  }
};

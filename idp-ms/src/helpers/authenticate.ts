import { Response, NextFunction } from 'express';
import { ProtectedRequest } from 'app-request';
import { AuthFailureError } from '../core/ApiError';
import messages from '../messages';
import { validateToken } from '../services/access';
import tracingHeaders from './tracingHeaders';

export default () => async (req: ProtectedRequest, res: Response, next: NextFunction) => {
  try {
    const data = await validateToken(
      req.headers.authorization,
      req.params.account,
      req.params.otp || req.body.otp,
      req.params.token,
      req.originalUrl,
      req.method,
      '',
      tracingHeaders(req)
    );
    req.user = data.user;
    req.accessToken = data.token;
    next();
  } catch (error) {
    next(new AuthFailureError(messages.authentication.authFail.en));
  }
};

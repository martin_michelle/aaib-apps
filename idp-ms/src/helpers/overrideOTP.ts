import { Response, NextFunction } from 'express';
import { ProtectedRequest } from 'app-request';
import { AuthFailureError } from '../core/ApiError';
import messages from '../messages';

export default () => async (req: ProtectedRequest, res: Response, next: NextFunction) => {
  try {
    req.body.OTP = req.body.otp;
    delete req.body.otp;
    next();
  } catch (error) {
    next(new AuthFailureError(messages.authentication.authFail.en));
  }
};

// Mapper for environment variables
import path from 'path';

export const environment = process.env.NODE_ENV;
export const port = process.env.PORT;

export const corsUrl = process.env.CORS_URL;

export const t24Url = process.env.T24_BASE_URL;

export const tokenInfo = {
  secret: process.env.JWT_SECRET || '',
  issuer: process.env.TOKEN_ISSUER || '',
  audience: process.env.TOKEN_AUDIENCE || ''
};

export const tokenApp = {
  secret: process.env.TOKEN_APP_SECRET
};

export const promiseTimeout = parseInt(process.env.PROMISE_TIMEOUT || '20000');

const hidHostProto = process.env.HID_HOST_PROTO || 'https';

export const hidCredentials = {
  duplicateCode: 48,
  blockedLogin: 2,
  expiredPasswordCode: 6004,
  conflictCode: 409,
  invalidPasswordCode: 2100,
  clientId: process.env.HID_CLIENT_ID || '',
  clientSecret: process.env.HID_CLIENT_SECRET || '',
  hostName: process.env.HID_HOST_NAME,
  tenantId: process.env.HID_TENANT_ID,
  loginUrl: `${hidHostProto}://${process.env.HID_HOST_NAME}/idp/${process.env.HID_TENANT_ID}/authn/token`,
  revokeUrl: `${hidHostProto}://${process.env.HID_HOST_NAME}/idp/${process.env.HID_TENANT_ID}/authn/revoke`,
  userInfo: `${hidHostProto}://${process.env.HID_HOST_NAME}/idp/${process.env.HID_TENANT_ID}/authn/userinfo`,
  searchUser: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Users/.search`,
  changePassword: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Authenticator/{{UserId}}.AT_CUSTPW`,
  registerDevice: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Device`,
  bindingDevice: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Device/{id}`,
  provisionDevice: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Device/Provision`,
  eventLog: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Event/.search`,
  userEventLog: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Users`,
  deleteUser: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Users/{{userId}}`,
  addPassword: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Authenticator`,
  customerOtp: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Policy/Authenticator/AT_CUSTOTP`,
  deleteOtpAuthenticatorUrl: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Authenticator/{{userId}}.AT_CUSTOTP`,
  searchDevices: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Device/.search`,
  unAssignDevice: `${hidHostProto}://${process.env.HID_HOST_NAME}/scim/${process.env.HID_TENANT_ID}/v2/Device/{{deviceId}}`
};

export const adminCredentials = {
  username: process.env.ADMIN_USER,
  password: process.env.ADMIN_PASSWORD
};

export const kafkaTopics = {
  t24Request: 't24Request',
  idpResponse: 'idpResponse',
  niRequest: 'niRequest',
  euronetRequest: 'euronetRequest',
  validateTopic: 'validateTopic',
  sendEmail: 'sendEmail',
  smtp: 'smtp',
  statistics: 'statistics',
  kibana: 'kibanastatistics',
  customerResponse: 'customerResponse'
};

export const kafkaOptions = {
  clientId: process.env.KAFKA_CLIENT_ID || '',
  hosts: process.env.KAFKA_HOSTS?.split(',') || [],
  connectionTimeout: parseInt(process.env.KAFKA_CONNECTION_TIMEOUT || '3000'),
  requestTimeout: parseInt(process.env.KAFKA_REQUEST_TIMEOUT || '25000'),
  initialRetryTime: parseInt(process.env.KAFKA_INITIAL_RETRY_TIME || '1'),
  retries: parseInt(process.env.KAFKA_RETRIES || '1'),
  producerPolicy: {
    allowAutoTopicCreation: true
  },
  consumerPolicy: {
    groupId: process.env.CONSUMER_GROUP_ID || 'idp-ms',
    maxWaitTimeInMs: Number(process.env.CONSUMER_MAX_WAIT_TIME || '100'),
    allowAutoTopicCreation: true,
    sessionTimeout: Number(process.env.CONSUMER_SESSION_TIMEOUT || '30000'),
    heartbeatInterval: Number(process.env.CONSUMER_HEART_BEAT || '3000'),
    retry: {
      retries: parseInt(process.env.KAFKA_RETRIES || '1')
    }
  },
  numberOfPartitions: Number(process.env.KAFKA_PARTITION_NUMBER || '3')
};

export const passphrase = String(process.env.CRYPTO_PASSPHRASE || '');

export const keyPath = String(process.env.CRYPTO_KEY_PATH || '');

export const dateFormat = 'YYYY-MM-DDThh:mm:ss+0000';

export const registrationSms = {
  device: {
    en: `Your AAIB Token activation code is {activationCode}\nPlease don't share with anyone. If you haven't requested to activate Token, please call us immediately on 19555 or (+202) 226733107`
  },
  bio: {
    en: `Your activation code for biometric login is {activationCode}\nPlease don't share this code with anyone. If you haven't requested a code please call us immediately on 19555`
  }
};

export const blockedSms = {
  blockedLogin: {
    en: 'You have exceeded the maximum number of password trials, your user ID has been locked, please reset your password through Forget Password or contact 19555 to reset the invalid trials'
  },
  blockedToken: {
    en: 'You have exceeded the maximum number of OTP trials, please contact 19555 to reset your Token App'
  }
};

export const loginStatus = {
  active: 'active',
  suspended: 'suspended'
};

export const mongoConfig = {
  url: process.env.DB_URL || ''
};

export const defaultCounter = Number(process.env.DEFAULT_COUNTER || '5');

export const statistics = {
  successLogin: 'successLogin',
  failedLogin: 'failedLogin',
  successUserCreation: 'successUserCreation',
  t24LoginFailure: 't24LoginFailure',
  notRegisteredFailure: 'notRegisteredFailure',
  inactiveUserLoginFailure: 'inactiveUserLoginFailure',
  duplicateLoginFailure: 'duplicateLoginFailure',
  minorUser: 'MinorUser',
  jointUser: 'JointUser',
  expiredLoginFailure: 'expiredLoginFailure',
  suspendedLoginFailure: 'suspendedLoginFailure',
  invalidCredentialsFailure: 'invalidCredentialsFailure'
};

export const staticError = {
  duplicateSession: 'Failed login - Duplicate session for user {user}',
  expiredPassword: 'Failed login - Expired password for user {user}',
  suspended: 'Failed login - User {user} is suspended',
  userNotFound: 'Failed login - T24 is down or user {user} not found',
  inactiveUser: 'Failed login - User {user} not active',
  invalidCredentials: 'Failed login - Invalid username or password for user {user}',
  minorUser: 'Failed login - User {user} is a minor',
  jointUser: 'Failed login - User {user} is a joint'
};

export const loginType = {
  credentials: 'Credentials',
  biometrics: 'Biometrics'
};

export const allSector = process.env.ALL_SECTOR || 'enable';

export const smtpCredentials = {
  username: process.env.SMTP_USERNAME || '',
  password: process.env.SMTP_PASSWORD || '',
  host: process.env.SMTP_HOST || '',
  port: process.env.SMTP_PORT || '',
  email: {
    from: process.env.SMTP_EMAIL_FROM || 'Mobile banking<SA-MobApp-01@aaib.com>',
    to: process.env.SMTP_EMAIL_TO || ''
  }
};

export const emailTemplates = {
  loan: {
    template: path.join(__dirname, '../emails/loan.pug'),
    subject: 'Loan request from user {cif}',
    attachments: [
      {
        filename: 'main_1.png',
        path: path.join(__dirname, '../images/main_1.png'),
        cid: 'main_1'
      },
      {
        filename: 'loan_1.png',
        path: path.join(__dirname, '../images/Loan_1.png'),
        cid: 'Loan_1'
      }
    ]
  },
  creditCard: {
    template: path.join(__dirname, '../emails/creditCard.pug'),
    subject: 'Credit card request from user {cif}',
    attachments: [
      {
        filename: 'main_1.png',
        path: path.join(__dirname, '../images/main_1.png'),
        cid: 'main_1'
      },
      {
        filename: 'cc.png',
        path: path.join(__dirname, '../images/cc.png'),
        cid: 'cc'
      }
    ]
  }
};

export const corporateRange = {
  min: 2000,
  max: 4999
};

export const retailRoutes = [
  {
    route: '/beneficiary',
    methods: ['get', 'post', 'put', 'delete']
  },
  {
    route: '/credit',
    methods: ['get', 'post', 'put', 'delete']
  },
  {
    route: '/debit',
    methods: ['get', 'post', 'put', 'delete']
  },
  {
    route: '/fawry',
    methods: ['get', 'post', 'put', 'delete']
  },
  {
    route: '/login/register/device',
    methods: ['post']
  },
  {
    route: '/accounts/transactions/acc2acc',
    methods: ['post']
  },
  {
    route: '/accounts/internal/acc2acc',
    methods: ['post']
  }
];

export const tokenRoutes = [
  {
    route: '/customer/mobileTransfer',
    methods: ['post']
  },
  {
    route: '/accounts/request/loan',
    methods: ['post']
  },
  {
    route: '/accounts/mailbox',
    methods: ['post']
  },
  {
    route: '/requestPayrollAdvance',
    methods: ['post']
  },
  {
    route: '/accounts/statement',
    methods: ['get']
  },
  {
    route: '/credit/request',
    methods: ['post']
  },
  {
    route: '/debit/replace',
    methods: ['post']
  },
  {
    route: '/debit/request',
    methods: ['post']
  },
  {
    route: '/login/portfolioStatement',
    methods: ['get']
  },
  {
    route: '/fawry/favorite',
    methods: ['post']
  },
  {
    route: '/fawry/favorite',
    methods: ['put']
  },
  {
    route: '/fawry/favorite',
    methods: ['delete']
  },
  {
    route: '/accounts/canRequestUAEAccount',
    methods: ['post']
  },
  {
    route: '/investment/requestSecuredLoan',
    methods: ['post']
  },
  {
    route: '/accounts/request/cd',
    methods: ['post']
  },
  {
    route: '/customer/submit/rating',
    methods: ['post']
  },
  {
    route: '/investment/topUpLoan',
    methods: ['post']
  }
];

export const jointMinorRoutes = [
  {
    route: '/beneficiary',
    methods: ['get', 'post', 'put', 'delete']
  },
  {
    route: '/fawry',
    methods: ['get', 'post', 'put', 'delete']
  },
  {
    route: '/accounts/transactions/acc2acc',
    methods: ['post']
  },
  {
    route: '/accounts/internal/acc2acc',
    methods: ['post']
  },
  {
    route: '/debit/setstatus',
    methods: ['post']
  },
  {
    route: '/debit/setOnlineCards',
    methods: ['post']
  },
  {
    route: '/debit/travelNote',
    methods: ['post', 'delete', 'get']
  },
  {
    route: '/debit/replace',
    methods: ['post']
  },
  {
    route: '/debit/canReplaceCard',
    methods: ['post']
  },
  {
    route: '/debit/termsAndConditions',
    methods: ['get']
  },
  {
    route: '/debit/canReplaceCard',
    methods: ['post']
  },
  {
    route: '/debit/request',
    methods: ['post']
  },
  {
    route: '/credit/setstatus',
    methods: ['post']
  },
  {
    route: '/credit/payment',
    methods: ['post']
  },
  {
    route: '/credit/settings',
    methods: ['get', 'post']
  },
  {
    route: '/credit/travelWindow',
    methods: ['post']
  },
  {
    route: '/credit/request',
    methods: ['post']
  },
  {
    route: '/credit/replace',
    methods: ['post']
  },
  {
    route: '/credit/canReplaceCard',
    methods: ['post']
  },
  {
    route: '/credit/termsAndConditions',
    methods: ['get']
  }
];

//for mocking
export const AECIFs = process.env.AE_CIFS?.split('-') || ['10333148'];
export const TwinCIFs = process.env.TWIN_CIFS?.split('-') || ['10101294'];
export const AEError = process.env.AE_ERROR === '1' ? true : false;
export const EGError = process.env.EG_ERROR === '1' ? true : false;

export const securityMode: boolean = process.env.SECURITY_MODE == '1' ? true : false;

export const takInterfaceUrl = `https://${process.env.TAK_DOMAIN}:${process.env.TAK_PORT}/${process.env.TAK_FMI_PATH}`;

export const deviceRTIProperties = ['root', 'appRepackaged'];

export const maxGap = Number(process.env.MAX_GAP_TAK) || 3;

export const takCrtPath = `${process.env.TAK_CRT_PATH}`;
export const takKeyPath = `${process.env.TAK_KEY_PATH}`;
export const userTypes = {
  corporate: 'CORPORATE'
};

import { CompressionTypes, Consumer, Kafka, Producer, ProducerRecord } from 'kafkajs';
import { kafkaOptions, kafkaTopics, promiseTimeout } from '../config';
import { EventEmitter } from 'events';
import { logger } from '../core/Logger';
import correlation from 'express-correlation-id';
import { sendEmailService, validateToken } from '../services/access';
import { ClientKafka } from '../libs/kafka-partition/client-kafka';
import { promiseWithTimeout } from '../helpers/asyncHandler';
import messages from '../messages';
let producer: Producer;
let consumer: Consumer;
let eventEmitter: EventEmitter;
let client: ClientKafka;
let interval: number;
const partitionId: any = {};

export const connector = () => {
  return new Kafka({
    clientId: kafkaOptions.clientId,
    brokers: kafkaOptions.hosts,
    connectionTimeout: kafkaOptions.connectionTimeout,
    requestTimeout: kafkaOptions.requestTimeout,
    retry: {
      initialRetryTime: kafkaOptions.initialRetryTime,
      retries: kafkaOptions.retries
    }
  });
};

export const createProducer = async () => {
  if (producer) return producer;
  const kafka = connector();
  producer = kafka.producer(kafkaOptions.producerPolicy);
  await producer.connect();
  return producer;
};

export const createKafkaClient = () => {
  if (client) return client;
  return new ClientKafka({
    consumer: {
      groupId: kafkaOptions.consumerPolicy.groupId,
      maxWaitTimeInMs: kafkaOptions.consumerPolicy.maxWaitTimeInMs
    },
    client: {
      clientId: kafkaOptions.clientId,
      brokers: kafkaOptions.hosts,
      connectionTimeout: kafkaOptions.connectionTimeout,
      requestTimeout: kafkaOptions.requestTimeout,
      retry: {
        initialRetryTime: kafkaOptions.initialRetryTime,
        retries: kafkaOptions.retries
      }
    }
  });
};

export const createConsumer = async () => {
  try {
    client = createKafkaClient();
    consumer = await client.connect();
    await client.createKafkaTopic(kafkaTopics.idpResponse, kafkaOptions.numberOfPartitions);
    await client.createKafkaTopic(kafkaTopics.validateTopic, kafkaOptions.numberOfPartitions);
    // await client.createKafkaTopic(kafkaTopics.sendEmail, kafkaOptions.numberOfPartitions);
    await consumer.connect();
    await consumer.subscribe({ topic: kafkaTopics.idpResponse });
    await consumer.subscribe({ topic: kafkaTopics.validateTopic });
    // await consumer.subscribe({ topic: kafkaTopics.sendEmail });
    const consumerAssignments: { [key: string]: number } = {};
    consumer.on(consumer.events.GROUP_JOIN, (data: any) => {
      Object.keys(data.payload.memberAssignment).forEach((memberId) => {
        consumerAssignments[memberId] = Math.min(...data.payload.memberAssignment[memberId]);
      });
      consumer.on(consumer.events.HEARTBEAT, ({ timestamp }) => {
        interval = timestamp;
      });
      partitionId[kafkaTopics.idpResponse] = consumerAssignments[kafkaTopics.idpResponse];
      partitionId[kafkaTopics.validateTopic] = consumerAssignments[kafkaTopics.validateTopic];
      // partitionId[kafkaTopics.sendEmail] = consumerAssignments[kafkaTopics.sendEmail];
    });

    await consumer.run({
      eachMessage: async ({ topic, message }) => {
        if (!message.headers?.correlationId) {
          logger.error('Missing correlationId');
        } else {
          if (topic === kafkaTopics.sendEmail)
            sendEmailService(message).catch((e) => {
              logger.debug(e.message);
            });
          if (topic === kafkaTopics.validateTopic) {
            const kafkaMessage = await prepareKafkaMessage(message);
            logger.info('authorization sent to idp', kafkaMessage.authorization);
            handleKafkaMessages(
              kafkaMessage.authorization,
              kafkaMessage.replyTopic,
              kafkaMessage.correlationId,
              kafkaMessage.partitionReply,
              kafkaMessage.account,
              kafkaMessage.otp,
              kafkaMessage.token,
              kafkaMessage.route,
              kafkaMessage.method,
              kafkaMessage.tracingHeaders
            );
          } else {
            const eventEmitter = createEventEmitter();
            eventEmitter.emit(message.headers.correlationId.toString(), message);
          }
        }
      }
    });
    return consumer;
  } catch (e) {
    logger.info(e.message);
  }
};

export const createEventEmitter = () => {
  if (eventEmitter) return eventEmitter;
  eventEmitter = new EventEmitter();
  return eventEmitter;
};

export const produce = async (data: any, tracingHeaders: any, waitForResponse = true) => {
  if (!producer) producer = await createProducer();
  const correlationId = data.correlationId || correlation.getId();
  const partitionReply = kafkaTopics.t24Request
    ? partitionId[kafkaTopics.idpResponse]
    : partitionId[kafkaTopics.validateTopic];
  const producerOptions = {
    topic: data.topic || kafkaTopics.t24Request,
    compression: CompressionTypes.GZIP,
    acks: 1,
    messages: [
      {
        value: JSON.stringify(data),
        headers: {
          correlationId,
          partitionReply: partitionReply?.toString() || '0',
          replyTopic: kafkaTopics.idpResponse,
          authorization: data.authorization || '',
          function: data.function || '',
          consumerType: data.consumerType || '',
          ...(tracingHeaders ? { tracingHeaders: JSON.stringify(tracingHeaders) } : {})
        }
      }
    ]
  };
  await producer.send(producerOptions);
  if (waitForResponse) {
    const eventEmitter = createEventEmitter();
    const response = () => {
      return new Promise((resolve, reject) => {
        eventEmitter.once(correlationId || '', async (value) => {
          try {
            resolve(value);
          } catch (e) {
            logger.info(e.message);
            reject(e);
          }
        });
      });
    };
    return await promiseWithTimeout(promiseTimeout, response, messages.authentication.timeout.en);
  }
};

export const prepareKafkaMessage = async (data: any) => {
  return {
    data: data.value.toString(),
    correlationId: data.headers.correlationId.toString(),
    replyTopic: data.headers.replyTopic.toString(),
    authorization: data.headers.authorization.toString(),
    partitionReply: data.headers.partitionReply ? data.headers.partitionReply.toString() : '0',
    account: data.headers.account?.toString(),
    otp: data.headers.otp?.toString(),
    token: data.headers.token?.toString(),
    route: data.headers.route?.toString(),
    method: data.headers.method?.toString(),
    tracingHeaders: data.headers.tracingHeaders
      ? JSON.parse(data.headers.tracingHeaders.toString())
      : {}
  };
};

export const handleKafkaMessages = async (
  authorization: string,
  replyTopic: string,
  correlationId: string,
  partitionReply: string,
  account: string,
  otp: string,
  token: string,
  route: string,
  method: string,
  tracingHeaders: any
) => {
  try {
    const userInfo = await validateToken(
      authorization,
      account,
      otp,
      token,
      route,
      method,
      correlationId,
      tracingHeaders
    );
    const response = {
      error: false,
      message: { data: userInfo }
    };
    logger.info('get user info after validate token', { correlationId });
    const producer = await createProducer();
    const producerOptions: ProducerRecord = {
      topic: replyTopic,
      compression: CompressionTypes.GZIP,
      messages: [
        {
          value: JSON.stringify(response),
          partition: Number(partitionReply),
          headers: {
            correlationId
          }
        }
      ]
    };
    await producer.send(producerOptions);
  } catch (e) {
    logger.info('kafka eror if get user was logged', { correlationId });
    logger.error(e.message, { correlationId });
    handleErrors(replyTopic, correlationId, e.message, partitionReply);
  }
};

export const handleErrors = async (
  replyTopic: string,
  correlationId: string,
  error: string,
  partitionReply: string
) => {
  const producer = await createProducer();
  const data = {
    error: true,
    message: error
  };
  const producerOptions: ProducerRecord = {
    topic: replyTopic,
    compression: CompressionTypes.GZIP,
    messages: [
      {
        value: JSON.stringify(data),
        partition: Number(partitionReply),
        headers: {
          correlationId
        }
      }
    ]
  };
  await producer.send(producerOptions);
};

export const disconnect = async () => {
  client = createKafkaClient();
  return client.close();
};

export const getHeartbeatTime = async () => {
  return {
    consumer,
    interval,
    partitionId
  };
};

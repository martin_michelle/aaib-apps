export const registrationCountries: any = {
  en: [
    {
      code: 'EG',
      image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EG.svg',
      name: 'Egypt'
    },
    {
      code: 'AE',
      image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AE.svg',
      name: 'United Arab Emirates'
    }
  ],
  ar: [
    {
      code: 'EG',
      image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EG.svg',
      name: 'مصر'
    },
    {
      code: 'AE',
      image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AE.svg',
      name: 'دولة الأمارات العربية المتحدة'
    }
  ]
};

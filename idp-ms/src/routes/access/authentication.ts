import express from 'express';
import { SuccessResponse } from '../../core/ApiResponse';
import { ProtectedRequest } from 'app-request';
import * as service from '../../services/access/index';
import { validate } from 'express-validation';
import decodeToken from '../../helpers/decodeToken';
import schema from './schema';
import asyncHandler from '../../helpers/asyncHandler';
import authenticate from '../../helpers/authenticate';
import decrypt from '../../helpers/decrypt';
import tracingHeaders from '../../helpers/tracingHeaders';
import decodeTokenAppJwt from '../../helpers/decodeTokenAppJwt';
import detectFraud from '../../helpers/detectFraud';
import overrideOTP from '../../helpers/overrideOTP';
import { hidCredentials } from '../../config';
import validateAndDecodeRefreshToken from '../../helpers/validateAndDecodeRefreshToken';

const router = express.Router();

router.post(
  '/basic',
  validate(schema.userCredential),
  decrypt(),
  detectFraud,
  asyncHandler(async (req, res) => {
    const result = await service.generateUserToken(
      req.body.user,
      req.body.password,
      '',
      req.body.otp,
      String(req.headers.platform),
      null,
      tracingHeaders(req)
    );
    new SuccessResponse('Login Success', result).send(res);
  })
);

router.get(
  '/validate/takId',
  detectFraud,
  asyncHandler(async (req, res) => {
    new SuccessResponse('Success', {
      url: `${hidCredentials.hostName}/${hidCredentials.tenantId}`
    }).send(res);
  })
);

router.post(
  '/refresh',
  decodeToken(),
  validate(schema.refreshToken),
  detectFraud,
  validateAndDecodeRefreshToken(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.generateUserToken(
      req.user.id,
      '',
      req.body.refreshToken,
      '',
      req.headers.platform,
      req.user,
      tracingHeaders(req)
    );
    new SuccessResponse('Refresh Success', result).send(res);
  })
);

router.post(
  '/revoke',
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.revokeToken(req.headers.authorization || '', tracingHeaders(req));
    new SuccessResponse('Revoke Success', result).send(res);
  })
);

router.get(
  '/info',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getLastLoginInfo(
      req.user.internalId,
      req.accessToken || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/profile',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getUserProfile(
      req.user,
      req.headers.authorization || '',
      '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/reset',
  decrypt(),
  validate(schema.resetPassword),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.resetPassword(req.body, req.headers.platform, tracingHeaders(req));
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/change',
  decrypt(),
  authenticate(),
  validate(schema.changePassword),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.changePassword(
      req.user.cif,
      req.user.id,
      req.body,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/change/password',
  decrypt(),
  authenticate(),
  validate(schema.changePasswordUsingSms),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.changePasswordWithSmsOtp(
      req.user.cif,
      req.user.id,
      req.body,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/forgot/username',
  decrypt(),
  validate(schema.forgotUsername),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.forgotUsername(
      {
        Type: req.body.type,
        Key: req.body.key,
        NationalId: req.body.nationalId
      },
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/phoneNumbers',
  decrypt(),
  validate(schema.phoneNumbers),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getUserPhoneNumbersByUserName(
      req.body.username,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/forgot/password',
  decrypt(),
  validate(schema.forgotPassword),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.forgotPassword(
      {
        Type: 'Username',
        Key: req.body.username
      },
      req.query.country as string,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/change/password/send/otp/sms',
  decrypt(),
  authenticate(),
  validate(schema.changePass),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.sendSmsForChangePassword(
      req.user.id,
      req.query.country as string,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/validate/password',
  validate(schema.validatePasswordOtp),
  decrypt(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.validateTokenOtp(
      {
        Token: req.body.token,
        Subject: 'PASSWORD',
        OTP: req.body.otp
      },
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/forgot/password/change',
  decrypt(),
  validate(schema.changeTokenPassword),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.changeTokenPassword(
      req.body.password,
      req.body.token,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/register/device',
  authenticate(),
  validate(schema.register),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.registerDevice(
      req.user.id,
      req.user.internalId,
      req.body.type,
      req.user.cif,
      req.user,
      req.body.country || 'EG',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/device/check',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.checkUserDevices(
      req.user.id,
      req.accessToken,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.delete(
  '/device/:username',
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.deleteUserDevices(req.params.username, tracingHeaders(req));
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/resend/otp',
  validate(schema.resendOtp),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.resendOtp(req.body.token, tracingHeaders(req));
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/send/otp',
  authenticate(),
  validate(schema.expiryDate),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.sendActivateOtp(
      req.user.id,
      req.user.cif,
      req.headers.authorization,
      {
        type: req.body.type,
        expiryDate: req.body.expiryDate,
        token: req.body.token
      },
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/validate/card',
  overrideOTP(),
  authenticate(),
  validate(schema.validateOtp),
  decrypt(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.validateTokenOtp(
      {
        Token: req.body.token,
        OTP: req.body.OTP,
        Subject: 'CARDACTIVATE'
      },
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/portfolioStatement/data',
  authenticate(),
  validate(schema.portfolioStatement),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.portfolioStatementData(
      req.user.cif,
      String(req.query.Month),
      String(req.query.Year),
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/portfolioStatement',
  authenticate(),
  validate(schema.portfolioStatement),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.portfolioStatement(
      req.user.cif,
      String(req.query.Month),
      String(req.query.Year),
      req.user.email,
      req.user.phoneNumber,
      req.user.dateOfBirth,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/tokenApp',
  decodeTokenAppJwt(),
  validate(schema.sendTokenAppLoginLog),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.sendTokenAppLoginLog(req.body, tracingHeaders(req));
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/remove/devices',
  authenticate(),
  validate(schema.removeDevices),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.removeDevices(
      req.body.exceptType,
      req.user.internalId,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

export default router;

import Joi from 'joi';

export default {
  userCredential: {
    body: Joi.object({
      user: Joi.string().required(),
      otp: Joi.string().optional(),
      password: Joi.string().optional()
    })
  },
  refreshToken: {
    body: Joi.object({
      refreshToken: Joi.string().required()
    })
  },
  resetPassword: {
    body: Joi.object({
      oldPassword: Joi.string().required(),
      newPassword: Joi.string().required(),
      username: Joi.string().required()
    })
  },
  changePassword: {
    body: Joi.object({
      oldPassword: Joi.string().required(),
      newPassword: Joi.string().required(),
      otp: Joi.string().optional()
    })
  },
  changePasswordUsingSms: {
    body: Joi.object({
      oldPassword: Joi.string().required(),
      newPassword: Joi.string().required(),
      otp: Joi.string().optional(),
      token: Joi.string().required(),
      smsOtp: Joi.string().required()
    })
  },
  forgotUsername: {
    body: Joi.object({
      type: Joi.string().required().valid('Card', 'Cif'),
      key: Joi.string().required(),
      nationalId: Joi.string().required()
    })
  },
  forgotPassword: {
    body: Joi.object({
      username: Joi.string().required()
    }),
    query: Joi.object({
      country: Joi.string().optional().valid('EG', 'AE')
    })
  },
  changePass: {
    query: Joi.object({
      country: Joi.string().optional().valid('EG', 'AE')
    })
  },
  phoneNumbers: {
    body: Joi.object({
      username: Joi.string().required()
    })
  },
  validateOtp: {
    body: Joi.object({
      token: Joi.string().required(),
      OTP: Joi.string().required()
    })
  },
  validatePasswordOtp: {
    body: Joi.object({
      token: Joi.string().required(),
      otp: Joi.string().required()
    })
  },
  expiryDate: {
    body: Joi.object({
      token: Joi.string().required(),
      expiryDate: Joi.string().required(),
      type: Joi.string().required().allow('creditCard', 'debitCard')
    })
  },
  changeTokenPassword: {
    body: Joi.object({
      token: Joi.string().required(),
      password: Joi.string().required()
    })
  },
  resendOtp: {
    body: Joi.object({
      token: Joi.string().required()
    })
  },
  register: {
    body: Joi.object({
      type: Joi.string().optional().valid('bio', 'otp', ''),
      country: Joi.string().optional().valid('EG', 'AE')
    })
  },
  portfolioStatement: {
    query: Joi.object({
      Month: Joi.string().required(),
      Year: Joi.string().required()
    })
  },
  sendTokenAppLoginLog: {
    body: Joi.object({
      user: Joi.string().required(),
      success: Joi.boolean().required(),
      subtype: Joi.string().valid('login', 'registration').required(),
      reason: Joi.when('success', {
        is: false,
        then: Joi.string().required(),
        otherwise: Joi.string().optional()
      })
    })
  },
  removeDevices: {
    body: Joi.object({
      exceptType: Joi.string().required().valid('bio', 'otp', 'none')
    })
  }
};

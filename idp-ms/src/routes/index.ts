import express from 'express';
import authentication from './access/authentication';
import registration from './registration/registration';

const router = express.Router();

router.use('/login', authentication);
router.use('/signup', registration);

export default router;

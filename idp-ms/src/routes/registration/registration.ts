import express from 'express';
import { SuccessResponse } from '../../core/ApiResponse';
import { ProtectedRequest } from 'app-request';
import * as service from '../../services/registration/index';
import { validate } from 'express-validation';
import schema from './schema';
import asyncHandler from '../../helpers/asyncHandler';
import decrypt from '../../helpers/decrypt';
import tracingHeaders from '../../helpers/tracingHeaders';
import authenticate from '../../helpers/authenticate';
import validateAndDecodeHeaderRefreshToken from '../../helpers/validateAndDecodeHeaderRefreshToken';
import detectFraud from '../../helpers/detectFraud';

const router = express.Router();

router.post(
  '/validate',
  decrypt(),
  detectFraud,
  validate(schema.validateCredentials),
  asyncHandler(async (req, res) => {
    const result = await service.validateNewUser(
      {
        Type: req.body.type,
        Key: req.body.key,
        NationalId: req.body.nationalId,
        Id: req.body.nationalId
      },
      tracingHeaders(req),
      req.headers.country as string
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/register',
  decrypt(),
  validate(schema.registerCredentials),
  asyncHandler(async (req, res) => {
    const result = await service.registerNewUser(
      {
        Token: req.body.token,
        OTP: req.body.otp,
        Username: req.body.username.toLowerCase(),
        password: req.body.password
      },
      tracingHeaders(req),
      req.headers.country as string
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/link',
  decrypt(),
  authenticate(),
  validate(schema.linkingCredentials),
  validateAndDecodeHeaderRefreshToken(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.linkToExistingUser(
      {
        Token: req.body.token,
        OTP: req.body.OTP,
        Username: req.user.id.toLowerCase()
      },
      req.headers.refreshtoken,
      req.headers.platform,
      req.headers.authorization || '',
      tracingHeaders(req),
      req.headers.country as string
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/request',
  validate(schema.requestOnboarding),
  asyncHandler(async (req, res) => {
    const result = await service.requestOnboarding(
      {
        fullName: req.body.fullName,
        mobileNumber: req.body.mobileNumber,
        productOfInterest: req.body.productOfInterest
      },
      tracingHeaders(req),
      String(req.headers.platform)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/registrationCountries',
  asyncHandler(async (req, res) => {
    const result = await service.getListRegistrationCountries(req.headers['accept-language']);
    new SuccessResponse('Success', result).send(res);
  })
);

export default router;

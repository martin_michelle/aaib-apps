import Joi from 'joi';
import { productOfInterest } from './config';

export default {
  validateCredentials: {
    body: Joi.object({
      type: Joi.string().required().valid('Cif', 'Card'),
      key: Joi.string().required(),
      nationalId: Joi.string().required()
    })
  },
  registerCredentials: {
    body: Joi.object({
      token: Joi.string().required(),
      otp: Joi.string().required(),
      username: Joi.string().required().min(6),
      password: Joi.string().required()
    })
  },
  linkingCredentials: {
    body: Joi.object({
      token: Joi.string().required(),
      OTP: Joi.string().required()
    })
  },
  requestOnboarding: {
    body: Joi.object({
      fullName: Joi.string().required(),
      // mobileNumber: Joi.string()
      //   .regex(/^\+?20[0-9]{10}/)
      //   .required(),
      mobileNumber: Joi.string().required(),
      productOfInterest: Joi.string()
        .required()
        .valid(...Object.keys(productOfInterest))
    })
  }
};

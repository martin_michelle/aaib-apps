import {
  AuthFailureError,
  InternalError,
  SessionError,
  PasswordExpiredError,
  ConflictError,
  InvalidPasswordError,
  ForbiddenError,
  SuspendedError,
  MaxAttemptError,
  MinorUserError,
  JointUserError,
  MaxDeviceError
} from '../../core/ApiError';
import messages from '../../messages';
import axios from 'axios';
import * as config from '../../config';
import model from '../models/model';
import lodash from 'lodash';
import * as crypto from 'crypto';
import { logHIDError, logger } from '../../core/Logger';
import { KafkaMessage } from 'kafkajs';
import { decode } from 'jsonwebtoken';
import JWT, { JwtPayload } from '../../core/JWT';
import generateAdminToken from '../../helpers/adminToken';
import updateStatistics from '../../helpers/statistics';
import { createUserPassword } from '../registration';
import nodemailer from 'nodemailer';
import pug from 'pug';
import { produce } from '../../kafka';
import {
  hidCredentials,
  dateFormat,
  registrationSms,
  blockedSms,
  loginStatus,
  kafkaTopics,
  staticError,
  corporateRange,
  allSector,
  AECIFs,
  TwinCIFs
} from '../../config';
import { maskString } from '../../helpers/maskString';
import moment from 'moment';
import promiseAllsettled from 'promise.allsettled';
import correlation from 'express-correlation-id';

export const mockAppendingAECif = (user: any) => {
  console.log({ TwinCIFs });
  // start appending from here as we have just one AE CIf
  if (TwinCIFs.includes(user.Customer)) {
    logger.info('done converted to twin');
    user.othercifs.push({ country: 'AE', cif: '10100348' });
  }

  // from here to force EG cif to be the main CIF
  const egyptianCifObject = user.othercifs?.find((cifObj: any) => {
    return cifObj.country === 'EG';
  });

  if (user.othercifs.length > 1 && user.country !== 'EG' && egyptianCifObject) {
    user.country = egyptianCifObject.country;
    user.Customer = egyptianCifObject.cif;
  }
  // end here
};

export const generateUserToken = async (
  user: string,
  password: string,
  refreshToken: string,
  otp: string,
  platform: string,
  userRecord: any,
  tracingHeaders?: any
) => {
  let token = '';
  let result: any;
  let isCorporate = false;
  try {
    const statistics = await getUserStatistics(user, tracingHeaders);
    if (otp) result = await validateOtp(otp, user, '0', tracingHeaders);
    else result = await authenticateByPassword(user, password, refreshToken, tracingHeaders);

    token = result.data.access_token;
    const refresh = await wrapRefreshToken(user, result.data.refresh_token);
    const expiryDate = result.data.expires_in;
    const userInfo =
      userRecord ||
      (await getUserInfo(user, token, otp, statistics?.userId || '', platform, tracingHeaders));

    isCorporate = lodash.inRange(Number(userInfo.sector), corporateRange.min, corporateRange.max);
    const accessToken = await JWT.encode(
      new JwtPayload(config.tokenInfo.issuer, { token, userInfo }, expiryDate - 30)
    );

    if (!refreshToken) {
      if (platform === 'web') {
        const authorization = await generateAdminToken(tracingHeaders);
        const data = {
          topic: kafkaTopics.customerResponse,
          function: 'increaseSuccessfulLogins',
          consumerType: 'customerSettings',
          body: {
            cif: userInfo.cif
          },
          authorization: `Bearer ${authorization}`
        };
        produce(data, tracingHeaders);
      }
      updateCustomerInfo(userInfo.cif, `Bearer ${accessToken}`, tracingHeaders);
      updateStatistics(config.statistics.successLogin, {
        cif: userInfo.cif,
        user,
        date: new Date(),
        type: 'login',
        sector: userInfo.sector,
        success: true,
        isCorporate,
        method: otp ? config.loginType.biometrics : config.loginType.credentials,
        platform: platform
      }).catch((e) => logger.error(e.message));
    }
    return {
      user: {
        id: userInfo.cif,
        name: userInfo.name,
        registrationDate: userInfo.registrationDate,
        sector: userInfo.sector,
        isCorporate,
        country: userInfo.country,
        allcifs: userInfo.allcifs
      },
      tokens: {
        accessToken: accessToken,
        refreshToken: refresh
      },
      statistics
    };
  } catch (e) {
    logHIDError(e);
    if (!refreshToken)
      updateStatistics(config.statistics.failedLogin).catch((e) => logger.error(e.message));
    if (token) {
      const accessToken = await JWT.encode(
        new JwtPayload(config.tokenInfo.issuer, { token }, 3000)
      );
      await revokeToken(`Bearer ${accessToken}`, tracingHeaders);
    }
    logger.debug(e.message);
    if (e.response?.data?.hid_failure?.reason === config.hidCredentials.blockedLogin) {
      sendSms(user, '', 'blockedLogin', tracingHeaders).catch((e) => logger.error(e.message));
    }
    if (e.response?.data?.hid_failure?.reason === config.hidCredentials.duplicateCode) {
      updateStatistics(config.statistics.duplicateLoginFailure, {
        user,
        date: new Date(),
        type: 'login',
        success: false,
        reason: 'Duplicate session',
        method: otp ? config.loginType.biometrics : config.loginType.credentials,
        platform: platform
      }).catch((e) => logger.error(e.message));
      logger.debug(staticError.duplicateSession.replace('{user}', user));
      throw new SessionError(messages.authentication.duplicateSession.en);
    }

    if (e.message === messages.authentication.notRegistered.en) {
      updateStatistics(config.statistics.notRegisteredFailure, {
        user,
        date: new Date(),
        type: 'login',
        success: false,
        reason: 'Not registered',
        method: otp ? config.loginType.biometrics : config.loginType.credentials,
        platform: platform
      }).catch((e) => logger.error(e.message));
      logger.debug(staticError.userNotFound.replace('{user}', user));
      throw new AuthFailureError(messages.authentication.authFail.en);
    }
    if (e.message === messages.authentication.duplicateSession.en) {
      updateStatistics(config.statistics.duplicateLoginFailure, {
        user,
        date: new Date(),
        type: 'login',
        success: false,
        reason: 'Duplicate session',
        method: otp ? config.loginType.biometrics : config.loginType.credentials,
        platform: platform
      }).catch((e) => logger.error(e.message));
      logger.debug(staticError.duplicateSession.replace('{user}', user));
      throw new SessionError(messages.authentication.duplicateSession.en);
    }
    if (e.response?.data?.hid_failure?.reason === config.hidCredentials.expiredPasswordCode) {
      updateStatistics(config.statistics.expiredLoginFailure, {
        user,
        date: new Date(),
        type: 'login',
        success: false,
        reason: 'Expired password',
        method: otp ? config.loginType.biometrics : config.loginType.credentials,
        platform: platform
      }).catch((e) => logger.error(e.message));
      logger.debug(staticError.expiredPassword.replace('{user}', user));
      throw new PasswordExpiredError(messages.authentication.passwordExpired.en);
    }
    if (e.type === 'SuspendedError') {
      updateStatistics(config.statistics.suspendedLoginFailure, {
        user,
        date: new Date(),
        type: 'login',
        success: false,
        reason: 'Suspended',
        method: otp ? config.loginType.biometrics : config.loginType.credentials,
        platform: platform
      }).catch((e) => logger.error(e.message));
      logger.debug(staticError.suspended.replace('{user}', user));
      throw new SuspendedError();
    }
    if (e.type === 'InternalError') {
      updateStatistics(config.statistics.t24LoginFailure, {
        user,
        date: new Date(),
        type: 'login',
        success: false,
        reason: 'T24 down',
        method: otp ? config.loginType.biometrics : config.loginType.credentials,
        platform: platform
      }).catch((e) => logger.error(e.message));
      throw new InternalError();
    }
    if (!refreshToken && e.message !== messages.authentication.inactiveUser.en) {
      logger.debug(staticError.invalidCredentials.replace('{user}', user));
      updateStatistics(config.statistics.invalidCredentialsFailure, {
        user,
        date: new Date(),
        type: 'login',
        success: false,
        reason: 'Invalid credentials',
        method: otp ? config.loginType.biometrics : config.loginType.credentials,
        platform: platform
      }).catch((e) => logger.error(e.message));
    }
    throw new AuthFailureError(messages.authentication.authFail.en);
  }
};

const wrapRefreshToken: (user: string, hidRefreshToken: string) => any = async (
  user: string,
  hidRefreshToken: string
) => {
  const newRefreshToken = { user, refreshToken: hidRefreshToken };
  const token = await JWT.encode(new JwtPayload(config.tokenInfo.issuer, newRefreshToken, 86400));
  return token;
};

export const getUserStatistics = async (user: string, tracingHeaders: any) => {
  try {
    const token = await hidClientAuthentication(tracingHeaders);
    const id = await getUserId(user, token, false, tracingHeaders);
    const auth = {
      timeout: config.promiseTimeout,
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/scim+json',
        ...tracingHeaders
      }
    };
    const url = config.hidCredentials.changePassword.replace('{{UserId}}', id);
    const record = await axios.get(url, auth);
    const statistics = record.data.statistics;
    if (statistics.lastSuccessfulDate && statistics.lastUnsuccessfulDate) {
      if (statistics.lastSuccessfulDate > statistics.lastUnsuccessfulDate)
        return {
          status: 'SUCCESS',
          date: statistics.lastSuccessfulDate,
          expiryDate: record.data.status.expiryDate,
          userId: id
        };
      else
        return {
          status: 'FAILED',
          date: statistics.lastUnsuccessfulDate,
          expiryDate: record.data.status.expiryDate,
          userId: id
        };
    }
    if (statistics.lastSuccessfulDate)
      return {
        status: 'SUCCESS',
        date: statistics.lastSuccessfulDate,
        expiryDate: record.data.status.expiryDate,
        userId: id
      };
    if (statistics.lastUnsuccessfulDate)
      return {
        status: 'FAILED',
        date: statistics.lastUnsuccessfulDate,
        expiryDate: record.data.status.expiryDate,
        userId: id
      };
    return {
      status: 'SUCCESS',
      date: new Date().toISOString(),
      expiryDate: record.data.status.expiryDate,
      userId: id
    };
  } catch (e) {
    logHIDError(e, user);
    logger.debug('Error in get statistics for user ' + user);
    logger.debug(e.message);
    return {
      status: 'SUCCESS',
      date: new Date().toISOString()
    };
  }
};

export const revokeToken = async (authorization: string, tracingHeaders: any) => {
  try {
    authorization = authorization.split(' ')[1];
    const accessTokenPayload = decode(authorization);
    // @ts-ignore
    if (accessTokenPayload && accessTokenPayload.sub && accessTokenPayload.sub.token) {
      // @ts-ignore
      const accessToken = accessTokenPayload.sub.token;
      const url = config.hidCredentials.revokeUrl;
      const body = `token=${encodeURIComponent(accessToken)}`;
      const auth = {
        username: config.hidCredentials.clientId,
        password: config.hidCredentials.clientSecret
      };
      await axios.post(url, body, {
        auth,
        timeout: config.promiseTimeout,
        headers: tracingHeaders
      });
      return { message: 'Success' };
    } else throw new AuthFailureError();
  } catch (e) {
    logHIDError(e);
    logger.error(e.message);
    throw new AuthFailureError(messages.authentication.authFail.en);
  }
};

export const getLastLoginInfo = async (
  internalId: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const auth = {
      timeout: config.promiseTimeout,
      headers: {
        Authorization: `Bearer ${authorization}`,
        'Content-Type': 'application/scim+json',
        ...tracingHeaders
      }
    };
    const eventLog: any = await getUserEventLogs(internalId, auth);
    return {
      status: eventLog.values?.response || 'SUCCESS',
      date: eventLog.meta?.created || new Date().toISOString()
    };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

export const prepareKafkaResponse = async (message: KafkaMessage) => {
  if (!message.value) throw new InternalError(messages.authentication.missingValue.en);
  const data = JSON.parse(message.value.toString());
  if (!data.error) {
    return data.message.Data ? data.message.Data : data.message;
  } else {
    logger.debug(data.message);
    if (data.message === messages.authentication.registered.en)
      throw new ConflictError(messages.authentication.registered.en);
    if (data.message === messages.authentication.corporateError.en)
      throw new ConflictError(messages.authentication.corporateError.en);
    if (data.message === messages.authentication.notRegistered.en)
      throw new ForbiddenError(messages.authentication.notRegistered.en);
    if (data.message === messages.authentication.invalidOtp.en)
      throw new ForbiddenError(messages.authentication.invalidOtp.en);
    if (data.message === messages.authentication.registerConflict.en) throw new ConflictError();
    if (data.message === messages.authentication.minorUserRegistration.en)
      throw new MinorUserError(messages.authentication.minorUserRegistration.en);
    if (data.message === messages.authentication.jointUserRegistration.en)
      throw new JointUserError(messages.authentication.jointUserRegistration.en);
    else throw new InternalError(messages.authentication.authFail.en);
  }
};

export const getUserEventLogs = async (userId: string, auth: any) => {
  try {
    const body = {
      schemas: ['urn:ietf:params:scim:api:messages:2.0:SearchRequest'],
      filter: `type eq primaryAuthenticateDevice resourceUris eq ${config.hidCredentials.userEventLog}/${userId}`,
      count: 3
    };
    const eventData = await axios.post(config.hidCredentials.eventLog, body, auth);
    let decodedData: any;
    if (eventData.data && eventData.data.eventTokens && eventData.data.eventTokens.length) {
      const data = eventData.data.eventTokens;
      if (data.length === 3) {
        decodedData = decode(data[1]);
        if (decodedData && decodedData.type === 'logout') return decode(data[2]);
        else return decodedData;
      } else if (data.length === 2) {
        decodedData = decode(data[1]);
        if (decodedData && decodedData.type !== 'logout') return decodedData;
        else return {};
      }
    }
    return {};
  } catch (error) {
    logHIDError(error, userId);
    throw error;
  }
};

export const getUserInfo = async (
  user: string,
  authorization: string,
  otp: string,
  userId: string,
  platform: string,
  tracingHeaders: any
) => {
  try {
    const adminToken = await generateAdminToken(tracingHeaders);
    const data = {
      route: `digital/userinfo/${user}`,
      authorization: `Bearer ${adminToken}`
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    if (result && result.length) {
      const user = result[0];
      if (!user.Customer) user.Customer = user.MainCustomer;
      if (user.UserType === config.userTypes.corporate) {
        user.Customer = user.MainCustomer;
        user.othercifs = [];
      }
      if (user.ChannelStatus?.toLowerCase() === loginStatus.suspended) throw new SuspendedError();
      const { sector } = await checkIfValid(user.Customer, user.country, tracingHeaders);
      if (user.ChannelStatus?.toLowerCase() !== loginStatus.active) {
        updateStatistics(config.statistics.inactiveUserLoginFailure, {
          user: user.Id,
          date: new Date(),
          type: 'login',
          success: false,
          reason: 'Inactive user',
          method: otp ? config.loginType.biometrics : config.loginType.credentials,
          platform: platform
        }).catch((e) => logger.error(e.message));
        logger.debug(staticError.inactiveUser.replace('{user}', ''));
        throw new AuthFailureError(messages.authentication.inactiveUser.en);
      }
      mockAppendingAECif(user);
      return {
        id: user.Id,
        internalId: userId,
        cif: user.Customer,
        name: user.CustomerName,
        country: user.country,
        startDate: user.StartDate,
        endDate: user.EndDate,
        sector,
        registrationDate: user.RegistrationDate,
        othercifs: user.othercifs, //when we link and the othercifs would return all cifs
        allcifs: user.othercifs
      };
    } else {
      logger.debug(staticError.userNotFound.replace('{user}', user));
      throw new InternalError();
    }
  } catch (error) {
    logger.error(error.message);
    if (error.type === 'InternalError') throw new InternalError();
    if (error.type === 'SuspendedError') throw new SuspendedError();
    if (error.type === 'notRegistered')
      throw new ForbiddenError(messages.authentication.notRegistered.en);
    else throw new AuthFailureError(error.message);
  }
};

export const getUserId = async (
  user: string,
  authorization: string,
  allRecord: boolean,
  tracingHeaders: any
) => {
  try {
    const auth = {
      timeout: config.promiseTimeout,
      headers: {
        Authorization: `Bearer ${authorization}`,
        'Content-Type': 'application/scim+json',
        ...tracingHeaders
      }
    };
    const body = {
      schemas: ['urn:ietf:params:scim:api:messages:2.0:SearchRequest'],
      filter: `externalId eq ${user.toString()}`,
      sortBy: 'id',
      sortOrder: 'descending',
      startIndex: 0,
      count: 1
    };
    const searchData = await axios.post(config.hidCredentials.searchUser, body, auth);
    if (allRecord) return searchData?.data?.resources[0] || null;
    else return searchData?.data?.resources[0]?.id || null;
  } catch (error) {
    logHIDError(error, user);
    throw error;
  }
};

export const validateToken = async (
  authorization: string,
  account: string,
  otp: string,
  smsToken: string,
  route = '',
  method = '',
  correlationId: string,
  tracingHeaders: any
) => {
  let user: any;
  let token = '';
  try {
    logger.info('validating token', { correlationId });
    authorization = authorization.split(' ')[1];
    const payload = await JWT.validate(authorization);
    if (payload && payload.sub && payload.sub.token) {
      token = payload.sub.token;
      user = payload.sub.userInfo;
      await checkUserRoles(Number(user.sector), route, method.toLowerCase());
      const additionalUserInfo = await addUserInfo(
        route,
        method.toLowerCase(),
        user,
        `Bearer ${authorization}`,
        correlationId,
        tracingHeaders
      );
      if (additionalUserInfo) {
        user.phoneNumber = additionalUserInfo.phoneNumber;
        user.email = additionalUserInfo.email;
        user.dateOfBirth = additionalUserInfo.dateOfBirth;
      }
      const auth = {
        timeout: config.promiseTimeout,
        headers: {
          Authorization: `Bearer ${token}`,
          ...tracingHeaders
        }
      };
      const axiosData = await axios.get(hidCredentials.userInfo, auth);
      if (axiosData && axiosData.data) {
        if (account && account.substring(0, 8) !== user.cif) {
          throw new AuthFailureError(messages.authentication.authFail.en);
        }
        if (otp) {
          await validateOtp(otp, user.id, '1', tracingHeaders);
        }
        if (smsToken) {
          const authorization = await generateAdminToken(tracingHeaders);
          const username = await getUsernameByToken(smsToken, authorization, tracingHeaders);
          if (username !== user.id) throw new InternalError();
        }
        logger.info(`validate token success ${user?.id}-${user?.cif}-${route}`, {
          correlationId,
          status: 200
        });
        return { user, token };
      } else {
        throw new AuthFailureError(messages.authentication.authFail.en);
      }
    } else throw new AuthFailureError('not valid payload');
  } catch (error) {
    logger.info(`validate token fail ${user?.id}-${user?.cif}-${route}`, { correlationId });
    logger.error(error.message, { correlationId });
    if (error.message === messages.authentication.forbidden.en) {
      return checkBlockedOtp(user.internalId, token, tracingHeaders);
    } else throw new AuthFailureError(messages.authentication.authFail.en);
  }
};

export const resetPassword = async (
  body: {
    oldPassword: string;
    newPassword: string;
    username: string;
  },
  platform: string,
  tracingHeaders?: any
) => {
  try {
    const authentication = await generateUserToken(
      body.username,
      body.oldPassword,
      '',
      '',
      platform,
      null,
      tracingHeaders
    );
    if (authentication.tokens.accessToken) {
      await revokeToken(`Bearer ${authentication.tokens.accessToken}`, tracingHeaders);
      throw new InternalError();
    }
  } catch (error) {
    if (error && error.message === messages.authentication.passwordExpired.en) {
      return updateUserPassword(body.username, body.newPassword, tracingHeaders);
    }
    logger.error(error.message);
    throw new InternalError(messages.authentication.authFail.en);
  }
};

export const updateUserPassword = async (
  username: string,
  password: string,
  tracingHeaders: any
) => {
  try {
    const authorization = await hidClientAuthentication(tracingHeaders);
    const userId = await getUserId(username, authorization, false, tracingHeaders);
    const body = {
      schemas: ['urn:hid:scim:api:idp:2.0:Authenticator', 'urn:hid:scim:api:idp:2.0:Password'],
      id: `${userId}.AT_CUSTPW`,
      status: {
        status: 'ENABLED',
        active: true
      },
      'urn:hid:scim:api:idp:2.0:Password': {
        username,
        password
      }
    };
    const url = config.hidCredentials.changePassword.replace('{{UserId}}', userId);
    await axios.put(url, body, {
      timeout: config.promiseTimeout,
      headers: {
        'Content-Type': 'application/scim+json',
        Authorization: `Bearer ${authorization}`,
        ...tracingHeaders
      }
    });
    return { message: 'Success' };
  } catch (e) {
    console.log(e);
    logHIDError(e, username);
    logger.error(e.message);
    if (e.response?.data?.errorCode === config.hidCredentials.invalidPasswordCode)
      throw new InvalidPasswordError(messages.authentication.invalidPassword.en);
    else throw new InternalError(messages.authentication.authFail.en);
  }
};

export const hidClientAuthentication = async (tracingHeaders: any) => {
  try {
    const url = config.hidCredentials.loginUrl;
    const body = `grant_type=client_credentials&scope=openid`;
    const auth = {
      username: config.hidCredentials.clientId,
      password: config.hidCredentials.clientSecret
    };
    const result = await axios.post(url, body, {
      auth,
      timeout: config.promiseTimeout,
      headers: tracingHeaders
    });
    return result.data.access_token;
  } catch (error) {
    logHIDError(error);
    throw error;
  }
};

export const getUserProfile = async (
  user: any,
  authorization: string,
  correlationId: string,
  tracingHeaders: any
) => {
  try {
    const mainCifCountry = user.country;
    const requests = user.allcifs.map(async (cifObj: { country: string; cif: string }) => {
      const currentCif = parseInt(cifObj.cif);
      const currentCountry = cifObj.country;
      try {
        if (config.AEError && cifObj.country === 'AE') throw new Error('AE is down');
        if (config.EGError && cifObj.country === 'EG') throw new Error('EG is down');
        const data = {
          route: `customer/${currentCif}`,
          authorization,
          correlationId: `${correlation.getId()}-${cifObj.cif}`,
          country: currentCountry
        };
        const producerData = await produce(data, tracingHeaders);
        const kafkaData = await prepareKafkaResponse(producerData);
        const singleResponse = kafkaData[0] ? kafkaData[0] : {};
        singleResponse.country = currentCountry;
        return singleResponse;
      } catch (error) {
        logger.error(`in ${currentCountry}: ${error.message}`);
        throw new Error(`${currentCountry}`);
      }
    });

    const results: any = [];
    let notFetched: string | null = null;
    const res = await promiseAllsettled(requests);

    res.forEach((result) => {
      if (result.status === 'fulfilled') results.push(result.value);
      else {
        notFetched = (result.reason as Error).message;
      }
    });

    const allError = res.every((result) => {
      return result.status !== 'fulfilled';
    });

    if (allError) throw new InternalError(messages.profile.allCountries.en);

    let response: any = { otherCifsData: [], notFetched };
    results.forEach((result: any) => {
      if (result.country === mainCifCountry) {
        response = { ...response, ...result };
      } else {
        response.otherCifsData.push(result);
      }
    });
    return response;
  } catch (error) {
    logger.error(error.message);
    throw new AuthFailureError(messages.profile.internalError.en);
  }
};

export const validateOtp = async (
  otp: string,
  username: string,
  noToken: string,
  tracingHeaders: any
) => {
  try {
    const url = config.hidCredentials.loginUrl;
    const body = `mode=SYNCHRONOUS&password=${otp}&grant_type=password&authType=AT_CUSTOTP&client_id=${config.hidCredentials.clientId}&username=${username}&scope=openid%20offline_access&noToken=${noToken}`;
    const auth = {
      username: config.hidCredentials.clientId,
      password: config.hidCredentials.clientSecret
    };
    const result = await axios.post(url, body, {
      auth,
      timeout: config.promiseTimeout,
      headers: { 'Content-Type': 'application/x-www-form-urlencoded', ...tracingHeaders }
    });
    return result || {};
  } catch (e) {
    logger.error(e);
    logHIDError(e, username);
    if (e.response?.data?.hid_failure?.reason === config.hidCredentials.blockedLogin) {
      sendSms(username, '', 'blockedToken', tracingHeaders).catch((e) => logger.error(e.message));
    }
    if (e.response?.data?.hid_failure?.reason === config.hidCredentials.duplicateCode)
      throw new SessionError(messages.authentication.duplicateSession.en);
    else throw new ForbiddenError();
  }
};

export const changePasswordUsingSms = async (
  cif: string,
  username: string,
  body: { oldPassword: string; newPassword: string; token: string; smsOtp: string },
  tracingHeaders: any
) => {
  try {
    const url = config.hidCredentials.loginUrl;
    const hidBody = `grant_type=password&username=${username}&password=${body.oldPassword}&scope=openid&noToken=1`;
    const auth = {
      username: config.hidCredentials.clientId,
      password: config.hidCredentials.clientSecret
    };
    await axios.post(url, hidBody, {
      auth,
      timeout: config.promiseTimeout,
      headers: tracingHeaders
    });
    await updateUserPassword(username, body.newPassword, tracingHeaders);
    return { message: 'Success' };
  } catch (e) {
    logHIDError(e, username);
    logger.debug(e.message);
    if (e.message === messages.authentication.invalidPassword.en) throw new InvalidPasswordError();
    else throw new InternalError();
  }
};

export const changePasswordWithSmsOtp = async (
  cif: string,
  username: string,
  body: { oldPassword: string; newPassword: string; token: string; smsOtp: string },
  tracingHeaders: any
) => {
  try {
    await validateTokenOtp(
      {
        Token: body.token,
        OTP: body.smsOtp,
        Subject: 'PASSWORD_CHANGE'
      },
      tracingHeaders
    );

    await changePassword(
      cif,
      username,
      {
        oldPassword: body.oldPassword,
        newPassword: body.newPassword
      },
      tracingHeaders
    );

    return { message: 'Success' };
  } catch (e) {
    logHIDError(e, username);
    logger.debug(e.message);
    if (e.message === messages.authentication.invalidPassword.en) throw new InvalidPasswordError();
    else throw new InternalError();
  }
};

export const changePassword = async (
  cif: string,
  username: string,
  body: { oldPassword: string; newPassword: string },
  tracingHeaders: any
) => {
  try {
    const url = config.hidCredentials.loginUrl;
    const hidBody = `grant_type=password&username=${username}&password=${body.oldPassword}&scope=openid&noToken=1`;
    const auth = {
      username: config.hidCredentials.clientId,
      password: config.hidCredentials.clientSecret
    };
    await axios.post(url, hidBody, {
      auth,
      timeout: config.promiseTimeout,
      headers: tracingHeaders
    });
    await updateUserPassword(username, body.newPassword, tracingHeaders);
    return { message: 'Success' };
  } catch (e) {
    logHIDError(e, username);
    logger.debug(e.message);
    if (e.message === messages.authentication.invalidPassword.en) throw new InvalidPasswordError();
    else throw new InternalError();
  }
};

export const sendSmsForChangePassword = async (
  username: string,
  country = 'EG',
  tracingHeaders: any
) => {
  try {
    await getPublicUserInfo(username, tracingHeaders);
    const authorization = await generateAdminToken(tracingHeaders);
    const data = {
      route: `admin/sendpwdotp`,
      authorization: `Bearer ${authorization}`,
      method: 'post',
      body: { Type: 'Username', Key: username, Action: 'PASSWORD_CHANGE' },
      country
    };
    const producerData = await produce(data, tracingHeaders);
    const kafkaResponse = await prepareKafkaResponse(producerData);
    return {
      token: kafkaResponse?.Token || ''
    };
  } catch (e) {
    logger.debug(e.message);
    return {
      token: crypto.createHmac('sha256', config.keyPath).update(username).digest('base64')
    };
  }
};

export const forgotUsername = async (
  body: { Type: string; Key: string; NationalId: string },
  tracingHeaders: any
) => {
  try {
    const authorization = await generateAdminToken(tracingHeaders);
    const data = {
      route: `admin/sendusername`,
      authorization: `Bearer ${authorization}`,
      method: 'post',
      body
    };
    const producerData = await produce(data, tracingHeaders);
    await prepareKafkaResponse(producerData);

    updateStatistics('', {
      date: new Date(),
      success: true,
      type: 'forgot',
      subType: 'forgotUsername',
      action: 'Forgot username',
      reason: ' ',
      ...(body.Type === 'Card' ? { debitCard: body.Key } : { cif: body.Key }),
      nationalId: body.NationalId
    });

    return { message: 'Success' };
  } catch (e) {
    logger.debug(e.message);
    updateStatistics('', {
      date: new Date(),
      success: false,
      type: 'forgot',
      subType: 'forgotUsername',
      action: 'Forgot username',
      reason: ' ',
      ...(body.Type === 'Card' ? { debitCard: body.Key } : { cif: body.Key }),
      nationalId: body.NationalId
    });
    throw new InternalError();
  }
};

export const getUserPhoneNumbersByUserName = async (userName: string, tracingHeaders: any) => {
  try {
    const adminToken = await generateAdminToken(tracingHeaders);
    const userInfo = await getPublicUserInfo(userName, tracingHeaders, adminToken);
    mockAppendingAECif(userInfo);
    const ProfileData = await getUserProfile(userInfo, `Bearer ${adminToken}`, '', tracingHeaders);

    const numbersNCountries: any[] = [];
    const mainCifNumberNCountry = {
      country: ProfileData.country,
      number: ProfileData.Sms && ProfileData.Sms.length ? maskString(ProfileData.Sms[0], 3) : ''
    };
    if (ProfileData.country && ProfileData.Sms) numbersNCountries.push(mainCifNumberNCountry);
    ProfileData.otherCifsData.forEach((profile: any) => {
      const numberNCountry = {
        country: profile.country,
        number: profile.Sms && profile.Sms.length ? maskString(profile.Sms[0], 3) : ''
      };
      numbersNCountries.push(numberNCountry);
    });
    const response = {
      data: numbersNCountries,
      notFetched: ProfileData.notFetched
    };

    return response;
  } catch (e) {
    logger.error(e.message);
    throw new AuthFailureError(messages.profile.internalError.en);
  }
};

export const forgotPassword = async (
  body: { Type: string; Key: string },
  country = 'EG',
  tracingHeaders: any
) => {
  try {
    await getPublicUserInfo(body.Key, tracingHeaders);
    const authorization = await generateAdminToken(tracingHeaders);
    const data = {
      route: `admin/sendpwdotp`,
      authorization: `Bearer ${authorization}`,
      method: 'post',
      body,
      country
    };
    const producerData = await produce(data, tracingHeaders);
    const kafkaResponse = await prepareKafkaResponse(producerData);
    return {
      token: kafkaResponse?.Token || ''
    };
  } catch (e) {
    logger.debug(e.message);
    return {
      token: crypto.createHmac('sha256', config.keyPath).update(body.Key).digest('base64')
    };
  }
};

export const validateTokenOtp = async (
  body: { Token: string; OTP: string; Subject: string },
  tracingHeaders: any
) => {
  try {
    const authorization = await generateAdminToken(tracingHeaders);
    const data = {
      route: `admin/validateotp`,
      authorization: `Bearer ${authorization}`,
      method: 'post',
      correlationId: '',
      body
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    return { token: result.Token };
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const changeTokenPassword = async (
  password: string,
  token: string,
  tracingHeaders?: any
) => {
  let userInfo;
  try {
    const authorization = await generateAdminToken(tracingHeaders);
    const username = await getUsernameByToken(token, authorization, tracingHeaders);
    if (!username) throw new InternalError();
    userInfo = await getPublicUserInfo(username, tracingHeaders);
    const hidToken = await hidClientAuthentication(tracingHeaders);
    const hidUser = await getUserId(username, hidToken, true, tracingHeaders);
    const found = checkIfHidPasswordExists(hidUser);
    if (found) await updateUserPassword(username, password, tracingHeaders);
    else await createUserPassword(username, hidToken, password, tracingHeaders);

    updateStatistics('', {
      date: new Date(),
      user: userInfo.Id,
      cif: userInfo.Customer,
      success: true,
      type: 'forgot',
      subType: 'forgotPassword',
      action: 'Forgot password',
      reason: ' '
    });
    return { message: 'Success' };
  } catch (e) {
    logHIDError(e);
    logger.debug(e.message);
    updateStatistics('', {
      date: new Date(),
      user: userInfo?.Id || null,
      cif: userInfo?.Customer || null,
      success: false,
      type: 'forgot',
      subType: 'forgotPassword',
      action: 'Forgot password',
      ...(e.message === messages.authentication.invalidPassword.en
        ? { reason: 'password has been used before' }
        : { reason: 'internal error' })
    });
    if (e.message === messages.authentication.invalidPassword.en)
      throw new InvalidPasswordError(messages.authentication.invalidPassword.en);
    else throw new InternalError();
  }
};

export const getUsernameByToken = async (token: string, auth: string, tracingHeaders: any) => {
  try {
    const data = {
      route: `admin/getusername`,
      method: 'post',
      correlationId: '',
      body: {
        Token: token
      },
      authorization: `Bearer ${auth}`
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    if (result && result.Username) return result.Username;
    else return null;
  } catch (e) {
    return null;
  }
};

export const registerDevice = async (
  username: string,
  userId: string,
  type: string,
  cif: string,
  user: any,
  country: string,
  tracingHeaders?: any
) => {
  try {
    if (user.allcifs) {
      cif = user.allcifs.find((item: any) => item.country === country)?.cif;
    }
    const authorization = await hidClientAuthentication(tracingHeaders);
    const body = {
      schemas: ['urn:hid:scim:api:idp:2.0:Device'],
      externalId: `${username}-${type}-${lodash.random(100000, 999999)}`,
      type: 'DT_TDSV4',
      status: {
        status: 'PENDING',
        active: false,
        expiryDate: moment().add(1, 'year').format(dateFormat),
        startDate: moment().add().format(dateFormat)
      }
    };
    if (type === 'otp') await deleteUserOtpDevices(userId, authorization, tracingHeaders);
    const url = config.hidCredentials.registerDevice;
    const result = await axios.post(url, body, {
      timeout: config.promiseTimeout,
      headers: {
        'Content-Type': 'application/scim+json',
        Authorization: `Bearer ${authorization}`,
        ...tracingHeaders
      }
    });
    await bindingDevice(result.data.id, username, authorization, tracingHeaders);
    return registerProvision(
      result.data.id,
      userId,
      authorization,
      username,
      type,
      cif,
      country,
      tracingHeaders
    );
  } catch (error) {
    logHIDError(error, username);
    logger.error(error);
    if (error.message === 'Assignment cannot be completed. Reached maximum number of Devices') {
      await removeDevices('none', userId, tracingHeaders);
      const result: any = await registerDevice(
        username,
        userId,
        type,
        cif,
        user,
        country,
        tracingHeaders
      );
      return result;
    }
    if (error?.response?.status === config.hidCredentials.conflictCode) {
      const authorization = await hidClientAuthentication(tracingHeaders);
      const deviceInfo = await getOldDevices(username, authorization, tracingHeaders);
      if (deviceInfo?.deviceId) {
        await bindingDevice(deviceInfo.deviceId, username, authorization, tracingHeaders);
        return registerProvision(
          deviceInfo.deviceId,
          userId,
          authorization,
          username,
          type,
          cif,
          country,
          tracingHeaders
        );
      }
    }
    updateStatistics('', {
      cif,
      user: username,
      date: new Date(),
      type: 'activationCode',
      subtype: type || 'device',
      success: false
    }).catch((e) => logger.error(e.message));
    logger.error(error.message);
    throw new InternalError();
  }
};

export const deleteUserOtpDevices = async (
  userId: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const body = {
      schemas: ['urn:hid:scim:api:idp:2.0:Authenticator'],
      owner: { value: userId },
      policy: { value: 'AT_CUSTOTP' }
    };
    const url = config.hidCredentials.deleteOtpAuthenticatorUrl.replace('{{userId}}', userId);
    await axios.delete(url, {
      data: body,
      timeout: config.promiseTimeout,
      headers: {
        'Content-Type': 'application/scim+json',
        Authorization: `Bearer ${authorization}`,
        ...tracingHeaders
      }
    });
  } catch (e) {
    logHIDError(e, userId);
    logger.error(e);
    if (e.response.data.status === '404') logger.debug('An authenticator could not be found');
    else throw new InternalError(messages.authentication.authFail.en);
  }
};

export const bindingDevice = async (
  id: string,
  username: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const body = {
      schemas: ['urn:hid:scim:api:idp:2.0:Device'],
      owner: { display: username }
    };
    const url = config.hidCredentials.bindingDevice.replace('{id}', id);
    await axios.put(url, body, {
      timeout: config.promiseTimeout,
      headers: {
        'Content-Type': 'application/scim+json',
        Authorization: `Bearer ${authorization}`,
        ...tracingHeaders
      }
    });
  } catch (e) {
    logHIDError(e, username);
    logger.error(e.message);
    if (e?.response?.data?.errorCode === 6068) throw new MaxDeviceError();
    throw new InternalError(messages.authentication.authFail.en);
  }
};

export const registerProvision = async (
  id: string,
  userId: string,
  authorization: string,
  username: string,
  type: string,
  cif: string,
  country: string,
  tracingHeaders: any
) => {
  try {
    const body = {
      schemas: ['urn:hid:scim:api:idp:2.0:Provision'],
      deviceType: 'DT_TDSV4',
      description: `did=${id},url=${config.hidCredentials.hostName}/${config.hidCredentials.tenantId},pch=CH_TDSPROV,pth=AT_TDSOOB,pct=CT_TDSOOB,pdt=DT_TDSOOB,mod=GEN,sec=`,
      owner: {
        value: userId
      },
      attributes: [
        {
          name: 'AUTH_TYPE',
          type: null,
          value: 'AT_SMK',
          readOnly: false
        }
      ]
    };
    const url = config.hidCredentials.provisionDevice;
    const result = await axios.post(url, body, {
      timeout: config.promiseTimeout,
      headers: {
        'Content-Type': 'application/scim+json',
        Authorization: `Bearer ${authorization}`,
        ...tracingHeaders
      }
    });
    const attributes = JSON.parse(result.data?.attributes[0]?.value || '{}');
    const base64Token = Buffer.from(attributes.pss, 'base64');
    sendSms(username, base64Token.toString('utf-8'), type, tracingHeaders, country).catch((e) => {
      logger.debug(e.message);
    });
    updateStatistics('', {
      cif,
      user: username,
      date: new Date(),
      type: 'activationCode',
      subtype: type || 'device',
      success: true
    }).catch((e) => logger.error(e.message));
    return { message: 'Success' };
  } catch (e) {
    logHIDError(e, userId);
    logger.error(e.message);
    throw new InternalError(messages.authentication.authFail.en);
  }
};

export const getOldDevices = async (
  username: string,
  authorization: string,
  tracingHeaders: any
) => {
  const userInfo = await getUserId(username, authorization, true, tracingHeaders);
  let deviceId: any;
  if (userInfo['urn:hid:scim:api:idp:2.0:UserDevice']?.devices) {
    const devices = userInfo['urn:hid:scim:api:idp:2.0:UserDevice']?.devices;
    devices.map((device: any) => {
      if (device.display.startsWith(username)) {
        deviceId = device.value;
      }
    });
  }
  return { deviceId };
};

export const checkUserDevices = async (username: string, auth: string, tracingHeaders: any) => {
  try {
    let found = false;
    const user = await getUserId(username, auth, true, tracingHeaders);
    const devices = user['urn:hid:scim:api:idp:2.0:UserDevice']?.devices || [];
    const token = await hidClientAuthentication(tracingHeaders);
    if (devices.length) {
      let result: any;
      for (const device of devices) {
        result = await axios.get(device.$ref, {
          timeout: config.promiseTimeout,
          headers: { Authorization: `Bearer ${token}`, ...tracingHeaders }
        });
        device.active = result?.data?.status?.active;
        if (result?.data?.status?.active) found = true;
      }
      return { registered: found };
    } else return { registered: false };
  } catch (e) {
    logHIDError(e, username);
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const deleteUserDevices = async (username: string, tracingHeaders: any) => {
  try {
    const token = await hidClientAuthentication(tracingHeaders);
    const userInfo = await getUserId(username, token, true, tracingHeaders);
    const devices = userInfo['urn:hid:scim:api:idp:2.0:UserDevice']?.devices || [];
    if (devices.length) {
      let result: any;
      let filtered: any = [];
      for (const device of devices) {
        result = await axios.get(device.$ref, {
          timeout: config.promiseTimeout,
          headers: { Authorization: `Bearer ${token}`, ...tracingHeaders }
        });
        device.active = result?.data?.status?.active;
        device.created = result?.data?.meta?.created;
        if (device.active) filtered.push(device);
      }
      filtered = lodash.orderBy(filtered, ['created'], ['desc']);
      filtered.shift();
      if (filtered.length) await deleteActiveDevices(filtered, token, tracingHeaders);
      return { message: 'Success' };
    } else return { message: 'Success' };
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const deleteActiveDevices = async (devices: any, token: string, tracingHeaders: any) => {
  try {
    for (const device of devices) {
      const body = {
        schemas: ['urn:hid:scim:api:idp:2.0:Device'],
        owner: {
          value: '',
          display: ''
        }
      };
      await axios.put(device.$ref, body, {
        timeout: config.promiseTimeout,
        headers: {
          'Content-Type': 'application/scim+json',
          Authorization: `Bearer ${token}`,
          ...tracingHeaders
        }
      });
      await axios.delete(device.$ref, {
        timeout: config.promiseTimeout,
        headers: {
          'Content-Type': 'application/scim+json',
          Authorization: `Bearer ${token}`,
          ...tracingHeaders
        }
      });
    }
    return { message: 'Success' };
  } catch (e) {
    throw new InternalError();
  }
};

export const sendSms = async (
  username: string,
  registrationCode: string,
  type: string,
  tracingHeaders: any,
  country = 'EG'
) => {
  try {
    const auth = await generateAdminToken(tracingHeaders);
    let text: string;
    switch (type) {
      case 'bio':
        text = registrationSms.bio.en.replace('{activationCode}', registrationCode);
        break;
      case 'blockedLogin':
        text = blockedSms.blockedLogin.en;
        break;
      case 'blockedToken':
        text = blockedSms.blockedToken.en;
        break;
      default:
        text = registrationSms.device.en.replace('{activationCode}', registrationCode);
    }
    const data = {
      route: `admin/sendsms`,
      method: 'post',
      body: {
        Type: type === 'bio' || 'otp' ? 'UserOTP' : 'User',
        Key: username,
        Text: text
      },
      country,
      authorization: `Bearer ${auth}`
    };
    const producerData = await produce(data, tracingHeaders);
    await prepareKafkaResponse(producerData);
    return { message: 'Success' };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e);
  }
};

export const resendOtp = async (token: string, tracingHeaders: any) => {
  try {
    const auth = await generateAdminToken(tracingHeaders);
    const data = {
      route: `admin/resendotp`,
      method: 'post',
      body: {
        Token: token
      },
      authorization: `Bearer ${auth}`
    };
    const producerData = await produce(data, tracingHeaders);
    await prepareKafkaResponse(producerData);
    return { message: 'Success' };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const sendActivateOtp = async (
  user: string,
  cif: string,
  authorization: string,
  settings: { token: string; type: string; expiryDate: string },
  tracingHeaders: any
) => {
  try {
    const expiryDate = await getCardExpiryDate(settings, authorization, cif, tracingHeaders);
    if (expiryDate !== settings.expiryDate) throw new InternalError();
    const adminAuthorization = await generateAdminToken(tracingHeaders);
    const body = {
      Type: 'Username',
      Key: user
    };
    const data = {
      route: `admin/sendactivateotp`,
      method: 'post',
      authorization: `Bearer ${adminAuthorization}`,
      body
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    return { token: result.Token };
  } catch (e) {
    logger.debug(e.message);
    if (e.message === messages.authentication.maxAttempt.en) throw new MaxAttemptError();
    else throw new InternalError();
  }
};

export const authenticateByPassword = async (
  user: string,
  password: string,
  refreshToken: string,
  tracingHeaders: any
) => {
  try {
    const url = config.hidCredentials.loginUrl;
    let body;
    if (!refreshToken)
      body = `grant_type=password&username=${user}&password=${password}&scope=openid%20offline_access`;
    else
      body = `grant_type=refresh_token&refresh_token=${refreshToken}&scope=openid%20offline_access`;
    const auth = {
      username: config.hidCredentials.clientId,
      password: config.hidCredentials.clientSecret
    };

    return axios.post(url, body, {
      auth,
      timeout: config.promiseTimeout,
      headers: tracingHeaders
    });
  } catch (e) {
    logHIDError(e, user);
    logger.debug(e.message);
    throw new AuthFailureError();
  }
};

export const checkBlockedOtp = async (user: string, authorization: string, tracingHeaders: any) => {
  try {
    const url = `${config.hidCredentials.addPassword}/${user}.AT_CUSTOTP`;
    const result = await axios.get(url, {
      timeout: config.promiseTimeout,
      headers: {
        'Content-Type': 'application/scim+json',
        Authorization: `Bearer ${authorization}`,
        ...tracingHeaders
      }
    });
    const consecutiveFailed = Number(result?.data?.statistics?.consecutiveFailed || '0');
    const maxNumber = await getCustomerOtpRules(authorization, tracingHeaders);
    if (consecutiveFailed >= maxNumber) {
      throw new ForbiddenError(messages.authentication.blockedOtp.en);
    } else throw new ForbiddenError(messages.authentication.authFail.en);
  } catch (e) {
    logHIDError(e, user);
    logger.error(e.message);
    if (e.message === messages.authentication.blockedOtp.en) {
      throw new ForbiddenError(messages.authentication.blockedOtp.en);
    } else throw new ForbiddenError(messages.authentication.invalidOtp.en);
  }
};

export const getCustomerOtpRules = async (authorization: string, tracingHeaders: any) => {
  try {
    const url = config.hidCredentials.customerOtp;
    const result = await axios.get(url, {
      timeout: config.promiseTimeout,
      headers: { Authorization: `Bearer ${authorization}`, ...tracingHeaders }
    });
    return Number(
      result?.data['urn:hid:scim:api:idp:2.0:policy:authenticator:Credential']?.disableThreshold ||
        '1'
    );
  } catch (e) {
    logHIDError(e);
    logger.error(e.message);
    throw new InternalError();
  }
};

export const getCardExpiryDate = async (
  settings: {
    token: string;
    type: string;
    expiryDate: string;
  },
  authorization: string,
  cif: string,
  tracingHeaders: any
) => {
  try {
    await validateExpiryDateCounter(settings, cif);
    let route = `creditcards/contract/${settings.token}`;
    let topic = kafkaTopics.niRequest;
    if (settings.type === 'debitCard') {
      route = `debitcards/cardtoken/${settings.token.replace('?', '%3F')}`;
      topic = kafkaTopics.euronetRequest;
    }
    const data = {
      route,
      topic,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    if (result.length) return moment(result[0]?.ExpiryDate?.toString()).format('YY/MM');
    else return '';
  } catch (e) {
    if (e.message === messages.authentication.maxAttempt.en)
      throw new MaxAttemptError(messages.authentication.maxAttempt.en);
    else throw new InternalError();
  }
};

export const validateExpiryDateCounter = async (
  settings: {
    token: string;
    type: string;
    expiryDate: string;
  },
  cif: string
) => {
  try {
    const record = await model.findOne({ cif, token: settings.token });
    if (record && record.counter >= config.defaultCounter) {
      throw new InternalError();
    }
    return model.updateOne(
      { cif, token: settings.token },
      { $inc: { counter: 1 } },
      { upsert: true }
    );
  } catch (e) {
    throw new ForbiddenError(messages.authentication.maxAttempt.en);
  }
};

export const getPublicUserInfo = async (user: string, tracingHeaders: any, adminToken?: string) => {
  try {
    const adminAuth = adminToken || (await generateAdminToken(tracingHeaders));
    const data = {
      route: `digital/userinfo/${user}`,
      authorization: `Bearer ${adminAuth}`
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    if (result && result.length) {
      result[0].allcifs = result[0].othercifs;
      const userData = result[0];
      if (userData.ChannelStatus?.toLowerCase() !== loginStatus.active) {
        logger.debug(`Forgot password - User ${user} is not active`);
        throw new AuthFailureError();
      }
      return userData;
    } else throw new InternalError();
  } catch (e) {
    throw new InternalError();
  }
};

export const checkIfHidPasswordExists = (user: any) => {
  let found = false;
  if (user && user['urn:hid:scim:api:idp:2.0:UserAuthenticator']?.authenticators) {
    const authenticators = user['urn:hid:scim:api:idp:2.0:UserAuthenticator'].authenticators;
    authenticators.map((item: any) => {
      if (item.display === 'AT_CUSTPW') {
        found = true;
      }
    });
  }
  if (!user) {
    logger.debug('forgot password - User not found');
  }
  return found;
};

export const checkIfValid = async (cif: string, country: string, tracingHeaders: any) => {
  try {
    const authorization = await generateAdminToken(tracingHeaders);
    const data = {
      route: `customer/${cif}`,
      authorization: `Bearer ${authorization}`,
      country
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    if (country === 'AE') result[0].Sector = result[0].MainSector;
    const customer = result[0];
    if (lodash.inRange(Number(customer.Sector), 1000, allSector === 'disable' ? 1999 : 4999))
      return { sector: customer.Sector };
    else throw new AuthFailureError();
  } catch (e) {
    logger.debug(`Login - User sector should be between 1000 and 4999`);
    throw new AuthFailureError();
  }
};

export const sendEmailService = async (message: any) => {
  try {
    if (!message.value) throw new InternalError(messages.authentication.missingValue.en);
    const data = JSON.parse(message.value.toString());
    if (!data.template) data.template = 'loan';
    // @ts-ignore
    const templateConfig = config.emailTemplates[data.template];
    const fn = pug.compileFile(templateConfig.template);
    const html = fn({
      ...data.body,
      cif: data.cif
    });
    const transporter = nodemailer.createTransport({
      host: config.smtpCredentials.host,
      port: Number(config.smtpCredentials.port),
      ...(config.smtpCredentials.username &&
        config.smtpCredentials.password && {
          auth: {
            user: config.smtpCredentials.username,
            pass: config.smtpCredentials.password
          }
        }),
      tls: { rejectUnauthorized: false }
    });
    await transporter.sendMail({
      from: config.smtpCredentials.email.from,
      to: config.smtpCredentials.email.to,
      subject: templateConfig.subject.replace('{cif}', data.cif),
      attachments: templateConfig.attachments,
      html
    });
    logger.info('Success send email', {
      correlationId: message.headers.correlationId.toString(),
      status: 200
    });
  } catch (e) {
    logger.error('error in sending email', {
      correlationId: message.headers.correlationId.toString(),
      status: 200
    });
    logger.error(e.message);
    return null;
  }
};

export const checkUserRoles = async (sector: number, route: string, method: string) => {
  try {
    let found = false;
    if (lodash.inRange(sector, corporateRange.min, corporateRange.max)) {
      config.retailRoutes.forEach((retailRoute: any) => {
        if (route && route.includes(retailRoute.route) && retailRoute.methods.indexOf(method) >= 0)
          found = true;
      });
    }
    if (sector === 1003 || sector === 1005) {
      config.jointMinorRoutes.forEach((jointMinorRoute: any) => {
        if (
          route &&
          route.includes(jointMinorRoute.route) &&
          jointMinorRoute.methods.indexOf(method) >= 0
        )
          found = true;
      });
    }
    if (found) throw new AuthFailureError();
  } catch (e) {
    logger.error('Permission denied - corporate user cannot access this route');
    throw new AuthFailureError();
  }
};

export const addUserInfo = async (
  route: string,
  method: string,
  user: any,
  authorization: string,
  correlationId: string,
  tracingHeaders: any
) => {
  try {
    let customerInfo: any = {};
    for (const tokenRoute of config.tokenRoutes) {
      if (route && route.includes(tokenRoute.route) && tokenRoute.methods.indexOf(method) >= 0) {
        customerInfo = await getUserProfile(user, authorization, correlationId, tracingHeaders);
        if (customerInfo) {
          return {
            phoneNumber: customerInfo.Sms && customerInfo.Sms.length ? customerInfo.Sms[0] : '',
            email: customerInfo.Email && customerInfo.Email.length ? customerInfo.Email[0] : '',
            dateOfBirth: customerInfo.BirthIncorpDate || ''
          };
        }
      }
    }
    return;
  } catch (e) {
    logger.error(e);
    throw new AuthFailureError();
  }
};

export const portfolioStatementData = async (
  Cif: string,
  Month: string,
  Year: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const data = {
      route: `portfolioStatement/data?Cif=${Cif}&Month=${Month}&Year=${Year}`,
      method: 'get',
      authorization,
      isFileNet: true
    };

    const producerData = await produce(data, tracingHeaders);
    return await prepareKafkaResponse(producerData);
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};
export const sendTokenAppLoginLog = async (body: any, tracingHeaders: any) => {
  try {
    const adminToken = await generateAdminToken(tracingHeaders);
    const data = {
      route: `digital/userinfo/${body.user}`,
      authorization: `Bearer ${adminToken}`
    };

    const producerData = await produce(data, tracingHeaders);
    const userInfo = await prepareKafkaResponse(producerData);
    updateStatistics('', {
      date: new Date(),
      user: body.user,
      cif: userInfo[0].Customer,
      type: 'tokenApp',
      subtype: body.subtype,
      success: body.success,
      reason: body.reason || ''
    });
    return { message: 'Success' };
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const portfolioStatement = async (
  Cif: string,
  Month: string,
  Year: string,
  email: string,
  phoneNumber: string,
  dateOfBirth: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    if (!email || !email.length) throw new InternalError(messages.authentication.emailNotFound.en);
    const data = {
      topic: kafkaTopics.smtp,
      route: `portfolioStatement?Cif=${Cif}&Month=${Month}&Year=${Year}`,
      cif: Cif,
      email: email,
      phoneNumber: phoneNumber,
      dateOfBirth: dateOfBirth,
      method: 'get',
      authorization,
      function: 'sendPortfolioStatement'
    };
    await produce(data, tracingHeaders, false);
    return { message: 'Success' };
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const updateCustomerInfo = async (
  cif: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    let data: any = {
      route: `customer/${cif}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    data = {
      topic: kafkaTopics.customerResponse,
      body: {
        data: result[0]
      },
      authorization,
      function: 'updateCustomerInfo'
    };
    await produce(data, tracingHeaders, false);
    return { message: 'Success' };
  } catch (e) {
    logger.debug(e.message);
  }
};

export const removeDevices = async (
  exceptType: string,
  internalId: string,
  tracingHeaders: any
) => {
  try {
    const token = await hidClientAuthentication(tracingHeaders);
    const devices: Array<any> = await getDevicesByInternalId(internalId, token, tracingHeaders);
    const deletePromises = devices
      .filter((device) => {
        if (exceptType === 'bio') return !device.externalId.includes('bio');
        else if (exceptType === 'otp') return device.externalId.includes('bio');
        else return true;
      })
      .map((device) => deleteDevice(device.id, token, tracingHeaders));
    await Promise.all(deletePromises);
    return {
      message: 'Success'
    };
  } catch (e) {
    logHIDError(e);
    logger.debug(e.message);
  }
};

export const deleteDevice = async (
  deviceId: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const url = config.hidCredentials.unAssignDevice.replace('{{deviceId}}', deviceId);
    const body = {
      schemas: ['urn:hid:scim:api:idp:2.0:Device'],
      status: {
        status: 'ACTIVE'
      },
      owner: {
        value: ''
      }
    };
    const axiosConfig = {
      timeout: config.promiseTimeout,
      headers: {
        Authorization: `Bearer ${authorization}`,
        'Content-Type': 'application/scim+json',
        ...tracingHeaders
      }
    };
    return axios.put(url, body, axiosConfig);
  } catch (e) {
    logHIDError(e);
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const getDevicesByInternalId = async (
  internalId: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const url = config.hidCredentials.searchDevices;
    const body = {
      schemas: ['urn:ietf:params:scim:api:messages:2.0:SearchRequest'],
      filter: `owner.value eq ${internalId}`,
      sortBy: 'id',
      sortOrder: 'descending',
      startIndex: 0,
      count: 100
    };
    const axiosConfig = {
      timeout: config.promiseTimeout,
      headers: {
        Authorization: `Bearer ${authorization}`,
        'Content-Type': 'application/scim+json',
        ...tracingHeaders
      }
    };
    const result = await axios.post(url, body, axiosConfig);
    return result.data.resources;
  } catch (e) {
    logHIDError(e);
    logger.debug(e.message);
    throw new InternalError();
  }
};

import {
  InternalError,
  ConflictError,
  ForbiddenError,
  ValidateUserError,
  MinorUserError,
  JointUserError
} from '../../core/ApiError';
import { logHIDError, logger } from '../../core/Logger';
import { produce } from '../../kafka';
import generateAdminToken from '../../helpers/adminToken';
import { prepareKafkaResponse, getUserId } from '../access';
import axios from 'axios';
import moment from 'moment';
import updateStatistics from '../../helpers/statistics';
import * as config from '../../config';
import messages from '../../messages';
import { kafkaTopics } from '../../config';
import { registrationCountries } from '../../data/registrationCountry';
import { generateUserToken } from '../access/index';

export const validateNewUser = async (
  body: { Type: string; Key: string; NationalId: string; Id: string },
  tracingHeaders: any,
  country?: string
) => {
  try {
    const authorization = await generateAdminToken(tracingHeaders);
    if (body.Type == 'Cif') {
      const data = {
        topic: 't24Request',
        route: `customer/${body.Key}`,
        authorization: `Bearer ${authorization}`
      };
      const producerData = await produce(data, tracingHeaders);
      const result = await prepareKafkaResponse(producerData);
      if (!result[0]) {
        throw new InternalError('cif does not exist');
      }
      if (result[0]?.LegalId != body.NationalId) {
        throw new InternalError('invalid legal id for custmer');
      }
    }

    const data = {
      route: `admin/register/validate`,
      method: 'post',
      authorization: `Bearer ${authorization}`,
      body,
      country
    };

    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    return { token: result.Token, username: result.Username, otpIdentity: result.OTPIdentity };
  } catch (e) {
    logger.debug(e.message);
    if (e.message === messages.authentication.corporateError.en) throw new ValidateUserError();

    if (e.message === messages.authentication.minorUserRegistration.en) {
      throw new MinorUserError(
        'Minor Customers are not allowed to have a mobile banking user. Please contact us for any inquiries or requests.'
      );
    }
    if (e.message === messages.authentication.jointUserRegistration.en) {
      throw new JointUserError(
        'Joint Customers are not allowed to have a mobile banking user. Please contact us for any inquiries or requests.'
      );
    } else throw new InternalError();
  }
};

export const registerNewUser = async (
  body: {
    Token: string;
    OTP: string;
    Username: string;
    password: string;
  },
  tracingHeaders?: any,
  country?: string
) => {
  try {
    const authorization = await generateAdminToken(tracingHeaders);
    await createT24User(body, `Bearer ${authorization}`, tracingHeaders, country);
    const hidToken = await hidClientAuthentication(tracingHeaders);
    const hidUser = await getUserId(body.Username, hidToken, false, tracingHeaders);
    if (hidUser) await removeHidUser(hidUser, hidToken);
    return createHidUser(body, hidToken, tracingHeaders);
  } catch (e) {
    logHIDError(e, body?.Username || '');
    logger.error(e);
    if (e.message === messages.authentication.conflict.en) {
      throw new ConflictError();
    } else if (e.message === messages.authentication.invalidOtp.en) {
      throw new ForbiddenError(messages.authentication.invalidOtp.en);
    } else throw new InternalError();
  }
};

export const linkToExistingUser = async (
  body: {
    Token: string;
    OTP: string;
    Username: string;
  },
  refreshToken: string,
  platform: string,
  authorization: string,
  tracingHeaders?: any,
  country?: string
) => {
  try {
    await createT24User(body, authorization, tracingHeaders, country);
    return generateUserToken(body.Username, '', refreshToken, '', platform, null, tracingHeaders);
  } catch (e) {
    logger.error(e);
    if (e.message === messages.authentication.invalidOtp.en) {
      throw new ForbiddenError(messages.authentication.invalidOtp.en);
    } else throw new InternalError();
  }
};

export const hidClientAuthentication = async (tracingHeaders: any) => {
  try {
    const url = config.hidCredentials.loginUrl;
    const body = `grant_type=client_credentials&scope=openid`;
    const auth = {
      username: config.hidCredentials.clientId,
      password: config.hidCredentials.clientSecret
    };
    const result = await axios.post(url, body, {
      auth,
      timeout: config.promiseTimeout,
      headers: tracingHeaders
    });
    return result.data.access_token;
  } catch (error) {
    logHIDError(error);
    throw error;
  }
};

export const createHidUser = async (body: any, auth: string, tracingHeaders: any) => {
  try {
    const requestBody = {
      schemas: [
        'urn:ietf:params:scim:schemas:core:2.0:User',
        'urn:hid:scim:api:idp:2.0:Attribute',
        'urn:hid:scim:api:idp:2.0:UserDevice'
      ],
      externalId: body.Username,
      groups: [
        {
          value: 'USG_CUST'
        }
      ],
      'urn:hid:scim:api:idp:2.0:UserAttribute': {
        attributes: [
          {
            name: 'ATR_STATUS',
            type: 'string',
            value: 'ACTIVE',
            readOnly: false
          },
          {
            name: 'ATR_CHAN',
            type: 'string',
            value: 'INTERNET',
            readOnly: false
          },
          {
            name: 'ATR_LOGIN',
            type: 'string',
            value: 'Password',
            readOnly: false
          }
        ]
      }
    };
    const url = config.hidCredentials.userEventLog;
    await axios.post(url, requestBody, {
      headers: {
        Authorization: `Bearer ${auth}`,
        'Content-Type': 'application/scim+json',
        ...tracingHeaders
      }
    });
    await createUserPassword(body.Username, auth, body.password, tracingHeaders);
    updateStatistics(config.statistics.successUserCreation, {
      user: body.Username,
      date: new Date(),
      type: 'registration',
      success: true
    }).catch((e) => logger.error(e.message));
    return { message: 'Success' };
  } catch (e) {
    logHIDError(e, body?.Username || '');
    logger.error(e);
    throw new InternalError();
  }
};

export const removeHidUser = async (userId: string, auth: string) => {
  try {
    return axios.delete(config.hidCredentials.deleteUser.replace('{{userId}}', userId), {
      headers: { authorization: `Bearer ${auth}` }
    });
  } catch (error) {
    logger.error(error.message);
    logHIDError(error, userId);
  }
};

export const createT24User = async (
  body: any,
  authorization: string,
  tracingHeaders: any,
  country?: string
) => {
  try {
    const data = {
      route: `admin/register/submit`,
      method: 'post',
      authorization,
      body: {
        Token: body.Token,
        OTP: body.OTP,
        Username: body.Username
      },
      country
    };
    const producerData = await produce(data, tracingHeaders);
    await prepareKafkaResponse(producerData);
    return { message: 'Success' };
  } catch (e) {
    logger.debug(e.message);
    if (e.message === messages.authentication.conflict.en) throw new ConflictError();
    if (e.message === messages.authentication.invalidOtp.en)
      throw new ForbiddenError(messages.authentication.invalidOtp.en);
    throw new InternalError();
  }
};

export const createUserPassword = async (
  username: string,
  auth: string,
  password: string,
  tracingHeaders: any
) => {
  try {
    const userId = await getUserId(username, auth, false, tracingHeaders);
    const body = {
      schemas: ['urn:hid:scim:api:idp:2.0:Authenticator'],
      policy: {
        value: 'AT_CUSTPW'
      },
      status: {
        status: 'ENABLED',
        expiryDate: moment().add(3, 'M').format('YYYY-MM-DDThh:mm:ss+00:00'),
        startDate: moment().subtract(1, 'days').format('YYYY-MM-DDThh:mm:ss+00:00')
      },
      owner: {
        value: userId
      },
      'urn:hid:scim:api:idp:2.0:Password': {
        username,
        password
      }
    };
    const url = config.hidCredentials.addPassword;
    await axios.post(url, body, {
      timeout: config.promiseTimeout,
      headers: { 'Content-Type': 'application/scim+json', Authorization: `Bearer ${auth}` }
    });
  } catch (e) {
    logHIDError(e, username);
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const requestOnboarding = async (
  body: {
    fullName: string;
    mobileNumber: string;
    productOfInterest: string;
  },
  tracingHeaders: any,
  platform: string
) => {
  try {
    const data = {
      topic: kafkaTopics.smtp,
      function: 'sendEmailService',
      body,
      template: 'newCustomerRequest'
    };
    await produce(data, tracingHeaders, false);

    updateStatistics('', {
      type: 'request',
      subType: 'newCustomerRequest',
      date: new Date(),
      microservice: 'idp',
      fullName: body.fullName,
      mobile: body.mobileNumber,
      requestDetail: body.productOfInterest,
      platform
    }).catch((e) => e.message);
    return { message: 'Success' };
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const getListRegistrationCountries = async (language = 'en') => {
  try {
    return { result: registrationCountries[language] || registrationCountries['en'] };
  } catch (e: any) {
    logger.debug(e.message);
    throw new InternalError(messages.authentication.internalError.en);
  }
};

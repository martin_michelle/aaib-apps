import mongoose from 'mongoose';

const model = new mongoose.Schema(
  {
    cif: String,
    token: String,
    counter: {
      type: Number,
      default: 0
    }
  },
  { timestamps: true }
);
model.index({ cif: 1, token: 1 });
model.index({ createdAt: 1 }, { expireAfterSeconds: 24 * 60 * 60 });
export default mongoose.model('cardsetting', model);

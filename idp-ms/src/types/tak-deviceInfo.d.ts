interface DeviceRti {
  root?: boolean;
  likelyRoot?: boolean;
  maybeRoot?: boolean;
  appLifted?: boolean;
  emulator?: boolean;
  hooking?: boolean;
  debugger?: boolean;
  appRepackaged?: boolean;
  underMitm?: boolean;
  safetyNetAttestationAvailable?: boolean;
  safetyNetAttestationTrusted?: boolean;
  basicIntegrity?: boolean;
  ctsProfileMatch?: boolean;
}

interface DeviceInstance {
  takId: string;
  userLink: string;
}

interface DeviceInfoDto {
  oem: string;
  modelName: string;
  modelId: string;
  os: string;
  osVersion: string;
  buildId: string;
  securityPatchLevel: string;
}

type TakLock = 'LOCK_REQUEST';
type TakUnlock = 'UNLOCK_REQUEST';
type TakWipe = 'WIPE_REQUEST';

export interface TakDFPVerificationResponse {
  state: string;
  userLink: string;
  deviceRti: DeviceRti;
  timestamp: string;
  trustLevel: number;
  supersededBy?: string;
  accountsSeenOnDevice?: [DeviceInstance];
  deviceInfo?: DeviceInfoDto;
}

export type TakStatus = TakLock | TakUnlock | TakWipe;

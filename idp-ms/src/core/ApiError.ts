import { Response } from 'express';
import { environment } from '../config';
import {
  AuthFailureResponse,
  AccessTokenErrorResponse,
  InternalErrorResponse,
  NotFoundResponse,
  BadRequestResponse,
  ForbiddenResponse,
  DuplicateError,
  PasswordExpired,
  Conflict,
  Validation,
  InvalidPassword,
  Suspended,
  MaxAttemptResponse,
  MinorUserErrorResponse,
  JointUserErrorResponse,
  MaxDeviceErrorResponse
} from './ApiResponse';

enum ErrorType {
  BAD_TOKEN = 'BadTokenError',
  TOKEN_EXPIRED = 'TokenExpiredError',
  UNAUTHORIZED = 'AuthFailureError',
  SUSPENDED = 'SuspendedError',
  MAX_ATTEMPTS = 'MaxAttemptsError',
  ACCESS_TOKEN = 'AccessTokenError',
  INTERNAL = 'InternalError',
  NOT_FOUND = 'NotFoundError',
  NO_ENTRY = 'NoEntryError',
  NO_DATA = 'NoDataError',
  BAD_REQUEST = 'BadRequestError',
  FORBIDDEN = 'ForbiddenError',
  DUPLICATION = 'Duplicate',
  PASSWORD_EXPIRED = 'PasswordExpired',
  CONFLICT = 'Conflict',
  VALIDATION = 'Validation',
  INVALID_PASSWORD = 'InvalidPassword',
  MINOR_USER_ERROR = 'MinorUserError',
  JOINT_USER_ERROR = 'JointUserError',
  MAX_DEVICE_ERROR = 'MaxDeviceError'
}

export abstract class ApiError extends Error {
  constructor(public type: any, public message: string = 'error') {
    super(type);
  }

  public static handle(err: ApiError, res: Response): Response {
    switch (err.type) {
      case ErrorType.BAD_TOKEN:
      case ErrorType.TOKEN_EXPIRED:
      case ErrorType.UNAUTHORIZED:
        return new AuthFailureResponse(err.message).send(res);
      case ErrorType.ACCESS_TOKEN:
        return new AccessTokenErrorResponse(err.message).send(res);
      case ErrorType.INTERNAL:
        return new InternalErrorResponse(err.message).send(res);
      case ErrorType.NOT_FOUND:
      case ErrorType.NO_ENTRY:
      case ErrorType.NO_DATA:
        return new NotFoundResponse(err.message).send(res);
      case ErrorType.BAD_REQUEST:
        return new BadRequestResponse(err.message).send(res);
      case ErrorType.FORBIDDEN:
        return new ForbiddenResponse(err.message).send(res);
      case ErrorType.DUPLICATION:
        return new DuplicateError(err.message).send(res);
      case ErrorType.PASSWORD_EXPIRED:
        return new PasswordExpired(err.message).send(res);
      case ErrorType.CONFLICT:
        return new Conflict(err.message).send(res);
      case ErrorType.VALIDATION:
        return new Validation(err.message).send(res);
      case ErrorType.INVALID_PASSWORD:
        return new InvalidPassword(err.message).send(res);
      case ErrorType.SUSPENDED:
        return new Suspended(err.message).send(res);
      case ErrorType.MAX_ATTEMPTS:
        return new MaxAttemptResponse(err.message).send(res);
      case ErrorType.MINOR_USER_ERROR:
        return new MinorUserErrorResponse(err.message).send(res);
      case ErrorType.JOINT_USER_ERROR:
        return new JointUserErrorResponse(err.message).send(res);
      case ErrorType.MAX_DEVICE_ERROR:
        return new MaxDeviceErrorResponse(err.message).send(res);
      default: {
        let message = err.message;
        // Do not send failure message in production as it may send sensitive data
        if (environment === 'production') message = 'Something wrong happened.';
        return new InternalErrorResponse(message).send(res);
      }
    }
  }
}

export class AuthFailureError extends ApiError {
  constructor(message = 'Invalid Credentials') {
    super(ErrorType.UNAUTHORIZED, message);
  }
}

export class SuspendedError extends ApiError {
  constructor(message = 'Authentication failure') {
    super(ErrorType.SUSPENDED, message);
  }
}

export class MaxAttemptError extends ApiError {
  constructor(message = 'Maximum attempts exceeded') {
    super(ErrorType.MAX_ATTEMPTS, message);
  }
}

export class InternalError extends ApiError {
  constructor(message = 'Internal error') {
    super(ErrorType.INTERNAL, message);
  }
}
export class MinorUserError extends ApiError {
  constructor(
    message = 'Minor customers are not allowed to manage their accounts. Please contact us for any inquiries or requests.'
  ) {
    super(ErrorType.MINOR_USER_ERROR, message);
  }
}

export class JointUserError extends ApiError {
  constructor(
    message = 'Joint customers are not allowed to manage their accounts. Please contact us for any inquiries or requests.'
  ) {
    super(ErrorType.JOINT_USER_ERROR, message);
  }
}

export class MaxDeviceError extends ApiError {
  constructor(message = 'Assignment cannot be completed. Reached maximum number of Devices') {
    super(ErrorType.MAX_DEVICE_ERROR, message);
  }
}

export class BadRequestError extends ApiError {
  constructor(message = 'Bad Request') {
    super(ErrorType.BAD_REQUEST, message);
  }
}

export class NotFoundError extends ApiError {
  constructor(message = 'Not Found') {
    super(ErrorType.NOT_FOUND, message);
  }
}

export class BadTokenError extends ApiError {
  constructor(message = 'Token is not valid') {
    super(ErrorType.BAD_TOKEN, message);
  }
}

export class TokenExpiredError extends ApiError {
  constructor(message = 'Token is expired') {
    super(ErrorType.TOKEN_EXPIRED, message);
  }
}

export class SessionError extends ApiError {
  constructor(message = 'Duplicate session') {
    super(ErrorType.DUPLICATION, message);
  }
}

export class PasswordExpiredError extends ApiError {
  constructor(message = 'Password expired') {
    super(ErrorType.PASSWORD_EXPIRED, message);
  }
}

export class ConflictError extends ApiError {
  constructor(message = 'Username already taken') {
    super(ErrorType.CONFLICT, message);
  }
}

export class ValidateUserError extends ApiError {
  constructor(message = 'Only allowed for individual customers') {
    super(ErrorType.VALIDATION, message);
  }
}

export class ForbiddenError extends ApiError {
  constructor(message = 'Permission denied') {
    super(ErrorType.FORBIDDEN, message);
  }
}

export class InvalidPasswordError extends ApiError {
  constructor(message = 'Invalid password') {
    super(ErrorType.INVALID_PASSWORD, message);
  }
}

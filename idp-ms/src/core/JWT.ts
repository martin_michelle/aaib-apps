import { promisify } from 'util';
import * as config from '../config';
import { sign, verify } from 'jsonwebtoken';
import { InternalError, BadTokenError, TokenExpiredError } from './ApiError';

export default class JWT {
  private static getSecret() {
    return config.tokenInfo.secret;
  }

  public static async encode(payload: JwtPayload): Promise<string> {
    const secret = await this.getSecret();
    if (!secret) throw new InternalError('Token generation failure');
    // @ts-ignore
    return promisify(sign)({ ...payload }, secret);
  }

  public static async validate(token: string): Promise<JwtPayload> {
    try {
      const secret = await this.getSecret();
      // @ts-ignore
      return await promisify(verify)(token, secret);
    } catch (e) {
      if (e && e.name === 'TokenExpiredError') throw new TokenExpiredError();
      throw new BadTokenError();
    }
  }
}

export class JwtPayload {
  sub: any;
  iss: string;
  iat: number;
  exp: number;

  constructor(issuer: string, subject: any, validity: number) {
    this.iss = issuer;
    this.sub = subject;
    this.iat = Math.floor(Date.now() / 1000);
    this.exp = this.iat + validity;
  }
}

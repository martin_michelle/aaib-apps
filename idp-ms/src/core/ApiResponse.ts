import { Response } from 'express';

// Helper code for the API consumer to understand the error and handle is accordingly
enum StatusCode {
  SUCCESS = '10000',
  FAILURE = '10001',
  RETRY = '10002',
  INVALID_ACCESS_TOKEN = '10003',
  SUSPENDED = '10004',
  MAX_ATTEMPT = '10007',
  VALIDATION_USER_ERROR = '10011',
  MINOR_USER_ERROR = '10015',
  JOINT_USER_ERROR = '10016',
  MAX_DEVICE_ERROR = '10017'
}

enum ResponseStatus {
  SUCCESS = 200,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404,
  CONFLICT = 409,
  INTERNAL_ERROR = 500,
  DUPLICATE = 601,
  EXPIRED_PASSWORD = 602,
  INVALID_PASSWORD = 603
}

abstract class ApiResponse {
  constructor(
    protected statusCode: StatusCode,
    protected status: ResponseStatus,
    protected message: string
  ) {}

  protected prepare<T extends ApiResponse>(res: Response, response: T): Response {
    return res.status(this.status).json(ApiResponse.sanitize(response));
  }

  public send(res: Response): Response {
    return this.prepare<ApiResponse>(res, this);
  }

  private static sanitize<T extends ApiResponse>(response: T): T {
    const clone: T = {} as T;
    Object.assign(clone, response);
    // @ts-ignore
    delete clone.status;
    for (const i in clone) if (typeof clone[i] === 'undefined') delete clone[i];
    return clone;
  }
}

export class AuthFailureResponse extends ApiResponse {
  constructor(message = 'Authentication Failure') {
    super(StatusCode.FAILURE, ResponseStatus.UNAUTHORIZED, message);
  }
}
export class MinorUserErrorResponse extends ApiResponse {
  constructor(
    message = 'Minor customers are not allowed to manage their accounts. Please contact us for any inquiries or requests.'
  ) {
    super(StatusCode.MINOR_USER_ERROR, ResponseStatus.UNAUTHORIZED, message);
  }
}

export class JointUserErrorResponse extends ApiResponse {
  constructor(
    message = 'Joint customers are not allowed to manage their accounts. Please contact us for any inquiries or requests.'
  ) {
    super(StatusCode.JOINT_USER_ERROR, ResponseStatus.UNAUTHORIZED, message);
  }
}
export class MaxDeviceErrorResponse extends ApiResponse {
  constructor(message: string) {
    super(StatusCode.MAX_DEVICE_ERROR, ResponseStatus.FORBIDDEN, message);
  }
}

export class NotFoundResponse extends ApiResponse {
  private url: string | undefined;

  constructor(message = 'Not Found') {
    super(StatusCode.FAILURE, ResponseStatus.NOT_FOUND, message);
  }

  send(res: Response): Response {
    this.url = res.req?.originalUrl;
    return super.prepare<NotFoundResponse>(res, this);
  }
}

export class ForbiddenResponse extends ApiResponse {
  constructor(message = 'Forbidden') {
    super(StatusCode.FAILURE, ResponseStatus.FORBIDDEN, message);
  }
}

export class DuplicateError extends ApiResponse {
  constructor(message = 'Duplicate session') {
    super(StatusCode.FAILURE, ResponseStatus.DUPLICATE, message);
  }
}

export class PasswordExpired extends ApiResponse {
  constructor(message = 'Password Expired') {
    super(StatusCode.FAILURE, ResponseStatus.EXPIRED_PASSWORD, message);
  }
}

export class Conflict extends ApiResponse {
  constructor(message = 'Username already taken') {
    super(StatusCode.FAILURE, ResponseStatus.CONFLICT, message);
  }
}

export class Validation extends ApiResponse {
  constructor(message = 'Only allowed for individual customers') {
    super(StatusCode.VALIDATION_USER_ERROR, ResponseStatus.FORBIDDEN, message);
  }
}

export class InvalidPassword extends ApiResponse {
  constructor(message = 'Invalid Password') {
    super(StatusCode.FAILURE, ResponseStatus.INVALID_PASSWORD, message);
  }
}

export class Suspended extends ApiResponse {
  constructor(message = 'Authentication Failure') {
    super(StatusCode.SUSPENDED, ResponseStatus.UNAUTHORIZED, message);
  }
}

export class MaxAttemptResponse extends ApiResponse {
  constructor(message = 'Max Attempts exceeded') {
    super(StatusCode.MAX_ATTEMPT, ResponseStatus.FORBIDDEN, message);
  }
}

export class BadRequestResponse extends ApiResponse {
  constructor(message = 'Bad Parameters') {
    super(StatusCode.FAILURE, ResponseStatus.BAD_REQUEST, message);
  }
}

export class InternalErrorResponse extends ApiResponse {
  constructor(message = 'Internal Error') {
    super(StatusCode.FAILURE, ResponseStatus.INTERNAL_ERROR, message);
  }
}

export class SuccessMsgResponse extends ApiResponse {
  constructor(message: string) {
    super(StatusCode.SUCCESS, ResponseStatus.SUCCESS, message);
  }
}

export class FailureMsgResponse extends ApiResponse {
  constructor(message: string) {
    super(StatusCode.FAILURE, ResponseStatus.SUCCESS, message);
  }
}

export class SuccessResponse<T> extends ApiResponse {
  constructor(message: string, private data: T) {
    super(StatusCode.SUCCESS, ResponseStatus.SUCCESS, message);
  }

  send(res: Response): Response {
    return super.prepare<SuccessResponse<T>>(res, this);
  }
}

export class AccessTokenErrorResponse extends ApiResponse {
  private instruction = 'refresh_token';

  constructor(message = 'Access token invalid') {
    super(StatusCode.INVALID_ACCESS_TOKEN, ResponseStatus.UNAUTHORIZED, message);
  }

  send(res: Response): Response {
    res.setHeader('instruction', this.instruction);
    return super.prepare<AccessTokenErrorResponse>(res, this);
  }
}

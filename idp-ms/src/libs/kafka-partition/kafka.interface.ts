import {
  Admin,
  AdminConfig,
  ConsumerEvents,
  ConsumerRunConfig,
  ConsumerSubscribeTopic,
  GroupDescription,
  IHeaders,
  IMemberAssignment,
  InstrumentationEvent,
  ISocketFactory,
  logCreator,
  Logger,
  logLevel,
  PartitionAssigner,
  PartitionMetadata,
  ProducerConfig,
  ProducerEvents,
  ProducerRecord,
  RemoveInstrumentationEventListener,
  RetryOptions,
  SASLOptions,
  Sender,
  TopicPartitionOffsetAndMetadata,
  TopicPartitions,
  Transaction,
  ValueOf
} from 'kafkajs';

export type Cluster = {
  findTopicPartitionMetadata(topic: string): PartitionMetadata[];
};

export type GroupMember = { memberId: string; memberMetadata: Buffer };

export type GroupMemberAssignment = {
  memberId: string;
  memberAssignment: Buffer;
};

export type GroupState = { name: string; metadata: Buffer };

export type MemberMetadata = {
  version: number;
  topics: string[];
  userData: Buffer;
};

export type BrokersFunction = () => string[] | Promise<string[]>;

export type Consumer = {
  connect(): Promise<void>;
  disconnect(): Promise<void>;
  subscribe(topic: ConsumerSubscribeTopic): Promise<void>;
  stop(): Promise<void>;
  run(config?: ConsumerRunConfig): Promise<void>;
  commitOffsets(topicPartitions: Array<TopicPartitionOffsetAndMetadata>): Promise<void>;
  seek(topicPartition: { topic: string; partition: number; offset: string }): void;
  describeGroup(): Promise<GroupDescription>;
  pause(topics: Array<{ topic: string; partitions?: number[] }>): void;
  paused(): TopicPartitions[];
  resume(topics: Array<{ topic: string; partitions?: number[] }>): void;
  on(
    eventName: ValueOf<ConsumerEvents>,
    listener: (...args: any[]) => void
  ): RemoveInstrumentationEventListener<typeof eventName>;
  events: ConsumerEvents;
  logger(): Logger;
};

export interface ConsumerConfig {
  groupId: string;
  partitionAssigners?: PartitionAssigner[];
  metadataMaxAge?: number;
  sessionTimeout?: number;
  rebalanceTimeout?: number;
  heartbeatInterval?: number;
  maxBytesPerPartition?: number;
  minBytes?: number;
  maxBytes?: number;
  maxWaitTimeInMs?: number;
  retry?: RetryOptions & {
    restartOnFailure?: (err: Error) => Promise<boolean>;
  };
  allowAutoTopicCreation?: boolean;
  maxInFlightRequests?: number;
  readUncommitted?: boolean;
  rackId?: string;
}

export type ConsumerGroupJoinEvent = InstrumentationEvent<{
  duration: number;
  groupId: string;
  isLeader: boolean;
  leaderId: string;
  groupProtocol: string;
  memberId: string;
  memberAssignment: IMemberAssignment;
}>;

export interface EachMessagePayload {
  topic: string;
  partition: number;
  message: KafkaMessage;
}

export declare class Kafka {
  constructor(config: KafkaConfig);
  producer(config?: ProducerConfig): Producer;
  consumer(config?: ConsumerConfig): Consumer;
  admin(config?: AdminConfig): Admin;
}

export interface KafkaConfig {
  brokers: string[] | BrokersFunction;
  sasl?: SASLOptions;
  clientId?: string;
  connectionTimeout?: number;
  authenticationTimeout?: number;
  reauthenticationThreshold?: number;
  requestTimeout?: number;
  enforceRequestTimeout?: boolean;
  retry?: RetryOptions;
  socketFactory?: ISocketFactory;
  logLevel?: logLevel;
  logCreator?: logCreator;
}

export type KafkaMessage = {
  key: Buffer;
  value: Buffer | null;
  timestamp: string;
  size: number;
  attributes: number;
  offset: string;
  headers?: IHeaders;
};

export type Producer = Sender & {
  connect(): Promise<void>;
  disconnect(): Promise<void>;
  isIdempotent(): boolean;
  events: ProducerEvents;
  on(
    eventName: ValueOf<ProducerEvents>,
    listener: (...args: any[]) => void
  ): RemoveInstrumentationEventListener<typeof eventName>;
  transaction(): Promise<Transaction>;
  logger(): Logger;
};

export interface KafkaParserConfig {
  keepBinary?: boolean;
}

export interface KafkaOptions {
  options?: {
    postfixId?: string;
    client?: KafkaConfig;
    consumer?: ConsumerConfig;
    run?: Omit<ConsumerRunConfig, 'eachBatch' | 'eachMessage'>;
    subscribe?: Omit<ConsumerSubscribeTopic, 'topic'>;
    producer?: ProducerConfig;
    send?: Omit<ProducerRecord, 'topic' | 'messages'>;
    parser?: KafkaParserConfig;
  };
}

export interface ReadPacket<T = any> {
  pattern: any;
  data: T;
}

export interface WritePacket<T = any> {
  err?: any;
  response?: T;
  isDisposed?: boolean;
  status?: string;
}

export interface KafkaRequest<T = any> {
  key: Buffer | string | null;
  value: T;
  headers: Record<string, any>;
}

export type ClientOptions =
  | RedisOptions
  | NatsOptions
  | MqttOptions
  | GrpcOptions
  | KafkaOptions
  | TcpClientOptions
  | RmqOptions;

export interface RedisOptions {
  options?: {
    url?: string;
    retryAttempts?: number;
    retryDelay?: number;
  } & ClientOptions;
}

export interface MqttOptions {
  options?: MqttClientOptions & {
    url?: string;
  };
}

export interface NatsOptions {
  options?: {
    authenticator?: any;
    debug?: boolean;
    ignoreClusterUpdates?: boolean;
    inboxPrefix?: string;
    encoding?: string;
    name?: string;
    user?: string;
    pass?: string;
    maxPingOut?: number;
    maxReconnectAttempts?: number;
    reconnectTimeWait?: number;
    reconnectJitter?: number;
    reconnectJitterTLS?: number;
    reconnectDelayHandler?: any;
    servers?: string[] | string;
    nkey?: any;
    reconnect?: boolean;
    pedantic?: boolean;
    tls?: any;
    queue?: string;
    userJWT?: string;
    nonceSigner?: any;
    userCreds?: any;
    useOldRequestStyle?: boolean;
    pingInterval?: number;
    preserveBuffers?: boolean;
    waitOnFirstConnect?: boolean;
    verbose?: boolean;
    noEcho?: boolean;
    noRandomize?: boolean;
    timeout?: number;
    token?: string;
    yieldTime?: number;
    tokenHandler?: any;
    [key: string]: any;
  };
}

export interface RmqOptions {
  options?: {
    urls?: string[] | RmqUrl[];
    queue?: string;
    prefetchCount?: number;
    isGlobalPrefetchCount?: boolean;
    queueOptions?: any; // AmqplibQueueOptions;
    socketOptions?: any; // AmqpConnectionManagerSocketOptions;
    noAck?: boolean;
    replyQueue?: string;
    persistent?: boolean;
  };
}

export interface GrpcOptions {
  options: {
    url?: string;
    maxSendMessageLength?: number;
    maxReceiveMessageLength?: number;
    maxMetadataSize?: number;
    keepalive?: {
      keepaliveTimeMs?: number;
      keepaliveTimeoutMs?: number;
      keepalivePermitWithoutCalls?: number;
      http2MaxPingsWithoutData?: number;
      http2MinTimeBetweenPingsMs?: number;
      http2MinPingIntervalWithoutDataMs?: number;
      http2MaxPingStrikes?: number;
    };
    channelOptions?: ChannelOptions;
    credentials?: any;
    protoPath: string | string[];
    package: string | string[];
    protoLoader?: string;
    loader?: {
      keepCase?: boolean;
      alternateCommentMode?: boolean;
      // eslint-disable-next-line @typescript-eslint/ban-types
      longs?: Function;
      // eslint-disable-next-line @typescript-eslint/ban-types
      enums?: Function;
      // eslint-disable-next-line @typescript-eslint/ban-types
      bytes?: Function;
      defaults?: boolean;
      arrays?: boolean;
      objects?: boolean;
      oneofs?: boolean;
      json?: boolean;
      includeDirs?: string[];
    };
  };
}

export interface TcpClientOptions {
  options?: {
    host?: string;
    port?: number;
  };
}

export interface MqttClientOptions {
  port?: number; // port is made into a number subsequently
  host?: string; // host does NOT include port
  hostname?: string;
  path?: string;
  protocol?: 'wss' | 'ws' | 'mqtt' | 'mqtts' | 'tcp' | 'ssl' | 'wx' | 'wxs';

  wsOptions?: {
    [x: string]: any;
  };
  /**
   *  10 seconds, set to 0 to disable
   */
  keepalive?: number;
  /**
   * 'mqttjs_' + Math.random().toString(16).substr(2, 8)
   */
  clientId?: string;
  /**
   * 'MQTT'
   */
  protocolId?: string;
  /**
   * 4
   */
  protocolVersion?: number;
  /**
   * true, set to false to receive QoS 1 and 2 messages while offline
   */
  clean?: boolean;
  /**
   * 1000 milliseconds, interval between two reconnections
   */
  reconnectPeriod?: number;
  /**
   * 30 * 1000 milliseconds, time to wait before a CONNACK is received
   */
  connectTimeout?: number;
  /**
   * the username required by your broker, if any
   */
  username?: string;
  /**
   * the password required by your broker, if any
   */
  password?: string;
  /**
   * a any for the incoming packets
   */
  incomingStore?: any;
  /**
   * a any for the outgoing packets
   */
  outgoingStore?: any;
  queueQoSZero?: boolean;
  reschedulePings?: boolean;
  servers?: Array<{
    host: string;
    port: number;
  }>;
  /**
   * true, set to false to disable re-subscribe functionality
   */
  resubscribe?: boolean;
  /**
   * a message that will sent by the broker automatically when the client disconnect badly.
   */
  will?: {
    /**
     * the topic to publish
     */
    topic: string;
    /**
     * the message to publish
     */
    payload: string;
    /**
     * the retain flag
     */
    retain: boolean;
  };
  transformWsUrl?: (url: string, options: any, client: any) => string;
}

export interface ChannelOptions {
  'grpc.ssl_target_name_override'?: string;
  'grpc.primary_user_agent'?: string;
  'grpc.secondary_user_agent'?: string;
  'grpc.default_authority'?: string;
  'grpc.service_config'?: string;
  'grpc.max_concurrent_streams'?: number;
  'grpc.initial_reconnect_backoff_ms'?: number;
  'grpc.max_reconnect_backoff_ms'?: number;
  'grpc.use_local_subchannel_pool'?: number;
  [key: string]: string | number | undefined;
}

export interface RmqUrl {
  protocol?: string;
  hostname?: string;
  port?: number;
  username?: string;
  password?: string;
  locale?: string;
  frameMax?: number;
  heartbeat?: number;
  vhost?: string;
}

export type MsFundamentalPattern = string | number;

export interface MsObjectPattern {
  [key: string]: MsFundamentalPattern | MsObjectPattern;
}

export type MsPattern = MsObjectPattern | MsFundamentalPattern;

FROM registry.newtools.digital.aaib/aaib/generic-images/node:14-alpine AS builder

WORKDIR /usr/src/app
COPY . .
RUN yarn config set registry http://npm.newtools.digital.aaib
RUN yarn install --ignore-scripts --pure-lockfile --verbose
RUN yarn build
RUN rm -rf node_modules && yarn install --ignore-scripts --pure-lockfile --production=true

FROM registry.newtools.digital.aaib/aaib/generic-images/node:14-alpine

ENV NODE_ENV=production
RUN apk add --no-cache tini
WORKDIR /usr/src/app
RUN chown node:node .
USER node
COPY --from=builder /usr/src/app/build/ build/ 
COPY --from=builder /usr/src/app/node_modules/ node_modules/
COPY --from=builder /usr/src/app/images/ images/
COPY --from=builder /usr/src/app/emails/ emails/
ENTRYPOINT [ "/sbin/tini","--", "node", "build/server.js" ]

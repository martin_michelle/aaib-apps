import * as service from '../../../src/services/registration';
import { mockKafkaSuccess } from '../../mocks/kafka';
import { mockAxiosCall } from '../../mocks/axios';

describe('Validate user', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    // mockAxiosCall('post', 'generateToken');
    // mockKafkaSuccess('validate');
    // const response = await service.validateNewUser(
    //   {
    //     Type: 'Cif',
    //     Key: '1010011',
    //     NationalId: 'testNational',
    //     Id: 'anotherCountry'
    //   },
    //   null
    // );
    // expect(response).toStrictEqual({ token: 'testToken', username: 'Charles' });
  });
});

describe('Register new user', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    mockAxiosCall('post', 'generateToken');
    mockAxiosCall('post', 'hid');
    mockAxiosCall('delete', '');
    mockKafkaSuccess('userInfo');
    mockKafkaSuccess('register');
    mockKafkaSuccess('otp');
    const response = await service.registerNewUser({
      Token: 'testToken',
      OTP: '123456',
      Username: 'testUsername',
      password: 'testPassword'
    });
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

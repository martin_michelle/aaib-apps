import * as service from '../../../src/services/access';
import { mockKafkaSuccess } from '../../mocks/kafka';
import {
  mockAxiosCall,
  finalLoginResponse,
  loginCredentials,
  profileData,
  platform
} from '../../mocks/axios';
import { mockEncode, createJWT } from '../../mocks/jwt';
import { PasswordExpired } from '../../../src/core/ApiResponse';
import { mockMongoSuccess } from '../../mocks/mongo';
let token: string;

describe('HID Authentication', () => {
  // beforeEach(() => {
  //   jest.clearAllMocks();
  // });
  // beforeAll(async () => {
  //   token = await createJWT();
  // });
  // it('Should return success', async () => {
  //   const credentials = loginCredentials;
  //   mockAxiosCall('post', '');
  //   mockAxiosCall('get', '');
  //   mockKafkaSuccess('userInfo');
  //   mockEncode(token);
  //   const response = await service.generateUserToken(
  //     credentials.username,
  //     credentials.password,
  //     '',
  //     platform.webPlatform,
  //     '',
  //     null
  //   );
  //   finalLoginResponse.tokens.accessToken = token;
  //   finalLoginResponse.statistics.date = response.statistics.date;
  //   expect(response).toStrictEqual(finalLoginResponse);
  // });
});

// describe('HID Revoke token', () => {
//   // beforeEach(() => {
//   //   jest.clearAllMocks();
//   // });

//   it('Should return success', async () => {
//     mockAxiosCall('post', '');
//     const response = await service.revokeToken(`Bearer ${token}`, null);
//     expect(response).toStrictEqual({ message: 'Success' });
//   });
// });

describe('HID Get Last Login Info', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return success', async () => {
    mockAxiosCall('post', '');
    const response = await service.getLastLoginInfo('1', `Bearer ${token}`, null);
    expect(response).toStrictEqual({ status: 'SUCCESS', date: '2021-01-01' });
  });
});

// describe('HID Validate Token', () => {
//   beforeEach(() => {
//     jest.clearAllMocks();
//   });
//   it('Should return success', async () => {
//     mockAxiosCall('get', '');
//     const response = await service.validateToken(`Bearer ${token}`, '', '', '', '', '', '', null);
//     expect(response).toStrictEqual({ token: '1234567890', user: { id: 1 } });
//   });
// });

// describe('HID Refresh Token', () => {
//   beforeEach(() => {
//     jest.clearAllMocks();
//   });
//   it('Should return success', async () => {
//     const credentials = loginCredentials;
//     mockAxiosCall('post', '');
//     mockKafkaSuccess('userInfo');
//     mockEncode(token);
//     const response = await service.generateUserToken(
//       credentials.username,
//       '',
//       '123456',
//       '123456',
//       platform.webPlatform,
//       '',
//       {
//         cif: '10100111',
//         name: 'Test User'
//       }
//     );
//     finalLoginResponse.tokens.accessToken = token;
//     finalLoginResponse.statistics.date = response.statistics.date;
//     expect(response).toStrictEqual(finalLoginResponse);
//   });
// });

describe('HID Reset password', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    const mock = jest.spyOn(service, 'generateUserToken');
    mock.mockImplementation(() => {
      throw new PasswordExpired();
    });
    mockAxiosCall('post', 'hid');
    mockAxiosCall('put', '');
    mockKafkaSuccess('userInfo');
    mockEncode(token);
    const response = await service.resetPassword(
      {
        oldPassword: '',
        newPassword: '',
        username: 'test'
      },
      platform.webPlatform
    );
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('Get user profile', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess('profile');
    const response = await service.getUserProfile(
      {
        allcifs: [
          {
            cif: '10100111',
            country: 'EG'
          }
        ],
        country: 'EG'
      },
      '',
      '',
      null
    );
    expect(response).toStrictEqual(profileData);
  });
});

describe('Change user password', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockAxiosCall('post', 'change');
    mockAxiosCall('put', '');
    const response = await service.changePassword(
      '10100111',
      'test',
      {
        oldPassword: 'old',
        newPassword: 'pass'
      },
      null
    );
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('Forgot username', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockAxiosCall('get', '');
    mockKafkaSuccess('forgotusername');
    const response = await service.forgotUsername(
      {
        Type: 'Cif',
        Key: '10100111',
        NationalId: 'test'
      },
      null
    );
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('Forgot password', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockAxiosCall('get', '');
    mockKafkaSuccess('forgotpassword');
    const response = await service.forgotPassword(
      {
        Type: 'Cif',
        Key: '10100111'
      },
      'EG',
      null
    );
    expect(response).toStrictEqual({ token: 'zCmgybXlXqauWxuDGzKGpOW8udlaoD5hlcDWPP2Tee0=' });
  });
});

describe('Validate Otp', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess('validateOtp');
    const response = await service.validateTokenOtp(
      {
        OTP: 'Cif',
        Subject: 'PASSWORD',
        Token: '10100111'
      },
      null
    );
    expect(response).toStrictEqual({ token: 'testToken' });
  });
});

describe('Check user device', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess('checkUser');
    const response = await service.checkUserDevices('test', '', null);
    expect(response).toStrictEqual({ registered: false });
  });
});

describe('delete user devices', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success (get active devices)', async () => {
    mockAxiosCall('post', 'devices');
    mockAxiosCall('get', '');
    const response = await service.deleteUserDevices('test', null);
    expect(response).toStrictEqual({ message: 'Success' });
  });

  it('Should return success (delete active devices)', async () => {
    mockAxiosCall('put', '');
    mockAxiosCall('delete', '');
    const response = await service.deleteActiveDevices(
      [{ $ref: 'https://tauth.aaib.com:443/scim/AAIBDOMAIN/v2/Device/92950' }],
      '',
      null
    );
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('resend otp', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess('resend');
    const response = await service.resendOtp('testToken', null);
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('send sms', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess('sendsms');
    const response = await service.sendSms('test', 'test', '', null);
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('Register Provision', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockAxiosCall('post', '');
    const response = await service.registerProvision(
      'did',
      'userId',
      '',
      'username',
      '',
      '',
      'EG',
      null
    );
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('Send activate otp', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess('debitCard');
    mockMongoSuccess();
    const response = await service.sendActivateOtp(
      'username',
      '123123',
      '',
      {
        type: 'debitCard',
        token: 'testToken',
        expiryDate: '21/01'
      },
      null
    );
    expect(response).toStrictEqual({ token: undefined });
  });
});

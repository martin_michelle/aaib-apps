import * as config from '../../src/config';
import axios from 'axios';
import { createEventJWT } from './jwt';
import { PasswordExpired } from '../../src/core/ApiResponse';

export const loginCredentials = {
  username: 'testinguser',
  password: 'testingpassword'
};

export const platform = {
  webPlatform: 'web',
  mobileplatform: 'mobile'
};

export const token = '3OGblwAAAXoUG5835GP+WQsxzrZFErnS4zXIfckT';

const loginData = { data: { access_token: token, refresh_token: '123456' } };

const hidData = { data: { access_token: token } };

const deviceData = { data: { status: { active: true } } };

const provision = {
  data: {
    attributes: [
      {
        value: `{"pss": "V0U4RUI3SzNWTg=="}`
      }
    ]
  }
};

const defaultResponse = { data: { message: 'Success' } };

const lastLoginInfo = { data: { resources: [{ id: 1 }] } };

export const finalLoginResponse = {
  tokens: {
    accessToken: '',
    refreshToken:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJhYWliIiwic3ViIjp7InRva2VuIjoiMTIzNDU2Nzg5MCIsInVzZXJJbmZvIjp7ImlkIjoxfSwiYWNjb3VudHMiOltdLCJ2YWx1ZXMiOnsicmVzcG9uc2UiOiJTVUNDRVNTIn19LCJpYXQiOjE2ODczNDM1NjQsImV4cCI6MTY4NzY0MzU2NH0.ZJuLiJpglXvpHS8XGQB29H18m_DBBxP_C9-Z0ECjAxo'
  },
  user: {
    id: '10100111',
    name: 'Test User',
    registrationDate: 20210101,
    isCorporate: false,
    sector: '1001',
    allcifs: [
      {
        cif: '10100111',
        country: 'EG'
      }
    ],
    country: 'EG'
  },
  statistics: {
    date: '',
    status: 'SUCCESS'
  }
};

const userIdData = { data: { resources: [{ id: 1 }] } };

export const profileData = {
  Id: 1,
  OldCifNo: '10100111',
  GivenName: 'Test User',
  Nationality: 'EG',
  country: 'EG',
  notFetched: null,
  otherCifsData: []
};

export const mockAxiosCall = (
  method: 'post' | 'get' | 'put' | 'delete',
  options: string | undefined
) => {
  const mock = jest.spyOn(axios, method);
  // @ts-ignore
  mock.mockImplementation(async (url) => {
    switch (url) {
      case config.hidCredentials.searchUser:
        return Promise.resolve(userIdData);
      case config.hidCredentials.loginUrl:
        if (options === 'expired') throw new PasswordExpired('Password Expired');
        if (options === 'hid') return Promise.resolve(hidData);
        if (options === 'change') return Promise.resolve(hidData);
        else return Promise.resolve(loginData);
      case config.hidCredentials.revokeUrl:
        return Promise.resolve({});
      case config.hidCredentials.userInfo:
        return Promise.resolve(lastLoginInfo);
      case config.hidCredentials.changePassword.replace('{{UserId}}', '1'):
        return Promise.resolve(defaultResponse);
      case `${config.t24Url}hid/generatetoken`:
        return Promise.resolve(finalLoginResponse);
      case config.hidCredentials.eventLog:
        const token = await createEventJWT();
        return Promise.resolve({ data: { eventTokens: [token, token] } });
      case config.hidCredentials.userEventLog:
        return Promise.resolve({ data: {} });
      case config.hidCredentials.deleteUser:
        return Promise.resolve({ data: {} });
      case `https://tauth.aaib.com:443/scim/AAIBDOMAIN/v2/Device/92950`:
        return Promise.resolve(deviceData);
      case config.hidCredentials.provisionDevice:
        return Promise.resolve(provision);
    }
    return Promise.resolve({});
  });
};

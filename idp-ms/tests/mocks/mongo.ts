import Model from '../../src/services/models/model';

export const mockMongoSuccess = () => {
  Model.findOne = jest.fn().mockResolvedValue({ counter: 1 });
  Model.updateOne = jest.fn().mockResolvedValue({ success: true });
};

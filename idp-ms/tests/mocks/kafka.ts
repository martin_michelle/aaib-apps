import * as kafka from '../../src/kafka';
export const produceResult = {
  login: {
    value:
      '{"error": false, "message": { "Data": [{"Id": 1, "Customer": "10100111","country":"EG", "CustomerName": "Test User", "ChannelStatus": "ACTIVE", "RegistrationDate": 20210101, "Sector": "1001", "othercifs":[{"cif": "10100111", "country":"EG"}] }] }}'
  },
  profile: {
    value:
      '{"error": false, "message": { "Data": [{"Id": 1, "OldCifNo": "10100111", "GivenName": "Test User", "Nationality": "EG" }] }}'
  },
  devices: {
    value:
      '{"error": false, "message": { "Data": [{"urn:hid:scim:api:idp:2.0:UserDevice": { "devices": [{ "display": "e0df1f5a-225c-4403-a8f2-914ee4c312b9", "value": "92950","$ref": "https://tauth.aaib.com:443/scim/AAIBDOMAIN/v2/Device/92950" }] } }] }}'
  },
  validate: {
    value:
      '{"error": false, "message": { "Data": { "Token": "testToken", "Username": "Charles", "OTPIdentity": "" }}}'
  },
  register: {
    value: '{"error": false, "message": { "Data": {  }}}'
  },
  username: {
    value: '{"error": false, "message": { "Data": [{ "message": "Success" }] }}'
  },
  password: {
    value: '{"error": false, "message": { "Data": { "Token": "testToken" }}}'
  },
  otp: {
    value: '{"error": false, "message": { "Data": {  }}}'
  },
  smsOtp: {
    value: '{"error": false, "message": { "Data": { "Token": "testToken" }}}'
  },
  debitCard: {
    value: '{"error": false, "message": { "Data": [{ "ExpiryDate": "20210101" }]}}'
  }
};

export const mockKafkaSuccess = (type: string) => {
  jest.spyOn(kafka, 'createConsumer');
  const mock = jest.spyOn(kafka, 'produce');
  switch (type) {
    case 'userInfo':
      mock.mockImplementation(() => Promise.resolve(produceResult.login));
      break;
    case 'profile':
      mock.mockImplementation(() => Promise.resolve(produceResult.profile));
      break;
    case 'forgotusername':
      mock.mockImplementation(() => Promise.resolve(produceResult.username));
      break;
    case 'forgotpassword':
      mock.mockImplementation(() => Promise.resolve(produceResult.password));
      break;
    case 'validateOtp':
      mock.mockImplementation(() => Promise.resolve(produceResult.validate));
      break;
    case 'validate':
      mock.mockImplementation(() => Promise.resolve(produceResult.validate));
      break;
    case 'register':
      mock.mockImplementation(() => Promise.resolve(produceResult.register));
      break;
    case 'otp':
      mock.mockImplementation(() => Promise.resolve(produceResult.register));
      break;
    case 'checkUser':
      mock.mockImplementation(() => Promise.resolve(produceResult.devices));
      break;
    case 'resend':
      mock.mockImplementation(() => Promise.resolve(produceResult.otp));
      break;
    case 'sendsms':
      mock.mockImplementation(() => Promise.resolve(produceResult.otp));
      break;
    case 'smsOtp':
      mock.mockImplementation(() => Promise.resolve(produceResult.smsOtp));
      break;
    case 'debitCard':
      mock.mockImplementation(() => Promise.resolve(produceResult.debitCard));
      break;
  }
};

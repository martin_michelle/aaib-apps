import jwt, { JwtPayload } from '../../src/core/JWT';
import { sign } from 'jsonwebtoken';
import * as config from '../../src/config';

export const token = '1234567890';

export const mockEncode = (token: any) => {
  const mock = jest.spyOn(jwt, 'encode');
  mock.mockImplementation(() => Promise.resolve(token));
};

export const createJWT = async () => {
  return jwt.encode(
    new JwtPayload(
      config.tokenInfo.issuer,
      { token, userInfo: { id: 1 }, accounts: [], values: { response: 'SUCCESS' } },
      300000
    )
  );
};

export const createEventJWT = async () => {
  return sign(
    { type: 'login', meta: { created: '2021-01-01' }, values: { response: 'SUCCESS' } },
    config.tokenInfo.secret
  );
};

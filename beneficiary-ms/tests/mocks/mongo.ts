import Model from '../../src/services/beneficiary/model';
import { BeneficiaryInterface } from '../../src/types/app-request';

export const beneficiaryData = [
  {
    _id: '5dbff32e367a343830cd2f49',
    name: 'Charles Kassouf 1',
    nickName: 'Charles',
    accountNumber: '',
    iban: 'LB62099900000001001901229114',
    country: 'Lebanon',
    bank: 'BLF',
    swift: 'ALCVLBBE',
    relationship: 'Brother',
    currency: 'USD',
    address: 'Zahle - Bekaa - Lebanon'
  }
];

export const body: BeneficiaryInterface = {
  name: 'Charles Kassouf 1',
  nickName: 'Charles',
  isCreditCardNumber: false,
  isMobileNumber: false,
  accountNumber: '',
  iban: 'LB62099900000001001901229114',
  country: 'Lebanon',
  bank: 'BLF',
  swift: 'ALCVLBBE',
  relationship: 'Brother',
  currency: 'USD',
  cif: '1234',
  address: 'Zahle - Bekaa - Lebanon'
};

export const mockMongoSuccess = () => {
  Model.aggregate = jest.fn().mockResolvedValue(beneficiaryData);
  Model.remove = jest.fn().mockResolvedValue({});
  Model.findOneAndDelete = jest.fn().mockResolvedValue({});
  Model.findOne = jest.fn().mockResolvedValue({});
  Model.find = jest.fn().mockResolvedValue([]);
  Model.updateOne = jest.fn().mockResolvedValue({});
  Model.prototype.save = jest.fn().mockResolvedValue({});
};

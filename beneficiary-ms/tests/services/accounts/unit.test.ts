import * as service from '../../../src/services/beneficiary';
import { mockMongoSuccess, beneficiaryData, body } from '../../mocks/mongo';

beforeAll(() => {
  mockMongoSuccess();
});
describe('Get Beneficiary', () => {
  it('Should return success', async () => {
    const data = await service.getUserBeneficiaries('1111', false);
    expect(data).toStrictEqual(beneficiaryData);
  });
});

describe('Delete Beneficiary', () => {
  it('Should return success', async () => {
    const data = await service.deleteUserBeneficiaries('1111', '', '', '');
    expect(data).toStrictEqual({ message: 'Success' });
  });
});

describe('Update Beneficiary', () => {
  it('Should return success', async () => {
    const data = await service.updateUserBeneficiaries('1111', '', body, '', '');
    expect(data).toStrictEqual({ message: 'Success' });
  });
});

describe('Add Beneficiary', () => {
  beforeAll(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    const data = await service.addUserBeneficiaries('1111', '', body, '');
    expect(data).toStrictEqual({ message: 'Success', id: data.id });
  });
});

export default {
  accounts: {
    notRegistered: {
      en: 'Account not registered'
    },
    timeout: {
      en: 'Request Timeout'
    },
    missingValue: {
      en: 'Missing Value'
    },
    internalError: {
      en: 'Internal Server Error'
    },
    notFound: {
      en: 'Account Not Found'
    },
    forbidden: {
      en: 'Permission denied'
    }
  },
  authorization: {
    notFound: {
      en: 'Authorization must be provided'
    },
    invalid: {
      en: 'Invalid token'
    },
    otpBlocked: {
      en: 'Your token is locked'
    },
    invalidOtp: {
      en: 'Invalid OTP'
    }
  },
  beneficiary: {
    invalid: {
      en: 'Invalid iban'
    }
  }
};

import { ClientOptions } from './kafka.interface';

export abstract class ClientProxy {
  public abstract connect(): Promise<any>;

  public abstract close(): any;

  protected getOptionsProp<T extends ClientOptions['options'], K extends keyof T>(
    obj: T,
    prop: K,
    defaultValue: T[K] | undefined = undefined
  ) {
    // @ts-ignore
    return (obj && obj[prop]) || defaultValue;
  }
}

import { loadPackage } from './load-package.util';
import {
  BrokersFunction,
  Consumer,
  ConsumerConfig,
  ConsumerGroupJoinEvent,
  KafkaOptions,
  Kafka,
  ReadPacket
} from './kafka.interface';
import { KafkaConfig, Producer } from 'kafkajs';
import { KafkaParser } from './helper/kafka-parser';
import { KafkaReplyPartitionAssigner } from './partitionAssigner';
import { ClientProxy } from './client-proxy';
import { randomStringGenerator } from './random-string-generator';

let kafkaPackage: any = {};

export class ClientKafka extends ClientProxy {
  // @ts-ignore
  protected client: Kafka = null;
  // @ts-ignore
  protected consumer: Consumer = null;
  // @ts-ignore
  protected producer: Producer = null;
  // @ts-ignore
  protected parser: KafkaParser = null;
  protected responsePatterns: string[] = [];
  protected consumerAssignments: { [key: string]: number } = {};

  protected brokers: string[] | BrokersFunction;
  protected clientId: string;
  protected groupId: string;

  constructor(protected readonly options: KafkaOptions['options']) {
    super();
    // @ts-ignore
    const clientOptions = this.getOptionsProp(this.options, 'client') || ({} as KafkaConfig);
    // @ts-ignore
    const consumerOptions = this.getOptionsProp(this.options, 'consumer') || ({} as ConsumerConfig);
    // @ts-ignore
    const postfixId = this.getOptionsProp(this.options, 'postfixId') || '-client';

    this.brokers = clientOptions.brokers;

    // Append a unique id to the clientId and groupId
    // so they don't collide with a microservices client
    this.clientId = clientOptions.clientId + postfixId;
    this.groupId = consumerOptions.groupId + postfixId; // todo return from the config

    kafkaPackage = loadPackage('kafkajs', ClientKafka.name, () => require('kafkajs'));
    this.parser = new KafkaParser((options && options.parser) || undefined);
  }

  public async close(): Promise<void> {
    this.producer && (await this.producer.disconnect());
    this.consumer && (await this.consumer.disconnect());
    // @ts-ignore
    this.producer = null;
    // @ts-ignore
    this.consumer = null;
    // @ts-ignore
    this.client = null;
  }

  public async connect(): Promise<Consumer> {
    if (this.client) {
      return this.consumer;
    }
    this.client = this.createClient();
    const partitionAssigners = [
      (config: ConstructorParameters<typeof KafkaReplyPartitionAssigner>[1]) =>
        new KafkaReplyPartitionAssigner(this, config)
    ] as any[];
    // @ts-ignore
    if (!this.options.consumer.sessionTimeout) this.options.consumer.sessionTimeout = 60000;
    // @ts-ignore
    if (!this.options.consumer.heartbeatInterval) this.options.consumer.heartbeatInterval = 6000;
    const consumerOptions = Object.assign(
      {
        partitionAssigners
      },
      this.options?.consumer || {},
      {
        groupId: this.groupId
      }
    );
    this.producer = this.client.producer(this.options?.producer || {});
    this.consumer = this.client.consumer(consumerOptions);

    // set member assignments on join and rebalance
    this.consumer.on(this.consumer.events.GROUP_JOIN, this.setConsumerAssignments.bind(this));
    return this.consumer;
  }

  public createClient<T = any>(): T {
    const kafkaConfig: KafkaConfig = Object.assign({}, this.options?.client, {
      brokers: this.brokers,
      clientId: this.clientId
    });

    return new kafkaPackage.Kafka(kafkaConfig);
  }

  public getConsumerAssignments() {
    return this.consumerAssignments;
  }

  protected getResponsePatternName(pattern: string): string {
    return `${pattern}.reply`;
  }

  protected setConsumerAssignments(data: ConsumerGroupJoinEvent): void {
    const consumerAssignments: { [key: string]: number } = {};

    // only need to set the minimum
    Object.keys(data.payload.memberAssignment).forEach((memberId) => {
      const minimumPartition = Math.min(...data.payload.memberAssignment[memberId]);

      consumerAssignments[memberId] = minimumPartition;
    });

    this.consumerAssignments = consumerAssignments;
  }

  protected assignPacketId(packet: ReadPacket): ReadPacket & { id: string } {
    const id = randomStringGenerator();
    return Object.assign(packet, { id });
  }

  public async createKafkaTopic(topic: string, partitionNumber: number): Promise<void> {
    const admin = this.client.admin();
    try {
      const topicData = await admin.fetchTopicMetadata({ topics: [topic] });
      if (topicData && topicData.topics && topicData.topics.length) {
        if (topicData.topics[0].partitions.length < partitionNumber) {
          await admin.createPartitions({
            timeout: 5000,
            topicPartitions: [
              {
                topic,
                count: partitionNumber
              }
            ]
          });
        }
      }
    } catch (e) {
      // @ts-ignore
      if (e.message === 'This server does not host this topic-partition') {
        await admin.createTopics({
          validateOnly: false,
          waitForLeaders: false,
          timeout: 5000,
          topics: [
            {
              topic,
              numPartitions: partitionNumber,
              replicationFactor: 1,
              replicaAssignment: [],
              configEntries: []
            }
          ]
        });
      } else {
        // @ts-ignore
        throw new Error(e);
      }
    }
  }
}

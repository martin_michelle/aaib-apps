import express from 'express';
import accounts from './beneficiary/beneficiary';

const router = express.Router();

router.use('/beneficiary', accounts);

export default router;

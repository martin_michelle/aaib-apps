import express from 'express';
import { SuccessResponse } from '../../core/ApiResponse';
import asyncHandler from '../../helpers/asyncHandler';
import * as service from '../../services/beneficiary';
import authenticate from '../../helpers/authenticate';
import { ProtectedRequest } from 'app-request';
import { validate } from 'express-validation';
import schema from './schema';
import tracingHeaders from '../../helpers/tracingHeaders';
// eslint-disable-next-line
const mongoose = require('mongoose');

const router = express.Router();

router.post(
  '/',
  authenticate(),
  validate(schema.addBeneficiary),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.addUserBeneficiaries(
      req.user.cif,
      req.user.id,
      req.body,
      String(req.headers.platform)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/',
  authenticate(),
  validate(schema.getUserBeneficiaries),
  asyncHandler(async (req: ProtectedRequest, res) => {
    let isCreditCardNumber = false;
    if (req.query.isCreditCardNumber && req.query.isCreditCardNumber === 'true') {
      isCreditCardNumber = true;
    }
    const result = await service.getUserBeneficiaries(req.user.cif, isCreditCardNumber);
    new SuccessResponse('Success', result).send(res);
  })
);

router.delete(
  '/:id',
  authenticate(),
  validate(schema.deleteBeneficiary),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const id = new mongoose.Types.ObjectId(req.params.id);
    const result = await service.deleteUserBeneficiaries(
      req.user.cif,
      id,
      req.user.id,
      String(req.headers.platform)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.put(
  '/:id',
  authenticate(),
  validate(schema.updateBeneficiary),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const id = mongoose.Types.ObjectId(req.params.id);
    const result = await service.updateUserBeneficiaries(
      req.user.cif,
      id,
      req.body,
      req.user.id,
      String(req.headers.platform)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/countries',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getListOfCountries(req.headers['accept-language']);
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/countries/list',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getModifiedListOfCountries(req.headers['accept-language']);
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/banks/:country',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getListOfBanks(String(req.params.country), tracingHeaders(req));
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/iban/:iban',
  authenticate(),
  validate(schema.ibanValidation),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.validateIBAN(String(req.params.iban));
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/currency',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getListOfCurrencies(tracingHeaders(req));
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/relationship',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getListOfRelationships();
    new SuccessResponse('Success', result).send(res);
  })
);

export default router;

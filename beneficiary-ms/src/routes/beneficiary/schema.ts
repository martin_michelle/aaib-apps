import Joi from 'joi';

export default {
  addBeneficiary: {
    body: Joi.object({
      name: Joi.string().required(),
      nickName: Joi.string().optional(),
      isMobileNumber: Joi.boolean().optional(),
      isCreditCardNumber: Joi.boolean().optional(),
      accountNumber: Joi.string().when('isCreditCardNumber', {
        is: true,
        then: Joi.required(),
        otherwise: Joi.string().when('isMobileNumber', {
          is: true,
          then: Joi.string().length(11).required(),
          otherwise: Joi.string().optional()
        })
      }),
      iban: Joi.string().optional(),
      country: Joi.string().required(),
      countryCode: Joi.string().optional().allow(''),
      bank: Joi.string().required(),
      swift: Joi.string().required(),
      relationship: Joi.string().optional().allow(''),
      currency: Joi.string().required(),
      otp: Joi.string().optional(),
      address: Joi.string().required().when('currency', {
        is: 'EGP',
        then: Joi.string().optional()
      })
    }).or('accountNumber', 'iban')
  },
  deleteBeneficiary: {
    params: Joi.object({
      id: Joi.string().required()
    })
  },
  getUserBeneficiaries: {
    query: Joi.object({
      isCreditCardNumber: Joi.boolean().optional()
    })
  },
  ibanValidation: {
    params: Joi.object({
      iban: Joi.string().required()
    })
  },
  updateBeneficiary: {
    params: Joi.object({
      id: Joi.string().required()
    }),
    body: Joi.object({
      name: Joi.string().required(),
      nickName: Joi.string().optional().allow(''),
      isCreditCardNumber: Joi.boolean().optional(),
      isMobileNumber: Joi.boolean().optional(),
      accountNumber: Joi.string().when('isCreditCardNumber', {
        is: true,
        then: Joi.required(),
        otherwise: Joi.string().when('isMobileNumber', {
          is: true,
          then: Joi.string().length(11).required(),
          otherwise: Joi.string().optional()
        })
      }),
      iban: Joi.string().required().optional().allow(''),
      country: Joi.string().required(),
      bank: Joi.string().required(),
      swift: Joi.string().required(),
      countryCode: Joi.string().optional().allow(''),
      relationship: Joi.string().optional().allow(''),
      currency: Joi.string().required(),
      address: Joi.string().required().when('currency', {
        is: 'EGP',
        then: Joi.string().optional()
      }),
      otp: Joi.string().optional()
    }).or('accountNumber', 'iban')
  }
};

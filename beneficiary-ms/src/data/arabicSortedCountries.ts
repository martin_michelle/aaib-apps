export const arabicSortedCountries: any = [
  {
    code: 'AM',
    unicode: 'U+1F1E6 U+1F1F2',
    currency: 'AMD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AM.svg',
    emoji: '🇦🇲',
    enName: 'Armenia',
    arName: 'أرمانيا'
  },
  {
    code: 'AW',
    unicode: 'U+1F1E6 U+1F1FC',
    currency: 'AWG',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AW.svg',
    emoji: '🇦🇼',
    enName: 'Aruba',
    arName: 'أروبا'
  },
  {
    code: 'AQ',
    unicode: 'U+1F1E6 U+1F1F6',
    currency: '',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AQ.svg',
    emoji: '🇦🇶',
    enName: 'Antarctica',
    arName: 'أنتركتيكا'
  },
  {
    code: 'AI',
    unicode: 'U+1F1E6 U+1F1EE',
    currency: 'XCD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AI.svg',
    emoji: '🇦🇮',
    enName: 'Anguilla',
    arName: 'أنجوليا'
  },
  {
    code: 'AD',
    unicode: 'U+1F1E6 U+1F1E9',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AD.svg',
    emoji: '🇦🇩',
    enName: 'Andorra',
    arName: 'أندورا'
  },
  {
    code: 'UZ',
    unicode: 'U+1F1FA U+1F1FF',
    currency: 'UZS',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UZ.svg',
    emoji: '🇺🇿',
    enName: 'Uzbekistan',
    arName: 'أوزبكستان'
  },
  {
    code: 'ET',
    unicode: 'U+1F1EA U+1F1F9',
    currency: 'ETB',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ET.svg',
    emoji: '🇪🇹',
    enName: 'Ethiopia',
    arName: 'اثيوبيا'
  },
  {
    code: 'AZ',
    unicode: 'U+1F1E6 U+1F1FF',
    currency: 'AZN',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AZ.svg',
    emoji: '🇦🇿',
    enName: 'Azerbaijan',
    arName: 'اذربيجان'
  },
  {
    code: 'ER',
    unicode: 'U+1F1EA U+1F1F7',
    currency: 'ERN',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ER.svg',
    emoji: '🇪🇷',
    enName: 'Eritrea',
    arName: 'اريتريا'
  },
  {
    code: 'ES',
    unicode: 'U+1F1EA U+1F1F8',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ES.svg',
    emoji: '🇪🇸',
    enName: 'Spain',
    arName: 'اسبانيا'
  },
  {
    code: 'AU',
    unicode: 'U+1F1E6 U+1F1FA',
    currency: 'AUD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AU.svg',
    emoji: '🇦🇺',
    enName: 'Australia',
    arName: 'استراليا'
  },
  {
    code: 'EE',
    unicode: 'U+1F1EA U+1F1EA',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EE.svg',
    emoji: '🇪🇪',
    enName: 'Estonia',
    arName: 'استونيا'
  },
  {
    code: 'AF',
    unicode: 'U+1F1E6 U+1F1EB',
    currency: 'AFN',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AF.svg',
    emoji: '🇦🇫',
    enName: 'Afghanistan',
    arName: 'افغانستان'
  },
  {
    code: 'AR',
    unicode: 'U+1F1E6 U+1F1F7',
    currency: 'ARS',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AR.svg',
    emoji: '🇦🇷',
    enName: 'Argentina',
    arName: 'الأرجنتين'
  },
  {
    code: 'JO',
    unicode: 'U+1F1EF U+1F1F4',
    currency: 'JOD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JO.svg',
    emoji: '🇯🇴',
    enName: 'Jordan',
    arName: 'الأردن'
  },
  {
    code: 'EC',
    unicode: 'U+1F1EA U+1F1E8',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EC.svg',
    emoji: '🇪🇨',
    enName: 'Ecuador',
    arName: 'الأكوادور'
  },
  {
    code: 'AX',
    unicode: 'U+1F1E6 U+1F1FD',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AX.svg',
    emoji: '🇦🇽',
    enName: 'Aland Islands',
    arName: 'الا ند ايسلا ند'
  },
  {
    code: 'RE',
    unicode: 'U+1F1F7 U+1F1EA',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RE.svg',
    emoji: '🇷🇪',
    enName: 'Reunion',
    arName: 'الاتحاد'
  },
  {
    code: 'AL',
    unicode: 'U+1F1E6 U+1F1F1',
    currency: 'ALL',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AL.svg',
    emoji: '🇦🇱',
    enName: 'Albania',
    arName: 'البانيا'
  },
  {
    code: 'BH',
    unicode: 'U+1F1E7 U+1F1ED',
    currency: 'BHD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BH.svg',
    emoji: '🇧🇭',
    enName: 'Bahrain',
    arName: 'البحرين'
  },
  {
    code: 'BR',
    unicode: 'U+1F1E7 U+1F1F7',
    currency: 'BRL',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BR.svg',
    emoji: '🇧🇷',
    enName: 'Brazil',
    arName: 'البرازيل'
  },
  {
    code: 'PT',
    unicode: 'U+1F1F5 U+1F1F9',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PT.svg',
    emoji: '🇵🇹',
    enName: 'Portugal',
    arName: 'البرتغال'
  },
  {
    code: 'BS',
    unicode: 'U+1F1E7 U+1F1F8',
    currency: 'BSD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BS.svg',
    emoji: '🇧🇸',
    enName: 'Bahamas',
    arName: 'البهامس'
  },
  {
    code: 'BA',
    unicode: 'U+1F1E7 U+1F1E6',
    currency: 'BAM',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BA.svg',
    emoji: '🇧🇦',
    enName: 'Bosnia and Herzegovina',
    arName: 'البوسنةوالهرسك'
  },
  {
    code: 'GA',
    unicode: 'U+1F1EC U+1F1E6',
    currency: 'XAF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GA.svg',
    emoji: '🇬🇦',
    enName: 'Gabon',
    arName: 'الجابون'
  },
  {
    code: 'DZ',
    unicode: 'U+1F1E9 U+1F1FF',
    currency: 'DZD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DZ.svg',
    emoji: '🇩🇿',
    enName: 'Algeria',
    arName: 'الجزائر'
  },
  {
    code: 'DK',
    unicode: 'U+1F1E9 U+1F1F0',
    currency: 'DKK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DK.svg',
    emoji: '🇩🇰',
    enName: 'Denmark',
    arName: 'الدنمارك'
  },
  {
    code: 'DM',
    unicode: 'U+1F1E9 U+1F1F2',
    currency: 'XCD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DM.svg',
    emoji: '🇩🇲',
    enName: 'Dominica',
    arName: 'الدومينيكان'
  },
  {
    code: 'CV',
    unicode: 'U+1F1E8 U+1F1FB',
    currency: 'CVE',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CV.svg',
    emoji: '🇨🇻',
    enName: 'Cape Verde',
    arName: 'الرأس الأخضر'
  },
  {
    code: 'SA',
    unicode: 'U+1F1F8 U+1F1E6',
    currency: 'SAR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SA.svg',
    emoji: '🇸🇦',
    enName: 'Saudi Arabia',
    arName: 'السعودية'
  },
  {
    code: 'SV',
    unicode: 'U+1F1F8 U+1F1FB',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SV.svg',
    emoji: '🇸🇻',
    enName: 'El Salvador',
    arName: 'السلفادور'
  },
  {
    code: 'SN',
    unicode: 'U+1F1F8 U+1F1F3',
    currency: 'XOF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SN.svg',
    emoji: '🇸🇳',
    enName: 'Senegal',
    arName: 'السنغال'
  },
  {
    code: 'SD',
    unicode: 'U+1F1F8 U+1F1E9',
    currency: 'SDG',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SD.svg',
    emoji: '🇸🇩',
    enName: 'Sudan',
    arName: 'السودان'
  },
  {
    code: 'SE',
    unicode: 'U+1F1F8 U+1F1EA',
    currency: 'SEK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SE.svg',
    emoji: '🇸🇪',
    enName: 'Sweden',
    arName: 'السويد'
  },
  {
    code: 'EH',
    unicode: 'U+1F1EA U+1F1ED',
    currency: 'MAD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EH.svg',
    emoji: '🇪🇭',
    enName: 'Western Sahara',
    arName: 'الصحراء الغربية'
  },
  {
    code: 'SO',
    unicode: 'U+1F1F8 U+1F1F4',
    currency: 'SOS',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SO.svg',
    emoji: '🇸🇴',
    enName: 'Somalia',
    arName: 'الصومال'
  },
  {
    code: 'CN',
    unicode: 'U+1F1E8 U+1F1F3',
    currency: 'CNY',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CN.svg',
    emoji: '🇨🇳',
    enName: 'China',
    arName: 'الصين'
  },
  {
    code: 'IQ',
    unicode: 'U+1F1EE U+1F1F6',
    currency: 'IQD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IQ.svg',
    emoji: '🇮🇶',
    enName: 'Iraq',
    arName: 'العراق'
  },
  {
    code: 'PH',
    unicode: 'U+1F1F5 U+1F1ED',
    currency: 'PHP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PH.svg',
    emoji: '🇵🇭',
    enName: 'Philippines',
    arName: 'الفلبين'
  },
  {
    code: 'CM',
    unicode: 'U+1F1E8 U+1F1F2',
    currency: 'XAF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CM.svg',
    emoji: '🇨🇲',
    enName: 'Cameroon',
    arName: 'الكاميرون'
  },
  {
    code: 'KW',
    unicode: 'U+1F1F0 U+1F1FC',
    currency: 'KWD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KW.svg',
    emoji: '🇰🇼',
    enName: 'Kuwait',
    arName: 'الكويت'
  },
  {
    code: 'MQ',
    unicode: 'U+1F1F2 U+1F1F6',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MQ.svg',
    emoji: '🇲🇶',
    enName: 'Martinique',
    arName: 'المارتين'
  },
  {
    code: 'DE',
    unicode: 'U+1F1E9 U+1F1EA',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DE.svg',
    emoji: '🇩🇪',
    enName: 'Germany',
    arName: 'المانيا'
  },
  {
    code: 'HU',
    unicode: 'U+1F1ED U+1F1FA',
    currency: 'HUF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HU.svg',
    emoji: '🇭🇺',
    enName: 'Hungary',
    arName: 'المجر'
  },
  {
    code: 'MA',
    unicode: 'U+1F1F2 U+1F1E6',
    currency: 'MAD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MA.svg',
    emoji: '🇲🇦',
    enName: 'Moroccan',
    arName: 'المغرب'
  },
  {
    code: 'IO',
    unicode: 'U+1F1EE U+1F1F4',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IO.svg',
    emoji: '🇮🇴',
    enName: 'British Indian Ocean Territory',
    arName: 'المقاطعة البريطانية في المحيط'
  },
  {
    code: 'MX',
    unicode: 'U+1F1F2 U+1F1FD',
    currency: 'MXN',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MX.svg',
    emoji: '🇲🇽',
    enName: 'Mexico',
    arName: 'المكسيك'
  },
  {
    code: 'GB',
    unicode: 'U+1F1EC U+1F1E7',
    currency: 'GBP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GB.svg',
    emoji: '🇬🇧',
    enName: 'United Kingdom',
    arName: 'المملكة المتحدة'
  },
  {
    code: 'NO',
    unicode: 'U+1F1F3 U+1F1F4',
    currency: 'NOK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NO.svg',
    emoji: '🇳🇴',
    enName: 'Norway',
    arName: 'النرويج'
  },
  {
    code: 'AT',
    unicode: 'U+1F1E6 U+1F1F9',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AT.svg',
    emoji: '🇦🇹',
    enName: 'Austria',
    arName: 'النمسا'
  },
  {
    code: 'NE',
    unicode: 'U+1F1F3 U+1F1EA',
    currency: 'XOF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NE.svg',
    emoji: '🇳🇪',
    enName: 'Niger',
    arName: 'النيجر'
  },
  {
    code: 'IN',
    unicode: 'U+1F1EE U+1F1F3',
    currency: 'INR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IN.svg',
    emoji: '🇮🇳',
    enName: 'India',
    arName: 'الهند'
  },
  {
    code: 'US',
    unicode: 'U+1F1FA U+1F1F8',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/US.svg',
    emoji: '🇺🇸',
    enName: 'United States of America',
    arName: 'الولايات المتحدة الامريكية'
  },
  {
    code: 'JP',
    unicode: 'U+1F1EF U+1F1F5',
    currency: 'JPY',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JP.svg',
    emoji: '🇯🇵',
    enName: 'Japan',
    arName: 'اليابان'
  },
  {
    code: 'YE',
    unicode: 'U+1F1FE U+1F1EA',
    currency: 'YER',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/YE.svg',
    emoji: '🇾🇪',
    enName: 'Yemen',
    arName: 'اليمن'
  },
  {
    code: 'GR',
    unicode: 'U+1F1EC U+1F1F7',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GR.svg',
    emoji: '🇬🇷',
    enName: 'Greece',
    arName: 'اليونان'
  },
  {
    code: 'AS',
    unicode: 'U+1F1E6 U+1F1F8',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AS.svg',
    emoji: '🇦🇸',
    enName: 'American Samoa',
    arName: 'امريكان ساموا'
  },
  {
    code: 'AG',
    unicode: 'U+1F1E6 U+1F1EC',
    currency: 'XCD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AG.svg',
    emoji: '🇦🇬',
    enName: 'Antiqua & Barbuda',
    arName: 'انتيجوا'
  },
  {
    code: 'AO',
    unicode: 'U+1F1E6 U+1F1F4',
    currency: 'AOA',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AO.svg',
    emoji: '🇦🇴',
    enName: 'Angola',
    arName: 'انجولا'
  },
  {
    code: 'ID',
    unicode: 'U+1F1EE U+1F1E9',
    currency: 'IDR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ID.svg',
    emoji: '🇮🇩',
    enName: 'Indonesia',
    arName: 'اندونيسيا'
  },
  {
    code: 'UY',
    unicode: 'U+1F1FA U+1F1FE',
    currency: 'UYU',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UY.svg',
    emoji: '🇺🇾',
    enName: 'Uraguay',
    arName: 'اوراجوى'
  },
  {
    code: 'UG',
    unicode: 'U+1F1FA U+1F1EC',
    currency: 'UGX',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UG.svg',
    emoji: '🇺🇬',
    enName: 'Uganda',
    arName: 'اوغندا'
  },
  {
    code: 'UA',
    unicode: 'U+1F1FA U+1F1E6',
    currency: 'UAH',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UA.svg',
    emoji: '🇺🇦',
    enName: 'Ukraine',
    arName: 'اوكرانيا'
  },
  {
    code: 'IE',
    unicode: 'U+1F1EE U+1F1EA',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IE.svg',
    emoji: '🇮🇪',
    enName: 'Ireland',
    arName: 'ايرلندا'
  },
  {
    code: 'IS',
    unicode: 'U+1F1EE U+1F1F8',
    currency: 'ISK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IS.svg',
    emoji: '🇮🇸',
    enName: 'Iceland',
    arName: 'ايس  لاند'
  },
  {
    code: 'IT',
    unicode: 'U+1F1EE U+1F1F9',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IT.svg',
    emoji: '🇮🇹',
    enName: 'Italy',
    arName: 'ايطاليا'
  },
  {
    code: 'PY',
    unicode: 'U+1F1F5 U+1F1FE',
    currency: 'PYG',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PY.svg',
    emoji: '🇵🇾',
    enName: 'Paraguay',
    arName: 'باراجواى'
  },
  {
    code: 'BB',
    unicode: 'U+1F1E7 U+1F1E7',
    currency: 'BBD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BB.svg',
    emoji: '🇧🇧',
    enName: 'Barbados',
    arName: 'باربادوس'
  },
  {
    code: 'PK',
    unicode: 'U+1F1F5 U+1F1F0',
    currency: 'PKR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PK.svg',
    emoji: '🇵🇰',
    enName: 'Pakistan',
    arName: 'باكستان'
  },
  {
    code: 'PW',
    unicode: 'U+1F1F5 U+1F1FC',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PW.svg',
    emoji: '🇵🇼',
    enName: 'Palau',
    arName: 'بالواه'
  },
  {
    code: 'BW',
    unicode: 'U+1F1E7 U+1F1FC',
    currency: 'BWP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BW.svg',
    emoji: '🇧🇼',
    enName: 'Botswana',
    arName: 'بتسوانا'
  },
  {
    code: 'BM',
    unicode: 'U+1F1E7 U+1F1F2',
    currency: 'BMD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BM.svg',
    emoji: '🇧🇲',
    enName: 'Bermuda',
    arName: 'برمودا'
  },
  {
    code: 'BN',
    unicode: 'U+1F1E7 U+1F1F3',
    currency: 'BND',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BN.svg',
    emoji: '🇧🇳',
    enName: 'Brunei Darussalam',
    arName: 'بروناى'
  },
  {
    code: 'BI',
    unicode: 'U+1F1E7 U+1F1EE',
    currency: 'BIF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BI.svg',
    emoji: '🇧🇮',
    enName: 'Burundi',
    arName: 'بروندى'
  },
  {
    code: 'BE',
    unicode: 'U+1F1E7 U+1F1EA',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BE.svg',
    emoji: '🇧🇪',
    enName: 'Belgium',
    arName: 'بلجيكا'
  },
  {
    code: 'BG',
    unicode: 'U+1F1E7 U+1F1EC',
    currency: 'BGN',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BG.svg',
    emoji: '🇧🇬',
    enName: 'Bulgaria',
    arName: 'بلغاريا'
  },
  {
    code: 'BD',
    unicode: 'U+1F1E7 U+1F1E9',
    currency: 'BDT',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BD.svg',
    emoji: '🇧🇩',
    enName: 'Bangladesh',
    arName: 'بنجلاديش'
  },
  {
    code: 'PA',
    unicode: 'U+1F1F5 U+1F1E6',
    currency: 'PAB',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PA.svg',
    emoji: '🇵🇦',
    enName: 'Panama',
    arName: 'بنما'
  },
  {
    code: 'BJ',
    unicode: 'U+1F1E7 U+1F1EF',
    currency: 'XOF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BJ.svg',
    emoji: '🇧🇯',
    enName: 'Benin',
    arName: 'بنين'
  },
  {
    code: 'PG',
    unicode: 'U+1F1F5 U+1F1EC',
    currency: 'PGK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PG.svg',
    emoji: '🇵🇬',
    enName: 'Papua New Guinea',
    arName: 'بوبا غينيا الجديدة'
  },
  {
    code: 'PR',
    unicode: 'U+1F1F5 U+1F1F7',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PR.svg',
    emoji: '🇵🇷',
    enName: 'Puerto Rico',
    arName: 'بورتوريكو'
  },
  {
    code: 'BF',
    unicode: 'U+1F1E7 U+1F1EB',
    currency: 'XOF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BF.svg',
    emoji: '🇧🇫',
    enName: 'Burkina Faso',
    arName: 'بوركينا فاسو'
  },
  {
    code: 'PL',
    unicode: 'U+1F1F5 U+1F1F1',
    currency: 'PLN',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PL.svg',
    emoji: '🇵🇱',
    enName: 'Poland',
    arName: 'بولندا'
  },
  {
    code: 'BO',
    unicode: 'U+1F1E7 U+1F1F4',
    currency: 'BOB',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BO.svg',
    emoji: '🇧🇴',
    enName: 'Bolivia',
    arName: 'بوليفيا'
  },
  {
    code: 'PF',
    unicode: 'U+1F1F5 U+1F1EB',
    currency: 'XPF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PF.svg',
    emoji: '🇵🇫',
    enName: 'French Polinesia',
    arName: 'بولينسيا الفرنسية'
  },
  {
    code: 'BQ',
    unicode: 'U+1F1E7 U+1F1F6',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BQ.svg',
    emoji: '🇧🇶',
    enName: 'Bonaire, Saint Eustatius and Saba',
    arName: 'بونير، سانت ستاتيوس&سابا'
  },
  {
    code: 'PN',
    unicode: 'U+1F1F5 U+1F1F3',
    currency: 'NZD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PN.svg',
    emoji: '🇵🇳',
    enName: 'Pitcairn Island',
    arName: 'بيتكيرن'
  },
  {
    code: 'PE',
    unicode: 'U+1F1F5 U+1F1EA',
    currency: 'PEN',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PE.svg',
    emoji: '🇵🇪',
    enName: 'Peru',
    arName: 'بيرو'
  },
  {
    code: 'BZ',
    unicode: 'U+1F1E7 U+1F1FF',
    currency: 'BZD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BZ.svg',
    emoji: '🇧🇿',
    enName: 'Belize',
    arName: 'بيليز'
  },
  {
    code: 'TH',
    unicode: 'U+1F1F9 U+1F1ED',
    currency: 'THB',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TH.svg',
    emoji: '🇹🇭',
    enName: 'Thailand',
    arName: 'تايلاند'
  },
  {
    code: 'TW',
    unicode: 'U+1F1F9 U+1F1FC',
    currency: 'TWD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TW.svg',
    emoji: '🇹🇼',
    enName: 'Taiwan',
    arName: 'تايوان'
  },
  {
    code: 'TM',
    unicode: 'U+1F1F9 U+1F1F2',
    currency: 'TMT',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TM.svg',
    emoji: '🇹🇲',
    enName: 'Turkmenistan',
    arName: 'تركمانستان'
  },
  {
    code: 'TR',
    unicode: 'U+1F1F9 U+1F1F7',
    currency: 'TRY',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TR.svg',
    emoji: '🇹🇷',
    enName: 'Turkey',
    arName: 'تركيا'
  },
  {
    code: 'TT',
    unicode: 'U+1F1F9 U+1F1F9',
    currency: 'TTD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TT.svg',
    emoji: '🇹🇹',
    enName: 'Trinidad and Tobago',
    arName: 'ترينداد و توباجو'
  },
  {
    code: 'TD',
    unicode: 'U+1F1F9 U+1F1E9',
    currency: 'XAF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TD.svg',
    emoji: '🇹🇩',
    enName: 'Chad',
    arName: 'تشاد'
  },
  {
    code: 'TZ',
    unicode: 'U+1F1F9 U+1F1FF',
    currency: 'TZS',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TZ.svg',
    emoji: '🇹🇿',
    enName: 'Tanzania',
    arName: 'تنزانيا'
  },
  {
    code: 'TG',
    unicode: 'U+1F1F9 U+1F1EC',
    currency: 'XOF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TG.svg',
    emoji: '🇹🇬',
    enName: 'Togo',
    arName: 'توجو'
  },
  {
    code: 'TV',
    unicode: 'U+1F1F9 U+1F1FB',
    currency: 'AUD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TV.svg',
    emoji: '🇹🇻',
    enName: 'Tuvalu',
    arName: 'توفالو'
  },
  {
    code: 'TK',
    unicode: 'U+1F1F9 U+1F1F0',
    currency: 'NZD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TK.svg',
    emoji: '🇹🇰',
    enName: 'Tokelau',
    arName: 'توكيلاو'
  },
  {
    code: 'TO',
    unicode: 'U+1F1F9 U+1F1F4',
    currency: 'TOP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TO.svg',
    emoji: '🇹🇴',
    enName: 'Tonga',
    arName: 'تونجا'
  },
  {
    code: 'TN',
    unicode: 'U+1F1F9 U+1F1F3',
    currency: 'TND',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TN.svg',
    emoji: '🇹🇳',
    enName: 'Tunisia',
    arName: 'تونس'
  },
  {
    code: 'GM',
    unicode: 'U+1F1EC U+1F1F2',
    currency: 'GMD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GM.svg',
    emoji: '🇬🇲',
    enName: 'Gambia',
    arName: 'جامبيا'
  },
  {
    code: 'JM',
    unicode: 'U+1F1EF U+1F1F2',
    currency: 'JMD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JM.svg',
    emoji: '🇯🇲',
    enName: 'Jamaica',
    arName: 'جاميكا'
  },
  {
    code: 'GI',
    unicode: 'U+1F1EC U+1F1EE',
    currency: 'GIP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GI.svg',
    emoji: '🇬🇮',
    enName: 'Gibraltar',
    arName: 'جبل طارق'
  },
  {
    code: 'GD',
    unicode: 'U+1F1EC U+1F1E9',
    currency: 'XCD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GD.svg',
    emoji: '🇬🇩',
    enName: 'Grenada',
    arName: 'جرينادا'
  },
  {
    code: 'KM',
    unicode: 'U+1F1F0 U+1F1F2',
    currency: 'KMF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KM.svg',
    emoji: '🇰🇲',
    enName: 'Comoros Islands',
    arName: 'جزر القمر'
  },
  {
    code: 'MV',
    unicode: 'U+1F1F2 U+1F1FB',
    currency: 'MVR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MV.svg',
    emoji: '🇲🇻',
    enName: 'Maldives',
    arName: 'جزر المالديف'
  },
  {
    code: 'TC',
    unicode: 'U+1F1F9 U+1F1E8',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TC.svg',
    emoji: '🇹🇨',
    enName: 'Turks and Caicos Islands',
    arName: 'جزر تركس وكايكوس'
  },
  {
    code: 'SB',
    unicode: 'U+1F1F8 U+1F1E7',
    currency: 'SBD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SB.svg',
    emoji: '🇸🇧',
    enName: 'Solomon Islands',
    arName: 'جزر سلومون'
  },
  {
    code: 'SC',
    unicode: 'U+1F1F8 U+1F1E8',
    currency: 'SCR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SC.svg',
    emoji: '🇸🇨',
    enName: 'Seychelles',
    arName: 'جزر سيشيل'
  },
  {
    code: 'FO',
    unicode: 'U+1F1EB U+1F1F4',
    currency: 'DKK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FO.svg',
    emoji: '🇫🇴',
    enName: 'Faeroe Islands',
    arName: 'جزر فارو'
  },
  {
    code: 'VI',
    unicode: 'U+1F1FB U+1F1EE',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VI.svg',
    emoji: '🇻🇮',
    enName: 'Virgin Islands',
    arName: 'جزر فرجن، الولايات المتحدة'
  },
  {
    code: 'FK',
    unicode: 'U+1F1EB U+1F1F0',
    currency: 'FKP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FK.svg',
    emoji: '🇫🇰',
    enName: 'Falkland Islands',
    arName: 'جزر فوكلاند'
  },
  {
    code: 'VG',
    unicode: 'U+1F1FB U+1F1EC',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VG.svg',
    emoji: '🇻🇬',
    enName: 'British Virgin Islands',
    arName: 'جزر فيرجن (البريطانية)'
  },
  {
    code: 'KY',
    unicode: 'U+1F1F0 U+1F1FE',
    currency: 'KYD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KY.svg',
    emoji: '🇰🇾',
    enName: 'Cayman Islands',
    arName: 'جزر كايمان'
  },
  {
    code: 'CK',
    unicode: 'U+1F1E8 U+1F1F0',
    currency: 'NZD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CK.svg',
    emoji: '🇨🇰',
    enName: 'Cook Islands',
    arName: 'جزر كوك'
  },
  {
    code: 'MH',
    unicode: 'U+1F1F2 U+1F1ED',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MH.svg',
    emoji: '🇲🇭',
    enName: 'Marshall Islands',
    arName: 'جزر مارشال'
  },
  {
    code: 'IM',
    unicode: 'U+1F1EE U+1F1F2',
    currency: 'GBP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IM.svg',
    emoji: '🇮🇲',
    enName: 'Isle of Man',
    arName: 'جزيرة الرجال'
  },
  {
    code: 'CX',
    unicode: 'U+1F1E8 U+1F1FD',
    currency: 'AUD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CX.svg',
    emoji: '🇨🇽',
    enName: 'Christmas Island',
    arName: 'جزيرة الكريسماس'
  },
  {
    code: 'BV',
    unicode: 'U+1F1E7 U+1F1FB',
    currency: 'NOK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BV.svg',
    emoji: '🇧🇻',
    enName: 'Bouvet Island',
    arName: 'جزيرة بوفيه'
  },
  {
    code: 'NF',
    unicode: 'U+1F1F3 U+1F1EB',
    currency: 'AUD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NF.svg',
    emoji: '🇳🇫',
    enName: 'Norfolk Island',
    arName: 'جزيرة نورفولك'
  },
  {
    code: 'CZ',
    unicode: 'U+1F1E8 U+1F1FF',
    currency: 'CZK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CZ.svg',
    emoji: '🇨🇿',
    enName: 'Czech Republic',
    arName: 'جمهورية التشيك'
  },
  {
    code: 'CG',
    unicode: 'U+1F1E8 U+1F1EC',
    currency: 'XAF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CG.svg',
    emoji: '🇨🇬',
    enName: 'The Republic of Congo',
    arName: 'جمهورية الكونغو'
  },
  {
    code: 'CD',
    unicode: 'U+1F1E8 U+1F1E9',
    currency: 'CDF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CD.svg',
    emoji: '🇨🇩',
    enName: 'The Democratic Republic of Congo',
    arName: 'جمهورية الكونغو الديمقراطية'
  },
  {
    code: 'DO',
    unicode: 'U+1F1E9 U+1F1F4',
    currency: 'DOP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DO.svg',
    emoji: '🇩🇴',
    enName: 'Dominican Republic',
    arName: 'جمهورية دومينيكان'
  },
  {
    code: 'MD',
    unicode: 'U+1F1F2 U+1F1E9',
    currency: 'MDL',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MD.svg',
    emoji: '🇲🇩',
    enName: 'Republic of Moldova',
    arName: 'جمهورية ملدوفا'
  },
  {
    code: 'ZA',
    unicode: 'U+1F1FF U+1F1E6',
    currency: 'ZAR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ZA.svg',
    emoji: '🇿🇦',
    enName: 'South Africa',
    arName: 'جنوب افريقيا'
  },
  {
    code: 'SS',
    unicode: 'U+1F1F8 U+1F1F8',
    currency: 'SSP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SS.svg',
    emoji: '🇸🇸',
    enName: 'South Sudan',
    arName: 'جنوب السودان'
  },
  {
    code: 'GT',
    unicode: 'U+1F1EC U+1F1F9',
    currency: 'GTQ',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GT.svg',
    emoji: '🇬🇹',
    enName: 'Guatemala',
    arName: 'جواتيمالا'
  },
  {
    code: 'GP',
    unicode: 'U+1F1EC U+1F1F5',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GP.svg',
    emoji: '🇬🇵',
    enName: 'Guadeloupe',
    arName: 'جواديلوب'
  },
  {
    code: 'GU',
    unicode: 'U+1F1EC U+1F1FA',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GU.svg',
    emoji: '🇬🇺',
    enName: 'Guam',
    arName: 'جوام'
  },
  {
    code: 'GE',
    unicode: 'U+1F1EC U+1F1EA',
    currency: 'GEL',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GE.svg',
    emoji: '🇬🇪',
    enName: 'Georgia',
    arName: 'جورجيا'
  },
  {
    code: 'GY',
    unicode: 'U+1F1EC U+1F1FE',
    currency: 'GYD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GY.svg',
    emoji: '🇬🇾',
    enName: 'Guyana',
    arName: 'جويانا'
  },
  {
    code: 'GF',
    unicode: 'U+1F1EC U+1F1EB',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GF.svg',
    emoji: '🇬🇫',
    enName: 'French Guiana',
    arName: 'جويانا الفرنسية'
  },
  {
    code: 'DJ',
    unicode: 'U+1F1E9 U+1F1EF',
    currency: 'DJF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DJ.svg',
    emoji: '🇩🇯',
    enName: 'Djibouti',
    arName: 'جيبوتى'
  },
  {
    code: 'JE',
    unicode: 'U+1F1EF U+1F1EA',
    currency: 'GBP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JE.svg',
    emoji: '🇯🇪',
    enName: 'Jersey',
    arName: 'جيرسى'
  },
  {
    code: 'GG',
    unicode: 'U+1F1EC U+1F1EC',
    currency: 'GBP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GG.svg',
    emoji: '🇬🇬',
    enName: 'Guernsey',
    arName: 'جيورنسي'
  },
  {
    code: 'AE',
    unicode: 'U+1F1E6 U+1F1EA',
    currency: 'AED',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AE.svg',
    emoji: '🇦🇪',
    enName: 'United Arab Emirates',
    arName: 'دولة الأمارات العربية المتحدة'
  },
  {
    code: 'RW',
    unicode: 'U+1F1F7 U+1F1FC',
    currency: 'RWF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RW.svg',
    emoji: '🇷🇼',
    enName: 'Rwanda',
    arName: 'رواندا'
  },
  {
    code: 'RU',
    unicode: 'U+1F1F7 U+1F1FA',
    currency: 'RUB',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RU.svg',
    emoji: '🇷🇺',
    enName: 'Union of Soviet Socialist Republic',
    arName: 'روسيا'
  },
  {
    code: 'BY',
    unicode: 'U+1F1E7 U+1F1FE',
    currency: 'BYR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BY.svg',
    emoji: '🇧🇾',
    enName: 'Belarus',
    arName: 'روسيا البيضاء'
  },
  {
    code: 'RO',
    unicode: 'U+1F1F7 U+1F1F4',
    currency: 'RON',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RO.svg',
    emoji: '🇷🇴',
    enName: 'Romania',
    arName: 'رومانيا'
  },
  {
    code: 'ZM',
    unicode: 'U+1F1FF U+1F1F2',
    currency: 'ZMW',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ZM.svg',
    emoji: '🇿🇲',
    enName: 'Zambia',
    arName: 'زامبيا'
  },
  {
    code: 'ZW',
    unicode: 'U+1F1FF U+1F1FC',
    currency: 'ZWL',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ZW.svg',
    emoji: '🇿🇼',
    enName: 'Zimbabwe',
    arName: 'زيمبابوي'
  },
  {
    code: 'CI',
    unicode: 'U+1F1E8 U+1F1EE',
    currency: 'XOF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CI.svg',
    emoji: '🇨🇮',
    enName: "Cote D'Ivoire",
    arName: 'ساحل العاج'
  },
  {
    code: 'WS',
    unicode: 'U+1F1FC U+1F1F8',
    currency: 'WST',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/WS.svg',
    emoji: '🇼🇸',
    enName: 'Samoa',
    arName: 'ساموا'
  },
  {
    code: 'SM',
    unicode: 'U+1F1F8 U+1F1F2',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SM.svg',
    emoji: '🇸🇲',
    enName: 'San Marino',
    arName: 'سان مارينو'
  },
  {
    code: 'BL',
    unicode: 'U+1F1E7 U+1F1F1',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BL.svg',
    emoji: '🇧🇱',
    enName: 'Saint Barthelemy',
    arName: 'سانت بارثيليمى'
  },
  {
    code: 'VC',
    unicode: 'U+1F1FB U+1F1E8',
    currency: 'XCD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VC.svg',
    emoji: '🇻🇨',
    enName: 'Saint Vincent and Grenadines',
    arName: 'سانت فنسنت'
  },
  {
    code: 'KN',
    unicode: 'U+1F1F0 U+1F1F3',
    currency: 'XCD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KN.svg',
    emoji: '🇰🇳',
    enName: 'St. Kitts-Nevis',
    arName: 'سانت كتس'
  },
  {
    code: 'SX',
    unicode: 'U+1F1F8 U+1F1FD',
    currency: 'ANG',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SX.svg',
    emoji: '🇸🇽',
    enName: 'Sint Maarten (Dutch Part)',
    arName: 'سانت مارتن (المانيا)'
  },
  {
    code: 'MF',
    unicode: 'U+1F1F2 U+1F1EB',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MF.svg',
    emoji: '🇲🇫',
    enName: 'Saint Martin (French Part)',
    arName: 'سانت مارتن (فرنسا)'
  },
  {
    code: 'SH',
    unicode: 'U+1F1F8 U+1F1ED',
    currency: 'SHP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SH.svg',
    emoji: '🇸🇭',
    enName: 'St Helena Ascension and Tristan Da Cunha',
    arName: 'سانت هيلننا'
  },
  {
    code: 'LC',
    unicode: 'U+1F1F1 U+1F1E8',
    currency: 'XCD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LC.svg',
    emoji: '🇱🇨',
    enName: 'Saint Lucia',
    arName: 'سانتا لوتشيا'
  },
  {
    code: 'ST',
    unicode: 'U+1F1F8 U+1F1F9',
    currency: 'STD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ST.svg',
    emoji: '🇸🇹',
    enName: 'Sao Tome and Principe',
    arName: 'ساو توم و برنسيبى'
  },
  {
    code: 'PM',
    unicode: 'U+1F1F5 U+1F1F2',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PM.svg',
    emoji: '🇵🇲',
    enName: 'St. Pierre and Miquelon',
    arName: 'ست بيير و ميكويلن'
  },
  {
    code: 'LK',
    unicode: 'U+1F1F1 U+1F1F0',
    currency: 'LKR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LK.svg',
    emoji: '🇱🇰',
    enName: 'Sri Lanka',
    arName: 'سرى لانكا'
  },
  {
    code: 'SJ',
    unicode: 'U+1F1F8 U+1F1EF',
    currency: 'NOK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SJ.svg',
    emoji: '🇸🇯',
    enName: 'Svalbard and Jan Mayen Islands',
    arName: 'سفالبارد وجزيرة جان ماين'
  },
  {
    code: 'SK',
    unicode: 'U+1F1F8 U+1F1F0',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SK.svg',
    emoji: '🇸🇰',
    enName: 'Slovakia',
    arName: 'سلوفاكيا'
  },
  {
    code: 'SI',
    unicode: 'U+1F1F8 U+1F1EE',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SI.svg',
    emoji: '🇸🇮',
    enName: 'Slovenia',
    arName: 'سلوفينيا'
  },
  {
    code: 'HM',
    unicode: 'U+1F1ED U+1F1F2',
    currency: 'AUD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HM.svg',
    emoji: '🇭🇲',
    enName: 'Heard & McDonald Islands',
    arName: 'سمعت وجزر مكدونالد'
  },
  {
    code: 'SG',
    unicode: 'U+1F1F8 U+1F1EC',
    currency: 'SGD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SG.svg',
    emoji: '🇸🇬',
    enName: 'Singapore',
    arName: 'سنغافورة'
  },
  {
    code: 'SZ',
    unicode: 'U+1F1F8 U+1F1FF',
    currency: 'SZL',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SZ.svg',
    emoji: '🇸🇿',
    enName: 'Swaziland',
    arName: 'سوازيلا ند'
  },
  {
    code: 'SR',
    unicode: 'U+1F1F8 U+1F1F7',
    currency: 'SRD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SR.svg',
    emoji: '🇸🇷',
    enName: 'Suriname',
    arName: 'سورينام'
  },
  {
    code: 'CH',
    unicode: 'U+1F1E8 U+1F1ED',
    currency: 'CHF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CH.svg',
    emoji: '🇨🇭',
    enName: 'Switzerland',
    arName: 'سويسرا'
  },
  {
    code: 'SL',
    unicode: 'U+1F1F8 U+1F1F1',
    currency: 'SLL',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SL.svg',
    emoji: '🇸🇱',
    enName: 'Sierra Leone',
    arName: 'سيراليون'
  },
  {
    code: 'CL',
    unicode: 'U+1F1E8 U+1F1F1',
    currency: 'CLP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CL.svg',
    emoji: '🇨🇱',
    enName: 'Chile',
    arName: 'شيلى'
  },
  {
    code: 'RS',
    unicode: 'U+1F1F7 U+1F1F8',
    currency: 'RSD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RS.svg',
    emoji: '🇷🇸',
    enName: 'Republic of Serbia',
    arName: 'صربيا'
  },
  {
    code: 'TJ',
    unicode: 'U+1F1F9 U+1F1EF',
    currency: 'TJS',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TJ.svg',
    emoji: '🇹🇯',
    enName: 'Tajikistan',
    arName: 'طاجيكستان'
  },
  {
    code: 'OM',
    unicode: 'U+1F1F4 U+1F1F2',
    currency: 'OMR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/OM.svg',
    emoji: '🇴🇲',
    enName: 'Oman',
    arName: 'عمان'
  },
  {
    code: 'GH',
    unicode: 'U+1F1EC U+1F1ED',
    currency: 'GHS',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GH.svg',
    emoji: '🇬🇭',
    enName: 'Ghana',
    arName: 'غانا'
  },
  {
    code: 'GL',
    unicode: 'U+1F1EC U+1F1F1',
    currency: 'DKK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GL.svg',
    emoji: '🇬🇱',
    enName: 'Greenland',
    arName: 'غرينلاند'
  },
  {
    code: 'GS',
    unicode: 'U+1F1EC U+1F1F8',
    currency: 'GBP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GS.svg',
    emoji: '🇬🇸',
    enName: 'South Georgia and the South Sandwich Is.',
    arName: 'غيرنسي، المنسب اللوني'
  },
  {
    code: 'GN',
    unicode: 'U+1F1EC U+1F1F3',
    currency: 'GNF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GN.svg',
    emoji: '🇬🇳',
    enName: 'Guinea',
    arName: 'غينيا'
  },
  {
    code: 'GQ',
    unicode: 'U+1F1EC U+1F1F6',
    currency: 'XAF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GQ.svg',
    emoji: '🇬🇶',
    enName: 'Equatorial Guinea',
    arName: 'غينيا الأستوائية'
  },
  {
    code: 'GW',
    unicode: 'U+1F1EC U+1F1FC',
    currency: 'XOF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GW.svg',
    emoji: '🇬🇼',
    enName: 'Guinea-Bissau',
    arName: 'غينيا بيساو'
  },
  {
    code: 'VU',
    unicode: 'U+1F1FB U+1F1FA',
    currency: 'VUV',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VU.svg',
    emoji: '🇻🇺',
    enName: 'Vanuatu',
    arName: 'فانواتو'
  },
  {
    code: 'FR',
    unicode: 'U+1F1EB U+1F1F7',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FR.svg',
    emoji: '🇫🇷',
    enName: 'France',
    arName: 'فرنسا'
  },
  {
    code: 'PS',
    unicode: 'U+1F1F5 U+1F1F8',
    currency: 'ILS',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PS.svg',
    emoji: '🇵🇸',
    enName: 'Palestine',
    arName: 'فلسطين'
  },
  {
    code: 'VE',
    unicode: 'U+1F1FB U+1F1EA',
    currency: 'VEF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VE.svg',
    emoji: '🇻🇪',
    enName: 'Venezuela',
    arName: 'فنزويلا'
  },
  {
    code: 'FI',
    unicode: 'U+1F1EB U+1F1EE',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FI.svg',
    emoji: '🇫🇮',
    enName: 'Finland',
    arName: 'فنلندا'
  },
  {
    code: 'VN',
    unicode: 'U+1F1FB U+1F1F3',
    currency: 'VND',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VN.svg',
    emoji: '🇻🇳',
    enName: 'Vietnam',
    arName: 'فيتنام'
  },
  {
    code: 'FJ',
    unicode: 'U+1F1EB U+1F1EF',
    currency: 'FJD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FJ.svg',
    emoji: '🇫🇯',
    enName: 'Fiji',
    arName: 'فيجى'
  },
  {
    code: 'CY',
    unicode: 'U+1F1E8 U+1F1FE',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CY.svg',
    emoji: '🇨🇾',
    enName: 'Cyprus',
    arName: 'قبرص'
  },
  {
    code: 'QA',
    unicode: 'U+1F1F6 U+1F1E6',
    currency: 'QAR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/QA.svg',
    emoji: '🇶🇦',
    enName: 'Quatar',
    arName: 'قطر'
  },
  {
    code: 'KZ',
    unicode: 'U+1F1F0 U+1F1FF',
    currency: 'KZT',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KZ.svg',
    emoji: '🇰🇿',
    enName: 'Kazakhstan',
    arName: 'كازاخستان'
  },
  {
    code: 'NC',
    unicode: 'U+1F1F3 U+1F1E8',
    currency: 'XPF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NC.svg',
    emoji: '🇳🇨',
    enName: 'New Caledonia',
    arName: 'كاليدونيا الحديثه'
  },
  {
    code: 'KG',
    unicode: 'U+1F1F0 U+1F1EC',
    currency: 'KGS',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KG.svg',
    emoji: '🇰🇬',
    enName: 'Kyrgyzstan',
    arName: 'كرجستان'
  },
  {
    code: 'HR',
    unicode: 'U+1F1ED U+1F1F7',
    currency: 'HRK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HR.svg',
    emoji: '🇭🇷',
    enName: 'Croatia',
    arName: 'كرواتيا'
  },
  {
    code: 'KH',
    unicode: 'U+1F1F0 U+1F1ED',
    currency: 'KHR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KH.svg',
    emoji: '🇰🇭',
    enName: 'Cambodia',
    arName: 'كمبوديا'
  },
  {
    code: 'CA',
    unicode: 'U+1F1E8 U+1F1E6',
    currency: 'CAD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CA.svg',
    emoji: '🇨🇦',
    enName: 'Canada',
    arName: 'كندا'
  },
  {
    code: 'KR',
    unicode: 'U+1F1F0 U+1F1F7',
    currency: 'KRW',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KR.svg',
    emoji: '🇰🇷',
    enName: 'Republic of Korea',
    arName: 'كوريا الجنوبية'
  },
  {
    code: 'CR',
    unicode: 'U+1F1E8 U+1F1F7',
    currency: 'CRC',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CR.svg',
    emoji: '🇨🇷',
    enName: 'Costa Rica',
    arName: 'كوستا ريكا'
  },
  {
    code: 'CC',
    unicode: 'U+1F1E8 U+1F1E8',
    currency: 'AUD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CC.svg',
    emoji: '🇨🇨',
    enName: 'Cocos Islands',
    arName: 'كوكوس (كيلينغ) جزر'
  },
  {
    code: 'CO',
    unicode: 'U+1F1E8 U+1F1F4',
    currency: 'COP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CO.svg',
    emoji: '🇨🇴',
    enName: 'Colombia',
    arName: 'كولومبيا'
  },
  {
    code: 'KI',
    unicode: 'U+1F1F0 U+1F1EE',
    currency: 'AUD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KI.svg',
    emoji: '🇰🇮',
    enName: 'Kirbati',
    arName: 'كيريباتي'
  },
  {
    code: 'KE',
    unicode: 'U+1F1F0 U+1F1EA',
    currency: 'KES',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KE.svg',
    emoji: '🇰🇪',
    enName: 'Kenya',
    arName: 'كينيا'
  },
  {
    code: 'CW',
    unicode: 'U+1F1E8 U+1F1FC',
    currency: 'ANG',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CW.svg',
    emoji: '🇨🇼',
    enName: 'Curacao',
    arName: 'كيوراساو'
  },
  {
    code: 'LV',
    unicode: 'U+1F1F1 U+1F1FB',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LV.svg',
    emoji: '🇱🇻',
    enName: 'Latvia',
    arName: 'لاتفيا'
  },
  {
    code: 'LA',
    unicode: 'U+1F1F1 U+1F1E6',
    currency: 'LAK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LA.svg',
    emoji: '🇱🇦',
    enName: "People's Republic of Lao",
    arName: 'لاوس'
  },
  {
    code: 'LB',
    unicode: 'U+1F1F1 U+1F1E7',
    currency: 'LBP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LB.svg',
    emoji: '🇱🇧',
    enName: 'Lebanon',
    arName: 'لبنان'
  },
  {
    code: 'LU',
    unicode: 'U+1F1F1 U+1F1FA',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LU.svg',
    emoji: '🇱🇺',
    enName: 'Luxembourg',
    arName: 'لوكسمبورج'
  },
  {
    code: 'LR',
    unicode: 'U+1F1F1 U+1F1F7',
    currency: 'LRD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LR.svg',
    emoji: '🇱🇷',
    enName: 'Liberia',
    arName: 'ليبريا'
  },
  {
    code: 'LY',
    unicode: 'U+1F1F1 U+1F1FE',
    currency: 'LYD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LY.svg',
    emoji: '🇱🇾',
    enName: 'Libyan Arab Jamahiriya',
    arName: 'ليبيا'
  },
  {
    code: 'LT',
    unicode: 'U+1F1F1 U+1F1F9',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LT.svg',
    emoji: '🇱🇹',
    enName: 'Lithuania',
    arName: 'ليتوانيا'
  },
  {
    code: 'LI',
    unicode: 'U+1F1F1 U+1F1EE',
    currency: 'CHF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LI.svg',
    emoji: '🇱🇮',
    enName: 'Liechtenstein',
    arName: 'ليختنشتاين'
  },
  {
    code: 'LS',
    unicode: 'U+1F1F1 U+1F1F8',
    currency: 'LSL',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LS.svg',
    emoji: '🇱🇸',
    enName: 'Lesotho',
    arName: 'ليسوتو'
  },
  {
    code: 'MW',
    unicode: 'U+1F1F2 U+1F1FC',
    currency: 'MWK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MW.svg',
    emoji: '🇲🇼',
    enName: 'Malawi',
    arName: 'مالاوى'
  },
  {
    code: 'MT',
    unicode: 'U+1F1F2 U+1F1F9',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MT.svg',
    emoji: '🇲🇹',
    enName: 'Malta',
    arName: 'مالطة'
  },
  {
    code: 'ML',
    unicode: 'U+1F1F2 U+1F1F1',
    currency: 'XOF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ML.svg',
    emoji: '🇲🇱',
    enName: 'Mali',
    arName: 'مالى'
  },
  {
    code: 'MY',
    unicode: 'U+1F1F2 U+1F1FE',
    currency: 'MYR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MY.svg',
    emoji: '🇲🇾',
    enName: 'Malaysia',
    arName: 'ماليزيا'
  },
  {
    code: 'YT',
    unicode: 'U+1F1FE U+1F1F9',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/YT.svg',
    emoji: '🇾🇹',
    enName: 'Mayotte',
    arName: 'مايوتي'
  },
  {
    code: 'MG',
    unicode: 'U+1F1F2 U+1F1EC',
    currency: 'MGA',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MG.svg',
    emoji: '🇲🇬',
    enName: 'Madagascar',
    arName: 'مدغشقر'
  },
  {
    code: 'EG',
    unicode: 'U+1F1EA U+1F1EC',
    currency: 'EGP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EG.svg',
    emoji: '🇪🇬',
    enName: 'Egypt',
    arName: 'مصر'
  },
  {
    code: 'TF',
    unicode: 'U+1F1F9 U+1F1EB',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TF.svg',
    emoji: '🇹🇫',
    enName: 'French Southern Territories',
    arName: 'مقاطعة جنوب فرنسا الغابون'
  },
  {
    code: 'MK',
    unicode: 'U+1F1F2 U+1F1F0',
    currency: 'MKD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MK.svg',
    emoji: '🇲🇰',
    enName: 'Macedonia',
    arName: 'مقدونيا'
  },
  {
    code: 'MO',
    unicode: 'U+1F1F2 U+1F1F4',
    currency: 'MOP',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MO.svg',
    emoji: '🇲🇴',
    enName: 'Macao',
    arName: 'مكاو'
  },
  {
    code: 'MC',
    unicode: 'U+1F1F2 U+1F1E8',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MC.svg',
    emoji: '🇲🇨',
    enName: 'Monaco',
    arName: 'مناكو'
  },
  {
    code: 'MN',
    unicode: 'U+1F1F2 U+1F1F3',
    currency: 'MNT',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MN.svg',
    emoji: '🇲🇳',
    enName: 'Mongolia',
    arName: 'منغوليا'
  },
  {
    code: 'MR',
    unicode: 'U+1F1F2 U+1F1F7',
    currency: 'MRO',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MR.svg',
    emoji: '🇲🇷',
    enName: 'Mauritania',
    arName: 'موريتانيا'
  },
  {
    code: 'MU',
    unicode: 'U+1F1F2 U+1F1FA',
    currency: 'MUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MU.svg',
    emoji: '🇲🇺',
    enName: 'Mauritius',
    arName: 'موريشيوس'
  },
  {
    code: 'MZ',
    unicode: 'U+1F1F2 U+1F1FF',
    currency: 'MZN',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MZ.svg',
    emoji: '🇲🇿',
    enName: 'Mozambique',
    arName: 'موزمبيق'
  },
  {
    code: 'MP',
    unicode: 'U+1F1F2 U+1F1F5',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MP.svg',
    emoji: '🇲🇵',
    enName: 'Northern Mariana Islands',
    arName: 'مونتسرات'
  },
  {
    code: 'MS',
    unicode: 'U+1F1F2 U+1F1F8',
    currency: 'XCD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MS.svg',
    emoji: '🇲🇸',
    enName: 'Montserrat',
    arName: 'مونتسيرات'
  },
  {
    code: 'ME',
    unicode: 'U+1F1F2 U+1F1EA',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ME.svg',
    emoji: '🇲🇪',
    enName: 'Montenegro',
    arName: 'مونتينيجرو'
  },
  {
    code: 'MM',
    unicode: 'U+1F1F2 U+1F1F2',
    currency: 'MMK',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MM.svg',
    emoji: '🇲🇲',
    enName: 'Myanmar',
    arName: 'ميانمار'
  },
  {
    code: 'FM',
    unicode: 'U+1F1EB U+1F1F2',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FM.svg',
    emoji: '🇫🇲',
    enName: 'Federated States of Micronesia',
    arName: 'ميكرونيزيا'
  },
  {
    code: 'NA',
    unicode: 'U+1F1F3 U+1F1E6',
    currency: 'NAD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NA.svg',
    emoji: '🇳🇦',
    enName: 'Namibia',
    arName: 'نامبيا'
  },
  {
    code: 'NR',
    unicode: 'U+1F1F3 U+1F1F7',
    currency: 'AUD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NR.svg',
    emoji: '🇳🇷',
    enName: 'Nauru',
    arName: 'ناورو'
  },
  {
    code: 'NP',
    unicode: 'U+1F1F3 U+1F1F5',
    currency: 'NPR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NP.svg',
    emoji: '🇳🇵',
    enName: 'Nepal',
    arName: 'نيبال'
  },
  {
    code: 'NG',
    unicode: 'U+1F1F3 U+1F1EC',
    currency: 'NGN',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NG.svg',
    emoji: '🇳🇬',
    enName: 'Nigeria',
    arName: 'نيجيريا'
  },
  {
    code: 'NI',
    unicode: 'U+1F1F3 U+1F1EE',
    currency: 'NIO',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NI.svg',
    emoji: '🇳🇮',
    enName: 'Nicaraguq',
    arName: 'نيكارجوا'
  },
  {
    code: 'NZ',
    unicode: 'U+1F1F3 U+1F1FF',
    currency: 'NZD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NZ.svg',
    emoji: '🇳🇿',
    enName: 'New Zealand',
    arName: 'نيوزلندا'
  },
  {
    code: 'NU',
    unicode: 'U+1F1F3 U+1F1FA',
    currency: 'NZD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NU.svg',
    emoji: '🇳🇺',
    enName: 'Niue',
    arName: 'نيوي'
  },
  {
    code: 'HT',
    unicode: 'U+1F1ED U+1F1F9',
    currency: 'HTG',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HT.svg',
    emoji: '🇭🇹',
    enName: 'Haiti',
    arName: 'هايتى'
  },
  {
    code: 'HN',
    unicode: 'U+1F1ED U+1F1F3',
    currency: 'HNL',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HN.svg',
    emoji: '🇭🇳',
    enName: 'Honduras',
    arName: 'هندوراس'
  },
  {
    code: 'NL',
    unicode: 'U+1F1F3 U+1F1F1',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NL.svg',
    emoji: '🇳🇱',
    enName: 'Netherlands',
    arName: 'هولندا'
  },
  {
    code: 'VA',
    unicode: 'U+1F1FB U+1F1E6',
    currency: 'EUR',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VA.svg',
    emoji: '🇻🇦',
    enName: 'Holy See (Vatican City State)',
    arName: 'هولى سى (فاتيكان سيتى)'
  },
  {
    code: 'HK',
    unicode: 'U+1F1ED U+1F1F0',
    currency: 'HKD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HK.svg',
    emoji: '🇭🇰',
    enName: 'Hong Kong',
    arName: 'هونج كونج'
  },
  {
    code: 'CF',
    unicode: 'U+1F1E8 U+1F1EB',
    currency: 'XAF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CF.svg',
    emoji: '🇨🇫',
    enName: 'Central African',
    arName: 'وسط افريقيا'
  },
  {
    code: 'WF',
    unicode: 'U+1F1FC U+1F1EB',
    currency: 'XPF',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/WF.svg',
    emoji: '🇼🇫',
    enName: 'Wallis and Futuna Islands',
    arName: 'ويلليس  و جزر فوتونا'
  },
  {
    code: 'UM',
    unicode: 'U+1F1FA U+1F1F2',
    currency: 'USD',
    image: 'https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UM.svg',
    emoji: '🇺🇲',
    enName: 'US Minor Outlaying Islands',
    arName: 'يونيتد ستاتس مينور ايسلا ندس'
  }
];

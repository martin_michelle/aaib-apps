export const countries: any = {
  en: [
    { name: 'Afghanistan', code: 'AF' },
    { name: 'Åland Islands', code: 'AX' },
    { name: 'Albania', code: 'AL' },
    { name: 'Algeria', code: 'DZ' },
    { name: 'American Samoa', code: 'AS' },
    { name: 'Andorra', code: 'AD' },
    { name: 'Angola', code: 'AO' },
    { name: 'Anguilla', code: 'AI' },
    { name: 'Antarctica', code: 'AQ' },
    { name: 'Antigua and Barbuda', code: 'AG' },
    { name: 'Argentina', code: 'AR' },
    { name: 'Armenia', code: 'AM' },
    { name: 'Aruba', code: 'AW' },
    { name: 'Australia', code: 'AU' },
    { name: 'Austria', code: 'AT' },
    { name: 'Azerbaijan', code: 'AZ' },
    { name: 'Bahamas', code: 'BS' },
    { name: 'Bahrain', code: 'BH' },
    { name: 'Bangladesh', code: 'BD' },
    { name: 'Barbados', code: 'BB' },
    { name: 'Belarus', code: 'BY' },
    { name: 'Belgium', code: 'BE' },
    { name: 'Belize', code: 'BZ' },
    { name: 'Benin', code: 'BJ' },
    { name: 'Bermuda', code: 'BM' },
    { name: 'Bhutan', code: 'BT' },
    { name: 'Bolivia', code: 'BO' },
    { name: 'Bosnia and Herzegovina', code: 'BA' },
    { name: 'Botswana', code: 'BW' },
    { name: 'Bouvet Island', code: 'BV' },
    { name: 'Brazil', code: 'BR' },
    { name: 'British Indian Ocean Territory', code: 'IO' },
    { name: 'Brunei Darussalam', code: 'BN' },
    { name: 'Bulgaria', code: 'BG' },
    { name: 'Burkina Faso', code: 'BF' },
    { name: 'Burundi', code: 'BI' },
    { name: 'Cambodia', code: 'KH' },
    { name: 'Cameroon', code: 'CM' },
    { name: 'Canada', code: 'CA' },
    { name: 'Cape Verde', code: 'CV' },
    { name: 'Cayman Islands', code: 'KY' },
    { name: 'Central African Republic', code: 'CF' },
    { name: 'Chad', code: 'TD' },
    { name: 'Chile', code: 'CL' },
    { name: 'China', code: 'CN' },
    { name: 'Christmas Island', code: 'CX' },
    { name: 'Cocos (Keeling) Islands', code: 'CC' },
    { name: 'Colombia', code: 'CO' },
    { name: 'Comoros', code: 'KM' },
    { name: 'Congo', code: 'CG' },
    { name: 'Congo, The Democratic Republic of the', code: 'CD' },
    { name: 'Cook Islands', code: 'CK' },
    { name: 'Costa Rica', code: 'CR' },
    { name: "Cote D'Ivoire", code: 'CI' },
    { name: 'Croatia', code: 'HR' },
    { name: 'Cuba', code: 'CU' },
    { name: 'Cyprus', code: 'CY' },
    { name: 'Czech Republic', code: 'CZ' },
    { name: 'Denmark', code: 'DK' },
    { name: 'Djibouti', code: 'DJ' },
    { name: 'Dominica', code: 'DM' },
    { name: 'Dominican Republic', code: 'DO' },
    { name: 'Ecuador', code: 'EC' },
    { name: 'Egypt', code: 'EG' },
    { name: 'El Salvador', code: 'SV' },
    { name: 'Equatorial Guinea', code: 'GQ' },
    { name: 'Eritrea', code: 'ER' },
    { name: 'Estonia', code: 'EE' },
    { name: 'Ethiopia', code: 'ET' },
    { name: 'Falkland Islands (Malvinas)', code: 'FK' },
    { name: 'Faroe Islands', code: 'FO' },
    { name: 'Fiji', code: 'FJ' },
    { name: 'Finland', code: 'FI' },
    { name: 'France', code: 'FR' },
    { name: 'French Guiana', code: 'GF' },
    { name: 'French Polynesia', code: 'PF' },
    { name: 'French Southern Territories', code: 'TF' },
    { name: 'Gabon', code: 'GA' },
    { name: 'Gambia', code: 'GM' },
    { name: 'Georgia', code: 'GE' },
    { name: 'Germany', code: 'DE' },
    { name: 'Ghana', code: 'GH' },
    { name: 'Gibraltar', code: 'GI' },
    { name: 'Greece', code: 'GR' },
    { name: 'Greenland', code: 'GL' },
    { name: 'Grenada', code: 'GD' },
    { name: 'Guadeloupe', code: 'GP' },
    { name: 'Guam', code: 'GU' },
    { name: 'Guatemala', code: 'GT' },
    { name: 'Guernsey', code: 'GG' },
    { name: 'Guinea', code: 'GN' },
    { name: 'Guinea-Bissau', code: 'GW' },
    { name: 'Guyana', code: 'GY' },
    { name: 'Haiti', code: 'HT' },
    { name: 'Heard Island and Mcdonald Islands', code: 'HM' },
    { name: 'Holy See (Vatican City State)', code: 'VA' },
    { name: 'Honduras', code: 'HN' },
    { name: 'Hong Kong', code: 'HK' },
    { name: 'Hungary', code: 'HU' },
    { name: 'Iceland', code: 'IS' },
    { name: 'India', code: 'IN' },
    { name: 'Indonesia', code: 'ID' },
    { name: 'Iran, Islamic Republic Of', code: 'IR' },
    { name: 'Iraq', code: 'IQ' },
    { name: 'Ireland', code: 'IE' },
    { name: 'Isle of Man', code: 'IM' },
    { name: 'Israel', code: 'IL' },
    { name: 'Italy', code: 'IT' },
    { name: 'Jamaica', code: 'JM' },
    { name: 'Japan', code: 'JP' },
    { name: 'Jersey', code: 'JE' },
    { name: 'Jordan', code: 'JO' },
    { name: 'Kazakhstan', code: 'KZ' },
    { name: 'Kenya', code: 'KE' },
    { name: 'Kiribati', code: 'KI' },
    { name: "Korea, Democratic People'S Republic of", code: 'KP' },
    { name: 'Korea, Republic of', code: 'KR' },
    { name: 'Kuwait', code: 'KW' },
    { name: 'Kyrgyzstan', code: 'KG' },
    { name: "Lao People'S Democratic Republic", code: 'LA' },
    { name: 'Latvia', code: 'LV' },
    { name: 'Lebanon', code: 'LB' },
    { name: 'Lesotho', code: 'LS' },
    { name: 'Liberia', code: 'LR' },
    { name: 'Libyan Arab Jamahiriya', code: 'LY' },
    { name: 'Liechtenstein', code: 'LI' },
    { name: 'Lithuania', code: 'LT' },
    { name: 'Luxembourg', code: 'LU' },
    { name: 'Macao', code: 'MO' },
    { name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK' },
    { name: 'Madagascar', code: 'MG' },
    { name: 'Malawi', code: 'MW' },
    { name: 'Malaysia', code: 'MY' },
    { name: 'Maldives', code: 'MV' },
    { name: 'Mali', code: 'ML' },
    { name: 'Malta', code: 'MT' },
    { name: 'Marshall Islands', code: 'MH' },
    { name: 'Martinique', code: 'MQ' },
    { name: 'Mauritania', code: 'MR' },
    { name: 'Mauritius', code: 'MU' },
    { name: 'Mayotte', code: 'YT' },
    { name: 'Mexico', code: 'MX' },
    { name: 'Micronesia, Federated States of', code: 'FM' },
    { name: 'Moldova, Republic of', code: 'MD' },
    { name: 'Monaco', code: 'MC' },
    { name: 'Mongolia', code: 'MN' },
    { name: 'Montserrat', code: 'MS' },
    { name: 'Morocco', code: 'MA' },
    { name: 'Mozambique', code: 'MZ' },
    { name: 'Myanmar', code: 'MM' },
    { name: 'Namibia', code: 'NA' },
    { name: 'Nauru', code: 'NR' },
    { name: 'Nepal', code: 'NP' },
    { name: 'Netherlands', code: 'NL' },
    { name: 'Netherlands Antilles', code: 'AN' },
    { name: 'New Caledonia', code: 'NC' },
    { name: 'New Zealand', code: 'NZ' },
    { name: 'Nicaragua', code: 'NI' },
    { name: 'Niger', code: 'NE' },
    { name: 'Nigeria', code: 'NG' },
    { name: 'Niue', code: 'NU' },
    { name: 'Norfolk Island', code: 'NF' },
    { name: 'Northern Mariana Islands', code: 'MP' },
    { name: 'Norway', code: 'NO' },
    { name: 'Oman', code: 'OM' },
    { name: 'Pakistan', code: 'PK' },
    { name: 'Palau', code: 'PW' },
    { name: 'Palestinian Territory, Occupied', code: 'PS' },
    { name: 'Panama', code: 'PA' },
    { name: 'Papua New Guinea', code: 'PG' },
    { name: 'Paraguay', code: 'PY' },
    { name: 'Peru', code: 'PE' },
    { name: 'Philippines', code: 'PH' },
    { name: 'Pitcairn', code: 'PN' },
    { name: 'Poland', code: 'PL' },
    { name: 'Portugal', code: 'PT' },
    { name: 'Puerto Rico', code: 'PR' },
    { name: 'Qatar', code: 'QA' },
    { name: 'Reunion', code: 'RE' },
    { name: 'Romania', code: 'RO' },
    { name: 'Russian Federation', code: 'RU' },
    { name: 'RWANDA', code: 'RW' },
    { name: 'Saint Helena', code: 'SH' },
    { name: 'Saint Kitts and Nevis', code: 'KN' },
    { name: 'Saint Lucia', code: 'LC' },
    { name: 'Saint Pierre and Miquelon', code: 'PM' },
    { name: 'Saint Vincent and the Grenadines', code: 'VC' },
    { name: 'Samoa', code: 'WS' },
    { name: 'San Marino', code: 'SM' },
    { name: 'Sao Tome and Principe', code: 'ST' },
    { name: 'Saudi Arabia', code: 'SA' },
    { name: 'Senegal', code: 'SN' },
    { name: 'Serbia and Montenegro', code: 'CS' },
    { name: 'Seychelles', code: 'SC' },
    { name: 'Sierra Leone', code: 'SL' },
    { name: 'Singapore', code: 'SG' },
    { name: 'Slovakia', code: 'SK' },
    { name: 'Slovenia', code: 'SI' },
    { name: 'Solomon Islands', code: 'SB' },
    { name: 'Somalia', code: 'SO' },
    { name: 'South Africa', code: 'ZA' },
    { name: 'South Georgia and the South Sandwich Islands', code: 'GS' },
    { name: 'Spain', code: 'ES' },
    { name: 'Sri Lanka', code: 'LK' },
    { name: 'Sudan', code: 'SD' },
    { name: 'Suriname', code: 'SR' },
    { name: 'Svalbard and Jan Mayen', code: 'SJ' },
    { name: 'Swaziland', code: 'SZ' },
    { name: 'Sweden', code: 'SE' },
    { name: 'Switzerland', code: 'CH' },
    { name: 'Syrian Arab Republic', code: 'SY' },
    { name: 'Taiwan, Province of China', code: 'TW' },
    { name: 'Tajikistan', code: 'TJ' },
    { name: 'Tanzania, United Republic of', code: 'TZ' },
    { name: 'Thailand', code: 'TH' },
    { name: 'Timor-Leste', code: 'TL' },
    { name: 'Togo', code: 'TG' },
    { name: 'Tokelau', code: 'TK' },
    { name: 'Tonga', code: 'TO' },
    { name: 'Trinidad and Tobago', code: 'TT' },
    { name: 'Tunisia', code: 'TN' },
    { name: 'Turkey', code: 'TR' },
    { name: 'Turkmenistan', code: 'TM' },
    { name: 'Turks and Caicos Islands', code: 'TC' },
    { name: 'Tuvalu', code: 'TV' },
    { name: 'Uganda', code: 'UG' },
    { name: 'Ukraine', code: 'UA' },
    { name: 'United Arab Emirates', code: 'AE' },
    { name: 'United Kingdom', code: 'GB' },
    { name: 'United States', code: 'US' },
    { name: 'United States Minor Outlying Islands', code: 'UM' },
    { name: 'Uruguay', code: 'UY' },
    { name: 'Uzbekistan', code: 'UZ' },
    { name: 'Vanuatu', code: 'VU' },
    { name: 'Venezuela', code: 'VE' },
    { name: 'Viet Nam', code: 'VN' },
    { name: 'Virgin Islands, British', code: 'VG' },
    { name: 'Virgin Islands, U.S.', code: 'VI' },
    { name: 'Wallis and Futuna', code: 'WF' },
    { name: 'Western Sahara', code: 'EH' },
    { name: 'Yemen', code: 'YE' },
    { name: 'Zambia', code: 'ZM' },
    { name: 'Zimbabwe', code: 'ZW' }
  ],
  ar: [
    {
      code: 'AD',
      name: 'أندورا'
    },
    {
      code: 'AE',
      name: 'الامارات العربية المتحدة'
    },
    {
      code: 'AF',
      name: 'أفغانستان'
    },
    {
      code: 'AG',
      name: 'أنتيغوا وباربودا'
    },
    {
      code: 'AI',
      name: 'أنجويلا'
    },
    {
      code: 'AL',
      name: 'ألبانيا'
    },
    {
      code: 'AM',
      name: 'أرمينيا'
    },
    {
      code: 'AO',
      name: 'أنجولا'
    },
    {
      code: 'AQ',
      name: 'القارة القطبية الجنوبية'
    },
    {
      code: 'AR',
      name: 'الأرجنتين'
    },
    {
      code: 'AS',
      name: 'ساموا الأمريكية'
    },
    {
      code: 'AT',
      name: 'النمسا'
    },
    {
      code: 'AU',
      name: 'أستراليا'
    },
    {
      code: 'AW',
      name: 'آروبا'
    },
    {
      code: 'AX',
      name: 'جزر أولان'
    },
    {
      code: 'AZ',
      name: 'أذربيجان'
    },
    {
      code: 'BA',
      name: 'البوسنة والهرسك'
    },
    {
      code: 'BB',
      name: 'بربادوس'
    },
    {
      code: 'BD',
      name: 'بنجلاديش'
    },
    {
      code: 'BE',
      name: 'بلجيكا'
    },
    {
      code: 'BF',
      name: 'بوركينا فاسو'
    },
    {
      code: 'BG',
      name: 'بلغاريا'
    },
    {
      code: 'BH',
      name: 'البحرين'
    },
    {
      code: 'BI',
      name: 'بوروندي'
    },
    {
      code: 'BJ',
      name: 'بنين'
    },
    {
      code: 'BL',
      name: 'سان بارتيلمي'
    },
    {
      code: 'BM',
      name: 'برمودا'
    },
    {
      code: 'BN',
      name: 'بروناي'
    },
    {
      code: 'BO',
      name: 'بوليفيا'
    },
    {
      code: 'BQ',
      name: 'الجزر الكاريبية الهولندية'
    },
    {
      code: 'BR',
      name: 'البرازيل'
    },
    {
      code: 'BS',
      name: 'الباهاما'
    },
    {
      code: 'BT',
      name: 'بوتان'
    },
    {
      code: 'BV',
      name: 'جزيرة بوفيه'
    },
    {
      code: 'BW',
      name: 'بتسوانا'
    },
    {
      code: 'BY',
      name: 'روسيا البيضاء'
    },
    {
      code: 'BZ',
      name: 'بليز'
    },
    {
      code: 'CA',
      name: 'كندا'
    },
    {
      code: 'CC',
      name: 'جزر كوكوس'
    },
    {
      code: 'CD',
      name: 'جمهورية الكونغو الديمقراطية'
    },
    {
      code: 'CF',
      name: 'جمهورية أفريقيا الوسطى'
    },
    {
      code: 'CG',
      name: 'جمهورية الكونغو'
    },
    {
      code: 'CH',
      name: 'سويسرا'
    },
    {
      code: 'CI',
      name: 'ساحل العاج'
    },
    {
      code: 'CK',
      name: 'جزر كوك'
    },
    {
      code: 'CL',
      name: 'شيلي'
    },
    {
      code: 'CM',
      name: 'الكاميرون'
    },
    {
      code: 'CN',
      name: 'الصين'
    },
    {
      code: 'CO',
      name: 'كولومبيا'
    },
    {
      code: 'CR',
      name: 'كوستاريكا'
    },
    {
      code: 'CU',
      name: 'كوبا'
    },
    {
      code: 'CV',
      name: 'الرأس الأخضر'
    },
    {
      code: 'CW',
      name: 'كوراساو'
    },
    {
      code: 'CX',
      name: 'جزيرة عيد الميلاد'
    },
    {
      code: 'CY',
      name: 'قبرص'
    },
    {
      code: 'CZ',
      name: 'جمهورية التشيك'
    },
    {
      code: 'DE',
      name: 'ألمانيا'
    },
    {
      code: 'DJ',
      name: 'جيبوتي'
    },
    {
      code: 'DK',
      name: 'الدانمرك'
    },
    {
      code: 'DM',
      name: 'دومينيكا'
    },
    {
      code: 'DO',
      name: 'جمهورية الدومينيكان'
    },
    {
      code: 'DZ',
      name: 'الجزائر'
    },
    {
      code: 'EC',
      name: 'الاكوادور'
    },
    {
      code: 'EE',
      name: 'استونيا'
    },
    {
      code: 'EG',
      name: 'مصر'
    },
    {
      code: 'EH',
      name: 'الصحراء الغربية'
    },
    {
      code: 'ER',
      name: 'اريتريا'
    },
    {
      code: 'ES',
      name: 'أسبانيا'
    },
    {
      code: 'ET',
      name: 'اثيوبيا'
    },
    {
      code: 'FI',
      name: 'فنلندا'
    },
    {
      code: 'FJ',
      name: 'فيجي'
    },
    {
      code: 'FK',
      name: 'جزر فوكلاند'
    },
    {
      code: 'FM',
      name: 'ميكرونيزيا'
    },
    {
      code: 'FO',
      name: 'جزر فارو'
    },
    {
      code: 'FR',
      name: 'فرنسا'
    },
    {
      code: 'GA',
      name: 'الجابون'
    },
    {
      code: 'GB',
      name: 'المملكة المتحدة'
    },
    {
      code: 'GD',
      name: 'جرينادا'
    },
    {
      code: 'GE',
      name: 'جورجيا'
    },
    {
      code: 'GF',
      name: 'غويانا'
    },
    {
      code: 'GG',
      name: 'جيرنزي'
    },
    {
      code: 'GH',
      name: 'غانا'
    },
    {
      code: 'GI',
      name: 'جبل طارق'
    },
    {
      code: 'GL',
      name: 'جرينلاند'
    },
    {
      code: 'GM',
      name: 'غامبيا'
    },
    {
      code: 'GN',
      name: 'غينيا'
    },
    {
      code: 'GP',
      name: 'جوادلوب'
    },
    {
      code: 'GQ',
      name: 'غينيا الاستوائية'
    },
    {
      code: 'GR',
      name: 'اليونان'
    },
    {
      code: 'GS',
      name: 'جورجيا الجنوبية وجزر ساندويتش الجنوبية'
    },
    {
      code: 'GT',
      name: 'جواتيمالا'
    },
    {
      code: 'GU',
      name: 'جوام'
    },
    {
      code: 'GW',
      name: 'غينيا بيساو'
    },
    {
      code: 'GY',
      name: 'غيانا'
    },
    {
      code: 'HK',
      name: 'هونغ كونغ'
    },
    {
      code: 'HM',
      name: 'جزيرة هيرد وجزر ماكدونالد'
    },
    {
      code: 'HN',
      name: 'هندوراس'
    },
    {
      code: 'HR',
      name: 'كرواتيا'
    },
    {
      code: 'HT',
      name: 'هايتي'
    },
    {
      code: 'HU',
      name: 'المجر'
    },
    {
      code: 'ID',
      name: 'اندونيسيا'
    },
    {
      code: 'IE',
      name: 'أيرلندا'
    },
    {
      code: 'IL',
      name: 'اسرائيل'
    },
    {
      code: 'IM',
      name: 'جزيرة مان'
    },
    {
      code: 'IN',
      name: 'الهند'
    },
    {
      code: 'IO',
      name: 'إقليم المحيط الهندي البريطاني'
    },
    {
      code: 'IQ',
      name: 'العراق'
    },
    {
      code: 'IR',
      name: 'ايران'
    },
    {
      code: 'IS',
      name: 'أيسلندا'
    },
    {
      code: 'IT',
      name: 'ايطاليا'
    },
    {
      code: 'JE',
      name: 'جيرسي'
    },
    {
      code: 'JM',
      name: 'جامايكا'
    },
    {
      code: 'JO',
      name: 'الأردن'
    },
    {
      code: 'JP',
      name: 'اليابان'
    },
    {
      code: 'KE',
      name: 'كينيا'
    },
    {
      code: 'KG',
      name: 'قرغيزستان'
    },
    {
      code: 'KH',
      name: 'كمبوديا'
    },
    {
      code: 'KI',
      name: 'كيريباتي'
    },
    {
      code: 'KM',
      name: 'جزر القمر'
    },
    {
      code: 'KN',
      name: 'سانت كيتس ونيفيس'
    },
    {
      code: 'KP',
      name: 'كوريا الشمالية'
    },
    {
      code: 'KR',
      name: 'كوريا الجنوبية'
    },
    {
      code: 'KW',
      name: 'الكويت'
    },
    {
      code: 'KY',
      name: 'جزر كايمان'
    },
    {
      code: 'KZ',
      name: 'كازاخستان'
    },
    {
      code: 'LA',
      name: 'لاوس'
    },
    {
      code: 'LB',
      name: 'لبنان'
    },
    {
      code: 'LC',
      name: 'سانت لوسيا'
    },
    {
      code: 'LI',
      name: 'ليختنشتاين'
    },
    {
      code: 'LK',
      name: 'سريلانكا'
    },
    {
      code: 'LR',
      name: 'ليبيريا'
    },
    {
      code: 'LS',
      name: 'ليسوتو'
    },
    {
      code: 'LT',
      name: 'ليتوانيا'
    },
    {
      code: 'LU',
      name: 'لوكسمبورج'
    },
    {
      code: 'LV',
      name: 'لاتفيا'
    },
    {
      code: 'LY',
      name: 'ليبيا'
    },
    {
      code: 'MA',
      name: 'المغرب'
    },
    {
      code: 'MC',
      name: 'موناكو'
    },
    {
      code: 'MD',
      name: 'مولدافيا'
    },
    {
      code: 'ME',
      name: 'الجبل الأسود'
    },
    {
      code: 'MF',
      name: 'تجمع سان مارتين'
    },
    {
      code: 'MG',
      name: 'مدغشقر'
    },
    {
      code: 'MH',
      name: 'جزر مارشال'
    },
    {
      code: 'MK',
      name: 'مقدونيا'
    },
    {
      code: 'ML',
      name: 'مالي'
    },
    {
      code: 'MM',
      name: 'ميانمار'
    },
    {
      code: 'MN',
      name: 'منغوليا'
    },
    {
      code: 'MO',
      name: 'ماكاو'
    },
    {
      code: 'MP',
      name: 'جزر ماريانا الشمالية'
    },
    {
      code: 'MQ',
      name: 'مارتينيك'
    },
    {
      code: 'MR',
      name: 'موريتانيا'
    },
    {
      code: 'MS',
      name: 'مونتسرات'
    },
    {
      code: 'MT',
      name: 'مالطا'
    },
    {
      code: 'MU',
      name: 'موريشيوس'
    },
    {
      code: 'MV',
      name: 'جزر المالديف'
    },
    {
      code: 'MW',
      name: 'ملاوي'
    },
    {
      code: 'MX',
      name: 'المكسيك'
    },
    {
      code: 'MY',
      name: 'ماليزيا'
    },
    {
      code: 'MZ',
      name: 'موزمبيق'
    },
    {
      code: 'NA',
      name: 'ناميبيا'
    },
    {
      code: 'NC',
      name: 'كاليدونيا الجديدة'
    },
    {
      code: 'NE',
      name: 'النيجر'
    },
    {
      code: 'NF',
      name: 'جزيرة نورفولك'
    },
    {
      code: 'NG',
      name: 'نيجيريا'
    },
    {
      code: 'NI',
      name: 'نيكاراجوا'
    },
    {
      code: 'NL',
      name: 'هولندا'
    },
    {
      code: 'NO',
      name: 'النرويج'
    },
    {
      code: 'NP',
      name: 'نيبال'
    },
    {
      code: 'NR',
      name: 'نورو'
    },
    {
      code: 'NU',
      name: 'نيوي'
    },
    {
      code: 'NZ',
      name: 'نيوزيلاندا'
    },
    {
      code: 'OM',
      name: 'عمان'
    },
    {
      code: 'PA',
      name: 'بنما'
    },
    {
      code: 'PE',
      name: 'بيرو'
    },
    {
      code: 'PF',
      name: 'بولينزيا الفرنسية'
    },
    {
      code: 'PG',
      name: 'بابوا غينيا الجديدة'
    },
    {
      code: 'PH',
      name: 'الفيلبين'
    },
    {
      code: 'PK',
      name: 'باكستان'
    },
    {
      code: 'PL',
      name: 'بولندا'
    },
    {
      code: 'PM',
      name: 'سان بيير وميكلون'
    },
    {
      code: 'PN',
      name: 'بتكايرن'
    },
    {
      code: 'PR',
      name: 'بورتوريكو'
    },
    {
      code: 'PS',
      name: 'فلسطين'
    },
    {
      code: 'PT',
      name: 'البرتغال'
    },
    {
      code: 'PW',
      name: 'بالاو'
    },
    {
      code: 'PY',
      name: 'باراجواي'
    },
    {
      code: 'QA',
      name: 'قطر'
    },
    {
      code: 'RE',
      name: 'روينيون'
    },
    {
      code: 'RO',
      name: 'رومانيا'
    },
    {
      code: 'RS',
      name: 'صربيا'
    },
    {
      code: 'RU',
      name: 'روسيا'
    },
    {
      code: 'RW',
      name: 'رواندا'
    },
    {
      code: 'SA',
      name: 'السعودية'
    },
    {
      code: 'SB',
      name: 'جزر سليمان'
    },
    {
      code: 'SC',
      name: 'سيشل'
    },
    {
      code: 'SD',
      name: 'السودان'
    },
    {
      code: 'SE',
      name: 'السويد'
    },
    {
      code: 'SG',
      name: 'سنغافورة'
    },
    {
      code: 'SH',
      name: 'سانت هيلينا وأسينشين وتريستان دا كونا'
    },
    {
      code: 'SI',
      name: 'سلوفينيا'
    },
    {
      code: 'SJ',
      name: 'سفالبارد ويان ماين'
    },
    {
      code: 'SK',
      name: 'سلوفاكيا'
    },
    {
      code: 'SL',
      name: 'سيراليون'
    },
    {
      code: 'SM',
      name: 'سان مارينو'
    },
    {
      code: 'SN',
      name: 'السنغال'
    },
    {
      code: 'SO',
      name: 'الصومال'
    },
    {
      code: 'SR',
      name: 'سورينام'
    },
    {
      code: 'SS',
      name: 'جنوب السودان'
    },
    {
      code: 'ST',
      name: 'ساو تومي وبرينسيب'
    },
    {
      code: 'SV',
      name: 'السلفادور'
    },
    {
      code: 'SX',
      name: 'سينت مارتن'
    },
    {
      code: 'SY',
      name: 'سوريا'
    },
    {
      code: 'SZ',
      name: 'سوازيلاند'
    },
    {
      code: 'TC',
      name: 'جزر توركس وكايكوس'
    },
    {
      code: 'TD',
      name: 'تشاد'
    },
    {
      code: 'TF',
      name: 'أراض فرنسية جنوبية وأنتارتيكية'
    },
    {
      code: 'TG',
      name: 'توجو'
    },
    {
      code: 'TH',
      name: 'تايلند'
    },
    {
      code: 'TJ',
      name: 'طاجكستان'
    },
    {
      code: 'TK',
      name: 'توكيلو'
    },
    {
      code: 'TL',
      name: 'تيمور الشرقية'
    },
    {
      code: 'TM',
      name: 'تركمانستان'
    },
    {
      code: 'TN',
      name: 'تونس'
    },
    {
      code: 'TO',
      name: 'تونجا'
    },
    {
      code: 'TR',
      name: 'تركيا'
    },
    {
      code: 'TT',
      name: 'ترينيداد وتوباغو'
    },
    {
      code: 'TV',
      name: 'توفالو'
    },
    {
      code: 'TW',
      name: 'تايوان'
    },
    {
      code: 'TZ',
      name: 'تانزانيا'
    },
    {
      code: 'UA',
      name: 'أوكرانيا'
    },
    {
      code: 'UG',
      name: 'أوغندا'
    },
    {
      code: 'UM',
      name: 'جزر الولايات المتحدة الصغيرة النائية'
    },
    {
      code: 'US',
      name: 'الولايات المتحدة'
    },
    {
      code: 'UY',
      name: 'أورجواي'
    },
    {
      code: 'UZ',
      name: 'أوزبكستان'
    },
    {
      code: 'VA',
      name: 'الفاتيكان'
    },
    {
      code: 'VC',
      name: 'سانت فينسنت والغرينادين'
    },
    {
      code: 'VE',
      name: 'فنزويلا'
    },
    {
      code: 'VG',
      name: 'جزر العذراء البريطانية'
    },
    {
      code: 'VI',
      name: 'جزر العذراء الأمريكية'
    },
    {
      code: 'VN',
      name: 'فيتنام'
    },
    {
      code: 'VU',
      name: 'فانواتو'
    },
    {
      code: 'WF',
      name: 'والس وفوتونا'
    },
    {
      code: 'WS',
      name: 'ساموا'
    },
    {
      code: 'YE',
      name: 'اليمن'
    },
    {
      code: 'YT',
      name: 'مايوت'
    },
    {
      code: 'ZA',
      name: 'جنوب أفريقيا'
    },
    {
      code: 'ZM',
      name: 'زامبيا'
    },
    {
      code: 'ZW',
      name: 'زيمبابوي'
    }
  ]
};

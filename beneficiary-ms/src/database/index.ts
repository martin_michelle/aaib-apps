// eslint-disable-next-line
const mongoose = require('mongoose');
import { logger } from '../core/Logger';
import { mongoConfig } from '../config';

export const connect = () => {
  try {
    const database = mongoConfig.url;
    mongoose.connect(database, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    const { connection } = mongoose;
    connection.on('connected', () => {
      logger.info('Database Connection was Successful');
    });
    connection.on('error', (err: any) => {
      logger.error(`Database Connection Failed ${err}`);
      throw new Error(err);
    });
    connection.on('disconnected', () => logger.info('Database Connection Disconnected'));

    return connection;
  } catch (e) {
    logger.error(e);
    throw e;
  }
};

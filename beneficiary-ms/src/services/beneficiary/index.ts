import {
  BlockedTokenError,
  DuplicateBeneficiaryError,
  ForbiddenError,
  InternalError,
  InvalidOtpError,
  NotFoundError
} from '../../core/ApiError';
import { logger } from '../../core/Logger';
import { BeneficiaryInterface } from 'app-request';
import { KafkaMessage } from 'kafkajs';
import Beneficiary from './model';
import Relation from './relationModel';
import { produce } from '../../kafka';
import { ibanKey, promiseTimeout, status, proxyEnabled, proxySettings } from '../../config';
import axios, { AxiosRequestConfig } from 'axios';
import messages from '../../messages';
import updateStatistics from '../../helpers/statistics';
import { countries } from '../../data/countries';
import { englishSortedCountries } from '../../data/englishSortedCountries';
import { arabicSortedCountries } from '../../data/arabicSortedCountries';

export const addUserBeneficiaries = async (
  cif: string,
  user: string,
  body: BeneficiaryInterface,
  platform: string
) => {
  body.cif = cif;

  const allBeneficiaries = await Beneficiary.find({ cif, swift: body?.swift });

  const duplicate = checkIfBeneficiaryExists(allBeneficiaries, body);

  if (duplicate) {
    throw new DuplicateBeneficiaryError();
  }

  const beneficiary = new Beneficiary(body);
  await beneficiary.save();

  updateStatistics('', {
    cif: cif,
    user: user,
    type: 'beneficiariesActions',
    subType: 'addUserBeneficiaries',
    status: status.success,
    date: new Date(),
    microservice: 'beneficiary',
    action: 'add',
    beneficiaryName: body.name,
    accountNumber: body.accountNumber ? body.accountNumber : body.iban,
    country: body.country,
    bank: body.bank,
    currency: body.currency,
    platform: platform,
    address: body.address
  }).catch((e: any) => logger.error(e.message));

  return { message: 'Success', id: beneficiary._id };
};

const checkIfBeneficiaryExists = (
  allBeneficiaries: BeneficiaryInterface[],
  body: BeneficiaryInterface
) => {
  try {
    let duplicate = false;

    if (allBeneficiaries.length > 0) {
      if (body.iban) {
        const existsBeneficiary = allBeneficiaries.filter((ben: any) => {
          if (ben?.iban === body?.iban || (body.iban && body?.iban?.includes(ben?.accountNumber))) {
            return ben;
          }
        });

        duplicate = existsBeneficiary.length > 0;
      }

      if (body.accountNumber) {
        const existsBeneficiary = allBeneficiaries.filter((ben: any) => {
          if (
            ben?.accountNumber === body?.accountNumber ||
            (ben.iban && ben?.iban.includes(body?.accountNumber))
          ) {
            return ben;
          }
        });

        duplicate = existsBeneficiary.length > 0;
      }

      if (body.isMobileNumber && body.accountNumber) {
        const existsBeneficiary = allBeneficiaries.filter((ben: any) => {
          if (ben?.accountNumber === body?.accountNumber) {
            return ben;
          }
        });

        duplicate = existsBeneficiary.length > 0;
      }
    }

    return duplicate;
  } catch (error) {
    logger.error(error.message);
    return false;
  }
};

export const getUserBeneficiaries = async (user: string, isCreditCardNumber: boolean) => {
  const agregatePipeline: any = [
    {
      $match: {
        cif: user
      }
    },
    {
      $sort: {
        name: 1
      }
    }
  ];
  if (isCreditCardNumber) {
    agregatePipeline[0].$match.isCreditCardNumber = isCreditCardNumber;
  } else {
    agregatePipeline[0].$match.$or = [
      {
        isCreditCardNumber
      },
      { isCreditCardNumber: { $exists: false } }
    ];
  }
  return Beneficiary.aggregate(agregatePipeline);
};

export const deleteUserBeneficiaries = async (
  cif: string,
  id: string,
  user: string,
  platform: string
) => {
  const beneficiary = await Beneficiary.findOneAndDelete({ cif: cif, _id: id });
  updateStatistics('', {
    cif: cif,
    user: user,
    type: 'beneficiariesActions',
    subType: 'deleteUserBeneficiaries',
    status: status.success,
    date: new Date(),
    microservice: 'beneficiary',
    action: 'delete',
    beneficiaryName: beneficiary.name,
    accountNumber: beneficiary.accountNumber ? beneficiary.accountNumber : beneficiary.iban,
    country: beneficiary.country,
    bank: beneficiary.bank,
    currency: beneficiary.currency,
    platform: platform,
    address: beneficiary.address
  }).catch((e: any) => logger.error(e.message));
  return { message: 'Success' };
};

export const updateUserBeneficiaries = async (
  cif: string,
  id: string,
  body: BeneficiaryInterface,
  user: string,
  platform: string
) => {
  if (body.accountNumber || body.iban) {
    if (body.accountNumber) body.iban = '';
    else body.accountNumber = '';
  }
  const beneficiary = await Beneficiary.findOne({ cif: cif, _id: id });

  const updatedFields = getUpdatedFields(body, beneficiary._doc);

  await Beneficiary.updateOne({ cif: cif, _id: id }, { $set: body });
  updateStatistics('', {
    cif: cif,
    user: user,
    type: 'beneficiariesActions',
    subType: 'updateUserBeneficiaries',
    status: status.success,
    date: new Date(),
    microservice: 'beneficiary',
    action: 'update',
    beneficiaryName: body.name,
    accountNumber: body.accountNumber ? body.accountNumber : body.iban,
    country: body.country,
    bank: body.bank,
    currency: body.currency,
    platform: platform,
    address: body.address,
    updatedFields
  }).catch((e: any) => logger.error(e.message));
  return { message: 'Success' };
};

export const prepareKafkaResponse = async (message: KafkaMessage) => {
  if (!message.value) throw new InternalError(messages.accounts.missingValue.en);
  const data = JSON.parse(message.value.toString());
  if (!data.error) {
    return data.message.Data || data.message.data;
  } else {
    logger.debug(data.message);
    if (data.message === messages.authorization.otpBlocked.en) {
      throw new BlockedTokenError();
    } else if (data.message === messages.accounts.forbidden.en) {
      throw new ForbiddenError();
    } else if (data.message === messages.authorization.invalidOtp.en) {
      throw new InvalidOtpError();
    } else throw new InternalError(messages.accounts.internalError.en);
  }
};

export const getListOfCountries = async (language = 'en') => {
  return { countries: countries[language] };
};

export const getModifiedListOfCountries = async (language = 'en') => {
  if (language === 'ar') {
    return { countries: arabicSortedCountries };
  } else {
    return { countries: englishSortedCountries };
  }
};

export const getListOfBanks = async (country: string, tracingHeaders: any) => {
  try {
    const data = {
      route: `list/swift/country/${country}`
    };
    const producerData = await produce(data, tracingHeaders);
    const kafkaResult = await prepareKafkaResponse(producerData);
    const countries: { name: any; swift: any; country: any }[] = [];
    kafkaResult.map((country: any) => {
      if (country.BIC && country.Institution) {
        countries.push({
          name: country.Institution,
          swift: country.BIC,
          country: country.CountryCode
        });
      }
    });
    return countries;
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

export const validateIBAN = async (iban: string) => {
  try {
    const url = `https://api.iban.com/clients/api/v4/iban/?format=json&api_key=${ibanKey}&iban=${iban}`;
    let config: AxiosRequestConfig = { timeout: promiseTimeout };
    if (proxyEnabled == 'true') {
      config = {
        timeout: promiseTimeout,
        proxy: proxySettings
      };
    }
    const ibanInfo = await axios.get(url, config);
    console.log(ibanInfo);
    const bankInfo = ibanInfo.data.bank_data;
    if (bankInfo && bankInfo.bic && bankInfo.bank) {
      return {
        info: {
          country: bankInfo.country,
          address: bankInfo.address,
          city: bankInfo.city,
          account: bankInfo.account,
          branch_code: bankInfo.branch_code,
          bank_code: bankInfo.bank_code,
          code: bankInfo.country_iso,
          swift: bankInfo.bic,
          bank: bankInfo.bank,
          branch: bankInfo.branch
        }
      };
    }
    throw new NotFoundError(messages.beneficiary.invalid.en);
  } catch (e) {
    logger.error(e.message);
    logger.error(e);
    console.log(e);
    throw new NotFoundError(messages.beneficiary.invalid.en);
  }
};

export const getListOfCurrencies = async (tracingHeaders: any) => {
  try {
    const data = {
      route: `list/currency`
    };
    const producerData = await produce(data, tracingHeaders);
    const currencies = await prepareKafkaResponse(producerData);
    return currencies;
  } catch (e) {
    logger.error(e.message);
    throw new NotFoundError(messages.beneficiary.invalid.en);
  }
};

export const getListOfRelationships = async () => {
  try {
    return Relation.find({}, { _id: 0 });
  } catch (e) {
    logger.error(e.message);
    throw new NotFoundError(messages.beneficiary.invalid.en);
  }
};

export const getUserBeneficiariesById = async (cif: string, id: string) => {
  return Beneficiary.findOne({ _id: id, cif });
};

export const getUpdatedFields = (updateBody: any, oldBenificiary: any = {}) => {
  try {
    const updatedFields: Array<string> = [];
    const notCheckedFields: Array<string> = [
      '_id',
      'createdAt',
      'updatedAt',
      '__v',
      'requiresOtp',
      'cif',
      'swift',
      'isCreditCardNumber',
      'relationship'
    ];
    Object.keys(oldBenificiary).forEach((key) => {
      if (!notCheckedFields.includes(key) && oldBenificiary[key] !== updateBody[key]) {
        if (key === 'iban') updatedFields.push('accountNumber');
        else updatedFields.push(key);
      }
    });
    return updatedFields;
  } catch (error) {
    logger.info(error.message);
  }
};

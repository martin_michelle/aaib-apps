import mongoose from 'mongoose';

const model = new mongoose.Schema({
  iban: String,
  country: String,
  code: String,
  swift: String,
  bank: String,
  branch: String
});
model.index({ iban: 1 });
export default mongoose.model('iban', model);

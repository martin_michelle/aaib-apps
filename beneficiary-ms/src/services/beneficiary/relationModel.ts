import mongoose from 'mongoose';

const model = new mongoose.Schema({
  name: String
});
export default mongoose.model('Relation', model);

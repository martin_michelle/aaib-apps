import mongoose from 'mongoose';

const model = new mongoose.Schema({
  name: String,
  country: String,
  swift: String
});
model.index({ name: 1 });
model.index({ country: 1 });
export default mongoose.model('Bank', model);

// eslint-disable-next-line
const mongoose = require('mongoose');

const model = new mongoose.Schema(
  {
    cif: String,
    name: String,
    nickName: String,
    isCreditCardNumber: {
      type: Boolean,
      default: false
    },
    isMobileNumber: {
      type: Boolean,
      default: false
    },
    accountNumber: String,
    iban: String,
    country: String,
    countryCode: String,
    bank: String,
    swift: String,
    relationship: String,
    currency: String,
    address: String,
    requiresOtp: {
      type: Boolean,
      default: true
    }
  },
  { timestamps: true }
);
model.index({ cif: 1, name: 1, isCreditCardNumber: 1, accountNumber: 1, iban: 1 });
export default mongoose.model('Beneficiary', model);

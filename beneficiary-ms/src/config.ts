// Mapper for environment variables

import { AxiosProxyConfig } from 'axios';

export const environment = process.env.NODE_ENV;
export const port = process.env.PORT;

export const corsUrl = process.env.CORS_URL;

export const kafkaOptions = {
  clientId: process.env.KAFKA_CLIENT_ID || '',
  hosts: process.env.KAFKA_HOSTS?.split(',') || [],
  connectionTimeout: parseInt(process.env.KAFKA_CONNECTION_TIMEOUT || '3000'),
  requestTimeout: parseInt(process.env.KAFKA_REQUEST_TIMEOUT || '25000'),
  initialRetryTime: parseInt(process.env.KAFKA_INITIAL_RETRY_TIME || '1'),
  retries: parseInt(process.env.KAFKA_RETRIES || '1'),
  producerPolicy: {
    allowAutoTopicCreation: true
  },
  consumerPolicy: {
    groupId: process.env.CONSUMER_GROUP_ID || 'beneficiary-ms',
    maxWaitTimeInMs: Number(process.env.CONSUMER_MAX_WAIT_TIME || '100'),
    allowAutoTopicCreation: true,
    sessionTimeout: Number(process.env.CONSUMER_SESSION_TIMEOUT || '30000'),
    heartbeatInterval: Number(process.env.CONSUMER_HEART_BEAT || '3000'),
    retry: {
      retries: parseInt(process.env.KAFKA_RETRIES || '1')
    }
  },
  numberOfPartitions: Number(process.env.KAFKA_PARTITION_NUMBER || '3')
};

export const kafkaTopics = {
  t24Request: 't24Request',
  beneficiaryResponse: 'beneficiaryResponse',
  beneficiaryRequest: 'beneficiaryRequest',
  validateTopic: 'validateTopic',
  statistics: 'statistics',
  kibana: 'kibanastatistics'
};

export const promiseTimeout = parseInt(process.env.PROMISE_TIMEOUT || '20000');

export const mongoConfig = {
  url: process.env.DB_URL || ''
};

export const proxyEnabled = process.env.PROXY_ENABLED;

export const proxySettings: AxiosProxyConfig = {
  host: process.env.PROXY_HOST || 'proxy',
  port: parseInt(process.env.PROXY_PORT as string) || 8080
};

export const ibanKey = process.env.IBAN_KEY || '';

export const status = {
  success: 'success',
  fail: 'fail',
  timeout: 'timeout'
};

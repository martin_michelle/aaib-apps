import express, { Request, Response, NextFunction } from 'express';
import { ValidationError } from 'express-validation';
import morgan from 'morgan';
import helmet from 'helmet';
import cors from 'cors';
import correlation from 'express-correlation-id';
import { logger } from './core/Logger';
import { corsUrl, environment } from './config';
import { createConsumer, disconnect } from './kafka';
import { connect } from './database';
import { NotFoundError, ApiError, InternalError } from './core/ApiError';
import routes from './routes';
import heartbeat from './health';

process.on('uncaughtException', (e) => {
  logger.error(e);
});

const app = express();

app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ limit: '10mb', extended: true, parameterLimit: 50000 }));
app.use(cors({ origin: corsUrl, optionsSuccessStatus: 200 }));

app.use(correlation());
//Logger
app.use(
  morgan('tiny', {
    skip: (req) => req.method === 'OPTIONS' || req.url === '/health' || req.baseUrl === '/health',
    stream: {
      write: (message) => {
        logger.info(message.substring(0, message.lastIndexOf('\n')));
      }
    }
  })
);
connect();
createConsumer().then();

// Routes
app.use('/', routes);
app.use('/health', heartbeat);

// Security
app.use(helmet());

// catch 404 and forward to error handler
app.use((req: any, res: any, next: (arg0: NotFoundError) => any) => next(new NotFoundError()));

// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof ApiError) {
    ApiError.handle(err, res);
  } else if (err instanceof ValidationError) {
    return res.status(err.statusCode).send(err);
  } else {
    if (environment === 'development') {
      logger.error(err);
      // @ts-ignore
      return res.status(500).send(err.message);
    }
    ApiError.handle(new InternalError(), res);
  }
});

process.on('SIGINT', async () => {
  await disconnect();
  process.exit(1);
});

export default app;

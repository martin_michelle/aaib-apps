export const handlePagination = (
  result: any,
  limit: number,
  lastIndex: string,
  indexName: string
) => {
  let index = 0;
  if (lastIndex) index = result.findIndex((x: any) => x[indexName] === lastIndex) + 1;
  return result.splice(index, limit);
};

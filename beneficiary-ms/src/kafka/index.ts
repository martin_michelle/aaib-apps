import { CompressionTypes, Consumer, Kafka, Producer, ProducerRecord } from 'kafkajs';
import { kafkaOptions, kafkaTopics, promiseTimeout } from '../config';
import { EventEmitter } from 'events';
import { logger } from '../core/Logger';
import correlation from 'express-correlation-id';
import { promiseWithTimeout } from '../helpers/asyncHandler';
import messages from '../messages';
import { ClientKafka } from '../libs/kafka-partition';
import * as service from '../services/beneficiary';
// eslint-disable-next-line
const mongoose = require('mongoose');

let producer: Producer;
let consumer: Consumer;
let partitionId: number;
let client: ClientKafka;
let interval: number;
let eventEmitter: EventEmitter;

export const connector = () => {
  return new Kafka({
    clientId: kafkaOptions.clientId,
    brokers: kafkaOptions.hosts,
    connectionTimeout: kafkaOptions.connectionTimeout,
    requestTimeout: kafkaOptions.requestTimeout,
    retry: {
      initialRetryTime: kafkaOptions.initialRetryTime,
      retries: kafkaOptions.retries
    }
  });
};

export const createProducer = async () => {
  if (producer) return producer;
  const kafka = connector();
  producer = kafka.producer(kafkaOptions.producerPolicy);
  await producer.connect();
  return producer;
};

export const createKafkaClient = () => {
  if (client) return client;
  return new ClientKafka({
    consumer: {
      groupId: kafkaOptions.consumerPolicy.groupId,
      maxWaitTimeInMs: kafkaOptions.consumerPolicy.maxWaitTimeInMs
    },
    client: {
      clientId: kafkaOptions.clientId,
      brokers: kafkaOptions.hosts,
      connectionTimeout: kafkaOptions.connectionTimeout,
      requestTimeout: kafkaOptions.requestTimeout,
      retry: {
        initialRetryTime: kafkaOptions.initialRetryTime,
        retries: kafkaOptions.retries
      }
    }
  });
};

export const createConsumer = async () => {
  try {
    client = createKafkaClient();
    consumer = await client.connect();
    await client.createKafkaTopic(kafkaTopics.beneficiaryResponse, kafkaOptions.numberOfPartitions);
    await client.createKafkaTopic(kafkaTopics.beneficiaryRequest, kafkaOptions.numberOfPartitions);
    await consumer.connect();
    await consumer.subscribe({ topic: kafkaTopics.beneficiaryResponse });
    await consumer.subscribe({ topic: kafkaTopics.beneficiaryRequest });
    const consumerAssignments: { [key: string]: number } = {};
    consumer.on(consumer.events.GROUP_JOIN, (data: any) => {
      Object.keys(data.payload.memberAssignment).forEach((memberId) => {
        consumerAssignments[memberId] = Math.min(...data.payload.memberAssignment[memberId]);
        partitionId = consumerAssignments[kafkaTopics.beneficiaryResponse];
      });
    });
    consumer.on(consumer.events.HEARTBEAT, ({ timestamp }) => {
      interval = timestamp;
    });
    await consumer.run({
      eachMessage: async ({ topic, message }) => {
        if (!message.headers?.correlationId) {
          logger.error('Missing correlationId');
        } else {
          if (topic === kafkaTopics.beneficiaryRequest) {
            const kafkaMessage = await prepareKafkaMessage(message);
            handleKafkaMessages(
              kafkaMessage.authorization,
              kafkaMessage.replyTopic,
              kafkaMessage.correlationId,
              kafkaMessage.partitionReply,
              kafkaMessage.params,
              kafkaMessage.function
            );
          } else {
            const eventEmitter = createEventEmitter();
            eventEmitter.emit(message.headers.correlationId.toString(), message);
          }
        }
      }
    });
    return consumer;
  } catch (e) {
    logger.debug(e.message);
  }
};

export const createEventEmitter = () => {
  if (eventEmitter) return eventEmitter;
  eventEmitter = new EventEmitter();
  return eventEmitter;
};

export const produce = async (data: any, tracingHeaders: any) => {
  if (!producer) producer = await createProducer();
  const correlationId = correlation.getId() || '';
  const producerOptions = {
    topic: data.topic || kafkaTopics.t24Request,
    compression: CompressionTypes.GZIP,
    acks: 1,
    messages: [
      {
        value: JSON.stringify(data),
        headers: {
          correlationId,
          partitionReply: partitionId.toString(),
          replyTopic: kafkaTopics.beneficiaryResponse,
          authorization: data.authorization || '',
          route: data.apiRoute || '',
          method: data.apiMethod || '',
          otp: data.otp || '',
          ...(tracingHeaders ? { tracingHeaders: JSON.stringify(tracingHeaders) } : {})
        }
      }
    ]
  };
  await producer.send(producerOptions);
  const eventEmitter = createEventEmitter();
  const response = () => {
    return new Promise((resolve, reject) => {
      eventEmitter.once(correlationId || '', async (value) => {
        try {
          resolve(value);
        } catch (e) {
          logger.error(e.message);
          reject(e);
        }
      });
    });
  };
  return await promiseWithTimeout(promiseTimeout, response, messages.accounts.timeout.en);
};

export const disconnect = async () => {
  client = createKafkaClient();
  return client.close();
};

export const getHeartbeatTime = async () => {
  return {
    consumer,
    interval,
    partitionId: partitionId === Infinity ? null : partitionId
  };
};

export const handleKafkaMessages = async (
  authorization: string,
  replyTopic: string,
  correlationId: string,
  partitionReply: string,
  params: any,
  functionName: string
) => {
  try {
    let beneficiaryResponse: any;
    switch (functionName) {
      case 'getUserBeneficiariesById':
        params.id = mongoose.Types.ObjectId(params.id);
        beneficiaryResponse = await service.getUserBeneficiariesById(
          String(params.cif || ''),
          params.id
        );
        break;
      case 'updateUserBeneficiaries':
        params.id = mongoose.Types.ObjectId(params.id);
        beneficiaryResponse = await service.updateUserBeneficiaries(
          String(params.cif || ''),
          params.id,
          params.body,
          String(params.user || ''),
          String(params.platform || '')
        );
        break;
    }
    const response = {
      error: false,
      message: { data: beneficiaryResponse }
    };
    const producer = await createProducer();
    const producerOptions: ProducerRecord = {
      topic: replyTopic,
      compression: CompressionTypes.GZIP,
      messages: [
        {
          value: JSON.stringify(response),
          partition: Number(partitionReply),
          headers: {
            correlationId
          }
        }
      ]
    };
    await producer.send(producerOptions);
    logger.info('Get Beneficiary by id', { correlationId, status: 200 });
  } catch (e) {
    logger.error(e.message);
    handleErrors(replyTopic, correlationId, e.message);
  }
};

export const prepareKafkaMessage = async (data: any) => {
  return {
    data: data.value.toString(),
    correlationId: data.headers.correlationId.toString(),
    replyTopic: data.headers.replyTopic.toString(),
    authorization: data.headers.authorization.toString(),
    partitionReply: data.headers.partitionReply ? data.headers.partitionReply.toString() : '0',
    params: JSON.parse(data.headers.params),
    function: data.headers.function.toString()
  };
};

export const handleErrors = async (replyTopic: string, correlationId: string, error: string) => {
  const producer = await createProducer();
  const data = {
    error: true,
    message: error
  };
  const producerOptions: ProducerRecord = {
    topic: replyTopic,
    compression: CompressionTypes.GZIP,
    messages: [
      {
        value: JSON.stringify(data),
        headers: {
          correlationId
        }
      }
    ]
  };
  await producer.send(producerOptions);
};

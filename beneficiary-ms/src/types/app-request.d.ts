import { Request } from 'express';

declare interface PublicRequest extends Request {
  apiKey: string;
}

declare interface RoleRequest extends PublicRequest {
  currentRoleCode: string;
}

declare interface ProtectedRequest extends RoleRequest {
  user: any;
  accessToken: string;
  correlationId: () => string;
}

declare interface BeneficiaryInterface {
  name: string;
  nickName: string;
  isCreditCardNumber: boolean;
  isMobileNumber: boolean;
  accountNumber: string;
  iban: string;
  country: string;
  bank: string;
  swift: string;
  relationship: string;
  currency: string;
  address: string;
  cif: string;
}

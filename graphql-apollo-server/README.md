# AAIB Graphql Apollo Server

## Usage

Please make sure that Nodejs, npm and kong are installed and running

```sh
git clone git@gitlab.tools.digital.aaib:mck/development/graphql-apollo-server.git
cd graphql-apollo-server
cp .env.example .env
npm install
npm run start
```

import 'reflect-metadata';
import 'dotenv/config';
import { ApolloServer } from 'apollo-server-express';
import { buildSchema } from 'type-graphql';
import { Container } from 'typedi';

import { CORS_OPTIONS } from './lib/config';
import { ErrorInterceptor } from './lib/globalMiddleware';
import { MyExpressContext } from 'app-context';
import { Server } from './lib/server';
import { formatResponse } from './lib/formatResponse';
import { formatError } from './lib/formatError';
import { getResolvers } from './lib/resolvers';
import { logger } from './lib/logger';

class GraphqlServer extends Server {
  constructor() {
    super();
  }

  async init() {
    await this.setupApollo();
    this.start();
  }

  async setupApollo() {
    const schema = await buildSchema({
      container: Container,
      resolvers: getResolvers(),
      globalMiddlewares: [ErrorInterceptor]
    });
    const apolloServer = new ApolloServer({
      introspection: false,
      context: ({ req, res }: MyExpressContext) => {
        console.log(req.ip);
        return { req, res };
      },
      formatError,
      formatResponse,
      schema
    });
    await apolloServer.start();
    apolloServer.applyMiddleware({
      cors: CORS_OPTIONS,
      app: this.app
    });
    logger.info('Apollo setup');
  }
}

new GraphqlServer().init().catch((error) => {
  logger.error(error);
  process.exit(1);
});

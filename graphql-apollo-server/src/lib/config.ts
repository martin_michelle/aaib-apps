import CORS from 'cors';
// ENV VARIABLES
export const {
  NODE_ENV = 'development',
  PORT = 4000,
  KONG_BASE_URL = 'http://localhost:8000'
} = process.env;

export const FAWRY_FILTER = process.env['FAWRY_FILTER']?.split(',') || [];

// IS PRODUCTION
export const IS_PRODUCTION = NODE_ENV === 'production';

// CORS
export const CORS_OPTIONS: CORS.CorsOptions = {
  origin: '*.aaib',
  maxAge: 86400
};

// GRAPHQL PATH
export const GRAPHQL_PATH = '/graphql';

// RESOLVER PATHS
export const RESOLVER_PATHS = '/modules/**/*resolver.js';

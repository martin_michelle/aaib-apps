import { GraphQLError } from 'graphql';
import { logger } from './logger';
import { IS_PRODUCTION } from './config';

export function formatError(err: GraphQLError): GraphQLError {
  if (err.message.includes('Apollo')) {
    logger.error(err.message);
    return new GraphQLError('Internal Server Error');
  }
  let message = err?.message;
  const error: any = err;

  if (err?.extensions?.code === 'GRAPHQL_VALIDATION_FAILED' && IS_PRODUCTION) {
    message = 'Invalid input';
    error.message = message;
  }

  if (
    (err?.extensions?.code === 'INTERNAL_SERVER_ERROR' || err.message.includes('request')) &&
    IS_PRODUCTION
  ) {
    message = 'Internal Server Error';
    error.message = message;
  }

  return error;
}

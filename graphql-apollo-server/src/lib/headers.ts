import { MyExpressContext } from 'app-context';
import { HeadersInit } from 'node-fetch';
import { randomUUID } from 'crypto';
import { logger } from './logger';

const propagated = [
  'x-b3-traceid',
  'x-b3-parentspanid',
  'x-b3-spanid',
  'x-b3-sampled',
  'traceparent',
  'tracestate'
];

const manipulateCorrelationId = (context: MyExpressContext, corr: string) => {
  if (corr) {
    const [correlationId, index] = corr.split('-sub');
    context.req.correlationId = correlationId + '-sub' + (Number(index) + 1);
  } else {
    context.req.correlationId = randomUUID() + '-sub0';
  }
};

export default function headersInit(context: MyExpressContext) {
  try {
    manipulateCorrelationId(context, context.req.correlationId as string);
    if (context.req.headers['x-b3-traceid']) {
      logger.info(`traceid: ${context.req.headers['x-b3-traceid']}`, {
        correlationId: context.req.correlationId
      });
    }

    if (context.req.headers['country'] === 'AE') {
      console.log('country', context.req.headers['country']);
      throw new Error('AE country shouldnt be sent');
    }

    const traceContextHeaders = Object.keys(context.req.headers)
      .filter((key) => propagated.includes(key))
      .reduce((obj: any, key: string) => {
        obj[key] = context.req.headers[key];
        return obj;
      }, {});
    return {
      'Content-Type': 'application/json',
      Authorization: context.req.headers.authorization || '',
      'accept-language': context.req.headers['accept-language'] || '',
      method: context.req.headers['method'] || '',
      'x-correlation-id': context.req.correlationId,
      platform: context.req.headers['platform'] ? 'web' : 'mobile',
      ...(context.req.headers['country'] ? { country: context.req.headers['country'] } : null),
      'x-twin': context.req.headers['x-twin'] ? context.req.headers['x-twin'] : 'false',
      enabletak: context.req.headers['enabletak'] === 'true' ? 'true' : 'false',
      'tak-id': context.req.headers['tak-id'],
      channel: context.req.headers['channel'],
      // Zipking and w3c trace context
      ...traceContextHeaders
    } as HeadersInit;
  } catch (e) {
    throw new Error(e.message);
  }
}

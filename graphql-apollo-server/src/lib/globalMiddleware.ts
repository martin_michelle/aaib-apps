import { MiddlewareFn, ArgumentValidationError } from 'type-graphql';
import { UserInputError, AuthenticationError } from 'apollo-server-express';
import { logger } from './logger';
import { MyExpressContext } from 'app-context';

export const ErrorInterceptor: MiddlewareFn<MyExpressContext> = async ({ context }, next) => {
  try {
    return await next();
  } catch (err) {
    if (
      !(err instanceof ArgumentValidationError) &&
      !(err instanceof UserInputError) &&
      !(err instanceof AuthenticationError)
    ) {
      logger.error(err, { correlationId: context.req.correlationId });
    }
    throw err;
  }
};

import 'reflect-metadata';
import 'dotenv/config';
import express from 'express';
import morgan from 'morgan';
import jwt from 'jsonwebtoken';
import { logger } from './logger';
import rateLimit from 'express-rate-limit';
import { parse, OperationDefinitionNode } from 'graphql/language';
import { IS_PRODUCTION, PORT } from './config';
import helmet from 'helmet';

morgan.token('correlationId', function (req: any) {
  return req.correlationId;
});

morgan.token('graphql', function (req: any) {
  const { queryName, operation, subqueries } = extractQueryName(req.body?.query);
  return `${operation} ${queryName} (${subqueries})`;
});

function extractTokenFromBearerHeader(header: string) {
  try {
    const parts = header.split(' ');

    if (parts.length === 2 && parts[0] === 'Bearer') {
      return parts[1];
    }

    return '';
  } catch (e: any) {
    return '';
  }
}

function isTokenExpired(req: any, token: string) {
  try {
    const decoded: any = jwt.decode(extractTokenFromBearerHeader(token));

    // Check if the token has expired
    if (decoded.exp < Date.now() / 1000) {
      req.user = false;
      return true;
    }

    req.user = decoded?.sub?.userInfo;
    return false;
  } catch (err) {
    req.user = false;
    return true;
  }
}

function extractQueryName(body: string): { queryName: string; operation: string; subqueries: any } {
  const ast = parse(body);
  const definition = ast?.definitions[0] as OperationDefinitionNode | undefined;

  const operation = definition?.operation
    ? definition.operation === 'mutation'
      ? 'MUTATION'
      : 'QUERY'
    : '';

  let queryName = '_';
  if (definition?.kind === 'OperationDefinition') {
    queryName = definition.name?.value || 'UnnamedQuery';
  } else if (definition?.kind === 'FragmentDefinition') {
    queryName = definition.name?.value || 'UnnamedQuery';
  }

  const subqueries = definition?.selectionSet.selections.map((selection) => {
    if (selection.kind === 'Field') {
      return selection.name.value;
    }
  });

  return { queryName, operation, subqueries };
}

const excludedQueries = [
  'forgotUsername',
  'forgotPasswordOtpValidation',
  'resetPassword',
  'registrationValidation',
  'SecurityTips',
  'securityTips',
  'fxCalculator',
  'currencyExchangeRates',
  'resetExpiredPassword',
  'register',
  'registerDevice',
  'isDeviceRegistered'
];

// Set up rate limiter for unauthenticated requests
const unauthRateLimiter = rateLimit({
  windowMs: 60 * 1000, // 1 minute
  max: 10, // limit each IP to 10 requests per minute
  // Check if the requester is authenticated or not
  keyGenerator: (req: any) => {
    return req.user ? req.user.id : req.ip;
  }
});

export class Server {
  private readonly _app: express.Application;

  constructor() {
    this._app = express()
      .enable('trust proxy')
      .use(express.json())
      .use(
        helmet({ contentSecurityPolicy: process.env.NODE_ENV === 'production' ? undefined : false })
      )
      .get('/graphql*', (req, res, next) => {
        if (IS_PRODUCTION) {
          // Return a 403 error code with a custom error message
          res.status(403).send(`<h1>403 Forbidden</h1>
          <p>Access to this resource is forbidden. You do not have permission to access this page.</p>`);
        } else {
          next();
        }
      })
      .use((req: any, res: express.Response, next: express.NextFunction) => {
        if (req?.method === 'GET' || req?.method === 'OPTIONS') {
          next();
        } else {
          const { queryName } = extractQueryName(req.body?.query);
          if (excludedQueries.includes(queryName)) {
            next();
          } else {
            const IsTokenExpired = isTokenExpired(req, req?.headers?.authorization as string);

            // If the requester is authenticated, skip the rate limiter
            if (!IsTokenExpired) {
              if (req.user) {
                return next();
              } else {
                return unauthRateLimiter(req, res, next);
              }
            } else {
              // Apply rate limiting to unauthenticated requests
              return unauthRateLimiter(req, res, next);
            }
          }
        }
      })
      .use(
        morgan(
          ':correlationId#cid#:graphql :status :response-time ms - :res[content-length] ":user-agent"',
          {
            skip: (req) => req.method !== 'POST',
            stream: {
              write: (message) => {
                const splitMessage = message.split('#cid#');
                logger.info(splitMessage[1].substring(0, splitMessage[1].lastIndexOf('\n')), {
                  correlationId: splitMessage[0]
                });
              }
            }
          }
        )
      )
      .use((req: any, res: express.Response, next: express.NextFunction) => {
        res.removeHeader('X-RateLimit-Limit');
        res.removeHeader('X-RateLimit-Remaining');
        res.removeHeader('X-RateLimit-Reset');
        next();
      });
  }

  protected get app(): express.Application {
    return this._app;
  }

  start(): void {
    this._app.listen(PORT, () =>
      logger.info(`Server started at http://localhost:${PORT} 🚀` + '\n')
    );
  }
}

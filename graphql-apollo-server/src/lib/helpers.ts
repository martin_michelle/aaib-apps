export const maskString = (stringToMask: string, n = 2): string => {
  let maskedString = stringToMask.slice(0, n);
  for (const character of stringToMask.slice(n, -n)) {
    maskedString += character == ' ' ? ' ' : '*';
  }
  maskedString += stringToMask.slice(-n);
  return maskedString;
};

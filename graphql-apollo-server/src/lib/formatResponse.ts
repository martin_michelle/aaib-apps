import { AuthenticationError, UserInputError, ForbiddenError } from 'apollo-server-express';
import { GraphQLResponse, GraphQLRequestContext } from 'apollo-server-types';
import { ApolloError } from 'apollo-server-errors';
import { MyExpressContext } from 'app-context';
import { logger } from './logger';
import { IS_PRODUCTION } from './config';

export function formatResponse(
  res: GraphQLResponse | null,
  context: GraphQLRequestContext | null
): GraphQLResponse {
  if (res?.errors) {
    const errors = res.errors.map((e) => {
      if (
        e?.extensions?.code === 'BAD_USER_INPUT' ||
        e?.extensions?.code === 'GRAPHQL_VALIDATION_FAILED'
      ) {
        let message = e?.message.split(';')[1] || e?.message.split(';')[0];
        logger.error(message, {
          correlationId: (context?.context as MyExpressContext).req.correlationId
        });
        if (IS_PRODUCTION) message = 'Invalid request.';
        return {
          message
        };
      }
      logger.error(e?.message, {
        correlationId: (context?.context as MyExpressContext).req.correlationId
      });
      if (e?.extensions?.exception) {
        delete e.extensions.exception;
      }
      return e;
    });
    res.errors = errors;
  }
  if (!res) throw Error('No response');
  return res;
}

export async function formatApiResponse(response: any) {
  const res = await response.json().catch(() => {
    throw new Error('Internal Server Error');
  });
  switch (response.status) {
    case 401:
      if (res?.statusCode === '10004') throw new ApolloError(res.message, 'SUSPENDED');
      if (res?.statusCode === '10015') throw new ApolloError(res.message, 'MINOR_USER_ERROR');
      if (res?.statusCode === '10016') throw new ApolloError(res.message, 'JOINT_USER_ERROR');
      //exp is returned from kong jwt plugin if token is expired
      throw new AuthenticationError(res.exp || res.message);
    case 403:
      if (res?.statusCode === '10005') throw new ApolloError(res.message, 'INVALID_OTP');
      if (res?.statusCode === '10006') throw new ApolloError(res.message, 'OVERLAP_ERROR');
      if (res?.statusCode === '10007') throw new ApolloError(res.message, 'MAX_ATTEMPT');
      if (res?.statusCode === '10008') throw new ApolloError(res.message, 'BLOCKED_OTP');
      if (res?.statusCode === '10009') throw new ApolloError(res.message, 'DUPLICATE_BENEFICIARY');
      if (res?.statusCode === '10011') throw new ApolloError(res.message, 'NOT_INDIVIDUAL');
      if (res?.statusCode === '10017') throw new ApolloError(res.message, 'MAX_DEVICE_ERROR');
      if (res?.statusCode === '10020') throw new ApolloError(res.message, 'CD_CANCEL_ERROR');

      throw new ForbiddenError(res.message || 'Forbidden');
    case 409:
      throw new ApolloError(res.message, 'CONFLICT');
    case 404:
      throw new ApolloError(res.message || 'Not Found', 'NOT_FOUND');
    case 601:
      throw new ApolloError(res.message, 'DUPLICATE_SESSION');
    case 602:
      throw new ApolloError(res.message, 'EXPIRED_PASSWORD');
    case 603:
      throw new ApolloError(res.message, 'INVALID_PASSWORD');
    case 500:
      if (res?.statusCode === '10014')
        throw new ApolloError(res.message, 'CARD_REPLACEMENT_EXISTS', {
          errorData: res?.errorData || null
        });

      if (res?.statusCode === '10025') throw new ApolloError(res.message, 'CD_AUTH_ERROR');
      throw new Error(res.message || 'Internal Error');
    case 502:
      throw new Error('Internal Server Error');
    default:
      break;
  }
  if (!res.data && typeof res.data !== 'boolean')
    throw new UserInputError(res.message || 'No Data Found!');
  return res.data;
}

import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class CardTravelNote {
  @Field()
  Id: string;

  @Field()
  Country: string;

  @Field()
  FromDate: string;

  @Field()
  ToDate: string;

  @Field()
  MobileNo: string;
}

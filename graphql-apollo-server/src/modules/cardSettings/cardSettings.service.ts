import { Service } from 'typedi';
import fetch from 'node-fetch';
import { formatApiResponse } from '../../lib/formatResponse';

import {
  DebitCardTravelWindowInput,
  SettingsInput,
  ChangePrimaryAccountInput
} from './inputs/cardSettings.input';
import { KONG_BASE_URL } from '../../lib/config';
import { MyExpressContext } from 'app-context';
import headersInit from '../../lib/headers';
import { CardTravelNote } from './response/cardSettings.response';

@Service()
export class SettingsService {
  async setDebitCardStatus(context: MyExpressContext, data: SettingsInput): Promise<String> {
    const statusResponse = await fetch(`${KONG_BASE_URL}/debit/setstatus`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(statusResponse);

    return res.message;
  }

  async setDebitCardOnlinePaymentStatus(
    context: MyExpressContext,
    data: SettingsInput
  ): Promise<String> {
    const statusResponse = await fetch(`${KONG_BASE_URL}/debit/setOnlineCards`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(statusResponse);

    return res.message;
  }

  async enableDebitCardTravelWindow(
    context: MyExpressContext,
    data: DebitCardTravelWindowInput
  ): Promise<String> {
    const twResponse = await fetch(`${KONG_BASE_URL}/debit/travelNote/enable`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(twResponse);

    return res.message;
  }

  async deleteDebitCardTravelWindow(context: MyExpressContext, id: String): Promise<String> {
    const twResponse = await fetch(`${KONG_BASE_URL}/debit/travelNote/delete/${id}`, {
      method: 'DELETE',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(twResponse);

    return res.message;
  }

  async getDebitCardTravelWindows(
    context: MyExpressContext,
    cardToken: String
  ): Promise<CardTravelNote[]> {
    const twResponse = await fetch(
      `${KONG_BASE_URL}/debit/travelNote/list?CardToken=${cardToken}`,
      {
        method: 'GET',
        headers: headersInit(context)
      }
    );

    return formatApiResponse(twResponse);
  }

  async changePrimaryAccount(
    context: MyExpressContext,
    data: ChangePrimaryAccountInput
  ): Promise<String> {
    const twResponse = await fetch(`${KONG_BASE_URL}/debit/debitcardaccount/setprimary`, {
      method: 'POST',
      headers: headersInit(context),
      body: JSON.stringify(data)
    });

    const res = await formatApiResponse(twResponse);
    return res.message;
  }
}

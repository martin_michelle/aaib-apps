import { InputType, Field } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class SettingsInput {
  @IsNotEmpty()
  @Field()
  action: string;

  @IsNotEmpty()
  @Field()
  cardToken: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  smsToken?: string;
}

@InputType()
export class DebitCardTravelWindowInput {
  @IsNotEmpty()
  @Field()
  CardToken: string;

  @IsNotEmpty()
  @Field()
  Country: string;

  @IsNotEmpty()
  @Field()
  FromDate: string;

  @IsNotEmpty()
  @Field()
  ToDate: string;

  @IsNotEmpty()
  @Field()
  MobileNo: string;
}

@InputType()
export class ChangePrimaryAccountInput {
  @IsNotEmpty()
  @Field()
  CardToken: string;

  @IsNotEmpty()
  @Field()
  AccId: string;

  @IsNotEmpty()
  @Field()
  Action: string;
}

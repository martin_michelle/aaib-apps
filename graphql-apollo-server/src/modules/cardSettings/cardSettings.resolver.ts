import { Arg, Mutation, Resolver, Ctx, Query } from 'type-graphql';
import { Inject, Service } from 'typedi';
import { SettingsService } from './cardSettings.service';
import {
  DebitCardTravelWindowInput,
  SettingsInput,
  ChangePrimaryAccountInput
} from './inputs/cardSettings.input';
import { MyExpressContext } from 'app-context';
import { CardTravelNote } from './response/cardSettings.response';

@Service()
@Resolver()
export default class SettingsResolver {
  @Inject(() => SettingsService)
  settingsService: SettingsService;

  @Mutation(() => String)
  async setDebitCardStatus(
    @Arg('data') data: SettingsInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.settingsService.setDebitCardStatus(context, data);
  }

  @Mutation(() => String)
  async setDebitCardOnlinePaymentStatus(
    @Arg('data') data: SettingsInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.settingsService.setDebitCardOnlinePaymentStatus(context, data);
  }

  @Mutation(() => String)
  async enableDebitCardTravelWindow(
    @Arg('data') data: DebitCardTravelWindowInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.settingsService.enableDebitCardTravelWindow(context, data);
  }

  @Mutation(() => String)
  async deleteDebitCardTravelWindow(
    @Arg('id') id: String,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.settingsService.deleteDebitCardTravelWindow(context, id);
  }

  @Mutation(() => String)
  async changePrimaryAccount(
    @Arg('data') data: ChangePrimaryAccountInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.settingsService.changePrimaryAccount(context, data);
  }

  @Query(() => [CardTravelNote])
  async getDebitCardTravelWindows(
    @Arg('cardToken') cardToken: String,
    @Ctx() context: MyExpressContext
  ): Promise<CardTravelNote[]> {
    return this.settingsService.getDebitCardTravelWindows(context, cardToken);
  }
}

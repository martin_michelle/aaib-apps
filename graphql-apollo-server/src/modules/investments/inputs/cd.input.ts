import { InputType, Field, Int, Float } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CdIssuanceInput {
  @IsNotEmpty()
  @Field()
  CDType: string;

  @IsNotEmpty()
  @Field(() => Int)
  numberOfCds: number;

  @IsNotEmpty()
  @Field(() => Float)
  freshAmount: number;

  @IsNotEmpty()
  @Field()
  renewal: string;

  @IsNotEmpty()
  @Field(() => Float)
  CBEAmount: number;

  @IsNotEmpty()
  @Field(() => Float)
  CBEDescription: number;

  @IsNotEmpty()
  @Field()
  endowmentCD: string;

  @IsNotEmpty()
  @Field()
  drawDownAccount: string;

  @IsNotEmpty()
  @Field()
  principleAccount: string;

  @IsNotEmpty()
  @Field()
  interestAccount: string;

  @IsNotEmpty()
  @Field()
  chargeAccount: string;
}
@InputType()
export class FilterCdsInput {
  @Field({ nullable: true })
  type: string;

  @Field({ nullable: true })
  category: string;
}
@InputType()
export class CdActionInput {
  @IsNotEmpty()
  @Field()
  transactionID: string;
}
@InputType()
export class TopUpLoanInput {
  @IsNotEmpty()
  @Field()
  loanName: string;
}

@InputType()
export class SecuredLoanInput {
  @IsNotEmpty()
  @Field()
  LoanType: string;

  @IsNotEmpty()
  @Field(() => Int)
  Amount: number;

  @IsNotEmpty()
  @Field()
  DisbursmentAccount: string;

  @IsNotEmpty()
  @Field()
  InstLiqAccount: string;

  @IsNotEmpty()
  @Field(() => Int)
  Tenor: number;

  @IsNotEmpty()
  @Field()
  Currency: string;

  @IsNotEmpty()
  @Field()
  ContractReference: string;

  @IsNotEmpty()
  @Field()
  otp: string;
}

import { ObjectType, Field, Float, Int } from 'type-graphql';

@ObjectType()
export class TreasuryData {
  @Field()
  ID: string;

  @Field()
  Company: string;

  @Field()
  Customer: string;

  @Field()
  CustomerName: string;

  @Field()
  CategoryDescription: string;

  @Field(() => Float)
  FaceValue: number;

  @Field(() => Float)
  PresentValue: number;

  @Field()
  Currency: string;

  @Field(() => Float)
  Yield: number;

  @Field(() => Float)
  Price: number;

  @Field(() => Int)
  IssueDate: number;

  @Field(() => Int)
  MaturityDate: number;

  @Field(() => Int)
  PurchaseDate: number;

  @Field(() => Float)
  BlockedAmount: number;

  @Field(() => Float)
  TaxesTillDate: number;

  @Field(() => Float, { nullable: true })
  CouponRate?: number;

  @Field(() => Int, { nullable: true })
  LastCouponDate?: number;

  @Field(() => Int, { nullable: true })
  nextCouponDate?: number;

  @Field(() => Float, { nullable: true })
  RemainingDays?: number;

  @Field(() => Float, { nullable: true })
  TaxesAtCouponEnd?: number;

  @Field(() => Float, { nullable: true })
  TaxesAtMaturity?: number;

  @Field({ nullable: true })
  Market?: string;

  @Field({ nullable: true })
  Unicode?: string;

  @Field()
  country: string;
}

@ObjectType()
export class Treasury {
  @Field(() => [TreasuryData])
  data: TreasuryData[];

  @Field()
  type: string;

  @Field()
  currency: string;

  @Field(() => Float)
  total: number;

  @Field(() => Int)
  count: number;
}

@ObjectType()
export class Treasuries {
  @Field(() => [Treasury])
  treasury: Treasury[];

  @Field()
  currency: string;

  @Field(() => Float)
  total: number;

  @Field(() => Int)
  billscount: number;

  @Field(() => Int)
  bondscount: number;

  @Field(() => [String])
  depositsCurrencies: string[];

  @Field(() => Boolean, { nullable: true })
  isError?: boolean;
}

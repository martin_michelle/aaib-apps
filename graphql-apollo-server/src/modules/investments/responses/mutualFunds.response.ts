import { ObjectType, Field, Float, Int } from 'type-graphql';
import { IsOptional } from 'class-validator';
@ObjectType()
export class MutualFund {
  @Field()
  Currency: string;

  @Field()
  FundId: string;

  @Field()
  FundName: string;

  @Field(() => Float)
  Units: number;

  @Field(() => Float)
  Price: number;

  @Field(() => Float)
  InitialPrice: number;

  @Field(() => Float)
  MarketValue: number;

  @Field(() => Float, { nullable: true })
  Frequency: number;

  @Field(() => Float)
  profit: number;

  @Field(() => [MutualFundDetails])
  details: MutualFundDetails[];

  @Field(() => String)
  country: string;
}

@ObjectType()
export class MutualFundDetails {
  @Field()
  ID: string;

  @Field()
  Company: string;

  @Field()
  Customer: string;

  @Field()
  CustomerName: string;

  @Field()
  FundId: string;

  @Field()
  FundName: string;

  @Field()
  Currency: string;

  @Field(() => Int)
  ValueDate: number;

  @Field(() => Int)
  PositionDate: number;

  @Field(() => Int)
  DealDate: number;

  @Field(() => Float)
  InitialPrice: number;

  @Field(() => Float)
  Units: number;

  @Field(() => Float)
  Price: number;

  @Field(() => Float)
  Pledge: number;

  @Field(() => Float)
  MarketValue: number;

  @Field(() => Float)
  Unrealized: number;
}

@ObjectType()
export class MutualFunds {
  @Field(() => [MutualFund])
  MutualFunds: MutualFund[];

  @Field({ nullable: true })
  currency: string;

  @Field(() => Float)
  total: number;

  @IsOptional()
  @Field(() => Boolean, { nullable: true })
  isError?: boolean;

  @IsOptional()
  @Field(() => Number, { nullable: true })
  allUnits?: number;

  @Field(() => [String])
  depositsCurrencies: string[];

  @IsOptional()
  @Field(() => Number, { nullable: true })
  gain?: number;
}

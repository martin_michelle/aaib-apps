import { IsOptional } from 'class-validator';
import { ObjectType, Field, Float, Int } from 'type-graphql';

@ObjectType()
export class Investment {
  @Field()
  currency: string;

  @Field(() => Float)
  total: number;

  @Field(() => Float, { nullable: true })
  gain: number;

  @Field()
  type: string;

  @Field(() => Float, { nullable: true })
  EGTotal: number;

  @Field(() => Float, { nullable: true })
  AETotal: number;

  @Field(() => Float, { nullable: true })
  UAETotal: number;

  @Field(() => Float, { nullable: true })
  EGGain: number;

  @Field(() => Float, { nullable: true })
  AEGain: number;

  @Field(() => Float, { nullable: true })
  UAEGain: number;

  @Field(() => Int, { nullable: true })
  count: number;

  @Field(() => [InvestmentData])
  data: InvestmentData[];
}

@ObjectType()
export class InvestmentData {
  @Field()
  ID: string;

  @Field()
  Company: string;

  @Field()
  Customer: string;

  @Field()
  CustomerName: string;

  @Field()
  Category: string;

  @Field()
  CategoryDescription: string;

  @Field()
  Currency: string;

  @Field()
  DrawDownAccount: string;

  @Field()
  PrinicipalAccount: string;

  @Field()
  InterestAccount: string;

  @Field({ nullable: true })
  ChargeAccount?: string;

  @Field()
  Frequency: string;

  @Field()
  AccountOfficer: string;

  @Field()
  Status: string;

  @Field({ nullable: true })
  ENDOWMENTCD?: string;

  @Field({ nullable: true })
  LDType?: string;

  @Field({ nullable: true })
  LDTypeDescription?: string;

  @Field({ nullable: true })
  BulletPercent?: string;

  @Field({ nullable: true })
  DonorID?: string;

  @Field(() => Int)
  AgreementDate: number;

  @Field(() => Int)
  ValueDate: number;

  @Field(() => Int)
  MaturityDate: number;

  @Field(() => Int)
  NextPaymentDate: number;

  @Field(() => Int, { nullable: true })
  NoOfRenewals?: number;

  @Field(() => Float, { nullable: true })
  WorkingBalance?: number;

  @Field(() => Float, { nullable: true })
  Amount?: number;

  @Field(() => Float, { nullable: true })
  TotalPaid?: number;

  @Field(() => Float, { nullable: true })
  InterestPaid?: number;

  @Field(() => Float, { nullable: true })
  LDAmount?: number;

  @Field(() => Float)
  InterestRate: number;

  @Field(() => Float, { nullable: true })
  AccruedToDate?: number;

  @Field(() => Float)
  NextPaymentAmount: number;

  @Field(() => Float, { nullable: true })
  CBEAmount?: number;

  @Field({ nullable: true })
  CBEDescription?: string;

  @Field(() => Int, { nullable: true })
  NoOfCDs?: number;

  @Field({ nullable: true })
  Renewal?: string;

  @Field({ nullable: true })
  PledgeAmount?: number;

  @Field({ nullable: true })
  country?: string;
}

@ObjectType()
export class Investments {
  @Field(() => [Investment])
  investments: Investment[];

  @Field(() => [String], { nullable: true })
  depositsCurrencies?: string[];

  @Field(() => [String], { nullable: true })
  CDsnTDsCurrencies?: string[];

  @Field(() => Boolean, { nullable: true })
  @IsOptional()
  isError?: boolean;

  @Field(() => String, { nullable: true })
  @IsOptional()
  notFetched?: string;
}

@ObjectType()
export class Redemption {
  @Field()
  NumberOfDays: string;

  @Field()
  Penalty: string;
}

@ObjectType()
export class TypeDetails {
  @Field()
  ID: string;

  @Field()
  DescriptionEn: string;

  @Field({ nullable: true })
  DescriptionAr: string;

  @Field()
  Category: string;

  @Field()
  Currency: string;

  @Field()
  LoanType: string;

  @Field(() => Int)
  Tenor: number;

  @Field()
  Frequency: string;

  @Field(() => Float)
  Rate: number;

  @Field(() => Float)
  IntRate: number;

  @Field()
  InterestSpread: string;

  @Field(() => Float)
  Denominations: number;

  @Field(() => Float)
  BulletPercent: number;

  @Field(() => [Redemption])
  Redemption: Redemption[];
}
@ObjectType()
export class CdType {
  @Field(() => [TypeDetails])
  Types: TypeDetails[];

  @Field()
  Type: string;
}

@ObjectType()
export class ListCdTypes {
  @Field()
  Type: string;

  @Field()
  UpTo: string;

  @Field(() => [CDsCategory])
  Categories: CDsCategory[];
}

@ObjectType()
export class CDsCategory {
  @Field()
  Id: string;

  @Field()
  Name: string;
}

@ObjectType()
export class CdIssuance {
  @Field()
  Status: string;

  @Field()
  TransactionId: string;

  @Field()
  RecordStatus: string;

  @Field()
  Message: string;

  @Field()
  InterestDueDate: string;

  @Field(() => Int)
  ValueDate: number;

  @Field(() => Int)
  MaturityDate: number;

  @Field()
  Currency: string;

  @Field(() => Float)
  Amount: number;

  @Field(() => Float)
  InterestRate: number;
}

@ObjectType()
export class SecuredLoan {
  @Field()
  Status: string;

  @Field()
  TransactionId: string;

  @Field()
  RecordStatus: string;

  @Field()
  Message: string;

  @Field(() => [String])
  Overrides: string[];

  @Field(() => [String])
  iScore: string[];

  @Field(() => Int)
  ValueDate: number;

  @Field(() => Int)
  MaturityDate: number;

  @Field(() => Int)
  FirstInstallmentDate: number;

  @Field(() => Float)
  Amount: number;

  @Field(() => Int)
  InstallmentAmount: number;

  @Field()
  Currency: string;

  @Field(() => Float)
  InterestRate: number;
}

@ObjectType()
export class CDSourceOFFund {
  @Field()
  code: number;

  @Field()
  ARName: string;

  @Field()
  ENName: string;
}

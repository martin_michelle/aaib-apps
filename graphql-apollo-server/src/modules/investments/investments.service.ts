import { Service } from 'typedi';
import fetch from 'node-fetch';

import {
  CdType,
  Investments,
  InvestmentData,
  CdIssuance,
  SecuredLoan,
  CDSourceOFFund,
  ListCdTypes
} from './responses/investments.response';
import { KONG_BASE_URL } from '../../lib/config';
import { formatApiResponse } from '../../lib/formatResponse';
import { MyExpressContext } from 'app-context';
import headersInit from '../../lib/headers';
import { URL } from 'url';
import { MutualFunds } from './responses/mutualFunds.response';
import { Treasuries } from './responses/treasuries.response';
import {
  CdActionInput,
  CdIssuanceInput,
  FilterCdsInput,
  SecuredLoanInput,
  TopUpLoanInput
} from './inputs/cd.input';

@Service()
export class InvestmentsService {
  async investments(context: MyExpressContext, currency?: string): Promise<Investments> {
    const url = new URL(`${KONG_BASE_URL}/investment`);
    if (currency) url.searchParams.append('currency', currency);
    const investments = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(investments);
  }

  async mutualFunds(context: MyExpressContext, currency: string): Promise<MutualFunds> {
    const url = new URL(`${KONG_BASE_URL}/investment/mutualfund`);
    if (currency) url.searchParams.append('currency', currency);
    const mutualFunds = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(mutualFunds);
  }

  async treasuries(context: MyExpressContext, currency?: string): Promise<Treasuries> {
    const url = new URL(`${KONG_BASE_URL}/investment/treasury`);
    if (currency) url.searchParams.append('currency', currency);
    const treasuries = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(treasuries);
  }

  async cdTypes(context: MyExpressContext, data: FilterCdsInput): Promise<CdType[]> {
    const url = new URL(`${KONG_BASE_URL}/investment/cd/types`);
    if (data?.type) url.searchParams.append('type', data?.type);
    if (data?.category) url.searchParams.append('category', data?.category);
    const cdTypes = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(cdTypes);
  }

  async listCdTypes(context: MyExpressContext, data: FilterCdsInput): Promise<ListCdTypes[]> {
    const url = new URL(`${KONG_BASE_URL}/investment/cd/types/list`);
    if (data?.type) url.searchParams.append('type', data?.type);
    if (data?.category) url.searchParams.append('category', data?.category);
    const cdTypes = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(cdTypes);
  }

  async unauthorizedCd(context: MyExpressContext): Promise<InvestmentData[]> {
    const url = new URL(`${KONG_BASE_URL}/investment/unauth`);
    const unauthorizedCd = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(unauthorizedCd);
  }

  async cdIssuance(context: MyExpressContext, data: CdIssuanceInput): Promise<CdIssuance> {
    const url = new URL(`${KONG_BASE_URL}/investment/cd/submit`);
    const cd = await fetch(url, {
      method: 'POST',
      headers: headersInit(context),
      body: JSON.stringify(data)
    });

    return formatApiResponse(cd);
  }

  async cdIssuanceAndAuth(context: MyExpressContext, data: CdIssuanceInput): Promise<CdIssuance> {
    const url = new URL(`${KONG_BASE_URL}/investment/cd/issue`);
    const cd = await fetch(url, {
      method: 'POST',
      headers: headersInit(context),
      body: JSON.stringify(data)
    });

    return formatApiResponse(cd);
  }

  async authorizeCd(context: MyExpressContext, data: CdActionInput): Promise<string> {
    const url = new URL(`${KONG_BASE_URL}/investment/cd/authorize`);
    const authorize = await fetch(url, {
      method: 'POST',
      headers: headersInit(context),
      body: JSON.stringify(data)
    });

    const res = await formatApiResponse(authorize);
    return res.message;
  }

  async cancelCd(context: MyExpressContext, data: CdActionInput): Promise<string> {
    const url = new URL(`${KONG_BASE_URL}/investment/cd/cancel`);
    const authorize = await fetch(url, {
      method: 'POST',
      headers: headersInit(context),
      body: JSON.stringify(data)
    });

    const res = await formatApiResponse(authorize);
    return res.message;
  }

  async requestSecuredLoan(context: MyExpressContext, productDetails?: string): Promise<string> {
    const url = new URL(`${KONG_BASE_URL}/investment/requestSecuredLoan`);
    let body = {};

    if (productDetails) {
      body = { productDetails };
    }

    const authorize = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: headersInit(context)
    });
    const res = await formatApiResponse(authorize);
    return res.message;
  }

  async requestTopUpLoan(context: MyExpressContext, data: TopUpLoanInput): Promise<string> {
    const url = new URL(`${KONG_BASE_URL}/investment/topUpLoan`);

    const authorize = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });
    const res = await formatApiResponse(authorize);
    return res.message;
  }

  async bookSecuredLoan(context: MyExpressContext, data: SecuredLoanInput): Promise<SecuredLoan> {
    const url = new URL(`${KONG_BASE_URL}/investment/book/securedLoan`);
    const securedLoan = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });
    return formatApiResponse(securedLoan);
  }

  async CDSourceOfFund(context: MyExpressContext): Promise<CDSourceOFFund[]> {
    const url = new URL(`${KONG_BASE_URL}/investment/cd/sourceOfFund`);
    const CDSourceOFFund = await fetch(url, {
      method: 'get',
      headers: headersInit(context)
    });

    return formatApiResponse(CDSourceOFFund);
  }
}

import { MyExpressContext } from 'app-context';
import { Query, Resolver, Ctx, Arg, Mutation } from 'type-graphql';
import { Inject, Service } from 'typedi';
import {
  CdActionInput,
  CdIssuanceInput,
  FilterCdsInput,
  SecuredLoanInput,
  TopUpLoanInput
} from './inputs/cd.input';
import { InvestmentsService } from './investments.service';
import {
  CdType,
  InvestmentData,
  Investments,
  CdIssuance,
  SecuredLoan,
  CDSourceOFFund,
  ListCdTypes
} from './responses/investments.response';
import { MutualFunds } from './responses/mutualFunds.response';
import { Treasuries } from './responses/treasuries.response';

@Service()
@Resolver(() => InvestmentsService)
export default class InvestmentsResolver {
  @Inject(() => InvestmentsService)
  investmentsService: InvestmentsService;

  @Query(() => Investments)
  async customerInvestments(
    @Ctx() context: MyExpressContext,
    @Arg('currency', { nullable: true }) currency?: string
  ): Promise<Investments> {
    return this.investmentsService.investments(context, currency);
  }

  @Query(() => MutualFunds)
  async customerMutualFunds(
    @Arg('currency', { nullable: true }) currency: string,
    @Ctx() context: MyExpressContext
  ): Promise<MutualFunds> {
    return this.investmentsService.mutualFunds(context, currency);
  }

  @Query(() => Treasuries)
  async customerTreasury(
    @Ctx() context: MyExpressContext,
    @Arg('currency', { nullable: true }) currency?: string
  ): Promise<Treasuries> {
    return this.investmentsService.treasuries(context, currency);
  }

  @Query(() => [CdType])
  async cdTypes(
    @Arg('data', { nullable: true }) data: FilterCdsInput,
    @Ctx() context: MyExpressContext
  ): Promise<CdType[]> {
    return this.investmentsService.cdTypes(context, data);
  }

  @Query(() => [ListCdTypes])
  async listCdTypes(
    @Arg('data', { nullable: true }) data: FilterCdsInput,
    @Ctx() context: MyExpressContext
  ): Promise<ListCdTypes[]> {
    return this.investmentsService.listCdTypes(context, data);
  }

  @Query(() => [InvestmentData])
  async unauthorizedCd(@Ctx() context: MyExpressContext): Promise<InvestmentData[]> {
    return this.investmentsService.unauthorizedCd(context);
  }

  @Mutation(() => CdIssuance)
  async cdIssuance(
    @Arg('data') data: CdIssuanceInput,
    @Ctx() context: MyExpressContext
  ): Promise<CdIssuance> {
    return this.investmentsService.cdIssuance(context, data);
  }

  @Mutation(() => CdIssuance)
  async cdIssuanceAndAuth(
    @Arg('data') data: CdIssuanceInput,
    @Ctx() context: MyExpressContext
  ): Promise<CdIssuance> {
    return this.investmentsService.cdIssuanceAndAuth(context, data);
  }

  @Mutation(() => String)
  async authorizeCd(
    @Arg('data') data: CdActionInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.investmentsService.authorizeCd(context, data);
  }

  @Mutation(() => String)
  async cancelCd(
    @Arg('data') data: CdActionInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.investmentsService.cancelCd(context, data);
  }
  @Mutation(() => String)
  async requestSecuredLoan(
    @Ctx() context: MyExpressContext,
    @Arg('productDetails', { nullable: true }) productDetails?: string
  ): Promise<string> {
    return this.investmentsService.requestSecuredLoan(context, productDetails);
  }

  @Mutation(() => String)
  async requestTopUpLoan(
    @Ctx() context: MyExpressContext,
    @Arg('data') data: TopUpLoanInput
  ): Promise<string> {
    return this.investmentsService.requestTopUpLoan(context, data);
  }

  @Mutation(() => SecuredLoan)
  async bookSecuredLoan(
    @Arg('data') data: SecuredLoanInput,
    @Ctx() context: MyExpressContext
  ): Promise<SecuredLoan> {
    return this.investmentsService.bookSecuredLoan(context, data);
  }

  @Query(() => [CDSourceOFFund])
  async CDSourceOFFund(@Ctx() context: MyExpressContext): Promise<CDSourceOFFund[]> {
    return this.investmentsService.CDSourceOfFund(context);
  }
}

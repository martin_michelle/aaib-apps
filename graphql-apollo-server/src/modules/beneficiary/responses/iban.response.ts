import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Iban {
  @Field()
  country: string;

  @Field()
  code: string;

  @Field()
  swift: string;

  @Field()
  bank: string;

  @Field({ nullable: true })
  branch?: string;
}

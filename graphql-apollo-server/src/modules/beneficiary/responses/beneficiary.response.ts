import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Beneficiary {
  @Field()
  name: string;

  @Field({ nullable: true })
  nickName?: string;

  @Field({ nullable: true })
  iban?: string;

  @Field({ nullable: true })
  accountNumber?: string;

  @Field()
  country: string;

  @Field()
  bank: string;

  @Field()
  swift: string;

  @Field({ nullable: true })
  relationship?: string;

  @Field({ nullable: true })
  isCreditCardNumber?: boolean;

  @Field(() => Boolean, { defaultValue: false })
  isMobileNumber?: boolean;

  @Field()
  currency: string;

  @Field({ nullable: true })
  address?: string;

  @Field()
  _id: string;
}

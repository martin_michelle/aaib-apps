import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Currencies {
  @Field()
  ID: string;

  @Field({ nullable: true })
  Code?: string;

  @Field({ nullable: true })
  EnglishName?: string;

  @Field({ nullable: true })
  ArabicName?: string;
}

import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Relationship {
  @Field()
  name: string;
}

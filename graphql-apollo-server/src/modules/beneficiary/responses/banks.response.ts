import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Countries {
  @Field()
  name: string;

  @Field()
  code: string;
}

@ObjectType()
export class ModifiedCountries {
  @Field()
  code: string;

  @Field()
  unicode: string;

  @Field()
  currency: string;

  @Field()
  image: string;

  @Field()
  emoji: string;

  @Field()
  enName: string;

  @Field()
  arName: string;
}
@ObjectType()
export class Banks {
  @Field()
  country: string;

  @Field()
  name: string;

  @Field()
  swift: string;
}

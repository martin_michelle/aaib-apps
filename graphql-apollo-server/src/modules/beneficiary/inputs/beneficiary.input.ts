import { InputType, Field } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class BeneficiaryInput {
  @IsNotEmpty()
  @Field()
  name: string;

  @Field({ nullable: true })
  nickName?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  iban?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  accountNumber?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  isCreditCardNumber?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  isMobileNumber?: boolean;

  @IsNotEmpty()
  @Field()
  country: string;

  @IsNotEmpty()
  @Field()
  bank: string;

  @IsNotEmpty()
  @Field()
  swift: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  relationship?: string;

  @IsNotEmpty()
  @Field()
  currency: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  address?: string;

  @IsNotEmpty()
  @Field()
  otp: string;
}

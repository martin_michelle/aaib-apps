import { Arg, Mutation, Resolver, Ctx, Query } from 'type-graphql';
import { Inject, Service } from 'typedi';
import { BeneficiaryService } from './beneficiary.service';
import { Beneficiary } from './responses/beneficiary.response';
import { BeneficiaryInput } from './inputs/beneficiary.input';
import { MyExpressContext } from 'app-context';
import { Banks, Countries, ModifiedCountries } from './responses/banks.response';
import { Iban } from './responses/iban.response';
import { Currencies } from './responses/currencies.response';
import { Relationship } from './responses/relationship.response';

@Service()
@Resolver()
export default class UserResolver {
  @Inject(() => BeneficiaryService)
  beneficiaryService: BeneficiaryService;

  @Mutation(() => String)
  async addBeneficiary(
    @Arg('data') data: BeneficiaryInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.beneficiaryService.addBeneficiary(context, data);
  }

  @Mutation(() => String)
  async editBeneficiary(
    @Arg('data') data: BeneficiaryInput,
    @Arg('BeneficiaryId') BeneficiaryId: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.beneficiaryService.editBeneficiary(context, BeneficiaryId, data);
  }

  @Mutation(() => String)
  async deleteBeneficiary(
    @Arg('BeneficiaryId') BeneficiaryId: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.beneficiaryService.deleteBeneficiary(context, BeneficiaryId);
  }

  @Query(() => [Beneficiary])
  async beneficiaries(
    @Ctx() context: MyExpressContext,
    @Arg('isCreditCardNumber', { nullable: true }) isCreditCardNumber?: boolean
  ): Promise<Beneficiary[]> {
    return this.beneficiaryService.listBeneficiaries(context, isCreditCardNumber);
  }

  @Query(() => [Countries])
  async countries(@Ctx() context: MyExpressContext): Promise<Countries[]> {
    return this.beneficiaryService.countries(context);
  }

  @Query(() => [ModifiedCountries])
  async modifiedCountries(@Ctx() context: MyExpressContext): Promise<ModifiedCountries[]> {
    return this.beneficiaryService.modifiedCountries(context);
  }

  @Query(() => [Banks])
  async banks(@Arg('country') country: string, @Ctx() context: MyExpressContext): Promise<Banks[]> {
    return this.beneficiaryService.banks(context, country);
  }

  @Query(() => Iban)
  async ibanValidation(@Arg('iban') iban: string, @Ctx() context: MyExpressContext): Promise<Iban> {
    return this.beneficiaryService.ibanValidation(context, iban);
  }

  @Query(() => [Currencies])
  async currencies(@Ctx() context: MyExpressContext): Promise<Currencies[]> {
    return this.beneficiaryService.currencies(context);
  }

  @Query(() => [Relationship])
  async relationships(@Ctx() context: MyExpressContext): Promise<Relationship[]> {
    return this.beneficiaryService.relationships(context);
  }
}

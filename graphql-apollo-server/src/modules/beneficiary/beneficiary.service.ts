import { Service } from 'typedi';
import fetch from 'node-fetch';
import { formatApiResponse } from '../../lib/formatResponse';

import { BeneficiaryInput } from './inputs/beneficiary.input';
import { Beneficiary } from './responses/beneficiary.response';
import { KONG_BASE_URL } from '../../lib/config';
import { MyExpressContext } from 'app-context';
import headersInit from '../../lib/headers';
import { Banks, Countries, ModifiedCountries } from './responses/banks.response';
import { Iban } from './responses/iban.response';
import { UserInputError } from 'apollo-server-express';
import { Currencies } from './responses/currencies.response';
import { Relationship } from './responses/relationship.response';
import { stringifyUrl } from 'query-string';

@Service()
export class BeneficiaryService {
  async addBeneficiary(context: MyExpressContext, data: BeneficiaryInput): Promise<string> {
    const addBeneficiaryResponse = await fetch(`${KONG_BASE_URL}/beneficiary`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(addBeneficiaryResponse);
    return res.id;
  }

  async editBeneficiary(
    context: MyExpressContext,
    BeneficiaryId: string,
    data: BeneficiaryInput
  ): Promise<string> {
    const editBeneficiaryResponse = await fetch(`${KONG_BASE_URL}/beneficiary/${BeneficiaryId}`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(editBeneficiaryResponse);
    return res.message;
  }

  async deleteBeneficiary(context: MyExpressContext, BeneficiaryId: string): Promise<string> {
    const deleteBeneficiaryResponse = await fetch(`${KONG_BASE_URL}/beneficiary/${BeneficiaryId}`, {
      method: 'DELETE',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(deleteBeneficiaryResponse);
    return res.message;
  }

  async listBeneficiaries(
    context: MyExpressContext,
    isCreditCardNumber?: boolean
  ): Promise<Beneficiary[]> {
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/beneficiary`,
        query: { isCreditCardNumber }
      },
      { skipNull: true, skipEmptyString: true }
    );
    const beneficiariesResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(beneficiariesResponse);
  }

  async countries(context: MyExpressContext): Promise<Countries[]> {
    const countriesResponse = await fetch(`${KONG_BASE_URL}/beneficiary/countries`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(countriesResponse);
    return res.countries || [];
  }

  async modifiedCountries(context: MyExpressContext): Promise<ModifiedCountries[]> {
    const countriesResponse = await fetch(`${KONG_BASE_URL}/beneficiary/countries/list`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(countriesResponse);
    return res.countries || [];
  }

  async banks(context: MyExpressContext, country: string): Promise<Banks[]> {
    const banksResponse = await fetch(`${KONG_BASE_URL}/beneficiary/banks/${country}`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(banksResponse);
  }

  async ibanValidation(context: MyExpressContext, iban: string): Promise<Iban> {
    const ibanResponse = await fetch(`${KONG_BASE_URL}/beneficiary/iban/${iban}`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(ibanResponse);
    if (!res.info) throw new UserInputError('No Data Found!');
    return res.info;
  }

  async currencies(context: MyExpressContext): Promise<Currencies[]> {
    const currencies = await fetch(`${KONG_BASE_URL}/beneficiary/currency`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(currencies);
  }

  async relationships(context: MyExpressContext): Promise<Relationship[]> {
    const relationships = await fetch(`${KONG_BASE_URL}/beneficiary/relationship`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(relationships);
  }
}

import {
  BillPaymentHistoryInput,
  EditScheduleFawryPayment,
  FawryAddFavoriteInput,
  FawryBillersInput,
  FawryBillInquiryInput,
  FawryEditFavoriteInput,
  FawryFeesInput,
  FawryGetBillerByIdInput,
  FawryPaymentInput,
  MultipleFawryBillInquiryInput,
  MultipleFawryPaymentInput,
  PayAndScheduleFawryPaymentInput,
  ScheduleFawryPayment
} from './inputs/fawry.input';
import { PaginationInput } from './inputs/pagination.input';
import { FawryService } from './fawry.service';
import {
  FawryBill,
  FawryBiller,
  FawryBillers,
  FawryBillFees,
  FawryBillPayment,
  FawryBillPaymentHistory,
  FawryCategories,
  FawryFavorites,
  MultipleFawryBill,
  MultipleFawryBillPayment,
  OnlineFawryFavorites,
  ScheduledFawryBillPayment,
  ScheduledFawryBillPaymentResponce
} from './responses/fawry.response';
import { Arg, Resolver, Ctx, Query, Mutation } from 'type-graphql';
import { Inject, Service } from 'typedi';
import { MyExpressContext } from 'app-context';

@Service()
@Resolver()
export default class FawryResolver {
  @Inject(() => FawryService)
  fawryService: FawryService;

  @Query(() => FawryCategories)
  async listFawryCategories(
    @Arg('data') data: PaginationInput,
    @Ctx() context: MyExpressContext
  ): Promise<FawryCategories> {
    return this.fawryService.listCategories(context, data);
  }

  @Query(() => FawryBillers)
  async listFawryBillers(
    @Arg('data') data: FawryBillersInput,
    @Ctx() context: MyExpressContext
  ): Promise<FawryBillers> {
    return this.fawryService.listBillers(context, data);
  }

  @Query(() => FawryBill)
  async fawryBillInquiry(
    @Arg('data') data: FawryBillInquiryInput,
    @Ctx() context: MyExpressContext
  ): Promise<FawryBill> {
    return this.fawryService.billInquiry(context, data);
  }
  @Query(() => MultipleFawryBill)
  async multipleFawryBillInquiry(
    @Arg('data') data: MultipleFawryBillInquiryInput,
    @Ctx() context: MyExpressContext
  ): Promise<MultipleFawryBill> {
    return this.fawryService.multipleBillInquiry(context, data);
  }

  @Mutation(() => FawryBillPayment)
  async fawryBillPayment(
    @Arg('data') data: FawryPaymentInput,
    @Ctx() context: MyExpressContext
  ): Promise<FawryBillPayment> {
    return this.fawryService.billPayment(context, data);
  }
  @Mutation(() => MultipleFawryBillPayment)
  async multipleFawryBillPayment(
    @Arg('data') data: MultipleFawryPaymentInput,
    @Ctx() context: MyExpressContext
  ): Promise<MultipleFawryBillPayment> {
    return this.fawryService.multipleBillPayment(context, data);
  }

  @Query(() => FawryBillPaymentHistory)
  async fawryBillPaymentHistory(
    @Arg('data') data: BillPaymentHistoryInput,
    @Ctx() context: MyExpressContext
  ): Promise<FawryBillPaymentHistory> {
    return this.fawryService.billPaymentHistory(context, data);
  }

  @Mutation(() => String)
  async addFawryFavorite(
    @Arg('data') data: FawryAddFavoriteInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.fawryService.addFavorite(context, data);
  }

  @Mutation(() => String)
  async editFawryFavorite(
    @Arg('data') data: FawryEditFavoriteInput,
    @Arg('FavoriteId') FavoriteId: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.fawryService.editFavorite(context, FavoriteId, data);
  }

  @Mutation(() => String)
  async deleteFawryFavorite(
    @Arg('FavoriteId') FavoriteId: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.fawryService.deleteFavorite(context, FavoriteId);
  }

  @Query(() => [FawryFavorites])
  async listFawryFavorite(@Ctx() context: MyExpressContext): Promise<FawryFavorites[]> {
    return this.fawryService.listFavorites(context);
  }
  @Query(() => [OnlineFawryFavorites])
  async listOnlineFawryFavorite(@Ctx() context: MyExpressContext): Promise<OnlineFawryFavorites[]> {
    return this.fawryService.listOnlineFawryFavorite(context);
  }

  @Query(() => FawryBiller)
  async getFawryBiller(
    @Arg('data') data: FawryGetBillerByIdInput,
    @Ctx() context: MyExpressContext
  ): Promise<FawryBiller> {
    return this.fawryService.getBiller(context, data);
  }

  @Query(() => FawryBillFees)
  async fawryBillFees(
    @Arg('data') data: FawryFeesInput,
    @Ctx() context: MyExpressContext
  ): Promise<FawryBillFees> {
    return this.fawryService.billFess(context, data);
  }

  @Mutation(() => String)
  async scheduleFawryBillPayment(
    @Arg('data') data: ScheduleFawryPayment,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.fawryService.scheduleBillPayment(context, data);
  }

  @Mutation(() => ScheduledFawryBillPaymentResponce)
  async scheduleOnlineFawryBillPayment(
    @Arg('data') data: ScheduleFawryPayment,
    @Ctx() context: MyExpressContext
  ): Promise<ScheduledFawryBillPaymentResponce> {
    return this.fawryService.scheduleOnlineBillPayment(context, data);
  }

  @Mutation(() => FawryBillPayment)
  async payAndScheduleFawryPayment(
    @Arg('data') data: PayAndScheduleFawryPaymentInput,
    @Ctx() context: MyExpressContext
  ): Promise<FawryBillPayment> {
    return this.fawryService.payAndScheduleFawryPayment(context, data);
  }

  @Mutation(() => String)
  async deleteBillPaymentSchedule(
    @Arg('scheduleId') scheduleId: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.fawryService.deleteSchedule(context, scheduleId);
  }

  @Query(() => [ScheduledFawryBillPayment])
  async listScheduledFawryBillPayments(
    @Ctx() context: MyExpressContext
  ): Promise<ScheduledFawryBillPayment[]> {
    return this.fawryService.listScheduledBillPayments(context);
  }

  @Mutation(() => String)
  async editScheduleFawryBillPayment(
    @Arg('data') data: EditScheduleFawryPayment,
    @Arg('paymentId') paymentId: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.fawryService.editScheduleFawryBillPayment(context, paymentId, data);
  }
}

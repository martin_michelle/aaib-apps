import { InputType, Field, Int } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class PaginationInput {
  @IsOptional()
  @IsNotEmpty()
  @Field(() => Int, { nullable: true })
  page?: number;

  @IsOptional()
  @IsNotEmpty()
  @Field(() => Int, { nullable: true })
  limit?: number;

  @IsOptional()
  @Field({ nullable: true })
  search?: string;
}

import { PaginationInput } from './pagination.input';
import { InputType, Field, Float } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class FawryBillersInput extends PaginationInput {
  @IsNotEmpty()
  @Field()
  category: string;
}

@InputType()
class FawryPaymentDetails {
  @IsNotEmpty()
  @Field()
  BillTypeCode: string;

  @IsNotEmpty()
  @Field(() => Float)
  Amount: number;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  DebitAcc?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  ContractId?: string;

  @IsNotEmpty()
  @Field()
  PmtType: string;

  @IsNotEmpty()
  @Field()
  BillingAcct: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  BillRefNumber?: string;

  @IsNotEmpty()
  @Field()
  BillerId: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  BillerName?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  ServiceName?: string;

  @IsNotEmpty()
  @Field()
  otp: string;
}
@InputType()
class FawryPaymentDetailsWithoutOtp {
  @IsNotEmpty()
  @Field()
  BillTypeCode: string;

  @IsNotEmpty()
  @Field(() => Float)
  Amount: number;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  DebitAcc?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  ContractId?: string;

  @IsNotEmpty()
  @Field()
  PmtType: string;

  @IsNotEmpty()
  @Field()
  BillingAcct: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  BillRefNumber?: string;

  @IsNotEmpty()
  @Field()
  BillerId: string;

  @IsNotEmpty()
  @Field()
  Name: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  BillerName?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  ServiceName?: string;
}

@InputType()
export class FawryPaymentInput {
  @IsNotEmpty()
  @Field()
  paymentType: string;

  @IsNotEmpty()
  @Field(() => FawryPaymentDetails)
  paymentDetails: FawryPaymentDetails;
}
@InputType()
export class MultipleFawryPaymentInput {
  @IsNotEmpty()
  @Field()
  paymentType: string;

  @IsNotEmpty()
  @Field(() => [FawryPaymentDetailsWithoutOtp])
  payments: FawryPaymentDetailsWithoutOtp[];

  @IsNotEmpty()
  @Field()
  otp: string;
}

@InputType()
export class FawryBillInquiryInput {
  @IsNotEmpty()
  @Field()
  BillingAcct: string;

  @IsNotEmpty()
  @Field()
  BillerId: string;

  @IsNotEmpty()
  @Field()
  BillTypeCode: string;

  @IsNotEmpty()
  @Field()
  PmtType: string;
}
@InputType()
export class MultipleFawryBillInquiryOnlineInput {
  @IsNotEmpty()
  @Field()
  BillingAcct: string;

  @IsNotEmpty()
  @Field()
  BillerId: string;

  @IsNotEmpty()
  @Field()
  BillTypeCode: string;

  @IsNotEmpty()
  @Field()
  PmtType: string;
  @IsNotEmpty()
  @Field()
  Name: string;
  @IsNotEmpty()
  @Field()
  BillTypeName: string;
}
@InputType()
export class MultipleFawryBillInquiryInput {
  @IsNotEmpty()
  @Field(() => [MultipleFawryBillInquiryOnlineInput])
  bills: MultipleFawryBillInquiryOnlineInput[];
}

@InputType()
export class FawryAddFavoriteInput {
  @IsNotEmpty()
  @Field()
  BillingAcct: string;

  @IsNotEmpty()
  @Field()
  BillTypeAcctLabel: string;

  @IsNotEmpty()
  @Field()
  BillerId: string;

  @IsNotEmpty()
  @Field()
  BillTypeCode: string;

  @IsNotEmpty()
  @Field()
  PmtType: string;

  @IsNotEmpty()
  @Field()
  name: string;

  @IsNotEmpty()
  @Field()
  category: string;
}

@InputType()
export class FawryEditFavoriteInput {
  @IsOptional()
  @Field({ nullable: true })
  BillingAcct: string;

  @IsNotEmpty()
  @Field()
  name: string;
}

@InputType()
export class BillPaymentHistoryInput extends PaginationInput {
  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  startDate?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  endDate?: string;
}

@InputType()
export class FawryGetBillerByIdInput {
  @IsNotEmpty()
  @Field()
  billerId: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  billTypeCode?: string;
}

@InputType()
export class FawryFeesInput {
  @IsNotEmpty()
  @Field()
  BillTypeCode: string;

  @IsNotEmpty()
  @Field()
  Amt: string;

  @IsNotEmpty()
  @Field()
  PmtType: string;

  @IsNotEmpty()
  @Field()
  BillingAcct: string;

  @IsNotEmpty()
  @Field()
  BillerId: string;
}

@InputType()
export class ScheduleFawryPayment {
  @IsNotEmpty()
  @Field()
  paymentType: string;

  @IsNotEmpty()
  @Field()
  scheduleDate: string;

  @IsNotEmpty()
  @Field()
  scheduleType: string;

  @IsNotEmpty()
  @Field()
  BillTypeCode: string;

  @IsNotEmpty()
  @Field(() => Float)
  Amount: number;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  DebitAcc?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  ContractId?: string;

  @IsNotEmpty()
  @Field()
  PmtType: string;

  @IsNotEmpty()
  @Field()
  BillingAcct: string;

  @IsNotEmpty()
  @Field()
  BillerId: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  BillerName?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  ServiceName?: string;

  @IsNotEmpty()
  @Field()
  otp: string;

  @IsNotEmpty()
  @Field()
  category: string;
}

@InputType()
export class ScheduleFawryPaymentWithoutOtp {
  @IsNotEmpty()
  @Field()
  paymentType: string;

  @IsNotEmpty()
  @Field()
  scheduleDate: string;

  @IsNotEmpty()
  @Field()
  scheduleType: string;

  @IsNotEmpty()
  @Field()
  BillTypeCode: string;

  @IsNotEmpty()
  @Field(() => Float)
  Amount: number;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  DebitAcc?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  ContractId?: string;

  @IsNotEmpty()
  @Field()
  PmtType: string;

  @IsNotEmpty()
  @Field()
  BillingAcct: string;

  @IsNotEmpty()
  @Field()
  BillerId: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  BillerName?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  ServiceName?: string;

  @IsNotEmpty()
  @Field()
  category: string;
}

@InputType()
export class PayAndScheduleFawryPaymentInput {
  @IsNotEmpty()
  @Field(() => ScheduleFawryPaymentWithoutOtp)
  schedulePaymentInput: ScheduleFawryPaymentWithoutOtp;

  @IsNotEmpty()
  @Field(() => FawryPaymentInput)
  fawryPaymentInput: FawryPaymentInput;

  @IsNotEmpty()
  @Field()
  otp: string;
}

@InputType()
export class EditScheduleFawryPayment {
  @IsNotEmpty()
  @Field()
  scheduleDate: string;

  @IsNotEmpty()
  @Field()
  scheduleType: string;

  @IsNotEmpty()
  @Field(() => Float)
  Amount: number;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  DebitAcc?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  ContractId?: string;

  @IsNotEmpty()
  @Field()
  paymentType: string;

  @IsNotEmpty()
  @Field()
  otp: string;
}

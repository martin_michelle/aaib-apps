import { Pagination } from './pagination.response';
import { ObjectType, Field, Int, Float } from 'type-graphql';

@ObjectType()
export class FawryCategories extends Pagination {
  @Field(() => [String])
  docs: string[];
}

@ObjectType()
class PaymentRules {
  @Field(() => Boolean, { nullable: true })
  IsInqRqr?: boolean;

  @Field(() => Boolean, { nullable: true })
  IsMobNtfy?: boolean;

  @Field(() => Boolean, { nullable: true })
  IsFracAcpt?: boolean;

  @Field(() => Boolean, { nullable: true })
  IsPrtAcpt?: boolean;

  @Field(() => Boolean, { nullable: true })
  IsOvrAcpt?: boolean;

  @Field(() => Boolean, { nullable: true })
  IsAdvAcpt?: boolean;

  @Field(() => Boolean, { nullable: true })
  IsAcptCardPmt?: boolean;
}

@ObjectType()
class BillerInfo {
  @Field(() => Int)
  BillTypeCode: number;

  @Field()
  Name: string;

  @Field()
  PmtType: string;

  @Field()
  ServiceType: string;

  @Field()
  ServiceName: string;

  @Field()
  BillTypeAcctLabel: string;

  @Field(() => Boolean)
  IsHidden: boolean;

  @Field(() => Boolean)
  BillRefType: boolean;

  @Field(() => PaymentRules)
  PaymentRules: PaymentRules;

  @Field()
  BillTypeStatus: string;

  @Field()
  BillTypeDescription: string;
}

@ObjectType()
export class FawryBiller {
  @Field()
  BillerId: string;

  @Field()
  BillerName: string;

  @Field(() => [BillerInfo])
  BillerInfo: BillerInfo[];
}

@ObjectType()
export class FawryBillers extends Pagination {
  @Field(() => [FawryBiller])
  docs: FawryBiller[];
}

@ObjectType()
class FawryPaymentRangeValues {
  @Field(() => Float)
  Amt: number;

  @Field()
  CurCode: string;
}
@ObjectType()
class FawryPaymentRange {
  @Field(() => FawryPaymentRangeValues)
  Lower: FawryPaymentRangeValues;

  @Field(() => FawryPaymentRangeValues)
  Upper: FawryPaymentRangeValues;
}

@ObjectType()
class FawryBillSummAmt {
  @Field()
  BillSummAmtCode: string;

  @Field(() => FawryPaymentRangeValues)
  CurAmt: FawryPaymentRangeValues;
}
@ObjectType()
class FawryBillInfo {
  @Field({ nullable: true })
  DueDt?: string;

  @Field({ nullable: true })
  ExpDt?: string;

  @Field({ nullable: true })
  IssueDt?: string;

  @Field(() => [FawryPaymentRange])
  PaymentRange: FawryPaymentRange[];

  @Field({ nullable: true })
  RulesAwareness?: string;

  @Field(() => [FawryBillSummAmt])
  BillSummAmt: FawryBillSummAmt[];
}
@ObjectType()
class FawryBillRec {
  @Field({ nullable: true })
  BillNumber?: string;

  @Field()
  BillingAcct: string;

  @Field()
  BillerId: string;

  @Field(() => Int)
  BillTypeCode: number;

  @Field({ nullable: true })
  BillRefNumber?: string;

  @Field(() => FawryBillInfo)
  BillInfo: FawryBillInfo;

  @Field({ nullable: true })
  BillStatus?: string;
  @Field({ nullable: true })
  PmtType?: string;
  @Field({ nullable: true })
  Name?: string;
  @Field({ nullable: true })
  BillTypeName?: string;
}
@ObjectType()
export class FawryBill {
  @Field(() => [FawryBillRec])
  BillRec: FawryBillRec[];
}

@ObjectType()
export class MultipleFawryBill {
  @Field(() => [FawryBill])
  success?: FawryBill[];

  @Field(() => [multipleFawryBillFailureData])
  failed?: multipleFawryBillFailureData[];
}
@ObjectType()
export class multipleFawryBillFailureData {
  @Field()
  BillingAcct: String;
  @Field()
  BillerId: String;
  @Field()
  BillTypeCode: String;
  @Field()
  Name: String;
  @Field()
  BillTypeName: String;
}

@ObjectType()
export class FawryBillPayment {
  @Field()
  transactionId: string;

  @Field({ nullable: true })
  duplicated?: boolean;

  @Field({ nullable: true })
  scheduledError?: boolean;

  @Field({ nullable: true })
  scheduleAmount?: string;

  @Field({ nullable: true })
  scheduleDate?: string;

  @Field({ nullable: true })
  scheduleErrorReason: string;
}

@ObjectType()
export class ScheduledFawryBillPaymentResponce {
  @Field({ nullable: true })
  duplicated?: boolean;

  @Field({ nullable: true })
  scheduledError?: boolean;

  @Field({ nullable: true })
  scheduleAmount?: string;

  @Field({ nullable: true })
  scheduleDate?: string;

  @Field({ nullable: true })
  scheduleErrorReason?: string;
}

@ObjectType()
export class SuccessFawryBillPayment {
  @Field()
  transactionId: string;
  @Field()
  BillerId: string;
  @Field()
  Name: string;
  @Field()
  Amount: string;
}
@ObjectType()
export class MultipleFawryBillPayment {
  @Field(() => [SuccessFawryBillPayment])
  success?: SuccessFawryBillPayment[];

  @Field(() => [FailedMultipleFawryBillPayment])
  failed?: FailedMultipleFawryBillPayment[];
}

@ObjectType()
export class FailedMultipleFawryBillPayment {
  @Field()
  BillerId: string;
  @Field()
  Amount: string;
  @Field()
  Name: string;
}

@ObjectType()
export class FawryFavorites {
  @Field()
  _id: string;

  @Field()
  BillingAcct: string;

  @Field()
  BillTypeAcctLabel: string;

  @Field()
  BillerId: string;

  @Field()
  BillTypeCode: string;

  @Field()
  PmtType: string;

  @Field()
  name: string;

  @Field({ nullable: true })
  category?: string;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;
}

@ObjectType()
export class OnlineFawryFavorites {
  @Field()
  BillTypeName: string;

  @Field()
  _id: string;

  @Field()
  BillingAcct: string;

  @Field()
  BillTypeAcctLabel: string;

  @Field()
  BillerId: string;

  @Field()
  BillTypeCode: string;

  @Field()
  PmtType: string;

  @Field()
  name: string;

  @Field({ nullable: true })
  category?: string;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;
}

@ObjectType()
class FawryBillPaymentHistoryRecord {
  @Field()
  _id: string;

  @Field()
  billTypeCode: string;

  @Field(() => Float)
  amount: number;

  @Field({ nullable: true })
  debitAcc?: string;

  @Field({ nullable: true })
  contractId?: string;

  @Field()
  pmtType: string;

  @Field()
  billingAcct: string;

  @Field({ nullable: true })
  billRefNumber?: string;

  @Field()
  billerId: string;

  @Field()
  transactionId: string;

  @Field(() => Float, { nullable: true })
  fees?: number;

  @Field({ nullable: true })
  billerName?: string;

  @Field({ nullable: true })
  serviceName?: string;

  @Field({ nullable: true })
  category?: string;

  @Field({ nullable: true })
  billTypeAcctLabel?: string;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;
}
@ObjectType()
export class FawryBillPaymentHistory extends Pagination {
  @Field(() => [FawryBillPaymentHistoryRecord])
  docs: FawryBillPaymentHistoryRecord[];
}

@ObjectType()
export class FawryBillFees {
  @Field({ nullable: true })
  Amt?: string;
}

@ObjectType()
class ScheduledFawryBillPaymentBody {
  @Field()
  BillTypeCode: string;

  @Field(() => Float)
  Amount: number;

  @Field({ nullable: true })
  DebitAcc?: string;

  @Field({ nullable: true })
  ContractId?: string;

  @Field()
  PmtType: string;

  @Field()
  BillingAcct: string;

  @Field()
  BillerId: string;

  @Field({ nullable: true })
  BillerName?: string;

  @Field({ nullable: true })
  ServiceName?: string;
}
@ObjectType()
export class ScheduledFawryBillPayment {
  @Field(() => ScheduledFawryBillPaymentBody)
  body: ScheduledFawryBillPaymentBody;

  @Field()
  _id: string;

  @Field()
  user: string;

  @Field()
  userId: string;

  @Field()
  scheduleDate: string;

  @Field()
  nextPaymentDate: string;

  @Field()
  scheduleType: string;

  @Field()
  paymentType: string;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;

  @Field()
  category: string;
}

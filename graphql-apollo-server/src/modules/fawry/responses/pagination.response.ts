import { ObjectType, Field, Int } from 'type-graphql';

@ObjectType()
export class Pagination {
  @Field(() => Int)
  totalDocs: number;

  @Field(() => Int)
  limit: number;

  @Field(() => Int)
  page: number;

  @Field(() => Int)
  totalPages: number;

  @Field(() => Int)
  pagingCounter: number;

  @Field(() => Int, { nullable: true })
  offset?: number;

  @Field(() => Int, { nullable: true })
  prevPage?: number;

  @Field(() => Int, { nullable: true })
  nextPage?: number;

  @Field()
  hasPrevPage: boolean;

  @Field()
  hasNextPage: boolean;
}

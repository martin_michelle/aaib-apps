import { PaginationInput } from './inputs/pagination.input';
import {
  FawryBill,
  FawryBillers,
  FawryCategories,
  FawryBillPayment,
  FawryFavorites,
  FawryBillPaymentHistory,
  FawryBiller,
  FawryBillFees,
  ScheduledFawryBillPayment,
  MultipleFawryBill,
  MultipleFawryBillPayment,
  OnlineFawryFavorites,
  ScheduledFawryBillPaymentResponce
} from './responses/fawry.response';
import { Service } from 'typedi';
import fetch from 'node-fetch';
import { formatApiResponse } from '../../lib/formatResponse';

import { FAWRY_FILTER, KONG_BASE_URL } from '../../lib/config';
import { MyExpressContext } from 'app-context';
import headersInit from '../../lib/headers';
import { stringifyUrl } from 'query-string';
import {
  FawryBillersInput,
  FawryBillInquiryInput,
  FawryPaymentInput,
  FawryAddFavoriteInput,
  FawryEditFavoriteInput,
  BillPaymentHistoryInput,
  FawryGetBillerByIdInput,
  FawryFeesInput,
  ScheduleFawryPayment,
  PayAndScheduleFawryPaymentInput,
  MultipleFawryBillInquiryInput,
  MultipleFawryPaymentInput,
  EditScheduleFawryPayment
} from './inputs/fawry.input';
import { ApolloError } from 'apollo-server-express';

@Service()
export class FawryService {
  async listCategories(context: MyExpressContext, data: PaginationInput): Promise<FawryCategories> {
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/fawry/categories`, query: { ...data } },
      { skipNull: true, skipEmptyString: true }
    );
    const categoriesResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(categoriesResponse);
    if (FAWRY_FILTER?.length) {
      res[0].docs = res[0].docs.filter((category: string) => !FAWRY_FILTER.includes(category));
    }
    return res[0];
  }

  async listBillers(context: MyExpressContext, data: FawryBillersInput): Promise<FawryBillers> {
    const { category, page, limit, search } = data;
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/fawry/billers/${category}`, query: { search, page, limit } },
      { skipNull: true, skipEmptyString: true }
    );

    const billersResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(billersResponse);
    return res[0];
  }

  async billInquiry(context: MyExpressContext, data: FawryBillInquiryInput): Promise<FawryBill> {
    const billersResponse = await fetch(`${KONG_BASE_URL}/fawry/bill`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(billersResponse);
    if (res[0]?.Status?.StatusCode !== 200 || !res[0]?.BillInqRs)
      throw new ApolloError('Invalid Billing Account', 'INVALID_BILLING_ACT');
    return res[0]?.BillInqRs;
  }

  async multipleBillInquiry(
    context: MyExpressContext,
    data: MultipleFawryBillInquiryInput
  ): Promise<MultipleFawryBill> {
    const billersResponse = await fetch(`${KONG_BASE_URL}/fawry/multipleBills`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });
    return formatApiResponse(billersResponse);
  }

  async billPayment(context: MyExpressContext, data: FawryPaymentInput): Promise<FawryBillPayment> {
    const paymentResponse = await fetch(
      `${KONG_BASE_URL}/fawry/payment?paymentType=${data.paymentType}`,
      {
        method: 'POST',
        body: JSON.stringify(data.paymentDetails),
        headers: headersInit(context)
      }
    );

    return formatApiResponse(paymentResponse);
  }
  async multipleBillPayment(
    context: MyExpressContext,
    data: MultipleFawryPaymentInput
  ): Promise<MultipleFawryBillPayment> {
    const paymentResponse = await fetch(
      `${KONG_BASE_URL}/fawry/multiplePayment?paymentType=${data.paymentType}`,
      {
        method: 'POST',
        body: JSON.stringify({ payments: data.payments, otp: data.otp }),
        headers: headersInit(context)
      }
    );

    return formatApiResponse(paymentResponse);
  }

  async billPaymentHistory(
    context: MyExpressContext,
    data: BillPaymentHistoryInput
  ): Promise<FawryBillPaymentHistory> {
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/fawry/history`, query: { ...data } },
      { skipNull: true, skipEmptyString: true }
    );
    const historyResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(historyResponse);
  }

  async addFavorite(context: MyExpressContext, data: FawryAddFavoriteInput): Promise<string> {
    const addFavoriteResponse = await fetch(`${KONG_BASE_URL}/fawry/favorite`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(addFavoriteResponse);
    return res.message;
  }

  async editFavorite(
    context: MyExpressContext,
    FavoriteId: string,
    data: FawryEditFavoriteInput
  ): Promise<string> {
    const editFavoriteResponse = await fetch(`${KONG_BASE_URL}/fawry/favorite/${FavoriteId}`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(editFavoriteResponse);
    return res.message;
  }

  async deleteFavorite(context: MyExpressContext, FavoriteId: string): Promise<string> {
    const deleteFavoriteResponse = await fetch(`${KONG_BASE_URL}/fawry/favorite/${FavoriteId}`, {
      method: 'DELETE',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(deleteFavoriteResponse);
    return res.message;
  }

  async listFavorites(context: MyExpressContext): Promise<FawryFavorites[]> {
    const favoriteResponse = await fetch(`${KONG_BASE_URL}/fawry/favorite`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(favoriteResponse);
  }

  //Includes extra field
  async listOnlineFawryFavorite(context: MyExpressContext): Promise<OnlineFawryFavorites[]> {
    const favoriteResponse = await fetch(`${KONG_BASE_URL}/fawry/onlineUserFavorite`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(favoriteResponse);
  }

  async getBiller(context: MyExpressContext, data: FawryGetBillerByIdInput): Promise<FawryBiller> {
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/fawry/biller`, query: { ...data } },
      { skipNull: true, skipEmptyString: true }
    );

    const billerResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(billerResponse);
  }

  async billFess(context: MyExpressContext, data: FawryFeesInput): Promise<FawryBillFees> {
    const feesResponse = await fetch(`${KONG_BASE_URL}/fawry/fees`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(feesResponse);
  }

  async scheduleBillPayment(
    context: MyExpressContext,
    data: ScheduleFawryPayment
  ): Promise<string> {
    const scheduleResponse = await fetch(`${KONG_BASE_URL}/fawry/schedule`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(scheduleResponse);
    return res.message;
  }

  async scheduleOnlineBillPayment(
    context: MyExpressContext,
    data: ScheduleFawryPayment
  ): Promise<ScheduledFawryBillPaymentResponce> {
    const scheduleResponse = await fetch(`${KONG_BASE_URL}/fawry/scheduleOnlinePayment`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(scheduleResponse);
    return res;
  }

  async payAndScheduleFawryPayment(
    context: MyExpressContext,
    data: PayAndScheduleFawryPaymentInput
  ): Promise<FawryBillPayment> {
    const response = await fetch(`${KONG_BASE_URL}/fawry/payment/schedule`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const returnedData = await formatApiResponse(response);

    return returnedData;
  }

  async listScheduledBillPayments(context: MyExpressContext): Promise<ScheduledFawryBillPayment[]> {
    const scheduledResponse = await fetch(`${KONG_BASE_URL}/fawry/schedule/list`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(scheduledResponse);
  }

  async deleteSchedule(context: MyExpressContext, scheduleId: string): Promise<string> {
    const deleteScheduleResponse = await fetch(`${KONG_BASE_URL}/fawry/schedule/${scheduleId}`, {
      method: 'DELETE',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(deleteScheduleResponse);
    return res.message;
  }

  async editScheduleFawryBillPayment(
    context: MyExpressContext,
    paymentId: string,
    data: EditScheduleFawryPayment
  ): Promise<string> {
    const scheduleResponse = await fetch(`${KONG_BASE_URL}/fawry/schedule/${paymentId}`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(scheduleResponse);
    return res.message;
  }
}

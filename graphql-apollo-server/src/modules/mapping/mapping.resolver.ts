import { Resolver, Ctx, Query } from 'type-graphql';
import { Inject, Service } from 'typedi';
import { MappingService } from './mapping.service';
import { MyExpressContext } from 'app-context';
import { CardMapping, Mapping } from './responses/mapping.response';

@Service()
@Resolver()
export default class MappingResolver {
  @Inject(() => MappingService)
  mappingService: MappingService;

  @Query(() => [Mapping])
  async accountNames(@Ctx() context: MyExpressContext): Promise<Mapping[]> {
    return this.mappingService.accountNames(context);
  }

  @Query(() => [CardMapping])
  async cardMapping(@Ctx() context: MyExpressContext): Promise<CardMapping[]> {
    return this.mappingService.cardMapping(context);
  }
}

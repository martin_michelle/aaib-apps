import { Service } from 'typedi';
import fetch from 'node-fetch';
import { formatApiResponse } from '../../lib/formatResponse';

import { KONG_BASE_URL } from '../../lib/config';
import { MyExpressContext } from 'app-context';
import headersInit from '../../lib/headers';
import { CardMapping, Mapping } from './responses/mapping.response';

@Service()
export class MappingService {
  async accountNames(context: MyExpressContext): Promise<Mapping[]> {
    const mappingResponse = await fetch(`${KONG_BASE_URL}/mapping`, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(mappingResponse);
  }

  async cardMapping(context: MyExpressContext): Promise<CardMapping[]> {
    const mappingResponse = await fetch(`${KONG_BASE_URL}/mapping/card`, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(mappingResponse);
  }
}

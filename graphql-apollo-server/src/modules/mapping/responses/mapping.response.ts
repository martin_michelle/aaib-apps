import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Mapping {
  @Field()
  key: string;
  @Field()
  value: string;
  @Field({ nullable: true })
  description?: string;
}
@ObjectType()
export class CardMapping {
  @Field()
  BIN: string;
  @Field()
  cardName: string;
  @Field()
  cardType: string;
  @Field()
  EGP: string;
  @Field()
  USD: string;
  @Field({ nullable: true })
  interestRate?: string;
}

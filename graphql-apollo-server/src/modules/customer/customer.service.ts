import { Faqs } from './responses/faqs.response';
import { Service } from 'typedi';
import fetch from 'node-fetch';
import { formatApiResponse } from '../../lib/formatResponse';
import { KONG_BASE_URL } from '../../lib/config';
import { MyExpressContext } from 'app-context';
import headersInit from '../../lib/headers';
import { Atms } from './responses/atm.response';
import { stringifyUrl } from 'query-string';
import { AtmsInput } from './inputs/atm.input';
import { MobileTransfer } from './responses/mobiletransfer.response';
import { MobileTransferInput } from './inputs/mobileTransfer.input';
import {
  checkLimitInput,
  CustomerSettingsInput,
  FaqInput,
  LinkDebitCardsInput,
  ModifyEmailInput,
  updateDeliveryMechanismInput
} from './inputs/customer.input';
import {
  CustomerLimit,
  CustomerSettings,
  CustomerDebitCards,
  updateDeliveryMechanismResp
} from './responses/customer.response';
import { BranchesInput } from './inputs/branches.input';
import { Branches } from './responses/branches.response';
import { ChequeBookRequestInput } from './inputs/chequebookRequest.input';
import { ChequebookFees } from './responses/chequebook.response';
import { ChequebookBranches, ChequebookHistory } from './responses/chequebook.response';
import { NotificationPaginationInput } from './inputs/pagination.input';
import { Notifications } from './responses/notification.response';
import { SubmitRatingInput } from './inputs/rating.input';
import { reportIssueInput } from './inputs/issue.input';
import { validateMobileNumberForTransfer } from './responses/validateMobileNumberForTransfer.response';
import { securityTips } from './responses/securityTips.responce';

@Service()
export class CustomerService {
  async editCustomerSettings(
    context: MyExpressContext,
    data: CustomerSettingsInput
  ): Promise<string> {
    const updateCustomerSettingsResponse = await fetch(`${KONG_BASE_URL}/customer`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(updateCustomerSettingsResponse);
    return res.message;
  }

  async customerLimit(context: MyExpressContext, data: checkLimitInput): Promise<CustomerLimit> {
    const customerLimitData = await fetch(`${KONG_BASE_URL}/customer/checklimit`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(customerLimitData);
  }

  async customerSettings(context: MyExpressContext): Promise<CustomerSettings> {
    const customerSettings = await fetch(`${KONG_BASE_URL}/customer`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(customerSettings);
  }

  async faqs(context: MyExpressContext, data: FaqInput): Promise<Faqs[]> {
    const faqs = await fetch(`${KONG_BASE_URL}/customer/faq?type=${data.type}`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(faqs);
    return res.result || [];
  }

  async termsAndConditionsAccount(context: MyExpressContext): Promise<String> {
    const termsAndConditions = await fetch(`${KONG_BASE_URL}/customer/termsAndConditions`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(termsAndConditions);
    return res.result;
  }

  async securityTips(context: MyExpressContext): Promise<securityTips> {
    const securityTips = await fetch(`${KONG_BASE_URL}/customer/security/tips`, {
      method: 'GET',
      headers: headersInit(context)
    });
    const res = await formatApiResponse(securityTips);
    return res.result;
  }

  async atms(context: MyExpressContext, data: AtmsInput): Promise<Atms[]> {
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/customer/atms`, query: { ...data } },
      { skipNull: true, skipEmptyString: true }
    );
    const atms = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(atms);
  }

  async mobileTransfer(context: MyExpressContext, mobileNumber?: string): Promise<MobileTransfer> {
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/customer/mobileTransfer`,
        query: { mobileNumber }
      },
      { skipNull: true, skipEmptyString: true }
    );
    const mobileTransferResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(mobileTransferResponse);
  }
  async validateMobileNumberForTransfer(
    context: MyExpressContext,
    mobileNumber: string
  ): Promise<validateMobileNumberForTransfer> {
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/customer/mobileTransfer/validateMobileNumberForTransfer`,
        query: { mobileNumber }
      },
      { skipNull: true, skipEmptyString: true }
    );
    const validateMobileNumberForTransferResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(validateMobileNumberForTransferResponse);
  }

  async updateMobileTransfer(
    context: MyExpressContext,
    data: MobileTransferInput
  ): Promise<string> {
    const updateCustomerSettingsResponse = await fetch(`${KONG_BASE_URL}/customer/mobileTransfer`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(updateCustomerSettingsResponse);
    return res.message;
  }
  async reportIssue(context: MyExpressContext, data: reportIssueInput): Promise<string> {
    const reportIssueResponse = await fetch(`${KONG_BASE_URL}/customer/submit/report`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(reportIssueResponse);
    return res.message;
  }

  async branches(context: MyExpressContext, data: BranchesInput): Promise<Branches[]> {
    const branchesList = await fetch(`${KONG_BASE_URL}/customer/branches?search=${data.search}`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(branchesList);
  }

  async submitChequeBookRequest(
    context: MyExpressContext,
    data: ChequeBookRequestInput
  ): Promise<String> {
    const updateCustomerSettingsResponse = await fetch(
      `${KONG_BASE_URL}/customer/chequebook/submit`,
      {
        method: 'POST',
        body: JSON.stringify(data),
        headers: headersInit(context)
      }
    );

    const res = await formatApiResponse(updateCustomerSettingsResponse);
    return res.message;
  }

  async validateChequeBookRequest(
    context: MyExpressContext,
    data: ChequeBookRequestInput
  ): Promise<ChequebookFees> {
    const updateCustomerSettingsResponse = await fetch(
      `${KONG_BASE_URL}/customer/chequebook/validate`,
      {
        method: 'POST',
        body: JSON.stringify(data),
        headers: headersInit(context)
      }
    );

    return formatApiResponse(updateCustomerSettingsResponse);
  }

  async termsAndConditionsChequebook(context: MyExpressContext): Promise<String> {
    const termsAndConditions = await fetch(
      `${KONG_BASE_URL}/customer/chequebook/termsAndConditions`,
      {
        method: 'GET',
        headers: headersInit(context)
      }
    );

    const res = await formatApiResponse(termsAndConditions);
    return res.result;
  }

  async chequebookBranches(context: MyExpressContext): Promise<ChequebookBranches[]> {
    const chequebookBranches = await fetch(`${KONG_BASE_URL}/customer/chequebook/branches`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(chequebookBranches);
  }

  async userNotifications(
    data: NotificationPaginationInput,
    context: MyExpressContext
  ): Promise<Notifications> {
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/customer/notifications`,
        query: { ...data }
      },
      { skipNull: true, skipEmptyString: true }
    );
    const notifications = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(notifications);
  }

  async chequebookHistory(
    context: MyExpressContext,
    accountId: string
  ): Promise<ChequebookHistory[]> {
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/customer/chequebook/history`,
        query: { accountId }
      },
      { skipNull: true, skipEmptyString: true }
    );
    const chequebookHistory = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(chequebookHistory);
  }

  async chequebookUnauthHistory(
    context: MyExpressContext,
    accountId: string
  ): Promise<ChequebookHistory[]> {
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/customer/chequebook/history/unauth`,
        query: { accountId }
      },
      { skipNull: true, skipEmptyString: true }
    );
    const chequebookHistory = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(chequebookHistory);
  }

  async submitRating(context: MyExpressContext, data: SubmitRatingInput): Promise<string> {
    const submitRatingResponse = await fetch(`${KONG_BASE_URL}/customer/submit/rating`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(submitRatingResponse);
    return res.message;
  }

  async modifyEmail(context: MyExpressContext, data: ModifyEmailInput): Promise<string> {
    const modifyEmailResponse = await fetch(`${KONG_BASE_URL}/customer/update/submit`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(modifyEmailResponse);

    return res.message;
  }

  async returnAddress(context: MyExpressContext): Promise<string[]> {
    const returnAddressResponse = await fetch(`${KONG_BASE_URL}/customer/address`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(returnAddressResponse);
    return res;
  }

  async customerDebitCards(context: MyExpressContext, AccID: string): Promise<CustomerDebitCards> {
    const returnDebitCardsResponse = await fetch(`${KONG_BASE_URL}/debit/customer/cards/${AccID}`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(returnDebitCardsResponse);
    return res;
  }

  async changeLinkStateToAccount(
    context: MyExpressContext,
    data: LinkDebitCardsInput
  ): Promise<string> {
    const changeLinkStateResponse = await fetch(
      `${KONG_BASE_URL}/debit/debitcardaccount/setStatus`,
      {
        method: 'POST',
        body: JSON.stringify(data),
        headers: headersInit(context)
      }
    );

    const res = await formatApiResponse(changeLinkStateResponse);
    return res.message;
  }

  async updateDeliveryMechanism(
    context: MyExpressContext,
    data: updateDeliveryMechanismInput
  ): Promise<updateDeliveryMechanismResp> {
    const resp = await fetch(`${KONG_BASE_URL}/customer/update/deliveryStatment`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });
    const res = await formatApiResponse(resp);

    return res;
  }
}

import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Notification {
  @Field()
  _id: string;

  @Field()
  title: string;

  @Field()
  message: string;

  @Field()
  notificationCode: string;

  @Field()
  notificationDate: string;

  @Field({ nullable: true })
  channel: string;

  @Field({ nullable: true })
  transactionSuccessful: boolean;
}
@ObjectType()
export class Notifications {
  @Field()
  totalCount: string;

  @Field(() => [Notification])
  notifications: Notification[];
}

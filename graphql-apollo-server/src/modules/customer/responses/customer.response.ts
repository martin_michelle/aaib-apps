import { ObjectType, Field, Int, Float } from 'type-graphql';
import { CardDetails } from '../../accounts/responses/debitCard.response';

@ObjectType()
export class CustomerSettings {
  @Field({ nullable: true })
  firstLogin?: boolean;

  @Field({ nullable: true })
  showSoftTokenApp?: boolean;

  @Field({ nullable: true })
  showBalance?: boolean;

  @Field({ nullable: true })
  showMobileAdd?: boolean;

  @Field({ nullable: true })
  showSharePopup?: boolean;

  @Field({ nullable: true })
  showMobileTransferPopup?: boolean;

  @Field({ nullable: true })
  defaultAccount?: string;

  @Field({ nullable: true })
  pushId?: string;

  @Field({ nullable: true })
  kycLastUpdatedDate?: string;

  @Field({ nullable: true })
  registrationAlertUpdatedDate?: string;

  @Field({ nullable: true })
  pushNotifications?: boolean;

  @Field(() => [String], { nullable: true })
  tutorials?: [string];

  @Field(() => Int, { nullable: true })
  numberOfWebLogins?: number;

  @Field({ nullable: true })
  hasReadNotification?: boolean;

  @Field({ nullable: true })
  lastNotificationId?: string;

  @Field(() => Float, { nullable: true })
  dailyTransferLimit?: number;

  @Field({ nullable: true })
  showRatePopUp?: boolean;

  @Field(() => Float, { nullable: true })
  transactionsCount?: number;

  @Field()
  deliveryMechanism?: string;

  @Field()
  deliveryMechanismFirstTime?: boolean;

  @Field({ nullable: true })
  showLinkingPopUp?: boolean;
}

@ObjectType()
export class CustomerLimit {
  @Field()
  Status: boolean;

  @Field()
  ChannelAvailable: Number;

  @Field()
  AvailableCurrency: string;

  @Field()
  Message: string;
}

@ObjectType()
export class CustomerDebitCards {
  @Field()
  isLinked: boolean;

  @Field(() => [CardDetails], { nullable: true })
  debitCards: CardDetails[];
}

@ObjectType()
export class updateDeliveryMechanismResp {
  @Field()
  message: string;
  @Field()
  status: string;
}

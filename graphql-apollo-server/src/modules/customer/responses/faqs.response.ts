import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Faqs {
  @Field()
  question: string;

  @Field()
  answer: string;
}

import { ObjectType, Field, Int, Float } from 'type-graphql';

@ObjectType()
export class Atms {
  @Field(() => Int, { nullable: true })
  atmId: number;

  @Field(() => Float)
  googleLatitude: number;

  @Field(() => Float)
  googleLongitude: number;

  @Field(() => Float, { nullable: true })
  distance: number;

  @Field()
  name: string;

  @Field()
  location: string;

  @Field()
  governorateName: string;

  @Field()
  type: string;

  @Field(() => [String])
  functionality: string[];
}

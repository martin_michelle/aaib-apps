import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class MobileTransfer {
  @Field({ nullable: true })
  mobileNumber?: string;

  @Field()
  fullName: string;

  @Field({ nullable: true })
  accountNumber: string;

  @Field()
  cif: string;

  @Field()
  enabled: boolean;
}

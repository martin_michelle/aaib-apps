import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Tips {
  @Field(() => [String])
  informationSecurity: string[];

  @Field(() => [String])
  Credentials: string[];

  @Field(() => [String])
  Fraud: string[];
}
@ObjectType()
export class securityTips {
  @Field(() => Tips)
  english: Tips;

  @Field(() => Tips)
  arabic: Tips;
}

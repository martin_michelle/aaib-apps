import { ObjectType, Field, Float } from 'type-graphql';

@ObjectType()
export class Branches {
  @Field(() => Float)
  googleLatitude: number;

  @Field(() => Float)
  googleLongitude: number;

  @Field()
  name: string;

  @Field()
  location: string;

  @Field()
  type: string;

  @Field()
  governorateName: string;

  @Field(() => [String])
  functionality: string[];
}

import { ObjectType, Field, Float } from 'type-graphql';

@ObjectType()
export class ChequebookFees {
  @Field(() => Float)
  Fees: number;
}
@ObjectType()
export class ChequebookBranches {
  @Field()
  ID: string;

  @Field()
  Code: string;

  @Field()
  EnglishName: string;

  @Field({ nullable: true })
  ArabicName?: string;

  @Field({ nullable: true })
  Value?: string;
}

@ObjectType()
export class ChequebookHistory {
  @Field()
  ID: string;

  @Field()
  AccountNo: string;

  @Field()
  Customer: string;

  @Field()
  CustomerName: string;

  @Field()
  Currency: string;

  @Field()
  ChequeStatus: string;

  @Field()
  StatusDate: number;

  @Field()
  StartSerial: number;

  @Field()
  EndSerial: number;

  @Field()
  NoOfCheques: number;

  @Field()
  DeliveryBranch: string;

  @Field()
  Fees: string;
}

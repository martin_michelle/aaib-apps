import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class validateMobileNumberForTransfer {
  @Field()
  isValid: boolean;

  @Field({ nullable: true })
  maskedName?: string;
}

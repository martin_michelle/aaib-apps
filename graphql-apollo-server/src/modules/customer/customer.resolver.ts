import { Faqs } from './responses/faqs.response';
import { Arg, Mutation, Resolver, Ctx, Query } from 'type-graphql';
import { Inject, Service } from 'typedi';
import { CustomerService } from './customer.service';
import { MyExpressContext } from 'app-context';
import { Atms } from './responses/atm.response';
import { AtmsInput } from './inputs/atm.input';
import { MobileTransfer } from './responses/mobiletransfer.response';
import { MobileTransferInput } from './inputs/mobileTransfer.input';
import {
  checkLimitInput,
  CustomerSettingsInput,
  FaqInput,
  LinkDebitCardsInput,
  ModifyEmailInput,
  updateDeliveryMechanismInput
} from './inputs/customer.input';
import {
  CustomerLimit,
  CustomerSettings,
  CustomerDebitCards,
  updateDeliveryMechanismResp
} from './responses/customer.response';
import { BranchesInput } from './inputs/branches.input';
import { Branches } from './responses/branches.response';
import { ChequeBookRequestInput } from './inputs/chequebookRequest.input';
import { ChequebookFees } from './responses/chequebook.response';
import { ChequebookBranches, ChequebookHistory } from './responses/chequebook.response';
import { NotificationPaginationInput } from './inputs/pagination.input';
import { Notifications } from './responses/notification.response';
import { SubmitRatingInput } from './inputs/rating.input';
import { reportIssueInput } from './inputs/issue.input';
import { logger } from '../../lib/logger';
import { validateMobileNumberForTransfer } from './responses/validateMobileNumberForTransfer.response';
import { securityTips } from './responses/securityTips.responce';

@Service()
@Resolver()
export default class UserResolver {
  @Inject(() => CustomerService)
  customerService: CustomerService;

  @Mutation(() => String)
  async editCustomerSettings(
    @Arg('data') data: CustomerSettingsInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.customerService.editCustomerSettings(context, data);
  }

  @Mutation(() => CustomerLimit)
  async customerLimit(
    @Arg('data') data: checkLimitInput,
    @Ctx() context: MyExpressContext
  ): Promise<CustomerLimit> {
    return this.customerService.customerLimit(context, data);
  }

  @Query(() => CustomerSettings)
  async customerSettings(@Ctx() context: MyExpressContext): Promise<CustomerSettings> {
    return this.customerService.customerSettings(context);
  }
  @Query(() => Notifications)
  async userNotifications(
    @Arg('data') data: NotificationPaginationInput,
    @Ctx() context: MyExpressContext
  ): Promise<Notifications> {
    return this.customerService.userNotifications(data, context);
  }

  @Query(() => [Faqs])
  async faqs(@Arg('data') data: FaqInput, @Ctx() context: MyExpressContext): Promise<Faqs[]> {
    return this.customerService.faqs(context, data);
  }

  @Query(() => String)
  async termsAndConditionsAccount(@Ctx() context: MyExpressContext): Promise<String> {
    return this.customerService.termsAndConditionsAccount(context);
  }
  @Query(() => securityTips)
  async securityTips(@Ctx() context: MyExpressContext): Promise<securityTips> {
    return this.customerService.securityTips(context);
  }

  @Query(() => [Atms])
  async atms(@Arg('data') data: AtmsInput, @Ctx() context: MyExpressContext): Promise<Atms[]> {
    return this.customerService.atms(context, data);
  }

  @Query(() => MobileTransfer)
  async mobileTransfer(
    @Ctx() context: MyExpressContext,

    @Arg('mobileNumber', { nullable: true }) mobileNumber?: string
  ): Promise<MobileTransfer> {
    return this.customerService.mobileTransfer(context, mobileNumber);
  }

  @Query(() => validateMobileNumberForTransfer)
  async validateMobileNumberForTransfer(
    @Ctx() context: MyExpressContext,
    @Arg('mobileNumber') mobileNumber: string
  ): Promise<validateMobileNumberForTransfer> {
    return this.customerService.validateMobileNumberForTransfer(context, mobileNumber);
  }

  @Mutation(() => String)
  async updateMobileTransfer(
    @Ctx() context: MyExpressContext,
    @Arg('data') data: MobileTransferInput
  ): Promise<String> {
    return this.customerService.updateMobileTransfer(context, data);
  }

  @Query(() => [Branches])
  async branches(
    @Arg('data') data: BranchesInput,
    @Ctx() context: MyExpressContext
  ): Promise<Branches[]> {
    return this.customerService.branches(context, data);
  }

  @Mutation(() => String)
  async submitChequeBookRequest(
    @Ctx() context: MyExpressContext,
    @Arg('data') data: ChequeBookRequestInput
  ): Promise<String> {
    return this.customerService.submitChequeBookRequest(context, data);
  }

  @Mutation(() => String)
  async reportIssue(
    @Ctx() context: MyExpressContext,
    @Arg('data') data: reportIssueInput
  ): Promise<String> {
    return this.customerService.reportIssue(context, data);
  }

  @Mutation(() => ChequebookFees)
  async validateChequeBookRequest(
    @Ctx() context: MyExpressContext,
    @Arg('data') data: ChequeBookRequestInput
  ): Promise<ChequebookFees> {
    return this.customerService.validateChequeBookRequest(context, data);
  }

  @Query(() => String)
  async termsAndConditionsChequebook(@Ctx() context: MyExpressContext): Promise<String> {
    return this.customerService.termsAndConditionsChequebook(context);
  }

  @Query(() => [ChequebookBranches])
  async chequebookBranches(@Ctx() context: MyExpressContext): Promise<ChequebookBranches[]> {
    return this.customerService.chequebookBranches(context);
  }

  @Query(() => [ChequebookHistory])
  async chequebookHistory(
    @Ctx() context: MyExpressContext,
    @Arg('accountId') accountId: string
  ): Promise<ChequebookHistory[]> {
    return this.customerService.chequebookHistory(context, accountId);
  }

  @Query(() => [ChequebookHistory])
  async chequebookUnauthHistory(
    @Ctx() context: MyExpressContext,
    @Arg('accountId') accountId: string
  ): Promise<ChequebookHistory[]> {
    return this.customerService.chequebookUnauthHistory(context, accountId);
  }

  @Mutation(() => String)
  async submitRating(
    @Arg('data') data: SubmitRatingInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.customerService.submitRating(context, data);
  }

  @Mutation(() => String)
  async modifyEmail(
    @Arg('data') data: ModifyEmailInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    logger.info('test');
    return this.customerService.modifyEmail(context, data);
  }

  @Query(() => [String])
  async returnAddress(@Ctx() context: MyExpressContext): Promise<string[]> {
    return this.customerService.returnAddress(context);
  }

  @Query(() => CustomerDebitCards)
  async customerDebitCards(
    @Arg('AccID') AccID: string,
    @Ctx() context: MyExpressContext
  ): Promise<CustomerDebitCards> {
    return this.customerService.customerDebitCards(context, AccID);
  }

  @Mutation(() => String)
  async changeLinkStateToAccount(
    @Arg('data') data: LinkDebitCardsInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.customerService.changeLinkStateToAccount(context, data);
  }

  @Mutation(() => updateDeliveryMechanismResp)
  async updateDeliveryMechanism(
    @Ctx() context: MyExpressContext,
    @Arg('data') data: updateDeliveryMechanismInput
  ): Promise<updateDeliveryMechanismResp> {
    return this.customerService.updateDeliveryMechanism(context, data);
  }
}

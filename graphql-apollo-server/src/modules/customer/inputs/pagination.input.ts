import { InputType, Field, Int } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class NotificationPaginationInput {
  @IsNotEmpty()
  @Field(() => Int)
  page: number;

  @IsNotEmpty()
  @Field(() => Int)
  limit: number;
}

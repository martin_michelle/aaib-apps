import { InputType, Field } from 'type-graphql';

@InputType()
export class reportIssueInput {
  @Field()
  category: string;

  @Field()
  comment: string;
}

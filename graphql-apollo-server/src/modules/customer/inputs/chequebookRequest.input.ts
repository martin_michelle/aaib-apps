import { InputType, Field, Float } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class ChequeBookRequestInput {
  @IsNotEmpty()
  @Field()
  AccId: string;

  @IsNotEmpty()
  @Field()
  DeliveryBranch: string;

  @IsNotEmpty()
  @Field(() => Float)
  NoofChequeBooks: number;

  @IsNotEmpty()
  @Field(() => Float)
  NumberOfLeaves: number;

  @IsNotEmpty()
  @Field()
  Currency: string;

  @IsNotEmpty()
  @Field()
  CountryCode: string;
}

import { IsOptional } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class BranchesInput {
  @IsOptional()
  @Field({ nullable: true })
  search: string;
}

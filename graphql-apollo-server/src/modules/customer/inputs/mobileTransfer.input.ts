import { InputType, Field } from 'type-graphql';
import { IsOptional } from 'class-validator';

@InputType()
export class MobileTransferInput {
  @Field()
  enabled: boolean;

  @IsOptional()
  @Field({ nullable: true })
  accountNumber?: string;
}

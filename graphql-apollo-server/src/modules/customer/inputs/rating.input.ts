import { InputType, Field } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class SubmitRatingInput {
  @IsNotEmpty()
  @Field()
  stars: number;

  @IsNotEmpty()
  @IsOptional()
  @Field({ nullable: true })
  comment?: string;
}

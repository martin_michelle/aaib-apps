import { InputType, Field } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class CustomerSettingsInput {
  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  showSoftTokenApp?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  showBalance?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  showMobileTransferPopup?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  pushNotifications?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  showSharePopup?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  showMobileAdd?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  defaultAccount?: string;

  @IsOptional()
  @Field({ nullable: true })
  pushId?: string;

  @IsOptional()
  @Field({ nullable: true })
  kycLastUpdatedDate?: string;

  @IsOptional()
  @Field({ nullable: true })
  registrationAlertUpdatedDate?: string;

  @IsOptional()
  @Field(() => [String], { nullable: true })
  tutorials?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  hasReadNotification?: boolean;

  @IsOptional()
  @Field({ nullable: true })
  lastNotificationId?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  showRatePopUp?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  lastRateDate?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  loginsInQuarterAfterRate?: number;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  deliveryMechanismFirstTime?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  showLinkingPopUp?: boolean;
}

@InputType()
export class FaqInput {
  @IsNotEmpty()
  @Field()
  type: string;
}

@InputType()
export class checkLimitInput {
  @IsOptional()
  @Field({ nullable: true })
  CountryCode?: string;

  @IsNotEmpty()
  @Field()
  TransferScope: string;

  @IsNotEmpty()
  @Field()
  Amount: number;

  @Field()
  Currency: string;
}

@InputType()
export class ModifyEmailInput {
  @IsNotEmpty()
  @Field()
  Email: string;

  @IsNotEmpty()
  @Field()
  otp: string;
}

@InputType()
export class LinkDebitCardsInput {
  @IsNotEmpty()
  @Field()
  CardToken: string;

  @IsNotEmpty()
  @Field()
  AccId: string;

  @IsNotEmpty()
  @Field()
  Action: string;
}

@InputType()
export class updateDeliveryMechanismInput {
  @IsNotEmpty()
  @Field()
  DeliveryMechanism: string;
}

import { InputType, Field, Int, Float } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class AtmsInput {
  @IsOptional()
  @Field(() => Float, { nullable: true })
  longitude: number;

  @IsOptional()
  @Field(() => Float, { nullable: true })
  latitude: number;

  @IsOptional()
  @Field(() => Int, { nullable: true })
  range: number;

  @IsOptional()
  @Field({ nullable: true })
  search: string;

  @IsNotEmpty()
  @Field()
  filter: string;
}

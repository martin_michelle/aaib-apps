import { Service } from 'typedi';
import fetch from 'node-fetch';

import { Accounts, AccountUsage } from './responses/account.response';
import { KONG_BASE_URL } from '../../lib/config';
import { Transaction } from './responses/transaction.response';
import { TransactionsInput, TransfersInput } from './inputs/transactions.input';
import { AccountDetails } from './responses/accountDetails.response';
import { formatApiResponse } from '../../lib/formatResponse';
import { MyExpressContext } from 'app-context';
import headersInit from '../../lib/headers';
import { CardDetails, DebitCardUsage } from './responses/debitCard.response';
import {
  InternalTransferInput,
  TransferInput,
  TransferByBeneficiaryInput,
  TransferAndAddBeneficiaryInput,
  MultipleTransfersInput
} from './inputs/transfer.input';
import {
  InternalTransfer,
  Transfer,
  TransfersHistory,
  MultipleTransfers,
  TransfersHistoryByDuration,
  TransferAndBeneficiary
} from './responses/transfer.response';
import { Loan, LoansFixed, PastDues, Repayment } from './responses/loan.response';
import { stringifyUrl } from 'query-string';
import { CurrencyData, CurrencyExchangeRates, conversinRate } from './responses/currency.response';
import { StatementInput } from './inputs/statement.input';
import { MailboxInput } from './inputs/mailbox.input';
import { OpenSubAccountInput } from './inputs/openSubAccount.input';
import { OpenSubAccount } from './responses/openSubAccount.response';
import { ReplaceDebitCardInput } from './inputs/replaceCard.input';
import { FxCalculatorInput } from './inputs/fxCalculator.input';
import { FxCalculator } from './responses/fxCalculator.response';
import { ScheduleAccountTransfer } from './inputs/scheduleTransfer.input';
import { ListScheduledAccountTransfers } from './responses/listScheduledAccountTransfers.response';
import { DownloadStatement } from './responses/statement.response';
import { GenerateTransferReceiptInput } from './inputs/generateTransferReceipt.input';
import { AccountDetailsInput } from './inputs/accounts.input';
import { LoanInput } from './inputs/loans.input';
import { mailboxCategoryData } from './responses/mailboxCategory.response';
import { logger } from '../../lib/logger';

@Service()
export class AccountsService {
  async accounts(context: MyExpressContext): Promise<Accounts> {
    const accountsResponse = await fetch(`${KONG_BASE_URL}/accounts/`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(accountsResponse);
  }

  async transactions(context: MyExpressContext, data: TransactionsInput): Promise<Transaction[]> {
    const { AccID, fromDate, toDate, limit, lastTransaction, search, country } = data;
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/accounts/transactions/${AccID}`,
        query: { fromDate, toDate, limit, lastTransaction, search, country }
      },
      { skipNull: true, skipEmptyString: true }
    );
    const transactionsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(transactionsResponse);
    return res.transactions || [];
  }

  async accountDetails(
    context: MyExpressContext,
    data: AccountDetailsInput
  ): Promise<AccountDetails> {
    const { AccID, country } = data;

    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/accounts/details/${AccID}`,
        query: { country }
      },
      { skipNull: true, skipEmptyString: true }
    );

    const accountDetailsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(accountDetailsResponse);
  }

  async debitCardDetails(context: MyExpressContext, accid: string): Promise<CardDetails[]> {
    const debitCardResponse = await fetch(`${KONG_BASE_URL}/debit/account/${accid}`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(debitCardResponse);
  }

  async ownAccountTransfer(context: MyExpressContext, data: TransferInput): Promise<Transfer> {
    const ownTransferResponse = await fetch(`${KONG_BASE_URL}/accounts/transactions/acc2acc`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(ownTransferResponse);
  }
  async cardDetails(context: MyExpressContext, cardNumber: string): Promise<CardDetails> {
    const body = { cardNumber };
    const cardDetailsResponse = await fetch(`${KONG_BASE_URL}/debit/card`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: headersInit(context)
    });

    return formatApiResponse(cardDetailsResponse);
  }

  async debitCardUsage(context: MyExpressContext, cardToken: string): Promise<DebitCardUsage> {
    const body = { cardToken };
    const cardDetailsResponse = await fetch(`${KONG_BASE_URL}/debit/usage`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: headersInit(context)
    });

    return formatApiResponse(cardDetailsResponse);
  }

  async internalTransfer(
    context: MyExpressContext,
    data: InternalTransferInput
  ): Promise<InternalTransfer> {
    const internalTransferResponse = await fetch(`${KONG_BASE_URL}/accounts/internal/acc2acc`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(internalTransferResponse);
  }

  async transferByBeneficiaryId(
    context: MyExpressContext,
    id: string,
    data: TransferByBeneficiaryInput
  ): Promise<InternalTransfer> {
    const internalTransferResponse = await fetch(
      `${KONG_BASE_URL}/accounts/internal/acc2acc/${id}`,
      {
        method: 'POST',
        body: JSON.stringify(data),
        headers: headersInit(context)
      }
    );

    return formatApiResponse(internalTransferResponse);
  }

  async multipleTransfers(
    context: MyExpressContext,
    data: MultipleTransfersInput
  ): Promise<MultipleTransfers> {
    const multipleTransfersResponse = await fetch(`${KONG_BASE_URL}/accounts/multiple/acc2acc`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });
    return formatApiResponse(multipleTransfersResponse);
  }

  async loans(context: MyExpressContext): Promise<[Loan]> {
    const loansResponse = await fetch(`${KONG_BASE_URL}/accounts/loans`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(loansResponse);
    return res.loans;
  }

  async loansFixed(context: MyExpressContext): Promise<LoansFixed> {
    const loansResponse = await fetch(`${KONG_BASE_URL}/accounts/loans`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(loansResponse);
    return res;
  }

  async loanPastDues(context: MyExpressContext, data: LoanInput): Promise<PastDues> {
    const { loanId, country } = data;

    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/accounts/loan/${loanId}`,
        query: { country }
      },
      { skipNull: true, skipEmptyString: true }
    );

    const loanPastDuesResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(loanPastDuesResponse);
  }

  async canReplaceDebitCard(context: MyExpressContext, cardNumber: string): Promise<string> {
    const request = await fetch(`${KONG_BASE_URL}/debit/canReplaceCard`, {
      method: 'POST',
      body: JSON.stringify({ cardNumber }),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(request);
    return res.message;
  }

  async loanRepayment(context: MyExpressContext, data: LoanInput): Promise<Repayment[]> {
    const { loanId, country } = data;

    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/accounts/repayment/${loanId}`,
        query: { country }
      },
      { skipNull: true, skipEmptyString: true }
    );

    const loanRepaymentResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(loanRepaymentResponse);
  }

  async debitCardTransactions(
    context: MyExpressContext,
    data: TransactionsInput
  ): Promise<Transaction[]> {
    const { AccID, fromDate, toDate, limit, lastTransaction, search, country } = data;
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/debit/transactions/${AccID}`,
        query: { fromDate, toDate, limit, lastTransaction, search, country }
      },
      { skipNull: true, skipEmptyString: true }
    );

    const transactionsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(transactionsResponse);
    return res.transactions || [];
  }

  async transferAndAddBeneficiary(
    context: MyExpressContext,
    data: TransferAndAddBeneficiaryInput
  ): Promise<TransferAndBeneficiary> {
    const internalTransferResponse = await fetch(`${KONG_BASE_URL}/accounts/internal/acc2acc`, {
      method: 'POST',
      body: JSON.stringify(data.transfer),
      headers: headersInit(context)
    });

    const transfer = await formatApiResponse(internalTransferResponse);

    try {
      const beneficiaryResponse = await fetch(`${KONG_BASE_URL}/beneficiary`, {
        method: 'POST',
        body: JSON.stringify(data.beneficiary),
        headers: headersInit(context)
      });
      const beneficiary = await formatApiResponse(beneficiaryResponse);
      return { ...transfer, beneficiaryId: beneficiary.id, beneficiaryDuplicated: false };
    } catch (error) {
      logger.error(error);
      return { ...transfer, beneficiaryId: false, beneficiaryDuplicated: true };
    }
  }

  async currencyExchangeRates(
    context: MyExpressContext,
    currency?: string
  ): Promise<CurrencyExchangeRates[]> {
    const url = new URL(`${KONG_BASE_URL}/accounts/currency`);
    if (currency) url.searchParams.append('currency', currency);
    const exchangeRates = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(exchangeRates);
  }

  async currencyConvertionRate(
    context: MyExpressContext,
    currency: string
  ): Promise<conversinRate> {
    const url = new URL(`${KONG_BASE_URL}/accounts/currencyConversionRate`);
    url.searchParams.append('currency', currency);
    const investments = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(investments);
  }

  async loanRequest(context: MyExpressContext): Promise<string> {
    const request = await fetch(`${KONG_BASE_URL}/accounts/request/loan`, {
      method: 'POST',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(request);

    return res.message;
  }

  async cdRequest(context: MyExpressContext): Promise<string> {
    const request = await fetch(`${KONG_BASE_URL}/accounts/request/cd`, {
      method: 'POST',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(request);

    return res.message;
  }

  async accountUsage(context: MyExpressContext, AccID: string): Promise<AccountUsage> {
    const accountUsage = await fetch(`${KONG_BASE_URL}/accounts/usage/${AccID}`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(accountUsage);
  }

  async debitCardRequest(context: MyExpressContext, accountNumber: string): Promise<string> {
    const body = { accountNumber };
    const request = await fetch(`${KONG_BASE_URL}/debit/request`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(request);

    return res.message;
  }

  async sendStatement(context: MyExpressContext, data: StatementInput): Promise<String> {
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/accounts/statement`, query: { ...data } },
      { skipNull: true, skipEmptyString: true }
    );

    const statementsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    const res = await formatApiResponse(statementsResponse);

    return res.message;
  }
  async downloadStatement(
    context: MyExpressContext,
    data: StatementInput
  ): Promise<DownloadStatement> {
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/accounts/statement`, query: { ...data } },
      { skipNull: true, skipEmptyString: true }
    );

    const statementsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(statementsResponse);
  }

  async generateTransferReceipt(
    context: MyExpressContext,
    data: GenerateTransferReceiptInput
  ): Promise<DownloadStatement> {
    const receiptResponse = await fetch(`${KONG_BASE_URL}/accounts/generateTransferReceipt`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(receiptResponse);
  }

  async subAccountCurrencies(context: MyExpressContext): Promise<CurrencyData[]> {
    const currenciesResponse = await fetch(`${KONG_BASE_URL}/accounts/subAcc/currency`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(currenciesResponse);
  }

  async sendToMailbox(context: MyExpressContext, data: MailboxInput): Promise<string> {
    const mailboxResponse = await fetch(`${KONG_BASE_URL}/accounts/mailbox`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(mailboxResponse);

    return res.message;
  }

  async mailboxCategory(context: MyExpressContext): Promise<mailboxCategoryData> {
    const mailboxCategoryResponse = await fetch(`${KONG_BASE_URL}/accounts//mailboxCategory`, {
      method: 'GET',

      headers: headersInit(context)
    });

    return formatApiResponse(mailboxCategoryResponse);
  }

  async openSubAccount(
    context: MyExpressContext,
    data: OpenSubAccountInput
  ): Promise<OpenSubAccount> {
    const openSubAccountResponse = await fetch(`${KONG_BASE_URL}/accounts/open/sub`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(openSubAccountResponse);
  }

  async replaceDebitCard(context: MyExpressContext, data: ReplaceDebitCardInput): Promise<string> {
    const request = await fetch(`${KONG_BASE_URL}/debit/replace`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(request);
    return res.message;
  }

  async FxCalculator(context: MyExpressContext, data: FxCalculatorInput): Promise<FxCalculator> {
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/accounts/currency/rates/fxcalculator`, query: { ...data } },
      { skipNull: true, skipEmptyString: true }
    );

    const statementsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    return await formatApiResponse(statementsResponse);
  }

  async scheduleAccountTransfer(
    context: MyExpressContext,
    data: ScheduleAccountTransfer
  ): Promise<string> {
    const scheduleResponse = await fetch(`${KONG_BASE_URL}/accounts/schedule/transfer`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });
    const res = await formatApiResponse(scheduleResponse);
    return res.message;
  }

  async updateTransferSchedule(
    context: MyExpressContext,
    data: ScheduleAccountTransfer,
    scheduleId: string
  ): Promise<string> {
    const scheduleResponse = await fetch(
      `${KONG_BASE_URL}/accounts/schedule/transfer/${scheduleId}`,
      {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: headersInit(context)
      }
    );

    const res = await formatApiResponse(scheduleResponse);
    return res.message;
  }

  async listScheduledAccountTransfers(
    context: MyExpressContext
  ): Promise<ListScheduledAccountTransfers[]> {
    const scheduledResponse = await fetch(`${KONG_BASE_URL}/accounts/schedule/transfer/list`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(scheduledResponse);
  }

  async deleteTransferSchedule(context: MyExpressContext, scheduleId: string): Promise<string> {
    const deleteScheduleResponse = await fetch(
      `${KONG_BASE_URL}/accounts/schedule/transfer/${scheduleId}`,
      {
        method: 'DELETE',
        headers: headersInit(context)
      }
    );

    const res = await formatApiResponse(deleteScheduleResponse);
    return res.message;
  }

  async termsAndConditionsDebitCard(context: MyExpressContext): Promise<String> {
    const termsAndConditions = await fetch(`${KONG_BASE_URL}/debit/termsAndConditions`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(termsAndConditions);
    return res.result;
  }

  async userTransfersHistory(
    context: MyExpressContext,
    count: number
  ): Promise<TransfersHistory[]> {
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/accounts/customer/payments`, query: { count } },
      { skipNull: true, skipEmptyString: true }
    );
    const statementsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(statementsResponse);
  }

  async userTransfersHistoryByDuration(
    context: MyExpressContext,
    data: TransfersInput
  ): Promise<TransfersHistoryByDuration[]> {
    const url = `${KONG_BASE_URL}/accounts/customer/payments/${data.fromDate}/${data.toDate}`;
    const transfersResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(transfersResponse);
  }

  async userTransfersHistoryByType(
    context: MyExpressContext,
    count: number,
    type: string
  ): Promise<TransfersHistory[]> {
    const url = stringifyUrl(
      { url: `${KONG_BASE_URL}/accounts/customer/payments/type`, query: { count, type } },
      { skipNull: true, skipEmptyString: true }
    );
    const statementsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(statementsResponse);
  }

  async userTransfersHistoryByBeneficiary(
    context: MyExpressContext,
    benificiaryAccount: string,
    count: number
  ): Promise<TransfersHistory[]> {
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/accounts/customer/payments/beneficiary`,
        query: { benificiaryAccount, count }
      },
      { skipNull: true, skipEmptyString: true }
    );
    const statementsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return formatApiResponse(statementsResponse);
  }

  async requestPayrollAdvance(context: MyExpressContext): Promise<string> {
    const url = new URL(`${KONG_BASE_URL}/accounts/requestPayrollAdvance`);

    const authorize = await fetch(url, {
      method: 'POST',
      headers: headersInit(context)
    });
    const res = await formatApiResponse(authorize);
    return res.message;
  }
  async canRequestPayroll(context: MyExpressContext): Promise<boolean> {
    const url = new URL(`${KONG_BASE_URL}/accounts/canRequestPayroll`);

    const authorize = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });
    return await formatApiResponse(authorize);
  }

  async canRequestUAEAccount(context: MyExpressContext): Promise<string> {
    const url = new URL(`${KONG_BASE_URL}/accounts/canRequestUAEAccount`);

    const authorize = await fetch(url, {
      method: 'POST',
      headers: headersInit(context)
    });
    const res = await formatApiResponse(authorize);
    return res.message;
  }
}

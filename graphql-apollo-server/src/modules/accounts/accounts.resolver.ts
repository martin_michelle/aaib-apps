import { Query, Resolver, Ctx, Arg, FieldResolver, Root, Mutation } from 'type-graphql';
import { Inject, Service } from 'typedi';
import { AccountsService } from './accounts.service';
import { Accounts, AccountUsage } from './responses/account.response';
import { MyExpressContext } from 'app-context';
import { Transaction } from './responses/transaction.response';
import { TransactionsInput, TransfersInput } from './inputs/transactions.input';
import { AccountDetails } from './responses/accountDetails.response';
import { CardDetails, DebitCardUsage } from './responses/debitCard.response';
import {
  Transfer,
  InternalTransfer,
  TransfersHistory,
  MultipleTransfers,
  TransfersHistoryByDuration,
  TransferAndBeneficiary
} from './responses/transfer.response';
import {
  InternalTransferInput,
  TransferAndAddBeneficiaryInput,
  TransferByBeneficiaryInput,
  TransferInput,
  MultipleTransfersInput
} from './inputs/transfer.input';
import { Loan, LoansFixed, PastDues, Repayment } from './responses/loan.response';
import { CurrencyData, CurrencyExchangeRates, conversinRate } from './responses/currency.response';
import { StatementInput } from './inputs/statement.input';
import { MailboxInput } from './inputs/mailbox.input';
import { OpenSubAccountInput } from './inputs/openSubAccount.input';
import { OpenSubAccount } from './responses/openSubAccount.response';
import { ReplaceDebitCardInput } from './inputs/replaceCard.input';
import { FxCalculator } from './responses/fxCalculator.response';
import { FxCalculatorInput } from './inputs/fxCalculator.input';
import { ScheduleAccountTransfer } from './inputs/scheduleTransfer.input';
import { ListScheduledAccountTransfers } from './responses/listScheduledAccountTransfers.response';
import { DownloadStatement } from './responses/statement.response';
import { GenerateTransferReceiptInput } from './inputs/generateTransferReceipt.input';
import { mailboxCategoryData } from './responses/mailboxCategory.response';

@Service()
@Resolver(() => AccountDetails)
export default class AccountsResolver {
  @Inject(() => AccountsService)
  accountsService: AccountsService;

  @Query(() => Accounts)
  async customerAccounts(@Ctx() context: MyExpressContext): Promise<Accounts> {
    return this.accountsService.accounts(context);
  }

  @Query(() => [Transaction])
  async transactions(
    @Arg('data') data: TransactionsInput,
    @Ctx() context: MyExpressContext
  ): Promise<Transaction[]> {
    return this.accountsService.transactions(context, data);
  }

  @Query(() => AccountDetails)
  async accountDetails(
    @Arg('AccID') AccID: string,
    @Ctx() context: MyExpressContext,
    @Arg('country', { nullable: true }) country?: string
  ): Promise<AccountDetails> {
    return this.accountsService.accountDetails(context, {
      AccID,
      country
    });
  }

  @FieldResolver(() => AccountDetails)
  debitCards(
    @Root() accountDetails: AccountDetails,
    @Ctx() context: MyExpressContext
  ): Promise<CardDetails[]> {
    return this.accountsService.debitCardDetails(context, accountDetails.currentAccount.number);
  }

  @Mutation(() => Transfer)
  async ownAccountTransfer(
    @Arg('data') data: TransferInput,
    @Ctx() context: MyExpressContext
  ): Promise<Transfer> {
    return this.accountsService.ownAccountTransfer(context, data);
  }

  @Mutation(() => CardDetails)
  async cardDetails(
    @Arg('cardNumber') cardNumber: string,
    @Ctx() context: MyExpressContext
  ): Promise<CardDetails> {
    return this.accountsService.cardDetails(context, cardNumber);
  }

  @Mutation(() => DebitCardUsage)
  async debitCardUsage(
    @Arg('cardToken') cardToken: string,
    @Ctx() context: MyExpressContext
  ): Promise<DebitCardUsage> {
    return this.accountsService.debitCardUsage(context, cardToken);
  }

  @Mutation(() => InternalTransfer)
  async internalTransfer(
    @Arg('data') data: InternalTransferInput,
    @Ctx() context: MyExpressContext
  ): Promise<InternalTransfer> {
    return this.accountsService.internalTransfer(context, data);
  }

  @Mutation(() => InternalTransfer)
  async transferByBeneficiaryId(
    @Arg('id') id: string,
    @Arg('data') data: TransferByBeneficiaryInput,
    @Ctx() context: MyExpressContext
  ): Promise<InternalTransfer> {
    return this.accountsService.transferByBeneficiaryId(context, id, data);
  }

  @Mutation(() => MultipleTransfers)
  async multipleTransfers(
    @Arg('data') data: MultipleTransfersInput,
    @Ctx() context: MyExpressContext
  ): Promise<MultipleTransfers> {
    return this.accountsService.multipleTransfers(context, data);
  }

  @Mutation(() => TransferAndBeneficiary)
  async transferAndAddBeneficiary(
    @Arg('data') data: TransferAndAddBeneficiaryInput,
    @Ctx() context: MyExpressContext
  ): Promise<TransferAndBeneficiary> {
    return this.accountsService.transferAndAddBeneficiary(context, data);
  }

  @Query(() => [Loan])
  async loans(@Ctx() context: MyExpressContext): Promise<[Loan]> {
    return this.accountsService.loans(context);
  }

  @Query(() => LoansFixed)
  async loansFixed(@Ctx() context: MyExpressContext): Promise<LoansFixed> {
    return this.accountsService.loansFixed(context);
  }

  @Query(() => PastDues)
  async pastDues(
    @Arg('loanId') loanId: string,
    @Ctx() context: MyExpressContext,
    @Arg('country', { nullable: true }) country?: string
  ): Promise<PastDues> {
    return this.accountsService.loanPastDues(context, {
      loanId,
      country
    });
  }

  @Query(() => [Repayment])
  async loanRepayment(
    @Arg('loanId') loanId: string,
    @Ctx() context: MyExpressContext,
    @Arg('country', { nullable: true }) country?: string
  ): Promise<Repayment[]> {
    return this.accountsService.loanRepayment(context, {
      loanId,
      country
    });
  }

  @Query(() => [Transaction])
  async debitCardTransactions(
    @Arg('data') data: TransactionsInput,
    @Ctx() context: MyExpressContext
  ): Promise<Transaction[]> {
    return this.accountsService.debitCardTransactions(context, data);
  }

  @Query(() => [CurrencyExchangeRates])
  async currencyExchangeRates(
    @Ctx() context: MyExpressContext,
    @Arg('currency', { nullable: true }) currency?: string
  ): Promise<CurrencyExchangeRates[]> {
    return this.accountsService.currencyExchangeRates(context, currency);
  }

  @Query(() => conversinRate)
  async currencyConvertionRate(
    @Arg('currency') currency: string,
    @Ctx() context: MyExpressContext
  ): Promise<conversinRate> {
    return this.accountsService.currencyConvertionRate(context, currency);
  }

  @Mutation(() => String)
  async loanRequest(@Ctx() context: MyExpressContext): Promise<string> {
    return this.accountsService.loanRequest(context);
  }

  @Mutation(() => String)
  async cdRequest(@Ctx() context: MyExpressContext): Promise<string> {
    return this.accountsService.cdRequest(context);
  }

  @Query(() => AccountUsage)
  async accountUsage(
    @Arg('AccID') AccID: string,
    @Ctx() context: MyExpressContext
  ): Promise<AccountUsage> {
    return this.accountsService.accountUsage(context, AccID);
  }

  @Mutation(() => String)
  async debitCardRequest(
    @Arg('accountNumber') accountNumber: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.accountsService.debitCardRequest(context, accountNumber);
  }

  @Query(() => String)
  async sendStatement(
    @Arg('data') data: StatementInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.accountsService.sendStatement(context, data);
  }

  @Query(() => DownloadStatement)
  async downloadStatement(
    @Arg('data') data: StatementInput,
    @Ctx() context: MyExpressContext
  ): Promise<DownloadStatement> {
    return this.accountsService.downloadStatement(context, data);
  }

  @Query(() => DownloadStatement)
  async generateTransferReceipt(
    @Arg('data') data: GenerateTransferReceiptInput,
    @Ctx() context: MyExpressContext
  ): Promise<DownloadStatement> {
    return this.accountsService.generateTransferReceipt(context, data);
  }

  @Query(() => [CurrencyData])
  async subAccountCurrencies(@Ctx() context: MyExpressContext): Promise<CurrencyData[]> {
    return this.accountsService.subAccountCurrencies(context);
  }

  @Query(() => mailboxCategoryData)
  async mailboxCategory(@Ctx() context: MyExpressContext): Promise<mailboxCategoryData> {
    return this.accountsService.mailboxCategory(context);
  }

  @Mutation(() => String)
  async sendToMailbox(
    @Arg('data') data: MailboxInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.accountsService.sendToMailbox(context, data);
  }

  @Mutation(() => OpenSubAccount)
  async openSubAccount(
    @Arg('data') data: OpenSubAccountInput,
    @Ctx() context: MyExpressContext
  ): Promise<OpenSubAccount> {
    return this.accountsService.openSubAccount(context, data);
  }

  @Mutation(() => String)
  async replaceDebitCard(
    @Arg('data') data: ReplaceDebitCardInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.accountsService.replaceDebitCard(context, data);
  }

  @Mutation(() => String)
  async canReplaceDebitCard(
    @Arg('cardNumber') cardNumber: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.accountsService.canReplaceDebitCard(context, cardNumber);
  }

  @Query(() => FxCalculator)
  async fxCalculator(
    @Arg('Data') data: FxCalculatorInput,
    @Ctx() context: MyExpressContext
  ): Promise<FxCalculator> {
    return this.accountsService.FxCalculator(context, data);
  }

  @Mutation(() => String)
  async scheduleAccountTransfer(
    @Arg('data') data: ScheduleAccountTransfer,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.accountsService.scheduleAccountTransfer(context, data);
  }

  @Mutation(() => String)
  async deleteTransferSchedule(
    @Arg('scheduleId') scheduleId: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.accountsService.deleteTransferSchedule(context, scheduleId);
  }

  @Mutation(() => String)
  async updateTransferSchedule(
    @Arg('scheduleId') scheduleId: string,
    @Arg('data') data: ScheduleAccountTransfer,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.accountsService.updateTransferSchedule(context, data, scheduleId);
  }

  @Query(() => [ListScheduledAccountTransfers])
  async listScheduledAccountTransfers(
    @Ctx() context: MyExpressContext
  ): Promise<ListScheduledAccountTransfers[]> {
    return this.accountsService.listScheduledAccountTransfers(context);
  }

  @Query(() => String)
  async termsAndConditionsDebitCard(@Ctx() context: MyExpressContext): Promise<String> {
    return this.accountsService.termsAndConditionsDebitCard(context);
  }

  @Query(() => [TransfersHistory])
  async userTransfersHistory(
    @Arg('count') count: number,
    @Ctx() context: MyExpressContext
  ): Promise<TransfersHistory[]> {
    return this.accountsService.userTransfersHistory(context, count);
  }

  @Query(() => [TransfersHistoryByDuration])
  async userTransfersHistoryByDuration(
    @Arg('data') data: TransfersInput,
    @Ctx() context: MyExpressContext
  ): Promise<TransfersHistoryByDuration[]> {
    return this.accountsService.userTransfersHistoryByDuration(context, data);
  }

  @Query(() => [TransfersHistory])
  async userTransfersHistoryByType(
    @Arg('count') count: number,
    @Arg('type') type: string,
    @Ctx() context: MyExpressContext
  ): Promise<TransfersHistory[]> {
    return this.accountsService.userTransfersHistoryByType(context, count, type);
  }

  @Query(() => [TransfersHistory])
  async userTransfersHistoryByBeneficiary(
    @Arg('benificiaryAccount') benificiaryAccount: string,
    @Arg('count') count: number,
    @Ctx() context: MyExpressContext
  ): Promise<TransfersHistory[]> {
    return this.accountsService.userTransfersHistoryByBeneficiary(
      context,
      benificiaryAccount,
      count
    );
  }

  @Mutation(() => String)
  async requestPayrollAdvance(@Ctx() context: MyExpressContext): Promise<string> {
    return this.accountsService.requestPayrollAdvance(context);
  }

  @Query(() => Boolean)
  async canRequestPayroll(@Ctx() context: MyExpressContext): Promise<boolean> {
    return this.accountsService.canRequestPayroll(context);
  }

  @Mutation(() => String)
  async canRequestUAEAccount(@Ctx() context: MyExpressContext): Promise<string> {
    return this.accountsService.canRequestUAEAccount(context);
  }
}

import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class LoanInput {
  @IsNotEmpty()
  @Field()
  loanId: string;

  @Field({ nullable: true })
  country?: string;
}

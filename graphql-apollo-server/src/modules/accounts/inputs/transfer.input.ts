import { InputType, Field, Float } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class TransferInput {
  @IsNotEmpty()
  @Field()
  debitAcc: string;

  @IsNotEmpty()
  @Field()
  creditAcc: string;

  @IsNotEmpty()
  @Field(() => Float)
  amount: number;

  @Field({ nullable: true })
  description?: string;
}

@InputType()
export class InternalTransferInput {
  @IsNotEmpty()
  @Field()
  debitAcc: string;

  @IsNotEmpty()
  @Field()
  creditAcc: string;

  @IsNotEmpty()
  @Field(() => Float)
  amount: number;

  @IsNotEmpty()
  @Field()
  currency: string;

  @IsNotEmpty()
  @Field()
  description: string;

  @IsNotEmpty()
  @Field()
  otp: string;

  @IsNotEmpty()
  @Field()
  beneficiaryName: string;

  @IsNotEmpty()
  @Field()
  swift: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  chargeType?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  isMobileNumber?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  country?: string;
}

@InputType()
export class TransferByBeneficiaryInput {
  @IsNotEmpty()
  @Field()
  debitAcc: string;

  @IsNotEmpty()
  @Field(() => Float)
  amount: number;

  @IsNotEmpty()
  @Field()
  currency: string;

  @IsNotEmpty()
  @Field()
  description: string;

  @IsNotEmpty()
  @Field()
  otp: string;
}

@InputType()
class BeneficiaryWithoutOTP {
  @IsNotEmpty()
  @Field()
  name: string;

  @IsNotEmpty()
  @Field({ nullable: true })
  nickName?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  iban?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  accountNumber?: string;

  @IsNotEmpty()
  @Field()
  country: string;

  @IsNotEmpty()
  @Field()
  bank: string;

  @IsNotEmpty()
  @Field()
  swift: string;

  @IsNotEmpty()
  @Field()
  currency: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  address?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  isCreditCardNumber?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  isMobileNumber?: boolean;
}

@InputType()
export class TransferAndAddBeneficiaryInput {
  @IsNotEmpty()
  @Field()
  transfer: InternalTransferInput;

  @IsNotEmpty()
  @Field()
  beneficiary: BeneficiaryWithoutOTP;
}
@InputType()
export class MultipleTransferData {
  @IsNotEmpty()
  @Field(() => Float)
  amount: number;

  @IsNotEmpty()
  @Field()
  debitAcc: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  chargeType?: string;

  @Field({ nullable: true })
  description?: string;

  @IsNotEmpty()
  @Field()
  currency: string;
}

@InputType()
export class MultipleTransfersArray {
  @IsNotEmpty()
  @Field()
  id: string;

  @IsNotEmpty()
  @Field(() => MultipleTransferData)
  accountData: MultipleTransferData;
}

@InputType()
export class MultipleTransfersInput {
  @IsNotEmpty()
  @Field(() => [MultipleTransfersArray])
  transfers: MultipleTransfersArray[];

  @IsNotEmpty()
  @Field()
  otp: string;
}

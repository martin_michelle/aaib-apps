import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class StatementInput {
  @IsNotEmpty()
  @Field()
  account: string;

  @IsNotEmpty()
  @Field()
  type: string;

  @Field({ nullable: true })
  fromDate?: string;

  @Field({ nullable: true })
  toDate?: string;
}

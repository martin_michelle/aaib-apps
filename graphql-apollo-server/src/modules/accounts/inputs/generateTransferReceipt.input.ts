import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class GenerateTransferReceiptInput {
  @IsNotEmpty()
  @Field()
  transactionDate: string;
  @IsNotEmpty()
  @Field()
  transactionId: string;
  @IsNotEmpty()
  @Field()
  accountName: string;
  @IsNotEmpty()
  @Field()
  accountNumber: string;
  @IsNotEmpty()
  @Field()
  bank: string;
  @IsNotEmpty()
  @Field()
  fromAccount: string;
  @IsNotEmpty()
  @Field()
  transferCurrency: string;
  @IsNotEmpty()
  @Field()
  transferAmount: string;
  @IsNotEmpty()
  @Field()
  transferReason: string;
}

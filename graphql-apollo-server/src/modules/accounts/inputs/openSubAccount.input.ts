import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class OpenSubAccountInput {
  @IsNotEmpty()
  @Field()
  Currency: string;

  @IsNotEmpty()
  @Field()
  Type: string;

  @Field({ nullable: true })
  SubType: string;

  @Field({ nullable: true })
  Frequency: string;
}

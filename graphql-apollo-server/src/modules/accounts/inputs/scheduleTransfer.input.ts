import { IsNotEmpty, IsOptional } from 'class-validator';
import { Field, InputType, Float } from 'type-graphql';

@InputType()
export class ScheduleAccountTransfer {
  @IsNotEmpty()
  @Field()
  debitAcc: string;

  @IsNotEmpty()
  @Field(() => Float)
  amount: number;

  @IsNotEmpty()
  @Field()
  description: string;

  @IsOptional()
  @Field({ nullable: true })
  currency?: string;

  @IsOptional()
  @Field({ nullable: true })
  chargeType?: string;

  @IsNotEmpty()
  @Field()
  beneficiaryID: string;

  @IsNotEmpty()
  @Field()
  scheduleDate: string;

  @IsNotEmpty()
  @Field()
  transferFrequency: string;

  @IsOptional()
  @Field(() => Float)
  totalTransferCount?: number;

  @IsNotEmpty()
  @Field()
  otp: string;
}

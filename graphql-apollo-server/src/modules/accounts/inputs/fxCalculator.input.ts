import { IsNotEmpty } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class FxCalculatorInput {
  @IsNotEmpty()
  @Field()
  srcCur: string;

  @IsNotEmpty()
  @Field()
  dstCur: string;

  @Field()
  amount: number;
}

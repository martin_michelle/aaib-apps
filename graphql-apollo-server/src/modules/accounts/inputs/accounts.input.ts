import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class AccountDetailsInput {
  @IsNotEmpty()
  @Field()
  AccID: string;

  @Field({ nullable: true })
  country?: string;
}

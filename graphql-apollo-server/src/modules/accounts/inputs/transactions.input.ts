import { InputType, Field, Int } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class TransactionsInput {
  @IsNotEmpty()
  @Field()
  AccID: string;

  @Field({ nullable: true })
  fromDate?: string;

  @Field({ nullable: true })
  toDate?: string;

  @Field(() => Int, { nullable: true })
  limit?: number;

  @Field({ nullable: true })
  lastTransaction?: string;

  @Field({ nullable: true })
  search?: string;

  @Field({ nullable: true })
  country?: string;
}

@InputType()
export class TransfersInput {
  @IsNotEmpty()
  @Field()
  fromDate: string;

  @IsNotEmpty()
  @Field()
  toDate: string;
}

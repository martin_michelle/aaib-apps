import { IsNotEmpty, IsOptional } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class ReplaceDebitCardInput {
  @IsNotEmpty()
  @Field()
  cardNumber: string;

  @IsNotEmpty()
  @Field()
  deliveryMethod: string;

  @IsOptional()
  @Field()
  pickupBranch?: string;
}

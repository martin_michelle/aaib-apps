import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class MailboxInput {
  @IsNotEmpty()
  @Field()
  message: string;

  @IsNotEmpty()
  @Field()
  category: string;
}

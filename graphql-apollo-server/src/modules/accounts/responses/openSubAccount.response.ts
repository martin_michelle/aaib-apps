import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class OpenSubAccount {
  @Field()
  AccID: string;

  @Field()
  Category: string;

  @Field()
  IBAN: string;

  @Field()
  AvailableBalance: string;

  @Field()
  Currency: string;

  @Field()
  Status: string;
}

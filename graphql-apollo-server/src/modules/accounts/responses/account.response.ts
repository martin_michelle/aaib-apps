import { ObjectType, Field, Float, Int } from 'type-graphql';
import { CardDetails } from './debitCard.response';

@ObjectType()
export class Balance {
  @Field()
  currency: string;

  @Field(() => Float)
  amount: number;

  @Field(() => Float, { nullable: true })
  EGAmount: number;

  @Field(() => Float, { nullable: true })
  AEAmount: number;

  @Field(() => Int, { nullable: true })
  EGAccountsCount: number;

  @Field(() => Int, { nullable: true })
  AEAccountsCount: number;
}

@ObjectType()
export class Account {
  @Field()
  AccID: string;

  @Field()
  CustomerName: string;

  @Field()
  Category: string;

  @Field()
  IBAN: string;

  @Field({ nullable: true })
  OpenDate: string;

  @Field()
  Currency: string;

  @Field(() => Float)
  WorkingBalance: number;

  @Field(() => Float)
  AvailableBalance: number;

  @Field({ nullable: true })
  CreditCard: string;

  @Field()
  Status: string;

  @Field()
  country: string;

  @Field(() => [CardDetails])
  debitCards: CardDetails[];
}

@ObjectType()
export class Accounts {
  @Field()
  balance: Balance;

  @Field(() => [Account])
  accounts: Account[];

  @Field(() => [String])
  accountsCurrencies: string[];

  @Field(() => String, { nullable: true })
  notFetched: string;
}

@ObjectType()
export class AccountUsage {
  @Field(() => Int)
  Start: number;

  @Field(() => Int)
  End: number;

  @Field(() => Float)
  TotalCreditM1: number;

  @Field(() => Float)
  TotalDebitM1: number;

  @Field(() => Float)
  TotalCreditM2: number;

  @Field(() => Float)
  TotalDebitM2: number;

  @Field(() => Float)
  TotalCreditM3: number;

  @Field(() => Float)
  TotalDebitM3: number;

  @Field(() => Float)
  TotalCreditM4: number;

  @Field(() => Float)
  TotalDebitM4: number;

  @Field(() => Float)
  TotalCreditM5: number;

  @Field(() => Float)
  TotalDebitM5: number;

  @Field(() => Float)
  TotalCreditM6: number;

  @Field(() => Float)
  TotalDebitM6: number;
}

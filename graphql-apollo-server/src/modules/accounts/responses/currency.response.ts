import { ObjectType, Field, Float } from 'type-graphql';

@ObjectType()
export class CurrencyExchangeRates {
  @Field({ nullable: true })
  CurrencyCode?: string;

  @Field({ nullable: true })
  CurrencyNameEnglish?: string;

  @Field({ nullable: true })
  CurrencyNameArabic?: string;

  @Field({ nullable: true })
  CurrencyMultiplyDivider?: string;

  @Field(() => Float, { nullable: true })
  CashBuyRate?: number;

  @Field(() => Float, { nullable: true })
  CashSellRate?: number;

  @Field(() => Float, { nullable: true })
  TransferBuyRate?: number;

  @Field(() => Float, { nullable: true })
  TransferSellRate?: number;

  @Field({ nullable: true })
  LastUpdateDate?: string;

  @Field({ nullable: true })
  LastUpdateTime?: string;

  @Field({ nullable: true })
  Status?: string;
}

@ObjectType()
export class CurrencyData {
  @Field()
  ID: string;

  @Field()
  Code: string;

  @Field()
  EnglishName: string;

  @Field()
  ArabicName: string;

  @Field()
  Value: string;
}

@ObjectType()
export class conversinRate {
  @Field(() => Float)
  theRate: number;
}

import { ObjectType, Field, Float, UseMiddleware, MiddlewareFn } from 'type-graphql';

//temp fix for default value, should be removed later
function DefaultValue<T>(defaultValue: T): MiddlewareFn {
  return async (_, next) => {
    const original = await next();
    if (original === undefined || original === null) {
      return defaultValue;
    }
    return original;
  };
}

@ObjectType()
export class Transaction {
  @Field()
  AccID: string;

  @Field()
  TransRef: string;

  @Field()
  correlationId: string;

  @Field()
  Description: string;

  @Field()
  ValueDate: string;

  @Field()
  BookingDate: string;

  @Field()
  Timestamp: string;

  @Field()
  Currency: string;

  @Field(() => Float, { nullable: true })
  Balance?: number;

  @Field(() => Float, { nullable: true })
  Withdrawals?: number;

  @Field(() => Float, { nullable: true })
  Deposits?: number;

  @Field()
  @UseMiddleware(DefaultValue('Completed'))
  Status: string;
}

import { ObjectType, Field, Float, Int } from 'type-graphql';

@ObjectType()
export class LoansData {
  @Field()
  currency: string;

  @Field(() => Float)
  amount: number;

  @Field(() => Float, { nullable: true })
  EGAmount: number;

  @Field(() => Float, { nullable: true })
  AEAmount: number;

  @Field(() => Int, { nullable: true })
  EGLoansCount: number;

  @Field(() => Int, { nullable: true })
  AELoansCount: number;
}

@ObjectType()
export class LoansFixed {
  @Field(() => [Loan])
  loans: Loan[];

  @Field()
  loansData: LoansData;

  @Field(() => String, { nullable: true })
  notFetched: string;
}

@ObjectType()
export class Loan {
  @Field()
  ID: string;

  @Field()
  Company: string;

  @Field()
  Category: string;

  @Field()
  CategoryDescription: string;

  @Field({ nullable: true })
  LDType?: string;

  @Field({ nullable: true })
  LDTypeDescription?: string;

  @Field(() => Float)
  LDAmount: number;

  @Field(() => Float)
  Outstanding: number;

  @Field(() => Float)
  TotalPaid: number;

  @Field()
  Currency: string;

  @Field(() => Float)
  InterestRate: number;

  @Field(() => Int)
  AgreementDate: number;

  @Field(() => Int)
  ValueDate: number;

  @Field(() => Int)
  MaturityDate: number;

  @Field(() => Float)
  NextPaymentAmount: number;

  @Field(() => Int)
  NextPaymentDate: number;

  @Field(() => Float, { nullable: true })
  LastPaymentAmount?: number;

  @Field(() => Int, { nullable: true })
  LastPaymentDate?: number;

  @Field()
  AccountNumber: string;

  @Field({ nullable: true })
  PaymentStatus?: string;

  @Field(() => Int)
  DueDate: number;

  @Field({ nullable: true })
  Frequency?: string;

  @Field()
  Status: string;

  @Field(() => Int)
  NoDayOverdue: number;

  @Field(() => Float)
  Penalty: number;

  @Field(() => Float)
  TotalOverDueAmount: number;

  @Field({ nullable: true })
  country: string;
}
@ObjectType()
export class PastDues {
  @Field({ nullable: true })
  ID?: string;

  @Field({ nullable: true })
  Company?: string;

  @Field({ nullable: true })
  Currency?: string;

  @Field({ nullable: true })
  LoanType?: string;

  @Field({ nullable: true })
  LoanDescription?: string;

  @Field(() => Float, { nullable: true })
  TotalAmountOverDue?: number;

  @Field(() => [PastDuesDetails], { nullable: true })
  PastDues?: PastDuesDetails[];
}

@ObjectType()
export class PastDuesDetails {
  @Field(() => Int)
  DueDate: number;

  @Field(() => Int)
  OverDueDays: number;

  @Field(() => Float)
  OverDueAmount: number;

  @Field(() => Float)
  Principal: number;

  @Field(() => Float)
  Interest: number;

  @Field(() => Float)
  PenaltyInterest: number;

  @Field(() => Float)
  PenaltySpread: number;

  @Field(() => Float)
  Charges: number;

  @Field(() => Int)
  Bucket: number;
}

@ObjectType()
export class Repayment {
  @Field(() => Int)
  Date: number;

  @Field(() => Float)
  PrincipalAmount: number;

  @Field(() => Float)
  InstallmentAmount: number;

  @Field(() => Float)
  InterestAmount: number;

  @Field(() => Float)
  ChargeAmount: number;

  @Field(() => Float)
  OutstandingAmount: number;
}

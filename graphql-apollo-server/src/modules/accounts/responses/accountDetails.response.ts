import { ObjectType, Field } from 'type-graphql';
import { Balance } from './account.response';
import { CardDetails } from './debitCard.response';

@ObjectType()
class CurrentAccount {
  @Field()
  number: string;

  @Field()
  name: string;
}

@ObjectType()
class GeneralInformation {
  @Field()
  accountName: string;

  @Field({ nullable: true })
  openingDate?: string;

  @Field()
  accountNumber: string;

  @Field()
  iban: string;

  @Field()
  status: string;
}

@ObjectType()
class AccountPlan {
  @Field()
  annualInterest: string;
}

@ObjectType()
class BalanceDetails {
  @Field()
  availableBalance: Balance;

  @Field()
  currentBalance: Balance;

  @Field()
  holdAmount: Balance;
}

@ObjectType()
class StatementDetails {
  @Field({ nullable: true })
  statementType?: string;

  @Field({ nullable: true })
  statementFrequency?: string;
}

@ObjectType()
export class AccountDetails {
  @Field()
  currentAccount: CurrentAccount;

  @Field()
  generalInformation: GeneralInformation;

  @Field()
  accountPlan: AccountPlan;

  @Field()
  balanceDetails: BalanceDetails;

  @Field()
  statementDetails: StatementDetails;

  @Field(() => [CardDetails])
  debitCards: CardDetails[];
}

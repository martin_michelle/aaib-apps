import { ObjectType, Field, Float } from 'type-graphql';

@ObjectType()
export class Transfer {
  @Field()
  transactionId: string;

  @Field()
  currency: string;

  @Field(() => Float)
  creditBalance: number;

  @Field(() => Float)
  debitBalance: number;
}

@ObjectType()
export class InternalTransfer {
  @Field()
  transactionId: string;

  @Field()
  currency: string;

  @Field(() => Float)
  debitBalance: number;
}
@ObjectType()
export class TransfersHistory {
  @Field({ nullable: true })
  _id: string;

  @Field()
  ReferenceId: string;

  @Field()
  DebitAcc: string;

  @Field()
  CreditAcc: string;

  @Field(() => Float)
  ProcessingDate: number;

  @Field(() => Float)
  Amount: number;

  @Field({ nullable: true })
  CreditReference: string;

  @Field({ nullable: true })
  DebitReference: string;

  @Field({ nullable: true })
  CreditDesc: string;

  @Field({ nullable: true })
  DebitDesc: string;

  @Field({ nullable: true })
  BenificiaryAccount: string;

  @Field({ nullable: true })
  BenificiaryName: string;

  @Field({ nullable: true })
  SwiftCode: string;

  @Field({ nullable: true })
  ReasonForPayment: string;

  @Field()
  Cif: string;

  @Field()
  Channel: string;

  @Field()
  Currency: string;

  @Field()
  TargetCurrency: string;

  @Field()
  Type: string;

  @Field()
  CustomerType: string;

  @Field()
  Scope: string;

  @Field(() => Float)
  Rate: number;

  @Field()
  Status: boolean;

  @Field()
  Message: string;

  @Field()
  RecordStatus: string;

  @Field(() => Float)
  TargetAmount: number;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;
}

@ObjectType()
export class TransfersHistoryByDuration {
  @Field()
  _id: string;

  @Field()
  ReferenceId: string;

  @Field()
  DebitAcc: string;

  @Field()
  CreditAcc: string;

  @Field(() => Float)
  ProcessingDate: number;

  @Field(() => Float)
  Amount: number;

  @Field({ nullable: true })
  CreditReference: string;

  @Field({ nullable: true })
  DebitReference: string;

  @Field({ nullable: true })
  CreditDesc: string;

  @Field({ nullable: true })
  DebitDesc: string;

  @Field()
  Cif: string;

  @Field()
  Channel: string;

  @Field()
  Currency: string;

  @Field()
  TargetCurrency: string;

  @Field()
  Type: string;

  @Field()
  CustomerType: string;

  @Field()
  Scope: string;

  @Field(() => Float)
  Rate: number;

  @Field()
  Status: boolean;

  @Field()
  Message: string;

  @Field()
  RecordStatus: string;

  @Field(() => Float)
  TargetAmount: number;

  @Field({ nullable: true })
  BenificiaryAccount: string;

  @Field({ nullable: true })
  BenificiaryName: string;

  @Field({ nullable: true })
  SwiftCode: string;

  @Field({ nullable: true })
  ReasonForPayment: string;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;
}

@ObjectType()
export class MultipleTransfers {
  @Field(() => [SuccessData])
  success?: SuccessData[];

  @Field(() => [FailureData])
  failed?: FailureData[];
}

@ObjectType()
export class SuccessData {
  @Field(() => Float)
  debitBalance: number;

  @Field()
  currency: string;

  @Field()
  transactionId: string;

  @Field(() => Float)
  amount: number;

  @Field()
  benefID: string;
}

@ObjectType()
export class FailureData {
  @Field()
  benefID: string;
}

@ObjectType()
export class TransferAndBeneficiary {
  @Field()
  transactionId: string;

  @Field()
  currency: string;

  @Field(() => Float)
  debitBalance: number;

  @Field()
  beneficiaryId: string;

  @Field()
  beneficiaryDuplicated: boolean;
}

import { ObjectType, Field, Float } from 'type-graphql';

@ObjectType()
export class CardDetails {
  @Field()
  CardToken: string;

  @Field()
  CardNumber: string;

  @Field()
  NameOnCard: string;

  @Field()
  CardStatus: string;

  @Field()
  CardType: string;

  @Field(() => Float)
  CashLimit: number;

  @Field(() => Float)
  PurchaseLimit: number;

  @Field()
  OnlinePayment: boolean;

  @Field()
  ATMWithdrawal: boolean;

  @Field()
  PrimaryAccount: string;
}

@ObjectType()
export class DebitCardUsage {
  @Field()
  WithdrawalMonthlyDomesticLimit: number;
  @Field()
  WithdrawalMonthlyDomesticCount: number;
  @Field()
  WithdrawalMonthlyDomesticTrx: number;
  @Field()
  WithdrawalMonthlyDomesticAmount: number;
  @Field()
  WithdrawalDailyDomesticLimit: number;
  @Field()
  WithdrawalDailyDomesticCount: number;
  @Field()
  WithdrawalDailyDomesticTrx: number;
  @Field()
  WithdrawalDailyDomesticAmount: number;
  @Field()
  PurchaseMonthlyInternationalLimit: number;
  @Field()
  PurchaseMonthlyInternationalCount: number;
  @Field()
  PurchaseMonthlyInternationalTrx: number;
  @Field()
  PurchaseMonthlyInternationalAmount: number;
  @Field()
  PurchaseDailyInternationalLimit: number;
  @Field()
  PurchaseDailyInternationalCount: number;
  @Field()
  PurchaseDailyInternationalTrx: number;
  @Field()
  PurchaseDailyInternationalAmount: number;
  @Field()
  PurchaseMonthlyDomesticLimit: number;
  @Field()
  PurchaseMonthlyDomesticCount: number;
  @Field()
  PurchaseMonthlyDomesticTrx: number;
  @Field()
  PurchaseMonthlyDomesticAmount: number;
  @Field()
  PurchaseDailyDomesticLimit: number;
  @Field()
  PurchaseDailyDomesticCount: number;
  @Field()
  PurchaseDailyDomesticTrx: number;
  @Field()
  PurchaseDailyDomesticAmount: number;
  @Field()
  WithdrawalMonthlyInternationalLimit: number;
  @Field()
  WithdrawalMonthlyInternationalCount: number;
  @Field()
  WithdrawalMonthlyInternationalTrx: number;
  @Field()
  WithdrawalMonthlyInternationalAmount: number;
  @Field()
  WithdrawalDailyInternationalAmount: number;
  @Field()
  WithdrawalDailInternationalLimit: number;
  @Field()
  WithdrawalDailInternationalTrx: number;
  @Field()
  WithdrawalDailInternationalCount: number;
  @Field()
  InternetMonthlyLimit: number;
  @Field()
  InternetMonthlyCount: number;
  @Field()
  InternetMonthlyTrx: number;
  @Field()
  InternetMonthlyAmount: number;
  @Field()
  InternetDailyLimit: number;
  @Field()
  InternetDailyCount: number;
  @Field()
  InternetDailyTrx: number;
  @Field()
  InternetDailyAmount: number;
}

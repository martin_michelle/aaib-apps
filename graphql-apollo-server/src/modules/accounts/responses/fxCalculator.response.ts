import { ObjectType, Field, Float } from 'type-graphql';

@ObjectType()
export class FxCalculator {
  @Field()
  SourceCurrency: string;

  @Field()
  DestinationCurrency: string;

  @Field(() => Float)
  SourceAmount: number;

  @Field(() => Float)
  DestinationAmount: number;

  @Field(() => Float, { nullable: true })
  ExchangeRate: number;
}

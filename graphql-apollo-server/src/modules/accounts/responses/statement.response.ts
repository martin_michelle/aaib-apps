import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class DownloadStatement {
  @Field()
  base64Data: string;

  @Field()
  fileName: string;
}

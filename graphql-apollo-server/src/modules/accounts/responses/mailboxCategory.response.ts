import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class mailboxCategoryData {
  @Field(() => [String])
  category: string[];
}

import { ObjectType, Field, Float } from 'type-graphql';
import { Beneficiary } from '../../beneficiary/responses/beneficiary.response';

@ObjectType()
class ScheduledAccountTransferBody {
  @Field(() => Float)
  amount: number;

  @Field()
  debitAcc: string;

  @Field()
  description: string;

  @Field()
  beneficiaryID: string;

  @Field({ nullable: true })
  currency?: string;

  @Field({ nullable: true })
  chargeType?: string;
}

@ObjectType()
export class ListScheduledAccountTransfers {
  @Field(() => ScheduledAccountTransferBody)
  body: ScheduledAccountTransferBody;

  @Field()
  _id: string;

  @Field()
  user: string;

  @Field()
  userId: string;

  @Field()
  scheduleDate: string;

  @Field({ nullable: true })
  nextTransferDate: string;

  @Field()
  transferFrequency: string;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;

  @Field()
  platform: string;

  @Field(() => Float)
  currentTransferCount: number;

  @Field(() => Float)
  totalTransferCount: number;

  @Field(() => Beneficiary)
  beneficiary: Beneficiary;
}

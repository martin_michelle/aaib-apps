import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class SavedCreditCard {
  @Field()
  _id: string;

  @Field()
  cif: string;

  @Field()
  name: string;

  @Field({ nullable: true })
  nickName?: string;

  @Field()
  cardNumber: string;
}

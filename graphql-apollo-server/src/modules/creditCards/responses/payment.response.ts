import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class CreditCardPayment {
  @Field()
  transactionId: string;
}

import { ObjectType, Field, Float, Int } from 'type-graphql';

import { Balance } from '../../accounts/responses/account.response';

@ObjectType()
class OnlineData {
  @Field({ nullable: true })
  CardUID?: string;

  @Field({ nullable: true })
  InternalAccount?: string;

  @Field({ nullable: true })
  AvailableBalance?: string;

  @Field({ nullable: true })
  LedgerBalance?: string;

  @Field({ nullable: true })
  AccountType?: string;

  @Field({ nullable: true })
  AccountStatus?: string;

  @Field({ nullable: true })
  CardStatus?: string;

  @Field({ nullable: true })
  ContactlessStatus?: string;

  @Field({ nullable: true })
  LastATMUsed?: string;

  @Field({ nullable: true })
  LastPOSUsed?: string;

  @Field({ nullable: true })
  LastTranTime?: string;

  @Field({ nullable: true })
  CardType?: string;

  @Field({ nullable: true })
  DueAmount?: string;

  @Field(() => Int, { nullable: true })
  DueDate?: number;
}
@ObjectType()
export class creditCard {
  @Field({ nullable: true })
  ContractId?: string;

  @Field(() => Int, { nullable: true })
  MBR?: number;

  @Field({ nullable: true })
  InternalAccount?: string;

  @Field({ nullable: true })
  Bucket?: string;

  @Field({ nullable: true })
  Account?: string;

  @Field({ nullable: true })
  NameOnCard?: string;

  @Field({ nullable: true })
  Company?: string;

  @Field({ nullable: true })
  CardNumber?: string;

  @Field({ nullable: true })
  CardFinancialProfile?: string;

  @Field({ nullable: true })
  AccountCurrency?: string;

  @Field({ nullable: true })
  CardCurrency?: string;

  @Field(() => Float, { nullable: true })
  Balance?: number;

  @Field(() => Float, { nullable: true })
  LastStmtBalalance?: number;

  @Field({ nullable: true })
  Status?: string;

  @Field({ nullable: true })
  SecurityIndicator?: string;

  @Field({ nullable: true })
  RepaymentAccount?: string;

  @Field(() => Int, { nullable: true })
  CreateDate?: number;

  @Field(() => Float, { nullable: true })
  CardLimit?: number;

  @Field(() => Int, { nullable: true })
  LastStmtDate?: number;

  @Field(() => Int, { nullable: true })
  LastPayDate?: number;

  @Field(() => Float, { nullable: true })
  LastPaidAmt?: number;

  @Field(() => Float, { nullable: true })
  LastOverdueAmt?: number;

  @Field(() => Int, { nullable: true })
  MinPaymentDueDate?: number;

  @Field(() => Float, { nullable: true })
  MinPaymentDueAmount?: number;

  @Field(() => Float, { nullable: true })
  DirectDebitRate?: number;

  @Field(() => Float, { nullable: true })
  CashLimit?: number;

  @Field(() => Float, { nullable: true })
  SaleLimit?: number;

  @Field({ nullable: true })
  PrmSupFlag?: string;

  @Field()
  OnlineData: OnlineData;
}
@ObjectType()
export class CreditCards {
  @Field()
  balance: Balance;

  @Field(() => [creditCard])
  cards: creditCard[];
}

@ObjectType()
export class CreditCardTransaction {
  @Field({ nullable: true })
  ApprovalNo?: string;

  @Field(() => Float, { nullable: true })
  SettlementAmount?: number;

  @Field(() => Float, { nullable: true })
  Amount?: number;

  @Field({ nullable: true })
  Currency?: string;

  @Field({ nullable: true })
  TrsDate?: string;

  @Field({ nullable: true })
  TrsTime?: string;

  @Field({ nullable: true })
  MBR?: string;

  @Field({ nullable: true })
  TerminalClass?: string;

  @Field({ nullable: true })
  TerminalID?: string;

  @Field({ nullable: true })
  TerminalLocation?: string;

  @Field({ nullable: true })
  TerminalCity?: string;

  @Field({ nullable: true })
  TerminalCountry?: string;

  @Field({ nullable: true })
  MerchantCategory?: string;

  @Field({ nullable: true })
  ChargeAmount?: string;

  @Field({ nullable: true })
  Description?: string;

  @Field({ nullable: true })
  timestamp?: string;

  @Field({ nullable: true })
  correlationId?: string;

  @Field({ nullable: true })
  description?: string;
}

@ObjectType()
export class CreditCardSettings {
  @Field({ nullable: true })
  CardNumber?: string;

  @Field({ nullable: true })
  eCommerce?: string;

  @Field({ nullable: true })
  eComOnUntil?: string;

  @Field({ nullable: true })
  Purchase?: string;

  @Field({ nullable: true })
  PurchaseOnUntil?: string;

  @Field({ nullable: true })
  Cash?: string;

  @Field({ nullable: true })
  CashOnUntil?: string;

  @Field({ nullable: true })
  AllowAnyForeign?: boolean;

  @Field({ nullable: true })
  AllowSpecificForeign?: boolean;

  @Field({ nullable: true })
  AllowForeignCountries?: string;

  @Field({ nullable: true })
  ForeignStart?: string;

  @Field({ nullable: true })
  ForeignEnd?: string;
}

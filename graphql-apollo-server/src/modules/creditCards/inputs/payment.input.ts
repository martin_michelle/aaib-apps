import { InputType, Field, Float } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CreditCardPaymentInput {
  @IsNotEmpty()
  @Field()
  debitAcc: string;

  @IsNotEmpty()
  @Field()
  contractId: string;

  @IsNotEmpty()
  @Field()
  currency: string;

  @IsNotEmpty()
  @Field(() => Float)
  amount: number;
}

import { IsNotEmpty, IsOptional } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class SaveCreditCardInput {
  @IsNotEmpty()
  @Field()
  name: string;

  @IsOptional()
  @Field({ nullable: true })
  nickName?: string;

  @IsNotEmpty()
  @Field()
  cardNumber: string;
}
@InputType()
export class TransferToCCByIdInput {
  @IsOptional()
  @Field({ nullable: true })
  currency?: string;

  @IsNotEmpty()
  @Field()
  amount: number;

  @IsNotEmpty()
  @Field()
  debitAcc: string;

  @IsNotEmpty()
  @Field()
  description: string;

  @IsNotEmpty()
  @Field()
  otp: string;
}

@InputType()
export class TransferToCreditCardInput {
  @IsNotEmpty()
  @Field()
  cardNumber: string;

  @IsNotEmpty()
  @Field()
  name: string;

  @IsNotEmpty()
  @Field()
  amount: number;

  @IsNotEmpty()
  @Field()
  debitAcc: string;

  @IsNotEmpty()
  @Field()
  description: string;

  @IsOptional()
  @Field({ nullable: true })
  currency?: string;

  @IsNotEmpty()
  @Field()
  otp: string;
}

@InputType()
export class TransferAndSaveCCInput {
  @IsNotEmpty()
  @Field(() => SaveCreditCardInput)
  creditCardDetails: SaveCreditCardInput;

  @IsNotEmpty()
  @Field(() => TransferToCreditCardInput)
  transferDetails: TransferToCreditCardInput;
}

import { IsNotEmpty } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class ReplaceCreditCardInput {
  @IsNotEmpty()
  @Field()
  cardNumber: string;
}

import { InputType, Field, Int } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CreditCardTransactionsInput {
  @IsNotEmpty()
  @Field()
  contractId: string;

  @Field({ nullable: true })
  fromDate: string;

  @Field({ nullable: true })
  toDate: string;

  @Field(() => Int, { nullable: true })
  limit: number;

  @Field({ nullable: true })
  lastTransaction: string;

  @Field({ nullable: true })
  search: string;
}

import { InputType, Field } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class CreditCardSettingsBaseInput {
  @IsNotEmpty()
  @Field()
  ContractId: string;
}
@InputType()
export class CreditCardStatusInput extends CreditCardSettingsBaseInput {
  @IsNotEmpty()
  @Field()
  Action: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  smsToken?: string;
}

@InputType()
export class CreditCardSettingsInput extends CreditCardStatusInput {
  @IsNotEmpty()
  @Field()
  Setting: string;
}

@InputType()
export class CreditCardTravelWindowInput {
  @IsNotEmpty()
  @Field()
  ContractId: string;

  @IsNotEmpty()
  @Field(() => [String])
  CountriesList: string[];

  @IsNotEmpty()
  @Field()
  StartDate: string;

  @IsNotEmpty()
  @Field()
  EndDate: string;
}

@InputType()
export class CardLedgerAmountInput {
  @IsNotEmpty()
  @Field()
  BenficiaryId: string;
}

import { MyExpressContext } from 'app-context';
import { Query, Resolver, Ctx, Arg, Mutation } from 'type-graphql';
import { Inject, Service } from 'typedi';
import { CreditCardsService } from './creditCards.service';
import { CreditCardPaymentInput } from './inputs/payment.input';
import {
  CreditCardSettingsBaseInput,
  CreditCardSettingsInput,
  CreditCardStatusInput,
  CreditCardTravelWindowInput,
  CardLedgerAmountInput
} from './inputs/settings.input';
import { CreditCardTransactionsInput } from './inputs/transactions.input';
import {
  CreditCards,
  CreditCardSettings,
  CreditCardTransaction
} from './responses/creditCards.response';
import { CreditCardPayment } from './responses/payment.response';
import { ReplaceCreditCardInput } from './inputs/replaceCard.input';
import {
  SaveCreditCardInput,
  TransferToCCByIdInput,
  TransferToCreditCardInput,
  TransferAndSaveCCInput
} from './inputs/savedCreditCard';
import { SavedCreditCard } from './responses/savedCreditCards.response';

@Service()
@Resolver(() => CreditCardsService)
export default class CreditCardsResolver {
  @Inject(() => CreditCardsService)
  creditCardsService: CreditCardsService;

  @Query(() => CreditCards)
  async creditCards(@Ctx() context: MyExpressContext): Promise<CreditCards> {
    return this.creditCardsService.creditCards(context);
  }

  @Query(() => [CreditCardTransaction])
  async creditCardTransactions(
    @Arg('data') data: CreditCardTransactionsInput,
    @Ctx() context: MyExpressContext
  ): Promise<CreditCardTransaction[]> {
    return this.creditCardsService.creditCardTransactions(context, data);
  }

  @Mutation(() => String)
  async setCreditCardStatus(
    @Arg('data') data: CreditCardStatusInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.creditCardsService.setCreditCardStatus(context, data);
  }

  @Mutation(() => CreditCardPayment)
  async creditCardPayment(
    @Arg('data') data: CreditCardPaymentInput,
    @Ctx() context: MyExpressContext
  ): Promise<CreditCardPayment> {
    return this.creditCardsService.creditCardPayment(context, data);
  }

  @Mutation(() => String)
  async setCreditCardSettings(
    @Arg('data') data: CreditCardSettingsInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.creditCardsService.setCreditCardSettings(context, data);
  }

  @Query(() => CreditCardSettings)
  async getCreditCardSettings(
    @Arg('data') data: CreditCardSettingsBaseInput,
    @Ctx() context: MyExpressContext
  ): Promise<CreditCardSettings> {
    return this.creditCardsService.getCreditCardSettings(context, data);
  }

  @Mutation(() => String)
  async enableCreditCardTravelWindow(
    @Arg('data') data: CreditCardTravelWindowInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.creditCardsService.enableCreditCardTravelWindow(context, data);
  }

  @Mutation(() => String)
  async disableCreditCardTravelWindow(
    @Arg('data') data: CreditCardSettingsBaseInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.creditCardsService.disableCreditCardTravelWindow(context, data);
  }

  @Mutation(() => String)
  async creditCardRequest(@Ctx() context: MyExpressContext): Promise<string> {
    return this.creditCardsService.creditCardRequest(context);
  }

  @Mutation(() => String)
  async creditCardHolderName(
    @Arg('CardNumber') CardNumber: number,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.creditCardsService.creditCardHolderName(context, CardNumber);
  }

  @Mutation(() => String)
  async replaceCreditCard(
    @Arg('data') data: ReplaceCreditCardInput,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.creditCardsService.replaceCreditCard(context, data);
  }

  @Mutation(() => String)
  async canReplaceCreditCard(
    @Arg('cardNumber') cardNumber: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.creditCardsService.canReplaceCreditCard(context, cardNumber);
  }

  @Mutation(() => String)
  async saveCreditCard(
    @Arg('data') data: SaveCreditCardInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.creditCardsService.saveCreditCard(context, data);
  }

  @Mutation(() => CreditCardPayment)
  async transferToCCById(
    @Arg('id') id: string,
    @Arg('data') data: TransferToCCByIdInput,
    @Ctx() context: MyExpressContext
  ): Promise<CreditCardPayment> {
    return this.creditCardsService.transferToCCById(context, id, data);
  }
  @Mutation(() => CreditCardPayment)
  async transferToCreditCard(
    @Arg('data') data: TransferToCreditCardInput,
    @Ctx() context: MyExpressContext
  ): Promise<CreditCardPayment> {
    return this.creditCardsService.transferToCreditCard(context, data);
  }

  @Mutation(() => CreditCardPayment)
  async transferAndSaveCC(
    @Arg('data') data: TransferAndSaveCCInput,
    @Ctx() context: MyExpressContext
  ): Promise<CreditCardPayment> {
    return this.creditCardsService.transferAndSaveCC(context, data);
  }

  @Query(() => [SavedCreditCard])
  async listSavedCreditCards(@Ctx() context: MyExpressContext): Promise<SavedCreditCard[]> {
    return this.creditCardsService.listSavedCreditCards(context);
  }

  @Mutation(() => String)
  async deleteCreditCard(
    @Arg('CreditCardId') CreditCardId: string,
    @Ctx() context: MyExpressContext
  ): Promise<string> {
    return this.creditCardsService.deleteCreditCard(context, CreditCardId);
  }

  @Query(() => String)
  async termsAndConditionsCreditCard(@Ctx() context: MyExpressContext): Promise<String> {
    return this.creditCardsService.termsAndConditionsCreditCard(context);
  }

  @Query(() => Number)
  async getCardLedgerAmountByBenficiaryId(
    @Arg('data') data: CardLedgerAmountInput,
    @Ctx() context: MyExpressContext
  ): Promise<Number> {
    return this.creditCardsService.getCardLedgerAmountByBenficiaryId(context, data);
  }
}

import { Service } from 'typedi';
import fetch from 'node-fetch';

import {
  CreditCards,
  CreditCardSettings,
  CreditCardTransaction
} from './responses/creditCards.response';
import { KONG_BASE_URL } from '../../lib/config';
import { formatApiResponse } from '../../lib/formatResponse';
import { MyExpressContext } from 'app-context';
import headersInit from '../../lib/headers';
import { CreditCardTransactionsInput } from './inputs/transactions.input';
import {
  CreditCardSettingsBaseInput,
  CreditCardSettingsInput,
  CreditCardStatusInput,
  CreditCardTravelWindowInput,
  CardLedgerAmountInput
} from './inputs/settings.input';
import { CreditCardPaymentInput } from './inputs/payment.input';
import { CreditCardPayment } from './responses/payment.response';
import { stringifyUrl } from 'query-string';
import { maskString } from '../../lib/helpers';
import { ReplaceCreditCardInput } from './inputs/replaceCard.input';
import {
  SaveCreditCardInput,
  TransferToCreditCardInput,
  TransferAndSaveCCInput,
  TransferToCCByIdInput
} from './inputs/savedCreditCard';
import { SavedCreditCard } from './responses/savedCreditCards.response';

@Service()
export class CreditCardsService {
  async creditCards(context: MyExpressContext): Promise<CreditCards> {
    const creditCards = await fetch(`${KONG_BASE_URL}/credit`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(creditCards);
  }

  async creditCardTransactions(
    context: MyExpressContext,
    data: CreditCardTransactionsInput
  ): Promise<CreditCardTransaction[]> {
    const { contractId, fromDate, toDate, limit, lastTransaction, search } = data;
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/credit/transactions/${contractId}`,
        query: { fromDate, toDate, limit, lastTransaction, search }
      },
      { skipNull: true, skipEmptyString: true }
    );

    const transactionsResponse = await fetch(url, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(transactionsResponse);
    return res.transactions || [];
  }

  async setCreditCardStatus(
    context: MyExpressContext,
    data: CreditCardStatusInput
  ): Promise<String> {
    const statusResponse = await fetch(`${KONG_BASE_URL}/credit/setstatus`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(statusResponse);

    return res.message;
  }

  async creditCardPayment(
    context: MyExpressContext,
    data: CreditCardPaymentInput
  ): Promise<CreditCardPayment> {
    const paymentResponse = await fetch(`${KONG_BASE_URL}/credit/payment`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(paymentResponse);
  }

  async getCreditCardSettings(
    context: MyExpressContext,
    data: CreditCardSettingsBaseInput
  ): Promise<CreditCardSettings> {
    const settingsResponse = await fetch(`${KONG_BASE_URL}/credit/settings/${data.ContractId}`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(settingsResponse);
  }

  async setCreditCardSettings(
    context: MyExpressContext,
    data: CreditCardSettingsInput
  ): Promise<String> {
    const settingsResponse = await fetch(`${KONG_BASE_URL}/credit/settings`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(settingsResponse);

    return res.message;
  }

  async enableCreditCardTravelWindow(
    context: MyExpressContext,
    data: CreditCardTravelWindowInput
  ): Promise<String> {
    const twResponse = await fetch(`${KONG_BASE_URL}/credit/travelWindow/enable`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(twResponse);

    return res.message;
  }

  async disableCreditCardTravelWindow(
    context: MyExpressContext,
    data: CreditCardSettingsBaseInput
  ): Promise<String> {
    const twResponse = await fetch(`${KONG_BASE_URL}/credit/travelWindow/disable`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(twResponse);

    return res.message;
  }

  async creditCardRequest(context: MyExpressContext): Promise<string> {
    const request = await fetch(`${KONG_BASE_URL}/credit/request`, {
      method: 'POST',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(request);

    return res.message;
  }

  async creditCardHolderName(context: MyExpressContext, CardNumber: number): Promise<String> {
    const body = { CardNumber };
    const cardDetails = await fetch(`${KONG_BASE_URL}/credit/card`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(cardDetails);
    return maskString(res.NameOnCard);
  }

  async replaceCreditCard(
    context: MyExpressContext,
    data: ReplaceCreditCardInput
  ): Promise<string> {
    const request = await fetch(`${KONG_BASE_URL}/credit/replace`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(request);
    return res.message;
  }

  async canReplaceCreditCard(context: MyExpressContext, cardNumber: string): Promise<string> {
    const request = await fetch(`${KONG_BASE_URL}/credit/canReplaceCard`, {
      method: 'POST',
      body: JSON.stringify({ cardNumber }),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(request);
    return res.message;
  }

  async saveCreditCard(context: MyExpressContext, data: SaveCreditCardInput): Promise<string> {
    const request = await fetch(`${KONG_BASE_URL}/credit/savedCreditCard`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(request);
    return res.message;
  }

  async transferToCCById(
    context: MyExpressContext,
    id: string,
    data: TransferToCCByIdInput
  ): Promise<CreditCardPayment> {
    const request = await fetch(`${KONG_BASE_URL}/credit/transferToSavedCard/${id}`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return await formatApiResponse(request);
  }
  async transferToCreditCard(
    context: MyExpressContext,
    data: TransferToCreditCardInput
  ): Promise<CreditCardPayment> {
    const request = await fetch(`${KONG_BASE_URL}/credit/transferToCreditCard`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return await formatApiResponse(request);
  }

  async transferAndSaveCC(
    context: MyExpressContext,
    data: TransferAndSaveCCInput
  ): Promise<CreditCardPayment> {
    const transferResponse = await fetch(`${KONG_BASE_URL}/credit/transferToCreditCard`, {
      method: 'POST',
      body: JSON.stringify(data.transferDetails),
      headers: headersInit(context)
    });

    const transfer = await formatApiResponse(transferResponse);

    await fetch(`${KONG_BASE_URL}/credit/savedCreditCard`, {
      method: 'POST',
      body: JSON.stringify(data.creditCardDetails),
      headers: headersInit(context)
    });

    return transfer;
  }

  async listSavedCreditCards(context: MyExpressContext): Promise<SavedCreditCard[]> {
    const termsAndConditions = await fetch(`${KONG_BASE_URL}/credit/savedCreditCard`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return await formatApiResponse(termsAndConditions);
  }

  async deleteCreditCard(context: MyExpressContext, CreditCardId: string): Promise<string> {
    const deleteCreditCardResponse = await fetch(
      `${KONG_BASE_URL}/credit/savedCreditCard/${CreditCardId}`,
      {
        method: 'DELETE',
        headers: headersInit(context)
      }
    );
    const res = await formatApiResponse(deleteCreditCardResponse);
    return res.message;
  }

  async termsAndConditionsCreditCard(context: MyExpressContext): Promise<String> {
    const termsAndConditions = await fetch(`${KONG_BASE_URL}/credit/termsAndConditions`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(termsAndConditions);
    return res.result;
  }

  async getCardLedgerAmountByBenficiaryId(
    context: MyExpressContext,
    data: CardLedgerAmountInput
  ): Promise<Number> {
    const getCardLedgerAmountByBenficiaryIdResponse = await fetch(
      `${KONG_BASE_URL}/credit/info/${data.BenficiaryId}`,
      {
        method: 'GET',
        headers: headersInit(context)
      }
    );

    return formatApiResponse(getCardLedgerAmountByBenficiaryIdResponse);
  }
}

import {
  ActivateCardValidationInput,
  ForgotPasswordValidationInput,
  SendChangePasswordSmsValidationInput,
  ValidationInput,
  phoneNumbersInput
} from './inputs/validation.input';
import { Service } from 'typedi';
import fetch from 'node-fetch';
import { formatApiResponse } from '../../lib/formatResponse';

import { LoginInput } from './inputs/login.input';
import { AuthResponse, TAK_tokenapp } from './responses/login.response';
import { KONG_BASE_URL } from '../../lib/config';
import { MyExpressContext } from 'app-context';
import headersInit from '../../lib/headers';
import {
  changePasswordInput,
  changePasswordUsingSmsInput,
  ResetExpiredPasswordInput,
  ResetPasswordInput
} from './inputs/ResetPassword.input';
import { TwinProfile } from './responses/profile.response';
import { RegistrationInput, LinkingInput, OnboardingCustomer } from './inputs/registration.input';
import { RegistrationValidation, RegistrationCountries } from './responses/registration.response';
import { OtpResponse } from './responses/otp.response';
import { PhoneNumbersResponse } from './responses/forgotPassword.response';
import { OtpValidationInput, ResendOtpInput } from './inputs/otp.input';
import { portfolioStatementInput } from './inputs/portfolio.input';
import { PortfolioStatement } from './responses/portfolio.response';
import { TokenAppLogInput } from './inputs/token-app.input';
import { stringifyUrl } from 'query-string';

@Service()
export class UserService {
  async login(context: MyExpressContext, data: LoginInput): Promise<AuthResponse> {
    const loginResponse = await fetch(`${KONG_BASE_URL}/login/basic`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(loginResponse);
  }

  async validateTokenApp(
    context: MyExpressContext,
    data: { takId: string }
  ): Promise<TAK_tokenapp> {
    const { takId } = data;

    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/login/validate/takId`,
        query: { takId }
      },
      { skipNull: true, skipEmptyString: true }
    );

    const validationResponse = await fetch(url, {
      method: 'GET',
      headers: {
        ...headersInit(context),
        'tak-id': takId
      }
    });

    return formatApiResponse(validationResponse);
  }

  async refresh(context: MyExpressContext, refreshToken: string): Promise<AuthResponse> {
    const refreshResponse = await fetch(`${KONG_BASE_URL}/login/refresh`, {
      method: 'POST',
      body: JSON.stringify({ refreshToken }),
      headers: headersInit(context)
    });

    return formatApiResponse(refreshResponse);
  }

  async revokeToken(context: MyExpressContext): Promise<String> {
    const revokeResponse = await fetch(`${KONG_BASE_URL}/login/revoke`, {
      method: 'POST',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(revokeResponse);

    return res.message;
  }

  async resetExpiredPassword(
    context: MyExpressContext,
    data: ResetExpiredPasswordInput
  ): Promise<String> {
    const ResetExpiredPasswordResponse = await fetch(`${KONG_BASE_URL}/login/reset`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(ResetExpiredPasswordResponse);

    return res.message;
  }

  async changePassword(context: MyExpressContext, data: changePasswordInput): Promise<String> {
    const changePasswordResponse = await fetch(`${KONG_BASE_URL}/login/change`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(changePasswordResponse);

    return res.message;
  }

  async changePasswordUsingSmsOtp(
    context: MyExpressContext,
    data: changePasswordUsingSmsInput
  ): Promise<String> {
    const changePasswordResponse = await fetch(`${KONG_BASE_URL}/login/change/password`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(changePasswordResponse);

    return res.message;
  }

  async profile(context: MyExpressContext): Promise<TwinProfile> {
    const profileResponse = await fetch(`${KONG_BASE_URL}/login/profile`, {
      method: 'GET',
      headers: headersInit(context)
    });

    return formatApiResponse(profileResponse);
  }

  async forgotUsername(context: MyExpressContext, data: ValidationInput): Promise<String> {
    const forgotUsernameResponse = await fetch(`${KONG_BASE_URL}/login/forgot/username`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(forgotUsernameResponse);

    return res.message;
  }

  async getPhoneNumbers(
    context: MyExpressContext,
    data: phoneNumbersInput
  ): Promise<PhoneNumbersResponse> {
    const getPhoneNumbersResponse = await fetch(`${KONG_BASE_URL}/login/phoneNumbers`, {
      method: 'post',
      body: JSON.stringify({ username: data.username }),
      headers: headersInit(context)
    });

    return await formatApiResponse(getPhoneNumbersResponse);
  }

  async forgotPassword(
    context: MyExpressContext,
    data: ForgotPasswordValidationInput
  ): Promise<OtpResponse> {
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/login/forgot/password`,
        query: { country: data.country }
      },
      { skipNull: true, skipEmptyString: true }
    );

    const forgotPasswordResponse = await fetch(url, {
      method: 'POST',
      body: JSON.stringify({ username: data.username }),
      headers: headersInit(context)
    });

    return formatApiResponse(forgotPasswordResponse);
  }

  async sendChangePasswordSms(
    context: MyExpressContext,
    data: SendChangePasswordSmsValidationInput
  ): Promise<OtpResponse> {
    const url = stringifyUrl(
      {
        url: `${KONG_BASE_URL}/login/change/password/send/otp/sms`,
        query: { country: data.country }
      },
      { skipNull: true, skipEmptyString: true }
    );

    const smsResponse = await fetch(url, {
      method: 'POST',
      headers: headersInit(context)
    });

    return formatApiResponse(smsResponse);
  }

  async forgotPasswordOtpValidation(
    context: MyExpressContext,
    data: OtpValidationInput
  ): Promise<OtpResponse> {
    const validationResponse = await fetch(`${KONG_BASE_URL}/login/validate/password`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(validationResponse);
  }

  async resetPassword(context: MyExpressContext, data: ResetPasswordInput): Promise<String> {
    const resetPasswordResponse = await fetch(`${KONG_BASE_URL}/login/forgot/password/change`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(resetPasswordResponse);
    return res.message;
  }

  async registrationValidation(
    context: MyExpressContext,
    data: ValidationInput
  ): Promise<RegistrationValidation> {
    const validationResponse = await fetch(`${KONG_BASE_URL}/signup/validate`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(validationResponse);
  }

  async register(context: MyExpressContext, data: RegistrationInput): Promise<String> {
    const registrationResponse = await fetch(`${KONG_BASE_URL}/signup/register`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(registrationResponse);

    return res.message;
  }

  async link(context: MyExpressContext, data: LinkingInput): Promise<AuthResponse> {
    const linkingResponse = await fetch(`${KONG_BASE_URL}/signup/link`, {
      method: 'POST',
      body: JSON.stringify({ token: data.token, OTP: data.otp }),
      headers: { refreshtoken: context.req.headers.refreshtoken as string, ...headersInit(context) }
    });

    return formatApiResponse(linkingResponse);
  }

  async onboardingCustomer(context: MyExpressContext, data: OnboardingCustomer): Promise<String> {
    const onboardingCustomerResponse = await fetch(`${KONG_BASE_URL}/signup/request`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(onboardingCustomerResponse);

    return res.message;
  }
  async tokenAppLog(context: MyExpressContext, data: TokenAppLogInput): Promise<String> {
    const tokenAppResponse = await fetch(`${KONG_BASE_URL}/login/tokenApp`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(tokenAppResponse);

    return res.message;
  }

  async registerDevice(context: MyExpressContext, type: string, country: string): Promise<String> {
    const registrationResponse = await fetch(`${KONG_BASE_URL}/login/register/device`, {
      method: 'POST',
      body: JSON.stringify({ type, country }),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(registrationResponse);
    return res.message;
  }

  async isDeviceRegistered(context: MyExpressContext): Promise<Boolean> {
    const isRegisteredResponse = await fetch(`${KONG_BASE_URL}/login/device/check`, {
      method: 'GET',
      headers: headersInit(context)
    });

    const res = await formatApiResponse(isRegisteredResponse);
    return res.registered || false;
  }

  async resendOtp(context: MyExpressContext, data: ResendOtpInput): Promise<String> {
    const resendOtpResponse = await fetch(`${KONG_BASE_URL}/login/resend/otp`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    const res = await formatApiResponse(resendOtpResponse);
    return res.message;
  }

  async sendActivationOtp(
    context: MyExpressContext,
    data: ActivateCardValidationInput
  ): Promise<OtpResponse> {
    const activationOtpResponse = await fetch(`${KONG_BASE_URL}/login/send/otp`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(activationOtpResponse);
  }

  async validateActivationOtp(
    context: MyExpressContext,
    data: OtpValidationInput
  ): Promise<OtpResponse> {
    const validateOtpResponse = await fetch(`${KONG_BASE_URL}/login/validate/card`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headersInit(context)
    });

    return formatApiResponse(validateOtpResponse);
  }

  async portfolioStatementData(
    context: MyExpressContext,
    data: portfolioStatementInput
  ): Promise<PortfolioStatement> {
    const portfolioStatementResponse = await fetch(
      `${KONG_BASE_URL}/login/portfolioStatement/data?Year=${data.Year}&Month=${data.Month}`,
      {
        method: 'GET',
        headers: headersInit(context)
      }
    );
    return formatApiResponse(portfolioStatementResponse);
  }

  async portfolioStatement(
    context: MyExpressContext,
    data: portfolioStatementInput
  ): Promise<String> {
    const portfolioStatementResponse = await fetch(
      `${KONG_BASE_URL}/login/portfolioStatement?Year=${data.Year}&Month=${data.Month}`,
      {
        method: 'GET',
        headers: headersInit(context)
      }
    );
    const response = await formatApiResponse(portfolioStatementResponse);
    return response.message;
  }

  async getRegistrationCountries(context: MyExpressContext): Promise<RegistrationCountries[]> {
    const countries = await fetch(`${KONG_BASE_URL}/signup/registrationCountries`, {
      method: 'GET',
      headers: headersInit(context)
    });
    const res = await formatApiResponse(countries);
    return res?.result || [];
  }
}

import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class ForgotPasswordResponse {
  @Field()
  token: string;
}

@ObjectType()
export class PhoneData {
  @Field()
  country: string;

  @Field()
  number: string;
}

@ObjectType()
export class PhoneNumbersResponse {
  @Field(() => [PhoneData])
  data: PhoneData[];

  @Field(() => String, { nullable: true })
  notFetched: string;
}

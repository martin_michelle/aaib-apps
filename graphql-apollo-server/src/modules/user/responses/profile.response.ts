import { ObjectType, Field, Int } from 'type-graphql';

@ObjectType()
export class Profile {
  @Field({ nullable: true })
  Id?: string;

  @Field({ nullable: true })
  OldCifNo?: string;

  @Field({ nullable: true })
  ShortName?: string;

  @Field({ nullable: true })
  GivenName?: string;

  @Field({ nullable: true })
  FamilyName?: string;

  @Field({ nullable: true })
  ArabicName?: string;

  @Field({ nullable: true })
  MainSector?: string;

  @Field({ nullable: true })
  MainSectorDescription?: string;

  @Field({ nullable: true })
  Sector?: string;

  @Field({ nullable: true })
  SectorDescription?: string;

  @Field({ nullable: true })
  Nationality?: string;

  @Field({ nullable: true })
  LegalId?: string;

  @Field({ nullable: true })
  LegalDocName?: string;

  @Field(() => Int, { nullable: true })
  BirthIncorpDate?: number;

  @Field({ nullable: true })
  Language?: string;

  @Field({ nullable: true })
  Company?: string;

  @Field({ nullable: true })
  Title?: string;

  @Field({ nullable: true })
  CustomerSince?: string;

  @Field(() => [String], { nullable: true })
  Sms?: string[];

  @Field(() => [String], { nullable: true })
  Email?: string[];

  @Field(() => [String], { nullable: true })
  RelCode?: string[];

  @Field(() => [String], { nullable: true })
  Relation?: string[];

  @Field(() => [String], { nullable: true })
  RelCustomer?: string[];

  @Field(() => Boolean, { nullable: true })
  Signed?: boolean;

  @Field(() => Int, { nullable: true })
  SignDate?: number;

  @Field({ nullable: true })
  Gender?: string;

  @Field(() => [String], { nullable: true })
  Address?: string[];

  @Field(() => [String], { nullable: true })
  AccPurpose?: string[];

  @Field(() => Int, { nullable: true })
  KYCDate?: number;

  @Field(() => [String], { nullable: true })
  ResidentAddress?: string[];

  @Field(() => String, { nullable: true })
  country?: string;
}

@ObjectType()
export class TwinProfile extends Profile {
  @Field(() => [Profile], { nullable: true })
  otherCifsData?: Profile[];

  @Field({ nullable: true })
  notFetched?: string;
}

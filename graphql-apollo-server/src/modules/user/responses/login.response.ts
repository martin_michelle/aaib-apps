import { ObjectType, Field, Int } from 'type-graphql';

@ObjectType()
class User {
  @Field()
  name: string;

  @Field()
  id: string;

  @Field(() => Int)
  registrationDate: number;

  @Field({ nullable: true })
  sector?: string;

  @Field({ nullable: true })
  isCorporate?: boolean;

  @Field()
  country: string;

  @Field(() => [CifCountry])
  allcifs: CifCountry[];
}

@ObjectType()
class CifCountry {
  @Field()
  country: string;

  @Field()
  cif: string;
}

@ObjectType()
export class Tokens {
  @Field()
  accessToken: string;

  @Field()
  refreshToken: string;
}

@ObjectType()
class Statistics {
  @Field()
  status: string;

  @Field()
  date: string;

  @Field({ nullable: true })
  expiryDate?: string;
}

@ObjectType()
export class AuthResponse {
  @Field()
  user: User;

  @Field()
  tokens: Tokens;

  @Field()
  statistics: Statistics;
}

@ObjectType()
export class TAK_tokenapp {
  @Field()
  url: string;
}

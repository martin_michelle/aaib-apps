import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class RegistrationValidation {
  @Field()
  token: string;

  @Field({ nullable: true })
  username?: string;

  @Field()
  otpIdentity?: string;
}

@ObjectType()
export class RegistrationCountries {
  @Field()
  code: string;

  @Field()
  image: string;

  @Field()
  name: string;
}

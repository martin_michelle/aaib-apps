import { ObjectType, Field } from 'type-graphql';
import { Profile } from './profile.response';

@ObjectType()
export class PortfolioStatement {
  @Field()
  fileData: string;

  @Field(() => [Profile])
  customerData: [Profile];
}

import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class OtpResponse {
  @Field()
  token: string;
}

import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class OtpValidationInput {
  @IsNotEmpty()
  @Field()
  otp: string;

  @IsNotEmpty()
  @Field()
  token: string;
}

@InputType()
export class ResendOtpInput {
  @IsNotEmpty()
  @Field()
  token: string;
}

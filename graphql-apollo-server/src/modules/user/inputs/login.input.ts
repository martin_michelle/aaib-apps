import { InputType, Field } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class LoginInput {
  @IsNotEmpty()
  @Field()
  user: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  password?: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  otp?: string;
}

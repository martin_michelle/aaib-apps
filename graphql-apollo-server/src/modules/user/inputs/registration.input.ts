import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class RegistrationInput {
  @IsNotEmpty()
  @Field()
  token: string;

  @IsNotEmpty()
  @Field()
  otp: string;

  @IsNotEmpty()
  @Field()
  username: string;

  @IsNotEmpty()
  @Field()
  password: string;
}

@InputType()
export class LinkingInput {
  @IsNotEmpty()
  @Field()
  token: string;

  @IsNotEmpty()
  @Field()
  otp: string;
}

@InputType()
export class OnboardingCustomer {
  @IsNotEmpty()
  @Field()
  fullName: string;

  @IsNotEmpty()
  @Field()
  mobileNumber: string;

  @IsNotEmpty()
  @Field()
  productOfInterest: string;
}

import { InputType, Field } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class ResetExpiredPasswordInput {
  @IsNotEmpty()
  @Field()
  username: string;

  @IsNotEmpty()
  @Field()
  oldPassword: string;

  @IsNotEmpty()
  @Field()
  newPassword: string;
}

@InputType()
export class changePasswordInput {
  @IsNotEmpty()
  @Field()
  oldPassword: string;

  @IsNotEmpty()
  @Field()
  newPassword: string;

  @IsOptional()
  @IsNotEmpty()
  @Field({ nullable: true })
  otp: string;
}

@InputType()
export class changePasswordUsingSmsInput extends changePasswordInput {
  @Field()
  smsOtp: string;

  @IsNotEmpty()
  @Field()
  token: string;
}

@InputType()
export class ResetPasswordInput {
  @IsNotEmpty()
  @Field()
  token: string;

  @IsNotEmpty()
  @Field()
  password: string;
}

import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class portfolioStatementInput {
  @IsNotEmpty()
  @Field()
  Year: string;

  @IsNotEmpty()
  @Field()
  Month: string;
}

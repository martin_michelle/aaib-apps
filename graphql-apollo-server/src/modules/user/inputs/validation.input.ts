import { InputType, Field } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class ValidationInput {
  @IsNotEmpty()
  @Field()
  key: string;

  @IsNotEmpty()
  @Field()
  type: string;

  @IsNotEmpty()
  @Field()
  nationalId: string;
}

@InputType()
export class ForgotPasswordValidationInput {
  @IsNotEmpty()
  @Field()
  username: string;

  @Field({ nullable: true })
  country?: string;
}

@InputType()
export class SendChangePasswordSmsValidationInput {
  @Field({ nullable: true })
  country?: string;
}

@InputType()
export class phoneNumbersInput {
  @IsNotEmpty()
  @Field()
  username: string;
}

@InputType()
export class ActivateCardValidationInput {
  @IsNotEmpty()
  @Field()
  token: string;

  @IsNotEmpty()
  @Field()
  expiryDate: string;

  @IsNotEmpty()
  @Field()
  type: string;
}

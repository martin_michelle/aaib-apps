import { InputType, Field } from 'type-graphql';
import { IsNotEmpty, IsOptional } from 'class-validator';

@InputType()
export class TokenAppLogInput {
  @IsNotEmpty()
  @Field()
  user: string;

  @IsNotEmpty()
  @Field()
  success: boolean;

  @IsNotEmpty()
  @Field()
  subtype: string;

  @IsOptional()
  @Field({ nullable: true })
  reason: string;
}

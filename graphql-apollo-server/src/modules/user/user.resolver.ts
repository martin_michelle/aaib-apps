import { RegistrationInput, OnboardingCustomer, LinkingInput } from './inputs/registration.input';
import {
  ActivateCardValidationInput,
  ForgotPasswordValidationInput,
  SendChangePasswordSmsValidationInput,
  ValidationInput,
  phoneNumbersInput
} from './inputs/validation.input';
import { Arg, Mutation, Resolver, Ctx, Query } from 'type-graphql';
import { Inject, Service } from 'typedi';
import { UserService } from './user.service';
import { AuthResponse, TAK_tokenapp } from './responses/login.response';
import { LoginInput } from './inputs/login.input';
import { MyExpressContext } from 'app-context';
import {
  changePasswordInput,
  changePasswordUsingSmsInput,
  ResetExpiredPasswordInput,
  ResetPasswordInput
} from './inputs/ResetPassword.input';
import { TwinProfile } from './responses/profile.response';
import { RegistrationValidation, RegistrationCountries } from './responses/registration.response';
import { ForgotPasswordResponse } from './responses/forgotPassword.response';
import { OtpValidationInput, ResendOtpInput } from './inputs/otp.input';
import { OtpResponse } from './responses/otp.response';
import { PhoneNumbersResponse } from './responses/forgotPassword.response';
import { portfolioStatementInput } from './inputs/portfolio.input';
import { PortfolioStatement } from './responses/portfolio.response';
import { TokenAppLogInput } from './inputs/token-app.input';

@Service()
@Resolver()
export default class UserResolver {
  @Inject(() => UserService)
  userService: UserService;

  // LOGIN
  @Mutation(() => AuthResponse)
  async login(
    @Arg('data') data: LoginInput,
    @Ctx() context: MyExpressContext
  ): Promise<AuthResponse> {
    return this.userService.login(context, data);
  }

  // validate token app
  @Query(() => TAK_tokenapp)
  async validateTokenApp(
    @Ctx() context: MyExpressContext,
    @Arg('takId') takId: string
  ): Promise<TAK_tokenapp> {
    return this.userService.validateTokenApp(context, { takId });
  }

  // Refresh Token
  @Mutation(() => AuthResponse)
  async refresh(
    @Arg('refreshToken') refreshToken: string,
    @Ctx() context: MyExpressContext
  ): Promise<AuthResponse> {
    return this.userService.refresh(context, refreshToken);
  }

  // Revoke Token
  @Mutation(() => String)
  async revokeToken(@Ctx() context: MyExpressContext): Promise<String> {
    return this.userService.revokeToken(context);
  }

  // Reset expired password
  @Mutation(() => String)
  async resetExpiredPassword(
    @Arg('data') data: ResetExpiredPasswordInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.resetExpiredPassword(context, data);
  }

  // Change password
  @Mutation(() => String)
  async changePassword(
    @Arg('data') data: changePasswordInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.changePassword(context, data);
  }

  // Change password using sms validation
  @Mutation(() => String)
  async changePasswordWithOtpValidation(
    @Arg('data') data: changePasswordUsingSmsInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.changePasswordUsingSmsOtp(context, data);
  }

  @Query(() => TwinProfile)
  async profile(@Ctx() context: MyExpressContext): Promise<TwinProfile> {
    return this.userService.profile(context);
  }

  @Mutation(() => String)
  async forgotUsername(
    @Arg('data') data: ValidationInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.forgotUsername(context, data);
  }

  @Mutation(() => RegistrationValidation)
  async registrationValidation(
    @Arg('data') data: ValidationInput,
    @Ctx() context: MyExpressContext
  ): Promise<RegistrationValidation> {
    return this.userService.registrationValidation(context, data);
  }

  @Mutation(() => String)
  async register(
    @Arg('data') data: RegistrationInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.register(context, data);
  }

  @Mutation(() => AuthResponse)
  async link(
    @Arg('data') data: LinkingInput,
    @Ctx() context: MyExpressContext
  ): Promise<AuthResponse> {
    return this.userService.link(context, data);
  }

  @Query(() => PhoneNumbersResponse)
  async phoneNumbers(
    @Arg('data') data: phoneNumbersInput,
    @Ctx() context: MyExpressContext
  ): Promise<PhoneNumbersResponse> {
    return this.userService.getPhoneNumbers(context, data);
  }

  @Mutation(() => ForgotPasswordResponse)
  async forgotPassword(
    @Arg('data') data: ForgotPasswordValidationInput,
    @Ctx() context: MyExpressContext
  ): Promise<ForgotPasswordResponse> {
    return this.userService.forgotPassword(context, data);
  }

  @Mutation(() => OtpResponse)
  async sendChangePasswordSms(
    @Arg('data') data: SendChangePasswordSmsValidationInput,
    @Ctx() context: MyExpressContext
  ): Promise<OtpResponse> {
    return this.userService.sendChangePasswordSms(context, data);
  }

  @Mutation(() => ForgotPasswordResponse)
  async forgotPasswordOtpValidation(
    @Arg('data') data: OtpValidationInput,
    @Ctx() context: MyExpressContext
  ): Promise<ForgotPasswordResponse> {
    return this.userService.forgotPasswordOtpValidation(context, data);
  }

  @Mutation(() => String)
  async resetPassword(
    @Arg('data') data: ResetPasswordInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.resetPassword(context, data);
  }

  @Mutation(() => String)
  async registerDevice(
    @Arg('type') type: string,
    @Arg('country', { nullable: true }) country: string,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.registerDevice(context, type, country);
  }

  @Query(() => Boolean)
  async isDeviceRegistered(@Ctx() context: MyExpressContext): Promise<Boolean> {
    return this.userService.isDeviceRegistered(context);
  }

  @Mutation(() => String)
  async resendOtp(
    @Arg('data') data: ResendOtpInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.resendOtp(context, data);
  }

  @Mutation(() => OtpResponse)
  async sendActivationOtp(
    @Arg('data') data: ActivateCardValidationInput,
    @Ctx() context: MyExpressContext
  ): Promise<OtpResponse> {
    return this.userService.sendActivationOtp(context, data);
  }

  @Mutation(() => OtpResponse)
  async validateActivationOtp(
    @Arg('data') data: OtpValidationInput,
    @Ctx() context: MyExpressContext
  ): Promise<OtpResponse> {
    return this.userService.validateActivationOtp(context, data);
  }

  @Query(() => PortfolioStatement)
  async portfolioStatementData(
    @Arg('data') data: portfolioStatementInput,
    @Ctx() context: MyExpressContext
  ): Promise<PortfolioStatement> {
    return this.userService.portfolioStatementData(context, data);
  }

  @Query(() => String)
  async portfolioStatement(
    @Arg('data') data: portfolioStatementInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.portfolioStatement(context, data);
  }

  @Mutation(() => String)
  async onboardingCustomer(
    @Arg('data') data: OnboardingCustomer,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.onboardingCustomer(context, data);
  }

  @Mutation(() => String)
  async tokenAppLog(
    @Arg('data') data: TokenAppLogInput,
    @Ctx() context: MyExpressContext
  ): Promise<String> {
    return this.userService.tokenAppLog(context, data);
  }

  @Query(() => [RegistrationCountries])
  async registrationCountries(@Ctx() context: MyExpressContext): Promise<RegistrationCountries[]> {
    return this.userService.getRegistrationCountries(context);
  }
}

import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';

import { Request } from 'express';

declare interface MyExpressContext extends ExpressContext {
  req: Request & { correlationId?: string };
}

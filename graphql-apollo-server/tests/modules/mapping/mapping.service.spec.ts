import * as service from '../../../src/modules/mapping/mapping.service';
import fetchMock from 'jest-fetch-mock';

import { MyExpressContext } from '../../../src/types/app-context';

const expressContext = {
  req: {
    correlationId: '64e89cf0-e338-4bbe-812d-c75170ecb672',
    headers: {
      'accept-language': 'en'
    }
  },
  res: {}
} as MyExpressContext;

const mappingService = new service.MappingService();

describe('List Account names mapping', () => {
  it('Should return success', async () => {
    const data = [
      {
        key: 'CurrAcc Demand',
        value: 'Current account demand'
      },
      {
        key: 'VS PLATINUM SEC',
        value: 'Visa Platinum sec'
      }
    ];
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const accountMappings = await mappingService.accountNames(expressContext);
    expect(accountMappings).toEqual(data);
  });
});

describe('List Card mapping', () => {
  it('Should return success', async () => {
    const data = [
      {
        'E-proofNumber': 5002,
        BIN: 530509,
        cardName: 'MasterCard Debit Card',
        cardType: 'Debit Card',
        EGP: 60,
        USD: 3
      },
      {
        'E-proofNumber': 4524,
        BIN: 454835,
        cardName: 'Visa Gold Credit Card',
        cardType: 'Credit Card',
        EGP: 225,
        USD: 10
      }
    ];
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const cardMappings = await mappingService.cardMapping(expressContext);
    expect(cardMappings).toEqual(data);
  });
});

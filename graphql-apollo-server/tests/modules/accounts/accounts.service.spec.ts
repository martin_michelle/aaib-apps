import * as service from '../../../src/modules/accounts/accounts.service';
import fetchMock from 'jest-fetch-mock';

import { MyExpressContext } from '../../../src/types/app-context';
import { UserInputError } from 'apollo-server-express';

const expressContext = {
  req: {
    correlationId: '64e89cf0-e338-4bbe-812d-c75170ecb672',
    headers: {}
  },
  res: {}
} as MyExpressContext;

const accountService = new service.AccountsService();

describe('List Accounts', () => {
  it('Should return success', async () => {
    const data = {
      accounts: [
        {
          AccID: '123456789',
          CustomerName: 'Short.name',
          Category: 'CARD DIF',
          Currency: 'EGP',
          OpenDate: '20201022',
          IBAN: 'ALT123456789',
          WorkingBalance: 0,
          AvailableBalance: 0,
          LockedAmount: 0,
          InterestRate: 0,
          StatementType: 'Send Mail',
          StatementFreq: 'Monthly',
          CreditCard: '',
          Status: 'ACTIVE'
        }
      ],
      balance: {
        amount: 0,
        currency: 'EGP'
      }
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const accounts = await accountService.accounts(expressContext);
    expect(accounts).toEqual(data);
  });

  it('Should return User input error', async () => {
    const message = 'No Data Found!';
    fetchMock.mockResponseOnce(
      JSON.stringify({
        message
      })
    );
    await expect(accountService.accounts(expressContext)).rejects.toThrow(UserInputError);
  });
});

describe('Account Details', () => {
  it('Should return success', async () => {
    const data = {
      currentAccount: {
        number: '123456789',
        name: 'SAVING AC MONTH'
      },
      generalInformation: {
        accountName: 'SAVING AC MONTH',
        openingDate: '20201022',
        accountNumber: '123456789',
        status: 'ACTIVE',
        iban: 'ALT123456789',
        cardAssigned: ''
      },
      accountPlan: {
        annualInterest: 7.5
      },
      balanceDetails: {
        availableBalance: {
          amount: 0,
          currency: 'EGP'
        },
        currentBalance: {
          amount: 0,
          currency: 'EGP'
        },
        holdAmount: {
          amount: 0,
          currency: 'EGP'
        }
      },
      statementDetails: {
        statementType: 'Electronic',
        statementFrequency: 'Quartely'
      }
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const accountDetails = await accountService.accountDetails(expressContext, {
      AccID: '123456789'
    });
    expect(accountDetails).toEqual(data);
  });
});

describe('Account Transactions', () => {
  it('Should return success', async () => {
    const data = {
      transactions: [
        {
          TransCode: '383',
          AccID: '123456789',
          BookingDate: '20210429',
          TransRef: '123456789-20210430',
          Description: 'Credit Interest (CR2)',
          ValueDate: '20210501',
          Withdrawals: 0,
          Deposits: 20.96,
          Balance: 398.63,
          Currency: 'EGP',
          Timestamp: '20210430.0010',
          isDbtCrdTrans: false,
          Status: 'Completed',
          correlationId: '123456789-20210430-20210430.0010'
        }
      ]
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const transactions = await accountService.transactions(expressContext, {
      AccID: '1010021560010201',
      fromDate: '20210101',
      toDate: '20210601',
      limit: 10,
      lastTransaction: '123456788-20210430-20210430.0010',
      search: 'credit'
    });
    expect(transactions).toEqual(data.transactions);
  });

  it('Should return empty response', async () => {
    const data = {};
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const transactions = await accountService.transactions(expressContext, {
      AccID: '1010021560010201',
      fromDate: '20210101',
      toDate: '20210601',
      limit: 10,
      lastTransaction: '123456789-20210430-20210430.0010',
      search: 'credit'
    });
    expect(transactions).toEqual([]);
  });
});

describe('Card Details', () => {
  it('Should return success', async () => {
    const data = [
      {
        Region: 'L',
        CardToken: '?C7L3YL2HX4KEXM5',
        CardNumber: '567890xxxxxx0001',
        CustomerNo: '10100616',
        BranchCode: 'HEA',
        CustomerName: 'Short.name',
        ExpiryDate: '20240131',
        CardStatus: 'ACTIVE',
        NameOnCard: 'Short.name',
        CashLimit: 50000,
        PurchaseLimit: 1000000,
        MobileNo: '01222222222',
        OnlinePayment: true,
        ATMWithdrawal: true,
        LimitProfile: 'AAIB CUS',
        CardType: 'MCD',
        LinkedAccounts: [
          {
            Number: '1010061660050201',
            Currency: 'EGP',
            Type: 'Savings',
            Status: 'A',
            Reason: ''
          }
        ]
      }
    ];
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const debitCardDetails = await accountService.debitCardDetails(expressContext, '123456789');
    expect(debitCardDetails).toEqual(data);
  });
});

describe('Transfer', () => {
  it('Own account transfer - Should return success', async () => {
    const data = {
      currency: 'EGP',
      debitBalance: 79990.77,
      creditBalance: 96638.65,
      transactionId: 'FT21124BMHVK'
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const transferDetails = await accountService.ownAccountTransfer(expressContext, {
      debitAcc: '1010011110010201',
      creditAcc: '1010011110010202',
      amount: 2,
      description: 'test'
    });
    expect(transferDetails).toEqual(data);
  });

  it('Internal account transfer - Should return success', async () => {
    const data = {
      currency: 'EGP',
      debitBalance: 79988.77,
      transactionId: 'FT21124BMHHQ'
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const transferDetails = await accountService.internalTransfer(expressContext, {
      debitAcc: '1020202310010201',
      creditAcc: '1021069910010201',
      amount: 2,
      currency: 'EGP',
      description: 'internal transfer',
      swift: 'ARAIEGCXXXX',
      beneficiaryName: 'test',
      chargeType: 'SHA',
      otp: '123456',
      country: 'Egypt'
    });
    expect(transferDetails).toEqual(data);
  });
});

describe('Loans', () => {
  it('List Loans - Should return success', async () => {
    const data = {
      loans: [
        {
          ID: 'LD2030039097',
          Company: 'EG0010001',
          Category: '21055',
          CategoryDescription: 'Unsecure Retail Loans',
          LDType: '10008',
          LDTypeDescription: 'CASH LOAN SALARIED AAIB STAFF',
          Outstanding: 72779.9,
          TotalPaid: 27220.100000000006,
          Currency: 'EGP',
          InterestRate: 15,
          AgreementDate: 20190214,
          ValueDate: 20201003,
          MaturityDate: 20240803,
          NextPaymentAmount: 1459.39,
          NextPaymentDate: 20210603,
          AccountNumber: '1010099860010201',
          PaymentStatus: 'CUR',
          DueDate: 20201203,
          Frequency: 'Monthly',
          Status: 'CUR',
          NoDayOverdue: 0,
          Penalty: 0,
          TotalOverDueAmount: 0
        }
      ],
      loansData: {
        amount: 0,
        currency: 'EGP'
      }
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const loans = await accountService.loans(expressContext);
    expect(loans).toEqual(data.loans);
  });

  // it('List Loans - Should return empty array', async () => {
  //   const data = {};
  //   fetchMock.mockResponseOnce(
  //     JSON.stringify({
  //       data
  //     })
  //   );
  //   const loans = await accountService.loans(expressContext);
  //   expect(loans).toEqual([]);
  // });
});

describe('Debit Card Transactions', () => {
  it('Should return success', async () => {
    const data = {
      transactions: [
        {
          TransCode: '383',
          AccID: '123456789',
          BookingDate: '20210429',
          TransRef: '123456789-20210430',
          Description: 'POS Purchase',
          ValueDate: '20210501',
          Withdrawals: 20,
          Deposits: 0,
          Balance: 390,
          Currency: 'EGP',
          Timestamp: '20210430.0010',
          Status: 'Completed',
          correlationId: '123456789-20210430-20210430.0010'
        }
      ]
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const transactions = await accountService.debitCardTransactions(expressContext, {
      AccID: '1010021560010201',
      fromDate: '20210101',
      toDate: '20210601',
      limit: 10,
      lastTransaction: '123456788-20210430-20210430.0010',
      search: 'POS'
    });
    expect(transactions).toEqual(data.transactions);
  });

  it('Should return empty response', async () => {
    const data = {};
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const transactions = await accountService.debitCardTransactions(expressContext, {
      AccID: '1010021560010201',
      fromDate: '20210101',
      toDate: '20210601',
      limit: 10,
      lastTransaction: '123456789-20210430-20210430.0010',
      search: 'ATM'
    });
    expect(transactions).toEqual([]);
  });
});

describe('Account Usage', () => {
  it('Should return success', async () => {
    const data = {
      accountUsage: {
        Start: 20210501,
        End: 20211021,
        TotalCreditM1: 0,
        TotalDebitM1: 1.78,
        TotalCreditM2: 0,
        TotalDebitM2: 1.69,
        TotalCreditM3: 0,
        TotalDebitM3: 16.66,
        TotalCreditM4: 0,
        TotalDebitM4: 1.95,
        TotalCreditM5: 0,
        TotalDebitM5: 1.99,
        TotalCreditM6: 2007202.15,
        TotalDebitM6: 582415.11
      }
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const accountUsage = await accountService.accountUsage(expressContext, '123456789');
    expect(accountUsage).toEqual(data);
  });
});

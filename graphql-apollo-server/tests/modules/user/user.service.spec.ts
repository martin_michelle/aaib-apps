import * as service from '../../../src/modules/user/user.service';
import fetchMock from 'jest-fetch-mock';

import { MyExpressContext } from '../../../src/types/app-context';
import { AuthenticationError } from 'apollo-server-express';
import { ApolloError } from 'apollo-server-errors';

const expressContext = {
  req: {
    correlationId: '64e89cf0-e338-4bbe-812d-c75170ecb673',
    headers: {}
  },
  res: {}
} as MyExpressContext;

const userService = new service.UserService();

describe('User Login', () => {
  it('Should return success', async () => {
    const data = {
      login: {
        user: {
          id: '12344',
          name: 'test user'
        },
        tokens: {
          accessToken: 'token',
          refreshToken: '12345678'
        }
      }
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const login = await userService.login(expressContext, { user: 'test', password: 'password' });
    expect(login).toEqual(data);
  });

  it('Should return Duplicate Session failure', async () => {
    const message = 'Duplicate session';
    fetchMock.mockResponseOnce(
      JSON.stringify({
        message
      }),
      { status: 601 }
    );
    await expect(
      userService.login(expressContext, { user: 'test', password: 'password' })
    ).rejects.toThrow(ApolloError);
  });

  it('Should return Expired Password failure', async () => {
    const message = 'Duplicate session';
    fetchMock.mockResponseOnce(
      JSON.stringify({
        message
      }),
      { status: 602 }
    );
    await expect(
      userService.login(expressContext, { user: 'expired', password: 'password' })
    ).rejects.toThrow(ApolloError);
  });
});

describe('Refresh Token', () => {
  it('Should return success', async () => {
    const data = {
      refresh: {
        user: {
          id: '12344',
          name: 'test user'
        },
        tokens: {
          accessToken: 'token',
          refreshToken: '123456788'
        }
      }
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const refresh = await userService.refresh(expressContext, '123456789');
    expect(refresh).toEqual(data);
  });
});

describe('Revoke token', () => {
  it('Should return success', async () => {
    const data = {
      message: 'Success'
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const revoke = await userService.revokeToken(expressContext);
    expect(revoke).toStrictEqual(data.message);
  });

  it('Should return Authentication failure', async () => {
    const message = 'Authentication failure';
    fetchMock.mockResponseOnce(
      JSON.stringify({
        message
      }),
      { status: 401 }
    );
    await expect(userService.revokeToken(expressContext)).rejects.toThrow(AuthenticationError);
  });
});

describe('Reset expired password', () => {
  it('Should return success', async () => {
    const data = {
      message: 'Success'
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const resetExpiredPasswordResponse = await userService.resetExpiredPassword(expressContext, {
      username: 'test',
      oldPassword: 'oldPassword',
      newPassword: 'newPassword'
    });
    expect(resetExpiredPasswordResponse).toStrictEqual(data.message);
  });
});

describe('Change password', () => {
  it('Should return success', async () => {
    const data = {
      message: 'Success'
    };
    fetchMock.mockResponseOnce(
      JSON.stringify({
        data
      })
    );
    const changePasswordResponse = await userService.changePassword(expressContext, {
      oldPassword: 'oldPassword',
      newPassword: 'newPassword',
      otp: '121213'
    });
    expect(changePasswordResponse).toStrictEqual(data.message);
  });
});

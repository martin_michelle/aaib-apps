import { Consumer } from 'kafkajs';
import express from 'express';
import { getHeartbeatTime } from '../kafka/consumer';
import { kafkaOptions } from '../config';
const router = express.Router();

const SESSION_TIMEOUT = kafkaOptions.consumerPolicy.sessionTimeout;
let consumer: Consumer;

let lastHeartbeat: number;

export const healthCheck = async () => {
  try {
    const options: { consumer: Consumer; interval: number } = await getHeartbeatTime();
    consumer = options.consumer;
    lastHeartbeat = options.interval;
    if (Date.now() - lastHeartbeat < SESSION_TIMEOUT) return true;
    const { state } = await consumer.describeGroup();
    const includes = ['CompletingRebalance', 'PreparingRebalance', 'Stable'].includes(state);
    if (includes) return true;
    else throw new Error();
  } catch (e) {
    throw new Error();
  }
};

router.get('/', async (req, res) => {
  try {
    const result = await healthCheck();
    res.send({
      statusCode: '10000',
      message: 'Success',
      data: result
    });
  } catch (e) {
    res.status(500).send({ message: 'Internal Error' });
  }
});

export default router;

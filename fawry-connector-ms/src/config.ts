// Mapper for environment variables
export const environment = process.env.NODE_ENV;
export const port = process.env.PORT;

export const corsUrl = process.env.CORS_URL;

export const kafkaOptions = {
  clientId: process.env.KAFKA_CLIENT_ID || '',
  hosts: process.env.KAFKA_HOSTS?.split(',') || [],
  connectionTimeout: parseInt(process.env.KAFKA_CONNECTION_TIMEOUT || '3000'),
  requestTimeout: parseInt(process.env.KAFKA_REQUEST_TIMEOUT || '25000'),
  initialRetryTime: parseInt(process.env.KAFKA_INITIAL_RETRY_TIME || '0'),
  retries: parseInt(process.env.KAFKA_RETRIES || '0'),
  producerPolicy: {
    allowAutoTopicCreation: true
  },
  consumerPolicy: {
    groupId: process.env.CONSUMER_GROUP_ID || 'fawry-connector-ms',
    maxWaitTimeInMs: Number(process.env.CONSUMER_MAX_WAIT_TIME || '100'),
    allowAutoTopicCreation: true,
    sessionTimeout: Number(process.env.CONSUMER_SESSION_TIMEOUT || '30000'),
    heartbeatInterval: Number(process.env.CONSUMER_HEART_BEAT || '3000'),
    retry: {
      retries: parseInt(process.env.KAFKA_RETRIES || '0')
    }
  },
  numberOfPartitions: Number(process.env.KAFKA_PARTITION_NUMBER || '3')
};

export const kafkaTopics = {
  fawryRequest: 'fawryRequest'
};

export const fawryBaseUrl = process.env.FAWRY_BASE_URL;

export const axiosTimeOut = parseInt(process.env.AXIOS_TIMEOUT || '15000');

export const defaultErrorMessage = `Fawry adapter call failed with status`;

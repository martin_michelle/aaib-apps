import { Consumer, Kafka } from 'kafkajs';
import { kafkaOptions, kafkaTopics } from '../config';
import { logger } from '../core/Logger';
import { ClientKafka } from '../libs/kafka-partition';
import { handleT24Request } from '../services';

let consumer: Consumer;
let client: ClientKafka;
let interval: number;
export const connector = () => {
  return new Kafka({
    clientId: kafkaOptions.clientId,
    brokers: kafkaOptions.hosts,
    connectionTimeout: kafkaOptions.connectionTimeout,
    requestTimeout: kafkaOptions.requestTimeout,
    retry: {
      initialRetryTime: kafkaOptions.initialRetryTime,
      retries: kafkaOptions.retries
    }
  });
};

export const createKafkaClient = () => {
  if (client) return client;
  return new ClientKafka({
    consumer: {
      groupId: kafkaOptions.consumerPolicy.groupId,
      maxWaitTimeInMs: kafkaOptions.consumerPolicy.maxWaitTimeInMs
    },
    client: {
      clientId: kafkaOptions.clientId,
      brokers: kafkaOptions.hosts,
      connectionTimeout: kafkaOptions.connectionTimeout,
      requestTimeout: kafkaOptions.requestTimeout,
      retry: {
        initialRetryTime: kafkaOptions.initialRetryTime,
        retries: kafkaOptions.retries
      }
    }
  });
};

export const createConsumer = async () => {
  try {
    client = createKafkaClient();
    consumer = await client.connect();
    await client.createKafkaTopic(kafkaTopics.fawryRequest, kafkaOptions.numberOfPartitions);
    await consumer.connect();
    await consumer.subscribe({ topic: kafkaTopics.fawryRequest });
    consumer.on(consumer.events.HEARTBEAT, ({ timestamp }) => {
      interval = timestamp;
    });
    await consumer.run({
      eachMessage: async ({ topic, message }) => {
        if (
          !message.headers ||
          !message.headers.correlationId ||
          !message.headers.replyTopic ||
          !message.headers.partitionReply ||
          !message.value
        ) {
          logger.error('Missing required fields');
        } else if (topic === kafkaTopics.fawryRequest) {
          handleT24Request({
            correlationId: message.headers.correlationId.toString(),
            replyTopic: message.headers.replyTopic.toString(),
            partitionReply: Number(message.headers.partitionReply),
            data: message.value.toString(),
            tracingHeaders: message.headers.tracingHeaders
              ? JSON.parse(message.headers.tracingHeaders.toString())
              : {}
          });
        }
      }
    });
  } catch (e) {
    logger.debug(e.message);
  }
};

export const disconnect = async () => {
  client = createKafkaClient();
  return client.close();
};

export const getHeartbeatTime = async () => {
  return {
    consumer,
    interval
  };
};

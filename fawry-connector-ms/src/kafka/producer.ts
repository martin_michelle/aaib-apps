import { Kafka, Producer } from 'kafkajs';
import { kafkaOptions } from '../config';
let producer: Producer;

export const connector = () => {
  return new Kafka({
    clientId: kafkaOptions.clientId,
    brokers: kafkaOptions.hosts,
    connectionTimeout: kafkaOptions.connectionTimeout,
    requestTimeout: kafkaOptions.requestTimeout,
    retry: {
      initialRetryTime: kafkaOptions.initialRetryTime,
      retries: kafkaOptions.retries
    }
  });
};

export const createProducer = async () => {
  if (producer) return producer;
  const kafka = connector();
  producer = kafka.producer(kafkaOptions.producerPolicy);
  await producer.connect();
  return producer;
};

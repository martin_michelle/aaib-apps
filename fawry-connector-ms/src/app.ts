import express from 'express';
import { logger } from './core/Logger';
import cors from 'cors';
import helmet from 'helmet';
import { corsUrl } from './config';
import heartbeat from './health';
import { createConsumer, disconnect } from './kafka/consumer';

process.on('uncaughtException', (e) => {
  logger.error(e);
});

createConsumer().then();

const app = express();

// Security
app.use(helmet());

app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ limit: '10mb', extended: true, parameterLimit: 50000 }));
app.use(cors({ origin: corsUrl, optionsSuccessStatus: 200 }));

process.on('SIGINT', async () => {
  await disconnect();
  process.exit(1);
});

app.use('/health', heartbeat);

export default app;

import { Request } from 'express';

declare interface PublicRequest extends Request {
  apiKey: string;
}

declare interface RoleRequest extends PublicRequest {
  currentRoleCode: string;
}

declare interface ProtectedRequest extends RoleRequest {
  user: string;
  accessToken: string;
}

declare interface ConsumerHeader {
  correlationId: string;
  replyTopic: string;
  partitionReply: number;
  data: string;
  tracingHeaders?: any;
}

export default {
  authorization: {
    notFound: {
      en: 'Authorization must be provided'
    },
    invalid: {
      en: 'Invalid token'
    }
  },
  kafka: {
    notFound: {
      en: 'This server does not host this topic-partition'
    }
  }
};

import { createProducer } from '../kafka/producer';
import { CompressionTypes, ProducerRecord } from 'kafkajs';
import axios, { AxiosResponse } from 'axios';
import { logger } from '../core/Logger';
import { fawryBaseUrl, axiosTimeOut, defaultErrorMessage } from '../config';
import { ConsumerHeader } from 'app-request';

export const handleT24Request = async (credentials: ConsumerHeader) => {
  const data = JSON.parse(credentials.data);
  try {
    let axiosData: AxiosResponse;
    if (!data.method) data.method = 'get';
    if (data.method === 'post') {
      axiosData = await axios.post(`${fawryBaseUrl}${data.route}`, data.body, {
        timeout: axiosTimeOut,
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.authorization || '',
          'x-correlation-id': credentials.correlationId,
          ...credentials.tracingHeaders
        }
      });
    } else {
      axiosData = await axios.get(`${fawryBaseUrl}${data.route}`, {
        timeout: axiosTimeOut,
        headers: {
          Authorization: data.authorization || '',
          'x-correlation-id': credentials.correlationId,
          ...credentials.tracingHeaders
        }
      });
    }
    if (axiosData.data && axiosData.data.Status) {
      const response = {
        error: false,
        message: axiosData.data
      };
      const producer = await createProducer();
      const producerOptions: ProducerRecord = {
        topic: credentials.replyTopic.toString(),
        compression: CompressionTypes.GZIP,
        messages: [
          {
            value: JSON.stringify(response),
            partition: credentials.partitionReply,
            headers: {
              correlationId: credentials.correlationId
            }
          }
        ]
      };
      await producer.send(producerOptions);
      logger.info(data.route, { correlationId: credentials.correlationId, status: 200 });
    } else {
      logger.error(`${defaultErrorMessage} ${500} ${data.method || 'get'} ${data.route}`, {
        correlationId: credentials.correlationId
      });
      logger.debug(data.route, {
        correlationId: credentials.correlationId,
        error: axiosData.data.Message,
        status: 500
      });
      await handleT24Errors(credentials, axiosData.data.Message);
    }
  } catch (e) {
    logger.error(
      `${defaultErrorMessage} ${e.response ? e.response.status : 500} ${data.method || 'get'} ${
        data.route
      }`,
      {
        correlationId: credentials.correlationId
      }
    );
    logger.debug(data.route, {
      correlationId: credentials.correlationId,
      error: e.message,
      status: e.response ? e.response.status : 500
    });
    await handleT24Errors(credentials, e.message);
  }
};

export const handleT24Errors = async (credentials: ConsumerHeader, error: string) => {
  const producer = await createProducer();
  const data = {
    error: true,
    message: error
  };
  const producerOptions: ProducerRecord = {
    topic: credentials.replyTopic.toString(),
    compression: CompressionTypes.GZIP,
    messages: [
      {
        value: JSON.stringify(data),
        partition: credentials.partitionReply,
        headers: {
          correlationId: credentials.correlationId
        }
      }
    ]
  };
  await producer.send(producerOptions);
};

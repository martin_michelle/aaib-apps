const mongoose = require('mongoose');
const axios = require('axios');
const https = require('https');
const allSettled = require('promise.allsettled');

const url = 'mongodb://0.0.0.0:27017/AAIB';

const agent = new https.Agent({
    rejectUnauthorized: false
});
  
const generateAdminToken = async () => {
    try {
      const body = { user: 'mobapptestuser', password: 'Aaib12345' };
  
      const url = 'https://t24apis.dev.digital.aaib/hid/generatetoken';
  
      const result = await axios.post(url, body,{
        timeout: 200000 ,
        httpsAgent: agent,
        headers: {}
      });
  
      return String(result.data?.tokens?.accessToken || '');
    } catch (e) {
      console.log(e);
      return false
    }
  };

async function getUserInfo(cif, adminToken) {

  const url = `https://t24apis.dev.digital.aaib/digital/userinfo/customer/${cif}`;
  console.log(`Start deal with CIF ${cif}`);

  const headers = {
    'accept': 'application/json',
    'Authorization': `Berear ${adminToken}`
  };

  try {
    const response = await axios.get(url, {
        timeout: 200000 ,
        httpsAgent: agent,
        headers
      });


    return [response?.data?.Data[0]?.Id, response?.data?.Data[0]?.country];
  } catch (error) {
    return [false, false]
  }
}

async function mapAllCifs(arrayToProcess, adminToken) {
  const promises = arrayToProcess.map(async (element) => {
    try {
      const cif = element;
      const [userName, country] = await getUserInfo(cif, adminToken);
      const user = {cif, userName, country};
      return user;
    } catch (err) {
      return {status: 'rejected', reason: err};
    }
  });

  const results = await allSettled(promises);

  const updatedArray = results.map(result => {
    if (result.status === 'fulfilled') {
      return result.value;
    } else {
      console.error(result.reason);
      return false;
    }
  });
  return updatedArray;
}

(async () => {
  try {
    const adminToken = await generateAdminToken();

    if(!adminToken) {
        throw new Error('failed to login as admin')
    }

    await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });

    const collection = mongoose.connection.collection('beneficiaries');

    const CifsOfCustomers = await collection.distinct('cif'); 

    const allUsers = await mapAllCifs(CifsOfCustomers, adminToken)

    const validUsers = allUsers.filter(u => {
      if(u.userName) return u
    })

    const inValidUsers = allUsers.filter(u => {
      if(!u.userName) return u
    })

    const operations = validUsers.map(obj => ({
      updateMany: {
        filter: { cif: obj.cif },
        update: { $set: { userName: obj.userName, country: obj.country } }
      }
    }));

    const result = await collection.bulkWrite(operations);

    console.log(result);

    mongoose.connection.close();
  } catch (err) {
    console.error(err);
  }
})();

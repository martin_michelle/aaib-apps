import * as kafka from '../../src/kafka';
export const produceResult = {
  creditCards: {
    value:
      '{"error": false, "message": { "Data": [{"Id": "1002275711570202", "Balance": 1000, "AccountCurrency": "EGP", "OnlineData": { "AvailableBalance": "1000" } }] }}'
  },
  conversion: {
    value:
      '{"error": false, "message": { "Data": [{"CurrencyCode": "EGP", "TransferBuyRate": 1}] }}'
  },
  transactions: {
    value:
      '{"error": false, "message": { "Data": [{"Amount": 500, "Currency": "EGP", "TrsDate": "20210901", "TrsTime": "112224", "TerminalLocation": "test", "TerminalClass": "ATM", "ApprovalNo": "350003"  }] }}'
  },
  creditTransfer: {
    value: '{"error": false, "message": { "TransactionId": "FT212731L24N-8851984-412050" }}'
  },
  getSettings: {
    value:
      '{"error": false, "message": { "Data": [{"CardNumber": "1234","eCommerce": "1234", "eComOnUntil": "1234", "Purchase": "1234","PurchaseOnUntil": "1234" }] }}'
  },
  default: {
    value: '{"error": false, "message": { "Data": [] }}'
  }
};

export const creditCardsResult = {
  cards: [
    {
      Id: '1002275711570202',
      Balance: 1000,
      AccountCurrency: 'EGP',
      OnlineData: {
        AvailableBalance: '1000'
      }
    }
  ],
  balance: {
    amount: 1000,
    currency: 'EGP'
  }
};

export const creditCardTransactions = {
  transactions: [
    {
      ApprovalNo: '350003',
      Amount: 500,
      Currency: 'EGP',
      TrsDate: '20210901',
      TrsTime: '112224',
      TerminalClass: 'ATM',
      TerminalLocation: 'test',
      timestamp: '20210901.112224',
      correlationId: '350003-20210901.112224',
      description: 'ATM test'
    }
  ]
};

export const creditCardSettings = {
  CardNumber: '1234',
  eCommerce: '1234',
  eComOnUntil: '1234',
  Purchase: '1234',
  PurchaseOnUntil: '1234'
};

export const mockKafkaSuccess = () => {
  jest.spyOn(kafka, 'createConsumer');
  const mock = jest.spyOn(kafka, 'produce');
  mock.mockImplementation(async (data) => {
    switch (data.route) {
      case 'creditcards/customer/10100215':
        return Promise.resolve(produceResult.creditCards);
      case 'rates/exchange/EGP':
        return Promise.resolve(produceResult.conversion);
      case 'creditcards/txnsbyperiod/12345/20210101/20210909':
        return Promise.resolve(produceResult.transactions);
      case 'creditcards/setstatus':
        return Promise.resolve(produceResult.default);
      case 'txn/pay/creditcard':
        return Promise.resolve(produceResult.creditTransfer);
      case 'creditcards/settings/1234':
        return Promise.resolve(produceResult.getSettings);
      case 'creditcards/setsetting':
        return Promise.resolve(produceResult.default);
      case 'creditcards/travelwindow/enablespecific':
        return Promise.resolve(produceResult.default);
      case 'creditcards/travelwindow/disable':
        return Promise.resolve(produceResult.default);
      case 'creditcards/contract/123123':
        return Promise.resolve(produceResult.default);
    }
  });
};

import * as service from '../../../src/services/cards';
import {
  mockKafkaSuccess,
  creditCardsResult,
  creditCardTransactions,
  creditCardSettings
} from '../../mocks/kafka';

describe('Get list of credit cards', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess();
    const response = await service.getCreditCardsPerAccount(10100215, '');
    expect(response).toStrictEqual(creditCardsResult);
  });
});

describe('Get list of credit cards transactions', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess();
    const response = await service.getCreditCardsTransactions(
      '12345',
      {
        fromDate: '20210101',
        toDate: '20210909',
        search: '',
        lastTransaction: '',
        limit: 2
      },
      ''
    );
    expect(response).toStrictEqual(creditCardTransactions);
  });
});

describe('Set card status', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess();
    const response = await service.updateCreditCardStatus(
      {
        Cif: '20210101',
        ContractId: '123123',
        Action: 'Open'
      },
      '',
      ''
    );
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('Own credit transfer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess();
    const response = await service.ownCreditTransfer(
      {
        CountryCode: 'EG',
        ContractId: '123456789',
        DebitAcc: '123456789',
        Cif: '10100111',
        Currency: 'EGP',
        Amount: 1
      },
      '',
      ''
    );
    expect(response).toStrictEqual({ transactionId: 'FT212731L24N-8851984-412050' });
  });
});

describe('Get credit card settings', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess();
    const response = await service.getCreditCardSettings('1234', '');
    expect(response).toStrictEqual(creditCardSettings);
  });
});

describe('Set card settings', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess();
    const response = await service.updateCreditCardSettings(
      {
        Cif: '20210101',
        Setting: 'eCommerce',
        ContractId: '123123',
        Action: 'ON_FOR_TIME'
      },
      '',
      ''
    );
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('Travel window enable', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess();
    const response = await service.enableTravelWindow(
      {
        Cif: '20210101',
        ContractId: '123123',
        CountriesList: ['lb'],
        StartDate: '20210101',
        EndDate: '20210102'
      },
      '',
      ''
    );
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

describe('Travel window disable', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('Should return success', async () => {
    mockKafkaSuccess();
    const response = await service.disableTravelWindow('123123', '20210101', '');
    expect(response).toStrictEqual({ message: 'Success' });
  });
});

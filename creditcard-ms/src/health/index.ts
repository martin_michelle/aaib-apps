import { Consumer } from 'kafkajs';
import express from 'express';
import asyncHandler from '../helpers/asyncHandler';
import { ProtectedRequest } from 'app-request';
import { SuccessResponse } from '../core/ApiResponse';
import { getHeartbeatTime } from '../kafka';
import { InternalError } from '../core/ApiError';
import { kafkaOptions } from '../config';
import { logger } from '../core/Logger';
const router = express.Router();

const SESSION_TIMEOUT = kafkaOptions.consumerPolicy.sessionTimeout;
let consumer: Consumer;

let lastHeartbeat: number;

export const healthCheck = async () => {
  try {
    const options: { consumer: Consumer; interval: number; partitionId: any } =
      await getHeartbeatTime();
    if (typeof options.partitionId === 'number') {
      consumer = options.consumer;
      lastHeartbeat = options.interval;
      if (Date.now() - lastHeartbeat < SESSION_TIMEOUT) return true;
      const { state } = await consumer.describeGroup();
      const includes = ['CompletingRebalance', 'PreparingRebalance', 'Stable'].includes(state);
      if (includes) return true;
      else throw new InternalError();
    } else throw new InternalError();
  } catch (e) {
    logger.error(e.message);
    throw new InternalError();
  }
};

router.get(
  '/',
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await healthCheck();
    new SuccessResponse('Success', result).send(res);
  })
);

export default router;

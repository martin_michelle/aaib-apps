// Mapper for environment variables

export const environment = process.env.NODE_ENV;
export const port = process.env.PORT;

export const corsUrl = process.env.CORS_URL;

export const kafkaOptions = {
  clientId: process.env.KAFKA_CLIENT_ID || '',
  hosts: process.env.KAFKA_HOSTS?.split(',') || [],
  connectionTimeout: parseInt(process.env.KAFKA_CONNECTION_TIMEOUT || '3000'),
  requestTimeout: parseInt(process.env.KAFKA_REQUEST_TIMEOUT || '25000'),
  initialRetryTime: parseInt(process.env.KAFKA_INITIAL_RETRY_TIME || '1'),
  retries: parseInt(process.env.KAFKA_RETRIES || '1'),
  producerPolicy: {
    allowAutoTopicCreation: true
  },
  consumerPolicy: {
    groupId: process.env.CONSUMER_GROUP_ID || 'creditcard-ms',
    maxWaitTimeInMs: Number(process.env.CONSUMER_MAX_WAIT_TIME || '100'),
    allowAutoTopicCreation: true,
    sessionTimeout: Number(process.env.CONSUMER_SESSION_TIMEOUT || '30000'),
    heartbeatInterval: Number(process.env.CONSUMER_HEART_BEAT || '3000'),
    retry: {
      retries: parseInt(process.env.KAFKA_RETRIES || '1')
    }
  },
  numberOfPartitions: Number(process.env.KAFKA_PARTITION_NUMBER || '3')
};

export const kafkaTopics = {
  niRequest: 'niRequest',
  creditResponse: 'creditResponse',
  statistics: 'statistics',
  validateTopic: 'validateTopic',
  kibana: 'kibanastatistics',
  smtp: 'smtp',
  t24Request: 't24Request',
  beneficiaryRequest: 'beneficiaryRequest'
};

export const t24Url = process.env.T24_BASE_URL;

export const transactionCodes = {
  ownAccountTransfer: {
    web: 'ACOI'
  },
  internalTransfer: {
    web: 'ACBI'
  },
  externalTransfer: {
    web: 'OTLI'
  },
  fawryPayment: {
    web: 'ACIF'
  },
  cardPayment: {
    web: 'ACCI'
  }
};

export const languages = {
  english: '1',
  arabic: '2'
};

export const customErrorTypes = {
  CardReplacementExistsError: 'CardReplacementExistsError'
};

export const promiseTimeout = parseInt(process.env.PROMISE_TIMEOUT || '20000');

export const defaultCurrency = 'EGP';

export const defaultCountry = 'EG';

export const defaultLanguage = 'en';

export const transOptions = {
  dateFormat: 'YYYYMMDD',
  duration: '3',
  countryCode: 'EG',
  currency: 'EGP',
  reference: 'Saved CC payment',
  defaultSwift: 'ARAIEGCXXXX'
};

export const mongoConfig = {
  url: process.env.DB_URL || ''
};

export const staticError = {
  failedCreditPayment: 'Failed payment - Failed payment for user {user}',
  timeoutCreditPayment: 'Failed payment - Timeout payment for user {user}'
};

export const statistics = {
  failedCreditPayment: 'failedCreditPayment',
  successCreditPayment: 'successCreditPayment',
  timeoutCreditPayment: 'timeoutCreditPayment'
};

export const status = {
  success: 'success',
  fail: 'fail',
  timeout: 'timeout'
};

export const deliveryMethod = {
  pickup: 'Pickup',
  delivery: 'Delivery'
};

export const adminCredentials = {
  username: process.env.ADMIN_USER,
  password: process.env.ADMIN_PASSWORD
};

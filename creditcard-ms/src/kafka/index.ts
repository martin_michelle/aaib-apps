import { CompressionTypes, Consumer, Kafka, Producer } from 'kafkajs';
import { kafkaOptions, kafkaTopics, promiseTimeout } from '../config';
import { EventEmitter } from 'events';
import { logger } from '../core/Logger';
import correlation from 'express-correlation-id';
import { ClientKafka } from '../libs/kafka-partition';
import { promiseWithTimeout } from '../helpers/asyncHandler';
import messages from '../messages';
let producer: Producer;
let consumer: Consumer;
let partitionId: number;
let client: ClientKafka;
let eventEmitter: EventEmitter;
let interval: number;

export const connector = () => {
  return new Kafka({
    clientId: kafkaOptions.clientId,
    brokers: kafkaOptions.hosts,
    connectionTimeout: kafkaOptions.connectionTimeout,
    requestTimeout: kafkaOptions.requestTimeout,
    retry: {
      initialRetryTime: kafkaOptions.initialRetryTime,
      retries: kafkaOptions.retries
    }
  });
};

export const createKafkaClient = () => {
  if (client) return client;
  return new ClientKafka({
    consumer: {
      groupId: kafkaOptions.consumerPolicy.groupId,
      maxWaitTimeInMs: kafkaOptions.consumerPolicy.maxWaitTimeInMs
    },
    client: {
      clientId: kafkaOptions.clientId,
      brokers: kafkaOptions.hosts,
      connectionTimeout: kafkaOptions.connectionTimeout,
      requestTimeout: kafkaOptions.requestTimeout,
      retry: {
        initialRetryTime: kafkaOptions.initialRetryTime,
        retries: kafkaOptions.retries
      }
    }
  });
};

export const createProducer = async () => {
  if (producer) return producer;
  const kafka = connector();
  producer = kafka.producer(kafkaOptions.producerPolicy);
  await producer.connect();
  return producer;
};

export const createConsumer = async () => {
  try {
    client = createKafkaClient();
    consumer = await client.connect();
    await client.createKafkaTopic(kafkaTopics.creditResponse, kafkaOptions.numberOfPartitions);
    await consumer.connect();
    await consumer.subscribe({ topic: kafkaTopics.creditResponse });
    const consumerAssignments: { [key: string]: number } = {};
    consumer.on(consumer.events.GROUP_JOIN, (data: any) => {
      Object.keys(data.payload.memberAssignment).forEach((memberId) => {
        consumerAssignments[memberId] = Math.min(...data.payload.memberAssignment[memberId]);
        partitionId = consumerAssignments[kafkaTopics.creditResponse];
      });
    });
    consumer.on(consumer.events.HEARTBEAT, ({ timestamp }) => {
      interval = timestamp;
    });
    await consumer.run({
      eachMessage: async ({ message }) => {
        if (!message.headers?.correlationId) {
          logger.error('Missing correlationId');
        } else {
          const eventEmitter = createEventEmitter();
          eventEmitter.emit(message.headers.correlationId.toString(), message);
        }
      }
    });
    return consumer;
  } catch (e) {
    logger.debug(e.message);
  }
};

export const createEventEmitter = () => {
  if (eventEmitter) return eventEmitter;
  eventEmitter = new EventEmitter();
  return eventEmitter;
};

export const produce = async (data: any, tracingHeaders: any, waitForResponse = true) => {
  if (!producer) producer = await createProducer();
  const correlationId = correlation.getId();
  const producerOptions = {
    topic: data.topic || kafkaTopics.niRequest,
    compression: CompressionTypes.GZIP,
    acks: 1,
    messages: [
      {
        value: JSON.stringify(data),
        headers: {
          correlationId,
          partitionReply: partitionId.toString(),
          replyTopic: kafkaTopics.creditResponse,
          authorization: data.authorization || '',
          route: data.apiRoute || '',
          method: data.apiMethod || '',
          token: data.token || '',
          function: data.function || '',
          params: data.params || '',
          ...(tracingHeaders ? { tracingHeaders: JSON.stringify(tracingHeaders) } : {})
        }
      }
    ]
  };
  await producer.send(producerOptions);
  if (waitForResponse) {
    const eventEmitter = createEventEmitter();
    const response = () => {
      return new Promise((resolve, reject) => {
        eventEmitter.once(correlationId || '', async (value) => {
          try {
            resolve(value);
          } catch (e) {
            logger.debug(e.message);
            reject(e);
          }
        });
      });
    };
    return await promiseWithTimeout(promiseTimeout, response, messages.cards.timeout.en);
  }
};

export const disconnect = async () => {
  client = createKafkaClient();
  return client.close();
};

export const getHeartbeatTime = async () => {
  return {
    consumer,
    interval,
    partitionId: partitionId === Infinity ? null : partitionId
  };
};

import express from 'express';
import cards from './cards/index';

const router = express.Router();

router.use('/credit', cards);

export default router;

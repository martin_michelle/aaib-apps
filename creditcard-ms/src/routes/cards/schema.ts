import Joi from 'joi';

export default {
  accountCards: {
    params: Joi.object().keys({
      account: Joi.string().required()
    })
  },
  creditTransactions: {
    params: Joi.object({
      contractId: Joi.string().required()
    }),
    query: Joi.object({
      fromDate: Joi.string().optional(),
      toDate: Joi.string().optional(),
      search: Joi.string().optional(),
      lastTransaction: Joi.string().optional(),
      limit: Joi.number().optional().min(1).max(100)
    })
  },
  setStatus: {
    body: Joi.object({
      ContractId: Joi.string().required(),
      Action: Joi.string().required().valid('Lost', 'Stolen', 'Open', 'Activate', 'Referral'),
      smsToken: Joi.when('Action', {
        is: 'Activate',
        then: Joi.string().required()
      }).when('Action', {
        is: 'Open',
        then: Joi.string().required()
      })
    })
  },
  creditPayment: {
    body: Joi.object({
      debitAcc: Joi.string().required(),
      contractId: Joi.string().required(),
      currency: Joi.string().required(),
      amount: Joi.number().required()
    })
  },
  getCreditCardSettings: {
    params: Joi.object({
      contractId: Joi.string().required()
    })
  },
  setCreditCardStatus: {
    body: Joi.object({
      ContractId: Joi.string().required(),
      Setting: Joi.string().required().valid('eCommerce', 'Purchase', 'Cash'),
      Action: Joi.string().required().valid('ON_FOR_TIME', 'ON_FOR_ONE', 'OFF')
    })
  },
  travelWindow: {
    body: Joi.object({
      ContractId: Joi.string().required(),
      CountriesList: Joi.array().items(Joi.string()).required(),
      StartDate: Joi.string().required(),
      EndDate: Joi.string().required()
    })
  },
  disableTravelWindow: {
    body: Joi.object({
      ContractId: Joi.string().required()
    })
  },
  getCreditCardDetails: {
    body: Joi.object({
      CardNumber: Joi.number().required()
    })
  },
  replaceCreditCard: {
    body: Joi.object({
      cardNumber: Joi.string().required()
    })
  },
  saveCreditCard: {
    body: Joi.object({
      cardNumber: Joi.string().required().length(16),
      name: Joi.string().required(),
      nickName: Joi.string().optional()
    })
  },
  transferToSavedCreditCardById: {
    params: Joi.object({
      id: Joi.string().required()
    }),
    body: Joi.object({
      amount: Joi.number().required().min(0),
      debitAcc: Joi.string().required(),
      description: Joi.string().optional().required(),
      currency: Joi.string().optional().allow(''),
      otp: Joi.string().required()
    })
  },
  transferToCreditCard: {
    body: Joi.object({
      cardNumber: Joi.string().required().length(16),
      name: Joi.string().required(),
      amount: Joi.number().required().min(0),
      debitAcc: Joi.string().required(),
      description: Joi.string().optional().required(),
      currency: Joi.string().optional().allow(''),
      otp: Joi.string().required()
    })
  },

  getCardByBenficiaryId: {
    params: Joi.object({
      beneficiaryId: Joi.string().required()
    })
  }
};

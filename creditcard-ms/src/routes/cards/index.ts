import express from 'express';
import { SuccessResponse } from '../../core/ApiResponse';
import asyncHandler from '../../helpers/asyncHandler';
import * as service from '../../services/cards';
import authenticate from '../../helpers/authenticate';
import { ProtectedRequest } from 'app-request';
import { validate } from 'express-validation';
import { defaultCountry } from '../../config';
import schema from './schema';
import generateAdminToken from '../../helpers/adminToken';
import tracingHeaders from '../../helpers/tracingHeaders';
import { InternalError } from '../../core/ApiError';

const router = express.Router();

router.get(
  '/',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getCreditCardsPerAccount(
      parseInt(req.user.cif),
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/transactions/:contractId',
  authenticate(),
  validate(schema.creditTransactions),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getCreditCardsTransactions(
      req.params.contractId,
      {
        fromDate: String(req.query.fromDate || ''),
        toDate: String(req.query.toDate || ''),
        search: String(req.query.search || '').toLowerCase(),
        lastTransaction: String(req.query.lastTransaction || ''),
        limit: Number(req.query.limit || '0')
      },
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/setstatus',
  authenticate(),
  validate(schema.setStatus),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.updateCreditCardStatus(
      {
        ContractId: req.body.ContractId,
        Action: req.body.Action,
        Cif: req.user.cif
      },
      req.user.id,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/payment',
  authenticate(),
  validate(schema.creditPayment),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.ownCreditTransfer(
      {
        CountryCode: defaultCountry,
        ContractId: req.body.contractId,
        DebitAcc: req.body.debitAcc,
        Cif: req.user.cif,
        Currency: req.body.currency,
        Amount: Number(req.body.amount)
      },
      req.headers.authorization || '',
      req.user.id,
      String(req.headers.platform),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/settings/:contractId',
  authenticate(),
  validate(schema.getCreditCardSettings),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getCreditCardSettings(
      req.params.contractId,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/settings',
  authenticate(),
  validate(schema.setCreditCardStatus),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.updateCreditCardSettings(
      {
        ContractId: req.body.ContractId,
        Setting: req.body.Setting,
        Action: req.body.Action,
        Cif: req.user.cif
      },
      req.user.id,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/travelWindow/enable',
  authenticate(),
  validate(schema.travelWindow),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.enableTravelWindow(
      {
        ContractId: req.body.ContractId,
        Cif: req.user.cif,
        CountriesList: req.body.CountriesList,
        StartDate: req.body.StartDate,
        EndDate: req.body.EndDate
      },
      req.user.id,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/travelWindow/disable',
  authenticate(),
  validate(schema.disableTravelWindow),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.disableTravelWindow(
      req.body.ContractId,
      req.user.cif,
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);
router.post(
  '/card',
  authenticate(),
  validate(schema.getCreditCardDetails),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const authorization = await generateAdminToken(tracingHeaders);
    const result = await service.getCreditCardDetails(
      req.body.CardNumber,
      `Bearer ${authorization}` || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/request',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.requestCreditCard(
      req.body,
      req.user.cif,
      req.user.id,
      req.user.phoneNumber,
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/replace',
  authenticate(),
  validate(schema.replaceCreditCard),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.replaceCreditCard(
      req.body,
      req.user.cif,
      req.user.id,
      req.user.phoneNumber,
      String(req.headers.platform),
      req.headers.authorization || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/canReplaceCard',
  authenticate(),
  validate(schema.replaceCreditCard),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.canReplaceCard(req.body.cardNumber, req.user.cif);
    new SuccessResponse('Success', result).send(res);
  })
);
router.post(
  '/savedCreditCard',
  authenticate(),
  validate(schema.saveCreditCard),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.saveCreditCard(
      req.body,
      req.user.cif,
      req.user.id,
      String(req.headers.platform)
    );
    new SuccessResponse('Success', result).send(res);
  })
);
router.delete(
  '/savedCreditCard/:id',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.deleteCreditCard(
      req.user.cif,
      req.user.id,
      req.params.id,
      String(req.headers.platform)
    );
    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/savedCreditCard',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getSavedCreditCards(req.user.cif);
    new SuccessResponse('Success', result).send(res);
  })
);

router.post(
  '/transferToSavedCard/:id',
  authenticate(),
  validate(schema.transferToSavedCreditCardById),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const authorization = await generateAdminToken(tracingHeaders);
    const result = await service.transferToSavedCreditCardById(
      req.params.id,
      req.user.cif,
      req.user.id,
      req.body,
      `Bearer ${authorization}` || '',
      String(req.headers.platform),
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);
router.post(
  '/transferToCreditCard',
  authenticate(),
  validate(schema.transferToCreditCard),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const authorization = await generateAdminToken(tracingHeaders);

    const result = await service.transferToCreditCard(
      req.user.cif,
      req.user.id,
      req.body,
      `Bearer ${authorization}` || '',
      String(req.headers.platform),
      tracingHeaders(req)
    );

    new SuccessResponse('Success', result).send(res);
  })
);

router.get(
  '/termsAndConditions',
  authenticate(),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const result = await service.getTermsAndConditions(req.headers['accept-language']);
    new SuccessResponse('Success', result).send(res);
  })
);
router.get(
  '/info/:beneficiaryId',
  authenticate(),
  validate(schema.getCardByBenficiaryId),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const authorization = await generateAdminToken(tracingHeaders);
    const result = await service.getCardByBenficiaryId(
      req.user.cif,
      req.params.beneficiaryId,
      `Bearer ${authorization}` || '',
      tracingHeaders(req)
    );
    new SuccessResponse('Success', result).send(res);
  })
);
export default router;

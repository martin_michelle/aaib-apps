export default {
  cards: {
    timeout: {
      en: 'Request Timeout'
    },
    missingValue: {
      en: 'Missing Value'
    },
    internalError: {
      en: 'Internal Server Error'
    },
    CardReplacementExists: {
      en: 'Card replacement already exists for this card'
    },
    CardAlreadyExists: {
      en: 'This Card already exists for this user'
    }
  },
  authorization: {
    notFound: {
      en: 'Authorization must be provided'
    },
    otpBlocked: {
      en: 'Your token is locked'
    },
    invalid: {
      en: 'Invalid token'
    }
  }
};

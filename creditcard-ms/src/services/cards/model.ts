import mongoose from 'mongoose';

const model = new mongoose.Schema(
  {
    cif: {
      type: String
    },
    isCreditCard: {
      type: Boolean
    },
    cardNumber: String
  },
  { timestamps: true, versionKey: false }
);
model.index({ cif: 1, cardNumber: 1 });
export default mongoose.model('cardReplacement', model);

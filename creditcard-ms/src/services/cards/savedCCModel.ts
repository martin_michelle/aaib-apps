import mongoose from 'mongoose';

const model = new mongoose.Schema(
  {
    cif: String,
    name: String,
    nickName: String,
    cardNumber: String
  },
  { timestamps: true }
);
model.index({ cif: 1, cardNumber: 1 });
export default mongoose.model('savedCreditCard', model);

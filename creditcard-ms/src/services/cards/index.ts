import { produce } from '../../kafka';
import {
  CardReplacementExistsError,
  ErrorType,
  InternalError,
  NotFoundError,
  ForbiddenError
} from '../../core/ApiError';
import { logger } from '../../core/Logger';
import { KafkaMessage } from 'kafkajs';
import { maskString } from '../../helpers/maskString';
import {
  defaultCurrency,
  staticError,
  transOptions,
  statistics,
  status,
  kafkaTopics,
  deliveryMethod,
  customErrorTypes,
  languages,
  defaultLanguage,
  transactionCodes
} from '../../config';
import moment from 'moment';
import updateStatistics from '../../helpers/statistics';
import { handlePagination } from '../../helpers/pagination';
import * as lodash from 'lodash';
import messages from '../../messages';
import CardReplacement from '../../services/cards/model';
import SavedCreditCardModel from '../../services/cards/savedCCModel';
import { termsAndConditions } from '../../helpers/termsAndConditions';

export const getCreditCardsPerAccount = async (
  user: number,
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `creditcards/customer/${user}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    let total = 0;
    const conversion = await getConversionRates(defaultCurrency, tracingHeaders);
    let amount: number;
    for (const item of result) {
      if (!item.OnlineData) item.OnlineData = {};
      if (item && item.OnlineData?.AvailableBalance) {
        amount = Number(item.OnlineData.AvailableBalance);
        if (item.AccountCurrency !== defaultCurrency) {
          total += amount * conversion[item.AccountCurrency];
        } else {
          total += amount;
        }
      }
    }
    return {
      cards: result,
      balance: {
        amount: Number(total.toFixed(2)),
        currency: defaultCurrency
      }
    };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(messages.cards.internalError.en);
  }
};

export const prepareKafkaResponse = async (message: KafkaMessage) => {
  if (!message.value) throw new InternalError(messages.cards.missingValue.en);
  const data = JSON.parse(message.value.toString());
  if (!data.error) {
    return data.message.Data || data.message.data || data.message;
  } else {
    logger.debug(data.message);
    throw new InternalError(messages.cards.internalError.en);
  }
};

export const getConversionRates = async (currency: string, tracingHeaders?: any) => {
  const data = {
    route: `rates/exchange/${currency}`,
    topic: 't24Request'
  };
  const producerData = await produce(data, tracingHeaders);
  const result = await prepareKafkaResponse(producerData);
  const conversion: { [key: string]: number } = {};
  for (const item of result) {
    conversion[item.CurrencyCode] = item.TransferBuyRate;
  }
  return conversion;
};

export const getCreditCardsTransactions = async (
  contractId: string,
  query: {
    fromDate: string;
    toDate: string;
    search: string;
    lastTransaction: string;
    limit: number;
  },
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    if (!query.toDate) query.toDate = moment().format(transOptions.dateFormat);
    if (!query.fromDate)
      query.fromDate = moment()
        .subtract(transOptions.duration, 'months')
        .format(transOptions.dateFormat);
    const data = {
      route: `creditcards/txnsbyperiod/${contractId}/${query.fromDate}/${query.toDate}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    let result = await prepareKafkaResponse(producerData);
    if (result && result.length)
      result.map((item: any) => {
        item.timestamp = `${item.TrsDate}.${item.TrsTime}`;
        item.correlationId = `${item.ApprovalNo}-${item.timestamp}`;
        item.description = `${item.TerminalClass} ${item.TerminalLocation}`;
      });
    if (query.search) result = handleSearch(result, query.search);
    result = lodash.orderBy(result, ['timestamp'], ['desc']);
    return {
      transactions: handlePagination(
        result,
        query.limit || 20,
        query.lastTransaction || '',
        'correlationId'
      )
    };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(messages.cards.internalError.en);
  }
};

export const handleSearch = (result: any, search: string) => {
  return lodash.filter(result, (item) => {
    return new RegExp('\\b' + search, 'gi').test(item.description);
  });
};

export const updateCreditCardStatus = async (
  body: {
    ContractId: string;
    Action: string;
    Cif: string;
  },
  user: string,
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `creditcards/setstatus`,
      method: 'post',
      authorization,
      body
    };
    const producerData = await produce(data, tracingHeaders);
    await prepareKafkaResponse(producerData);
    let cardNumber = await getCardByContractIdFullDetails(
      body.ContractId,
      authorization,
      tracingHeaders
    );
    cardNumber = cardNumber?.[0] ? cardNumber[0].CardNumber : '';
    updateStatistics('', {
      cif: body.Cif,
      user,
      type: 'creditCardActions',
      subType: 'updateCreditCardStatus',
      requestType: body.Action,
      date: new Date(),
      microservice: 'creditCard',
      source: 'card',
      contractId: body.ContractId,
      cardNumber,
      status: status.success
    }).catch((e: any) => logger.error(e.message));
    return { message: 'Success' };
  } catch (e) {
    let cardNumber = await getCardByContractIdFullDetails(
      body.ContractId,
      authorization,
      tracingHeaders
    );
    cardNumber = cardNumber?.[0] ? cardNumber[0].CardNumber : '';
    updateStatistics('', {
      cif: body.Cif,
      user,
      type: 'creditCardActions',
      subType: 'updateCreditCardStatus',
      requestType: body.Action,
      date: new Date(),
      microservice: 'creditCard',
      source: 'card',
      contractId: body.ContractId,
      cardNumber,
      status: status.fail
    }).catch((e: any) => logger.error(e.message));
    logger.error(e.message);
    throw new InternalError(messages.cards.internalError.en);
  }
};

export const ownCreditTransfer = async (
  body: {
    CountryCode: string;
    ContractId: string;
    DebitAcc: string;
    Cif: string;
    Currency: string;
    Amount: number;
    TransCode?: string;
  },
  authorization: string,
  user: string,
  platform: string,
  tracingHeaders?: any
) => {
  try {
    if (platform === 'web') {
      body.TransCode = transactionCodes.cardPayment.web;
    }
    const data = {
      route: `txn/pay/creditcard`,
      method: 'post',
      authorization,
      body
    };
    const producerData = await produce(data, tracingHeaders);
    const kafkaData = await prepareKafkaResponse(producerData);
    updateStatistics(statistics.successCreditPayment, {
      cif: body.Cif,
      user,
      type: 'transfer',
      subType: 'creditTransfer',
      date: new Date(),
      microservice: 'creditCard',
      source: 'card',
      platform,
      debit: body.DebitAcc,
      beneficiary: body.ContractId,
      transactionId: kafkaData.TransactionId,
      amount: `EGP ${body.Amount}`,
      status: status.success
    }).catch((e: any) => logger.error(e.message));
    return {
      transactionId: kafkaData.TransactionId
    };
  } catch (e) {
    logger.error(e.message);
    if (e.message === messages.cards.timeout.en) {
      logger.debug(staticError.timeoutCreditPayment.replace('{user}', user));
      updateStatistics(statistics.timeoutCreditPayment, {
        cif: body.Cif,
        user,
        type: 'transfer',
        subType: 'creditTransfer',
        date: new Date(),
        microservice: 'creditCard',
        source: 'card',
        debit: body.DebitAcc,
        beneficiary: body.ContractId,
        amount: `EGP ${body.Amount}`,
        status: e.message === messages.cards.timeout.en ? status.timeout : status.fail
      }).catch((e: any) => logger.error(e.message));
    } else {
      logger.debug(staticError.failedCreditPayment.replace('{user}', user));
      updateStatistics(statistics.failedCreditPayment, {
        cif: body.Cif,
        user,
        type: 'transfer',
        subType: 'creditTransfer',
        date: new Date(),
        microservice: 'creditCard',
        source: 'card',
        debit: body.DebitAcc,
        beneficiary: body.ContractId,
        amount: `EGP ${body.Amount}`,
        status: e.message === messages.cards.timeout.en ? status.timeout : status.fail
      }).catch((e: any) => logger.error(e.message));
    }
    throw new InternalError(messages.cards.internalError.en);
  }
};
export const transferToSavedCreditCardById = async (
  id: string,
  cif: string,
  user: string,
  body: any,
  authorization: string,
  platform: string,
  tracingHeaders?: any
) => {
  try {
    const savedCreditCard: any = await SavedCreditCardModel.findOne({ cif, _id: id });
    if (!savedCreditCard) throw new NotFoundError();

    const { ContractId } = await getCreditCardDetails(
      savedCreditCard.cardNumber,
      authorization,
      tracingHeaders
    );
    const data = {
      route: `txn/pay/creditcard`,
      method: 'post',
      authorization,
      body: {
        Cif: cif,
        CountryCode: transOptions.countryCode,
        DebitAcc: body.debitAcc,
        ContractId: ContractId,
        Currency: body.currency || transOptions.currency,
        Amount: body.amount,
        DebitDesc: body.description,
        CreditDesc: body.description,
        DebitReference: transOptions.reference,
        CreditReference: transOptions.reference
      }
    };

    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    updateStatistics(statistics.successCreditPayment, {
      cif,
      user,
      type: 'transfer',
      subType: 'savedCreditCardTransfer',
      date: new Date(),
      microservice: 'creditCard',
      source: 'card',
      platform,
      debit: body.debitAcc,
      creditCardName: savedCreditCard.name,
      creditCardNumber: maskString(savedCreditCard.cardNumber),
      transactionId: result.TransactionId,
      amount: `EGP ${body.amount}`,
      status: status.success
    }).catch((e: any) => logger.error(e.message));
    return {
      transactionId: result.TransactionId
    };
  } catch (e) {
    logger.error(e.message);
    if (e.message === messages.cards.timeout.en) {
      logger.debug(staticError.timeoutCreditPayment.replace('{user}', user));
      updateStatistics(statistics.timeoutCreditPayment, {
        cif,
        user,
        type: 'transfer',
        subType: 'savedCreditCardTransfer',
        date: new Date(),
        microservice: 'creditCard',
        platform: platform,
        debit: body.debitAcc,
        source: 'card',
        amount: `EGP ${body.amount}`,
        status: e.message === messages.cards.timeout.en ? status.timeout : status.fail
      }).catch((e: any) => logger.error(e.message));
    } else {
      logger.debug(staticError.failedCreditPayment.replace('{user}', user));
      updateStatistics(statistics.failedCreditPayment, {
        cif,
        user,
        type: 'transfer',
        subType: 'savedCreditCardTransfer',
        date: new Date(),
        microservice: 'creditCard',
        platform: platform,
        debit: body.debitAcc,
        source: 'card',
        amount: `EGP ${body.amount}`,
        status: e.message === messages.cards.timeout.en ? status.timeout : status.fail
      }).catch((e: any) => logger.error(e.message));
    }
    throw new InternalError(messages.cards.internalError.en);
  }
};
export const transferToCreditCard = async (
  cif: string,
  user: string,
  body: any,
  authorization: string,
  platform: string,
  tracingHeaders?: any
) => {
  try {
    const { ContractId } = await getCreditCardDetails(
      body.cardNumber,
      authorization,
      tracingHeaders
    );
    const data = {
      route: `txn/pay/creditcard`,
      method: 'post',
      authorization,
      body: {
        Cif: cif,
        CountryCode: transOptions.countryCode,
        DebitAcc: body.debitAcc,
        ContractId: ContractId,
        Currency: body.currency || transOptions.currency,
        Amount: body.amount,
        DebitDesc: body.description,
        CreditDesc: body.description,
        DebitReference: transOptions.reference,
        CreditReference: transOptions.reference
      }
    };
    const producerData = await produce(data, tracingHeaders);
    const result = await prepareKafkaResponse(producerData);
    updateStatistics(statistics.successCreditPayment, {
      cif,
      user,
      type: 'transfer',
      subType: 'creditCardTransfer',
      date: new Date(),
      microservice: 'creditCard',
      source: 'card',
      platform,
      debit: body.debitAcc,
      creditCardName: body.name,
      creditCardNumber: maskString(body.cardNumber),
      transactionId: result.TransactionId,
      amount: `EGP ${body.amount}`,
      status: status.success
    }).catch((e: any) => logger.error(e.message));
    return {
      transactionId: result.TransactionId
    };
  } catch (e) {
    logger.error(e.message);
    if (e.message === messages.cards.timeout.en) {
      logger.debug(staticError.timeoutCreditPayment.replace('{user}', user));
      updateStatistics(statistics.timeoutCreditPayment, {
        cif,
        user,
        type: 'transfer',
        subType: 'creditCardTransfer',
        date: new Date(),
        microservice: 'creditCard',
        platform: platform,
        debit: body.debitAcc,
        source: 'card',
        amount: `EGP ${body.amount}`,
        status: e.message === messages.cards.timeout.en ? status.timeout : status.fail
      }).catch((e: any) => logger.error(e.message));
    } else {
      logger.debug(staticError.failedCreditPayment.replace('{user}', user));
      updateStatistics(statistics.failedCreditPayment, {
        cif,
        user,
        type: 'transfer',
        subType: 'creditCardTransfer',
        date: new Date(),
        microservice: 'creditCard',
        platform: platform,
        debit: body.debitAcc,
        source: 'card',
        amount: `EGP ${body.amount}`,
        status: e.message === messages.cards.timeout.en ? status.timeout : status.fail
      }).catch((e: any) => logger.error(e.message));
    }
    throw new InternalError(messages.cards.internalError.en);
  }
};

export const getCreditCardSettings = async (
  contractId: string,
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `creditcards/settings/${contractId}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const kafkaData = await prepareKafkaResponse(producerData);
    return kafkaData[0] || {};
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(messages.cards.internalError.en);
  }
};

export const updateCreditCardSettings = async (
  body: {
    ContractId: string;
    Setting: string;
    Cif: string;
    Action: string;
  },
  user: string,
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `creditcards/setsetting`,
      method: 'post',
      authorization,
      body
    };
    const producerData = await produce(data, tracingHeaders);
    await prepareKafkaResponse(producerData);
    let cardNumber = await getCardByContractIdFullDetails(
      body.ContractId,
      authorization,
      tracingHeaders
    );
    cardNumber = cardNumber?.[0] ? cardNumber[0].CardNumber : '';
    updateStatistics('', {
      cif: body.Cif,
      user,
      type: 'creditCardActions',
      subType: 'updateCreditCardSettings',
      setting: body.Setting,
      requestType: body.Action,
      date: new Date(),
      microservice: 'creditCard',
      source: 'card',
      contractId: body.ContractId,
      cardNumber,
      status: status.success
    }).catch((e: any) => logger.error(e.message));
    return { message: 'Success' };
  } catch (e) {
    let cardNumber = await getCardByContractIdFullDetails(
      body.ContractId,
      authorization,
      tracingHeaders
    );
    cardNumber = cardNumber?.[0] ? cardNumber[0].CardNumber : '';
    updateStatistics('', {
      cif: body.Cif,
      user,
      type: 'creditCardActions',
      subType: 'updateCreditCardSettings',
      setting: body.Setting,
      requestType: body.Action,
      date: new Date(),
      microservice: 'creditCard',
      source: 'card',
      contractId: body.ContractId,
      cardNumber,
      status: status.fail
    }).catch((e: any) => logger.error(e.message));
    logger.error(e.message);
    throw new InternalError(messages.cards.internalError.en);
  }
};

export const enableTravelWindow = async (
  body: {
    ContractId: string;
    Cif: string;
    CountriesList: [string];
    StartDate: string;
    EndDate: string;
  },
  user: string,
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `creditcards/travelwindow/enablespecific`,
      method: 'post',
      authorization,
      body
    };
    const producerData = await produce(data, tracingHeaders);
    await prepareKafkaResponse(producerData);
    let cardNumber = await getCardByContractIdFullDetails(
      body.ContractId,
      authorization,
      tracingHeaders
    );
    cardNumber = cardNumber?.[0] ? cardNumber[0].CardNumber : '';
    updateStatistics('', {
      cif: body.Cif,
      user,
      type: 'creditCardActions',
      subType: 'enableTravelWindow',
      startDate: body.StartDate,
      endDate: body.EndDate,
      countries: body.CountriesList,
      date: new Date(),
      microservice: 'creditCard',
      source: 'card',
      contractId: body.ContractId,
      cardNumber,
      status: status.success
    }).catch((e: any) => logger.error(e.message));
    return { message: 'Success' };
  } catch (e) {
    let cardNumber = await getCardByContractIdFullDetails(
      body.ContractId,
      authorization,
      tracingHeaders
    );
    cardNumber = cardNumber?.[0] ? cardNumber[0].CardNumber : '';
    updateStatistics('', {
      cif: body.Cif,
      user,
      type: 'creditCardActions',
      subType: 'enableTravelWindow',
      startDate: body.StartDate,
      endDate: body.EndDate,
      countries: body.CountriesList,
      date: new Date(),
      microservice: 'creditCard',
      source: 'card',
      contractId: body.ContractId,
      cardNumber,
      status: status.fail
    }).catch((e: any) => logger.error(e.message));
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const disableTravelWindow = async (
  ContractId: string,
  Cif: string,
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      route: `creditcards/travelwindow/disable`,
      method: 'post',
      authorization,
      body: {
        ContractId,
        Cif
      }
    };
    const producerData = await produce(data, tracingHeaders);
    await prepareKafkaResponse(producerData);
    return { message: 'Success' };
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const getCardByContractIdFullDetails = async (
  contractId: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const data = {
      route: `creditcards/contract/${contractId}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const card = await prepareKafkaResponse(producerData);
    return card;
  } catch (e) {
    logger.debug(e.message);
    throw new InternalError();
  }
};

export const getCreditCardDetails = async (
  CardNumber: string,
  authorization: string,
  tracingHeaders?: any
) => {
  try {
    const data = {
      topic: 't24Request',
      route: `creditcards/card`,
      method: 'post',
      authorization,
      body: {
        CardNumber
      }
    };
    const producerData = await produce(data, tracingHeaders);
    const cardDetails = await prepareKafkaResponse(producerData);
    if (cardDetails && cardDetails.length > 0) {
      return cardDetails[0];
    } else {
      throw new NotFoundError();
    }
  } catch (e) {
    logger.debug(e.message);
    throw new NotFoundError();
  }
};

export const requestCreditCard = async (
  body: any,
  cif: string,
  user: string,
  phoneNumber: string,
  tracingHeaders: any
) => {
  try {
    const data = {
      topic: kafkaTopics.smtp,
      function: 'sendEmailService',
      body,
      cif,
      template: 'creditCard'
    };
    await produce(data, tracingHeaders, false);
    updateStatistics('', {
      cif,
      user,
      type: 'request',
      subType: 'requestCreditCard',
      date: new Date(),
      microservice: 'creditCard',
      mobile: phoneNumber,
      status: status.success
    }).catch((e) => e.message);
    return { message: 'Success' };
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(e.message);
  }
};

export const replaceCreditCard = async (
  body: any,
  cif: string,
  user: string,
  phoneNumber: string,
  platform: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const address = await getUserAddress(cif, authorization, tracingHeaders);
    body.cardExchangeLocation = address;
    body.deliveryMethod = deliveryMethod.delivery;
    const data = {
      topic: kafkaTopics.smtp,
      function: 'sendEmailService',
      body,
      cif,
      template: 'replaceCreditCard'
    };
    await produce(data, tracingHeaders, false);
    const cardNumber = body.cardNumber.slice(-4);
    const exists: any = await CardReplacement.findOne({ cif, cardNumber, isCreditCard: true });
    if (exists) {
      throw new CardReplacementExistsError(messages.cards.CardReplacementExists.en, {
        date: moment(exists.createdAt).format('l')
      });
    }
    const cardReplacement = new CardReplacement({
      cif,
      cardNumber,
      isCreditCard: true
    });

    await cardReplacement.save();

    updateStatistics('', {
      cif,
      user,
      type: 'replace',
      subType: 'replaceCreditCard',
      date: new Date(),
      microservice: 'creditCard',
      mobile: phoneNumber,
      platform,
      status: status.success
    }).catch((e) => e.message);
    return { message: 'Success' };
  } catch (e) {
    logger.error(e.message);
    if (e.type === customErrorTypes.CardReplacementExistsError) {
      throw new CardReplacementExistsError(e.message, e.data);
    } else {
      throw new InternalError(e.message);
    }
  }
};

export const canReplaceCard = async (creditCardNumber: string, cif: string) => {
  const cardNumber = creditCardNumber.slice(-4);
  const exists: any = await CardReplacement.findOne({ cif, cardNumber, isCreditCard: true });
  if (exists) {
    throw new CardReplacementExistsError(messages.cards.CardReplacementExists.en, {
      date: moment(exists.createdAt).format('l')
    });
  } else {
    return { message: 'Success' };
  }
};

const getUserAddress = async (cif: string, authorization: string, tracingHeaders: any) => {
  try {
    const data = {
      topic: kafkaTopics.t24Request,
      route: `customer/${cif}`,
      authorization
    };
    const producerData = await produce(data, tracingHeaders);
    const kafkaData = await prepareKafkaResponse(producerData);
    if (kafkaData) {
      return kafkaData[0].Language.toString() === languages.english
        ? kafkaData[0].Address[0]
        : kafkaData[0].Address[1];
    } else return {};
  } catch (error) {
    logger.error(error.message);
    throw new InternalError();
  }
};

export const getTermsAndConditions = async (language = 'en') => {
  const data = termsAndConditions[language] || termsAndConditions[defaultLanguage];
  return { result: data.replace(/[\n]/gm, '') };
};

export const getSavedCreditCards = async (cif: string) => {
  return await SavedCreditCardModel.find({ cif });
};

export const saveCreditCard = async (body: any, cif: string, user: string, platform: string) => {
  try {
    body.cif = cif;
    const existCC = await SavedCreditCardModel.findOne({ cif: cif, cardNumber: body.cardNumber });
    if (existCC) {
      throw new InternalError(messages.cards.CardAlreadyExists.en);
    } else {
      const creditCard = new SavedCreditCardModel(body);
      await creditCard.save();
      updateStatistics('', {
        cif: cif,
        user: user,
        type: 'savedCreditCard',
        subType: 'addSavedCreditCard',
        status: status.success,
        date: new Date(),
        microservice: 'creditcard',
        action: 'add',
        creditCardName: body.name,
        cardNumber: maskString(body.cardNumber),
        platform: platform
      });
      return { message: 'Success', id: creditCard._id };
    }
  } catch (e) {
    logger.error(e.message);
    throw new InternalError(messages.cards.internalError.en);
  }
};
export const deleteCreditCard = async (cif: string, user: string, id: string, platform: string) => {
  const creditCard: any = await SavedCreditCardModel.findOneAndDelete({ cif, _id: id });
  updateStatistics('', {
    cif: cif,
    user: user,
    name: creditCard.name,
    cardNumber: creditCard.cardNumber,
    type: 'savedCreditCard',
    subType: 'deleteSavedCreditCard',
    status: status.success,
    date: new Date(),
    microservice: 'creditcard',
    action: 'delete',
    platform: platform
  }).catch((e: any) => logger.error(e.message));
  return { message: 'Success' };
};

export const getCardByBenficiaryId = async (
  cif: string,
  beneficiaryId: string,
  authorization: string,
  tracingHeaders: any
) => {
  try {
    const params = JSON.stringify({
      id: beneficiaryId,
      cif
    });
    const data = {
      authorization,
      topic: kafkaTopics.beneficiaryRequest,
      function: 'getUserBeneficiariesById',
      params
    };
    const producerData = await produce(data, tracingHeaders);
    const beneficiary = await prepareKafkaResponse(producerData);
    if (!beneficiary._id) {
      throw new NotFoundError();
    }
    if (beneficiary.isCreditCardNumber == false) {
      throw new ForbiddenError();
    }

    const card = await getCardByContractIdFullDetails(
      beneficiary.accountNumber,
      authorization,
      tracingHeaders
    );
    return card[0].OnlineData ? card[0].OnlineData.LedgerBalance : null;
  } catch (e) {
    logger.debug(e.message);
    if (e.type === ErrorType.NOT_FOUND) {
      throw new NotFoundError('beneficiary not found');
    }
    if (e.type === ErrorType.FORBIDDEN) {
      throw new ForbiddenError();
    }
    if (e.type === ErrorType.NOT_FOUND) throw new InternalError();
  }
};

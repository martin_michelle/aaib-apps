import { Response, NextFunction } from 'express';
import { ProtectedRequest } from 'app-request';
import { kafkaTopics } from '../config';
import { AuthFailureError } from '../core/ApiError';
import messages from '../messages';
import { produce } from '../kafka';
import { prepareKafkaResponse } from '../services/cards';
import tracingHeaders from './tracingHeaders';

export default () => async (req: ProtectedRequest, res: Response, next: NextFunction) => {
  try {
    const data = {
      topic: kafkaTopics.validateTopic,
      authorization: req.headers.authorization,
      apiRoute: req.originalUrl,
      apiMethod: req.method,
      token: req.body.smsToken || ''
    };
    const producerData = await produce(data, tracingHeaders(req));
    const result = await prepareKafkaResponse(producerData);
    if (result && result.user) {
      req.user = result.user;
      req.accessToken = result.token;
      next();
    } else next(new AuthFailureError(messages.authorization.invalid.en));
  } catch (error) {
    if (error.message === 'otpBlocked') {
      next(new AuthFailureError(messages.authorization.otpBlocked.en));
    } else next(new AuthFailureError(messages.authorization.invalid.en));
  }
};

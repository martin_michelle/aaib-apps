export const termsAndConditions: any = {
  en: `<p>I will bear all the due expenses and commissions, and I confirm to pay all commissions, interests and any amounts due on credit card account mentioned above within a month of the credit Card closure on the bank’s system. I understand that the bank will convert the outstanding balance on the damaged/lost card to the new card account when issued. I acknowledge that I had the card stopped (In case of lost cards only).</p>

  <p>In case of damaged cards, a new credit card will be issued with the same number of the damaged one.</p>`,
  ar: `<p>وذلك مع تحملي كافة المصاريف والعمولات الواجبة. وأتعهد بسداد كافة المبالغ والعمولات والفوائد وأي مديونية قد تستحق على حساب بطاقة الائتمان المذكورة أعلاه ترد لكم خلال شهر من غلق البطاقة على نظام البنك علماً بأن البنك سوف يحول الرصيد القائم على البطاقة المفقودة /التالفة إلى حساب البطاقة الجديدة حال صدورها. وأقر بأنني سبق وأوقفت البطاقة (في حالة البطاقات المفقودة فقط).</p>  <p>في حالة البدل التالف يتم إصدار بطاقة ائتمان جديدة بنفس رقم البطاقة التالفة.</p>`
};
